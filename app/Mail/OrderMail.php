<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;
    public $order;
    // public $metode_bayar;
    // public $invoice;
    // public $metode_kirim;
    // public $customer;
    // public $date;
    // public $harga;
    // public $deadline;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
        // $this->metode_bayar = $metode_bayar;
        // $this->invoice = $invoice;
        // $this->metode_kirim = $metode_kirim;
        // $this->customer = $customer;
        // $this->date = $date;
        // $this->harga = $harga;
        // $this->deadline = $deadline;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('admin@biztekweb.com', "Admin Biztekweb")->view('emails.order-success');
    }
}
