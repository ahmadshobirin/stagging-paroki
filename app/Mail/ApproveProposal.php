<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveProposal extends Mailable
{
    use Queueable, SerializesModels;


    protected $text;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $text, string $subject = "Approve Proposal")
    {
        $this->text = $text;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('warta.erka@gmail.com', "Paroki")
            ->subject($this->subject)
            ->view('emails/reminder', [
                'text' => $this->text
            ]);
    }
}
