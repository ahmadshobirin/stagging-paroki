<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\MUserModel;
use App\Models\MRoleModel;
use Auth;

class KontributorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $roleCustomer = MRoleModel::where('id',Auth::user()->role )->first();
        return $next($request);

        //dd($roleCustomer);

        // if($roleCustomer->name == "Kontributor"){
        //     return $next($request);
        // }else {
        //     return redirect('/admin');
        // }
    }
}
