<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\MUserModel;
use App\Models\MRoleModel;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$roleCustomer = MRoleModel::where('name', 'Customer')->first();

        // if( Auth::user()->role == $roleCustomer->id){
        //     return redirect('/');
        // }else {
        //     return $next($request);
        // }
        $roleCustomer = MRoleModel::where('id',Auth::user()->role)->first();

        //dd($roleCustomer);

        if($roleCustomer){
            return $next($request);
        }else {
            return redirect('/admin');
        }
        //return $next($request);
    }
}
