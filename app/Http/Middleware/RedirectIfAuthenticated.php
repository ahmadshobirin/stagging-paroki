<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\MRoleModel;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if (Auth::guard($guard)->check()) {
        //     $roleCust = MRoleModel::where('name', 'Admin')->orWhere('name',"Super Admin")->first();
            
        //     if( Auth::guard($guard)->user()->role == $roleCust->id ){
        //         // session(['link' => url()->previous()]);
        //         // return redirect(session('link'))->with('success_message','Login Berhasil');
        //         return redirect()->intended()->with('success_message','Login Berhasil');
        //     }else{
        //         return redirect('/admin');
        //     }
        // }

        if (Auth::guard($guard)->check()) {
            return redirect('/admin');
        }

        return $next($request);
    }
}
