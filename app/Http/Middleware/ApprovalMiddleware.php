<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\MUserModel;
use App\Models\MRoleModel;
use Auth;

class ApprovalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //return $next($request);

        $roleCustomer = MRoleModel::where('id',Auth::user()->role )->first();

        if($roleCustomer->name == "Admin" || $roleCustomer->name == "Super Admin"){
            return $next($request);
        }else {
            return redirect('/admin');
        }
    }
}
