<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class BidangCtrl extends Controller
{
	public function index($id)
	{
        $bidang = DB::table('m_bidang')->where('id_bidang',$id)->first();
        $artikel = DB::table('m_posting')
                        ->select('m_posting.*','m_bidang.nama_bidang')
                        ->join('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
                        ->where('type_post','artikel')
                        ->where('m_posting.id_bidang', $id)
                        ->where('m_posting.status_post', 'approved')
                        ->orderBy('created_at','desc')
                        ->paginate(10);
        
        $slider = DB::table('m_banner')->where('page_banner','bidang')->first();

       
        return view('frontend.bidang',compact('artikel','slider','bidang'));
	}

       
}
?>