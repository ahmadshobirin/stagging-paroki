<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class SejarahCtrl extends Controller
{
	public function index($id)
	{
        
        $slider = DB::table('m_banner')->where('page_banner','sejarah')->first();
        
        $sejarah = DB::table('m_sejarah')->where('id_sejarah',$id)->first();

        //dd($agenda);


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('frontend.sejarah',compact('sejarah','slider'));
	}

       
}
?>