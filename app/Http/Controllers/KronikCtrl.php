<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;
use Share;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class KronikCtrl extends Controller
{
	public function index()
	{
        $kronik = DB::table('m_posting')
                        ->select('m_posting.*','m_bidang.nama_bidang')
                        ->leftjoin('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
                        ->where('type_post','kronik')
                        ->where('status_post','approved')
                        ->orderBy('created_at','desc')
                        ->paginate(10);
        $slider = DB::table('m_banner')->where('page_banner','agenda')->first();
        $bidang = DB::table('m_bidang')->get();


        return view('frontend.kronik',compact('kronik','slider','bidang'));
	}


    public function indexKategori($id)
    {
        $kronik = DB::table('m_posting')
                        ->select('m_posting.*','m_bidang.nama_bidang')
                        ->leftjoin('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
                        ->where('type_post','kronik')
                        ->where('status_post','approved')
                        ->where('m_posting.id_bidang', $id)
                        ->orderBy('created_at','desc')
                        ->paginate(10);
        $slider = DB::table('m_banner')->where('page_banner','agenda')->first();
        $bidang = DB::table('m_bidang')->get();


        return view('frontend.kronik',compact('kronik','slider','bidang'));
    }

    public function detailKronik($id)
    {
        $kronik = DB::table('m_posting')
                        ->select('m_posting.*','m_bidang.nama_bidang')
                        ->leftjoin('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
                        ->where('id_posting',$id)
                        ->first();

        $kronik_all = DB::table('m_posting')
                        ->select('m_posting.*','m_bidang.nama_bidang')
                        ->leftjoin('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
                        ->where('type_post','kronik')
                        ->where('m_posting.status_post', 'approved')
                        ->where('m_posting.id_posting', '!=' ,$kronik->id_posting)
                        ->orderBy('created_at','desc')
                        ->limit(5)
                        ->get();

        $slider = DB::table('m_banner')->where('page_banner','agenda')->first();

        $date_now =  date('Y-m-d 00:00:00');
        $date_end = date('Y-m-d 23:59:59');

        $agenda = DB::table('m_agenda')
                    ->select('m_agenda.*','m_tempat.nama_tempat')
                    ->join('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                    ->where('date_agenda','>=', $date_now)
                    ->where('date_agenda','<=', $date_end)
                    ->where('type_agenda','umum')
                    ->limit(5)
                    ->orderBy('id_agenda','desc')
                    ->get();
        $agenda_lain = [];
        if(count($agenda) == 0){
            $agenda_lain = DB::table('m_agenda')
                    ->select('date_agenda')
                    ->where('date_agenda','>=', $date_end)
                    ->orderBy('date_agenda','asc')
                    ->groupBy('date_agenda')
                    ->get();
            $cek_event = 0;
            foreach ($agenda_lain as $key_agenda => $value_agenda) {

                if($cek_event < 6){
                    $agenda_det = DB::table('m_agenda')
                            ->select('m_agenda.*','m_tempat.nama_tempat')
                            ->join('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                            ->where('date_agenda', $value_agenda->date_agenda)
                            ->where('type_agenda', "umum")
                            ->orderBy('waktu_agenda','asc')
                            ->get();

                    $value_agenda->detail_agenda = $agenda_det;
                    $cek_event = $cek_event + count($agenda_det);
                }else{
                    $value_agenda->detail_agenda = "";
                }

                if($value_agenda->detail_agenda == ""){
                    unset($agenda_lain[$key_agenda]);
                }
            }
        }

        $web = 'http://google.com';
        $fb = Share::page($web, 'Renungan Gereja Paroki Roh Kudus', ['class' => 'pr-page-detail__content-share-link'],'<ul>','</ul>')
                ->facebook()
                ->twitter()
                ->googlePlus();

        return view('frontend.kronik_detail',compact('kronik','slider','kronik_all','agenda','agenda_lain','fb'));
    }


}
?>