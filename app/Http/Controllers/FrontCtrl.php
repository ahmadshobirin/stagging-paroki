<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\MUserModel;
use App\Models\MRoleModel;
use App\Mail\RegisterMail;
use App\Mail\OrderMail;
use App\Mail\ContactMail;
use App\Mail\PaymentConfirmMail;
use Illuminate\Support\Facades\Crypt;
use Gloudemans\Shoppingcart\Facades\Cart;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class FrontCtrl extends Controller
{
	public function index()
	{
        $date_now =  date('Y-m-d 00:00:00');
        $date_end = date('Y-m-d 23:59:59');
        $slider = DB::table('m_slider')->where('category_slider','home')->get();
        
        $header_misa = DB::table('h_jadwal_misa')->get();
        foreach ($header_misa as $index_misa => $value_misa) {
            $detail_misa = DB::table('d_jadwal_misa')->where('id_jadwal_misa', $value_misa->id_jadwal_misa)->get();
            if(count($detail_misa) == 0 ){
                unset($header_misa[$index_misa]);
            }
        }

        $header_misa = array_values($header_misa->toArray());

        foreach ($header_misa as $key => $value) {
            $hari_misa = DB::table('d_jadwal_misa')->select('hari')->where('id_jadwal_misa', $value->id_jadwal_misa)->groupBy('hari')->orderBy('hari',"desc")->get();
            $value->hari_misa = $hari_misa;
        }

        foreach ($header_misa as $key1 => $value1) {
            foreach ($value1->hari_misa as $key2 => $value2) {
                $waktu_misa = DB::table('d_jadwal_misa')->select('waktu')->where('id_jadwal_misa', $value1->id_jadwal_misa)->where('hari', $value2->hari)->get();
                    $value2->waktu_misa = $waktu_misa;       
            }
        }

        //dd($header_misa);

        // $agenda = DB::table('m_agenda')
        //             ->where('start_agenda','<=',$date_now)
        //             ->where('end_agenda','>=', $date_end)
        //             ->limit(3)
        //             ->orderBy('id_agenda','desc')
        //             ->get();
        $agenda = DB::table('m_agenda')
                    ->select('m_agenda.*','m_tempat.nama_tempat')
                    ->join('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                    ->where('date_agenda','>=', $date_now)
                    ->where('date_agenda','<=', $date_end)
                    ->where('type_agenda','umum')
                    ->limit(6)
                    ->orderBy('id_agenda','desc')
                    ->get();

        $posting = DB::table('m_posting')
                    ->select('m_posting.*','m_kategori_user.nama_kategori_user')
                    ->join('m_user','m_user.id','m_posting.id_user')
                    ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_user.id_header_user')
                    ->where('status_post', 'approved')
                    ->where('published', 1)
                    ->whereIn('type_post', ['info','artikel'])
                    ->limit(3)
                    ->orderBy('id_posting','desc')
                    ->get();
                    
        $artikel = DB::table('m_posting')
                    ->select('m_posting.*','m_kategori_user.nama_kategori_user')
                    ->join('m_user','m_user.id','m_posting.id_user')
                    ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_user.id_header_user')
                    ->where('status_post', 'approved')
                    ->where('type_post', 'artikel')
                    ->limit(3)
                    ->orderBy('id_posting','desc')
                    ->get();

        $renungan = DB::table('m_renungan')
                    ->select('m_renungan.*','m_romo.nama_romo')
                    ->leftjoin('m_romo','m_romo.id_romo','m_renungan.author_renungan')
                    ->limit(6)
                    ->orderBy('id_renungan','desc')
                    ->get();
        $agenda_lain = [];
        if(count($agenda) == 0){
            $agenda_lain = DB::table('m_agenda')
                    ->select('date_agenda')
                    ->where('date_agenda','>=', $date_end)
                    ->orderBy('date_agenda','asc')
                    ->groupBy('date_agenda')
                    ->get();
            $cek_event = 0;
            foreach ($agenda_lain as $key_agenda => $value_agenda) {
                
                if($cek_event < 6){
                    $agenda_det = DB::table('m_agenda')
                            ->select('m_agenda.*','m_tempat.nama_tempat')
                            ->join('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                            ->where('date_agenda', $value_agenda->date_agenda)
                            ->where('type_agenda', "umum")
                            ->orderBy('waktu_agenda','asc')
                            ->get();

                    $value_agenda->detail_agenda = $agenda_det;
                    $cek_event = $cek_event + count($agenda_det);
                }else{
                    $value_agenda->detail_agenda = "";                    
                }
                
                if($value_agenda->detail_agenda == ""){
                    unset($agenda_lain[$key_agenda]);
                }
            }
        }

       
        return view('frontend.home',compact('slider','header_misa','agenda','posting','renungan','agenda_lain','artikel'));
	}

        

        public function contact()
        {
                $slider = DB::table('m_banner')->where('page_banner','kontak')->first();
                
                $office = DB::table('m_office')
                            ->select('m_office.*','m_kelurahan_desa.name as nama_kel','m_kelurahan_desa.zipcode','m_kecamatan.name as nama_kec','m_kota_kab.name as nama_kota','m_kota_kab.type as type_kota','m_provinsi.name as nama_prov')
                            ->join('m_kelurahan_desa','m_kelurahan_desa.id', 'm_office.id_kelurahan')
                            ->join('m_kecamatan','m_kecamatan.id', 'm_kelurahan_desa.kecamatan')
                            ->join('m_kota_kab','m_kota_kab.id', 'm_kecamatan.kota_kab')
                            ->join('m_provinsi','m_provinsi.id', 'm_kota_kab.provinsi')
                            ->first();
                return view('frontend.contact', compact('office','slider'));
        }

        public function sendContact(Request $request)
        {
                $contact = [];
                $contact['nama'] = $request->nama;
                $contact['email'] = $request->email;
                $contact['telp'] = $request->telp;
                $contact['alamat'] = $request->alamat;
                $contact['pesan'] = $request->pesan;

                $emailto = "warta_erka@yahoo.ie";
                
                Mail::to($emailto)->send(new ContactMail($contact));
                return redirect()->back()->with('success_message', 'Berhasil Mengirim Pesan');
        }

        public function about(){
            $data = DB::table('m_office')->select('visi','misi')->first();

            $slider = DB::table('m_banner')->where('page_banner','sejarah')->first();

            return view('frontend.about', compact('data','slider'));
        }

         public function getForm($name){
            
            $data = DB::table('m_form')->where('tipe_form', $name)->orderBy('id_form','desc')->paginate(10);

            $slider = DB::table('m_banner')->where('page_banner','form')->first();


            return view('frontend.form', compact('data','slider','name'));
        }

        public function download($id){
            $data = DB::table('m_form')->where('id_form', $id)->first();

            $file= public_path()."/upload/form/".$data->file_form;

            return response()->download($file);
        }

        
}
?>