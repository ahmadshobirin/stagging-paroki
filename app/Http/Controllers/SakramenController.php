<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Response;
use Carbon\Carbon;
use Image;
use Mail;
use PDF;
use Session;
use App\Models\AgendaModel;
use App\Models\BaptisModel;
use App\Models\KomuniModel;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Exports\TransactionReport;
use App\Models\SakramenTransaction;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\ApproveProposal;
use App\Models\MPeriode;

class SakramenController extends Controller
{
    protected $usersUpline;
    public function index($type, $typebaptis = null)
    {
        switch ($type) {
            case 'nikah':
                return $this->indexSakramenNikah();
                break;
            case 'baptis':
                return $this->indexSakramenBaptis($typebaptis);
                break;
            case 'komuni':
                return $this->indexSakramenKomuni();
                break;

            case 'krisma':
                return $this->indexSakramenKrisma();
                break;

            case 'transaction':
                return $this->traindex();
                break;

            default:
                # code...
                break;
        }
    }

    public function indexTtd($type, $typebaptis = null)
    {
        switch ($type) {
            case 'nikah':
                return $this->indexTTDSakramenNikah();
                break;
            case 'baptis':
                return $this->indexTTDSakramenBaptis($typebaptis);
                break;
            case 'komuni':
                return $this->indexTTDSakramenKomuni();
                break;

            case 'krisma':
                return $this->indexTTDSakramenKrisma();
                break;

            default:
                # code...
                break;
        }
    }

    public function indexSakramenNikah()
    {
        return view('admin.sakramen.nikah.index');
    }

    public function indexTTDSakramenNikah()
    {
        return view('admin.sakramen.nikah.index_ttd');
    }

    public function indexSakramenBaptis($nama)
    {
        return view('admin.sakramen.baptis.index_' . $nama);
    }

    public function indexTTDSakramenBaptis($nama)
    {
        return view('admin.sakramen.baptis.index_ttd_' . $nama);
    }

    public function traindex()
    {
        return view('admin.sakramen.transaction.index');
    }

    public function indexSakramenKomuni()
    {
        return view('admin.sakramen.komuni.index');
    }

    public function indexTTDSakramenKomuni()
    {
        return view('admin.sakramen.komuni.index_ttd');
    }

    public function indexSakramenKrisma()
    {
        return view('admin.sakramen.krisma.index');
    }

    public function indexTTDSakramenKrisma()
    {
        return view('admin.sakramen.krisma.index_ttd');
    }


    public function create($type, $typebaptis = null)
    {
        switch ($type) {
            case 'nikah':
                return $this->createSakramenNikah();
                break;

            case 'baptis':
                return $this->createSakramenBaptis($typebaptis);
                break;

            case 'komuni':
                return $this->createSakramenKomuni();
                break;

            case 'krisma':
                return $this->createSakramenKrisma();
                break;
            case 'transaction':
                return $this->reportTransaction();
                break;
            default:
                abort(404);
                break;
        }
    }

    public function createSakramenNikah()
    {
        $cek = DB::table('m_agenda')->where('id_user',Auth::user()->id)->where("type_agenda","pernikahan")->whereIn('status',['approved','in process','in approval'])->get();
        if(count($cek) == 0){
            $lingkungan = DB::table('m_kategori_user')->where('id_bidang', 10)->where('type', 'lingkungan')->where('status', 'active')->get();

            return view('admin.sakramen.nikah.create', [
                'url' => url('/admin/sakramen-store'),
                'agenda' => null,
                'lingkungan' => $lingkungan
            ]);
        }else{
            return redirect()->back()->with('message', 'Anda sudah membuat pendaftaran sakramen pernikahan');
        }
    }

    public function createSakramenBaptis($type)
    {
        $date = date("Y-m-d");
        $lingkungan = DB::table('m_kategori_user')->where('id_bidang', 10)->where('type', 'lingkungan')->where('status', 'active')->get();
        $periode = DB::table('m_periode')
            ->where('type_periode', "baptis_" . $type)
            ->where('start_date', '<=', $date)
            ->where('end_date', '>', $date)
            ->where('status_periode', 'active')
            ->get();

        return view('admin.sakramen.baptis.create', [
            'url' => url('/admin/sakramen-store'),
            'baptis' => null,
            'lingkungan' => $lingkungan,
            'periode' => $periode,
            'type' => $type,
        ]);
    }

    public function createSakramenKomuni()
    {
        $date = date("Y-m-d");
        $lingkungan = DB::table('m_kategori_user')->where('id_bidang', 10)->where('type', 'lingkungan')->where('status', 'active')->get();
        $periode = DB::table('m_periode')
            ->where('type_periode', "komuni")
            ->where('start_date', '<=', $date)
            ->where('end_date', '>', $date)
            ->where('status_periode', 'active')
            ->get();

        return view('admin.sakramen.komuni.create', [
            'url' => url('/admin/sakramen-store'),
            'komuni' => null,
            'lingkungan' => $lingkungan,
            'periode' => $periode,
            'type' => "komuni",
        ]);
    }

    public function createSakramenKrisma()
    {
        $date = date("Y-m-d");
        $lingkungan = DB::table('m_kategori_user')->where('id_bidang', 10)->where('type', 'lingkungan')->where('status', 'active')->get();
        $periode = DB::table('m_periode')
            ->where('type_periode', "krisma")
            ->where('start_date', '<=', $date)
            ->where('end_date', '>', $date)
            ->where('status_periode', 'active')
            ->get();

        return view('admin.sakramen.krisma.create', [
            'url' => url('/admin/sakramen-store'),
            'komuni' => null,
            'lingkungan' => $lingkungan,
            'periode' => $periode,
            'type' => "krisma",
        ]);
    }

    public function reportTransaction()
    {
        return view('admin/sakramen/transaction/form-report');
    }

    public function applyFilterReport(Request $req)
    {
        $start = substr($req->periode, 0, 10);
        $end = substr($req->periode, 13, 10);

        ($req->status == 'null') ? $status = ['debt','paid'] : $status = [$req->status];

        if( $req->type == "baptis" ){
            $q = SakramenTransaction::selectRaw('periode_id,date_implementation,sum(price) as allprice,sakramen_name,place')
                    ->where('date_implementation', '>=', date('Y-m-d', strtotime($start)))
                    ->where('date_implementation', '<', date('Y-m-d', strtotime($end . ' + 1 days')))
                    ->where('sakramen_type','!=','pernikahan')
                    ->whereIn('status',$status)
                    ->groupBy('periode_id','date_implementation','sakramen_type','sakramen_name','place')
                    ->get();
        }elseif($req->type == 'pernikahan'){
            $q = SakramenTransaction::selectRaw('periode_id,date_implementation,price as allprice,sakramen_name,place')
            ->where('date_implementation', '>=', date('Y-m-d', strtotime($start)))
            ->where('date_implementation', '<', date('Y-m-d', strtotime($end . ' + 1 days')))
            ->whereIn('status',$status)
            ->where('sakramen_type','=','pernikahan')
            ->get();
        }

        return Excel::download(new TransactionReport($q,[ 'start' => $start, 'end' => $end ]), 'transaction-' . date('dmy') . ".xlsx");
    }

    public function store(Request $request, $id = null)
    {
        // dd($request->type,$id);

        switch ($request->type) {
            case 'pernikahan':
                return $this->pernikahan($request, $id);
                break;

            case 'balita':
                return $this->sakramenBalita($request, $id);
                break;

            case 'anak':
                return $this->sakramenAnak($request, $id);
                break;

            case 'dewasa':
                return $this->sakramenDewasa($request, $id);
                break;

            case 'komuni':
                return $this->sakramenKomuni($request, $id);
                break;

            case 'krisma':
                return $this->sakramenKrisma($request, $id);
                break;

            default:
                # code...
                break;
        }
    }

    public function sakramenBalita($request, $id = null)
    {
        // dd($request->all());
        //validate

        if ($request->_method == 'PUT' && $id != null) //only -edit
            $sb = DB::table('m_sakramen_baptis')->where('id', $id)->first();

        DB::beginTransaction();
        try {

            $pathImage = null;
            $pathImageSuratBaptis = null;
            $pathImageSuratNikahOrtu = null;
            $namePath = null;
            $namePathSuratBaptis = null;
            $namePathSuratNikahOrtu = null;
            //editmode-delete
            if ($request->hasFile('image')) {
                $file   = $request->image;
                $pathImage = date("ymdHis") . $request->nama_diri . ".jpg";
                $destinationPath = 'upload/sakramen-baptis/balita';
                $file->move($destinationPath, $pathImage);
                $namePath = $destinationPath.'/'.$pathImage;

                if ($request->_method == 'PUT')
                    File::delete($sb->image_baptis); //delete old images
            }

            if ($request->hasFile('image1')) {
                $file1   = $request->image1;
                $pathImageSuratBaptis = date("ymdHis")."suratbaptiswali_". $request->nama_diri . ".jpg";
                $destinationPathSuratBaptis = 'upload/sakramen-baptis/balita';
                $file1->move($destinationPathSuratBaptis, $pathImageSuratBaptis);
                $namePathSuratBaptis = $destinationPathSuratBaptis.'/'.$pathImageSuratBaptis;

                if ($request->_method == 'PUT')
                    File::delete($sb->surat_baptis_wali); //delete old images
            }

            if ($request->hasFile('image2')) {
                $file2   = $request->image2;
                $pathImageSuratNikahOrtu = date("ymdHis")."suratnikahortu_". $request->nama_diri . ".jpg";
                $destinationPathSuratNikahOrtu = 'upload/sakramen-baptis/balita';
                $file2->move($destinationPathSuratNikahOrtu, $pathImageSuratNikahOrtu);
                $namePathSuratNikahOrtu = $destinationPathSuratNikahOrtu.'/'.$pathImageSuratNikahOrtu;

                if ($request->_method == 'PUT')
                    File::delete($sb->surat_nikah_ortu); //delete old images
            }


            $request->merge([
                'tanggal_lahir' => ($request->tanggal_lahir != null) ? date("Y-m-d", strtotime($request->tanggal_lahir)) : "",
                "user_id" => Auth::user()->id,
                "createuser" => Auth::user()->name,
                'status' => 'in process',
                'created_at' => date('Y-m-d H:i:s'),
                "image_baptis" => $namePath,
                "surat_baptis_wali" => $namePathSuratBaptis,
                "surat_nikah_ortu" => $namePathSuratNikahOrtu,
            ]);

            //dd($request->all());

            $req = $request->except(
                '_token',
                '_method',
                "image",
                "image1",
                "image2"
            );

            //dd($req);

            if ($request->_method == "PUT") {
                DB::table('m_sakramen_baptis')->where('id', $id)->update($req);
            } else {
                DB::table('m_sakramen_baptis')->insert($req);
            }


            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect('admin/sakramen/baptis/' . $request->type);
    }

    public function sakramenAnak($request, $id = null)
    {
        // dd($request->all());
        //validate

        if ($request->_method == 'PUT' && $id != null) //only -edit
            $sb = DB::table('m_sakramen_baptis')->where('id', $id)->first();

        DB::beginTransaction();
        try {

            $pathImage = null;
            $pathImageSuratNikahOrtu = null;
            $namePath = null;
            $namePathSuratNikahOrtu = null;

            //editmode-delete
            if ($request->hasFile('image')) {
                $pathImage = date("ymdHis") . $request->nama_diri . ".jpg";
                $destinationPath = 'upload/sakramen-baptis/anak';
                $request->image->move($destinationPath, $pathImage);
                $namePath = $destinationPath.'/'.$pathImage;

                if ($request->_method == 'PUT')
                    File::delete($sb->image_baptis); //delete old images
            }

            if ($request->hasFile('image2')) {
                $pathImageSuratNikahOrtu = date("ymdHis") ."suratnikahortu_". $request->nama_diri . ".jpg";
                $destinationPathSuratNikahOrtu = 'upload/sakramen-baptis/anak';
                $request->image2->move($destinationPathSuratNikahOrtu, $pathImageSuratNikahOrtu);
                $namePathSuratNikahOrtu = $destinationPathSuratNikahOrtu.'/'.$pathImageSuratNikahOrtu;

                if ($request->_method == 'PUT')
                    File::delete($sb->surat_nikah_ortu); //delete old images
            }

            $request->merge([
                'tanggal_lahir' => ($request->tanggal_lahir != null) ? date("Y-m-d", strtotime($request->tanggal_lahir)) : "",
                "image_baptis" => $namePath,
                "surat_nikah_ortu" => $namePathSuratNikahOrtu,
                "user_id" => Auth::user()->id,
                "createuser" => Auth::user()->name,
                'status' => 'in process',
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $req = $request->except(
                '_token',
                '_method',
                'image',
                'image2'
            );

            //dd($req);

            if ($request->_method == "PUT") {
                DB::table('m_sakramen_baptis')->where('id', $id)->update($req);
            } else {
                DB::table('m_sakramen_baptis')->insert($req);
            }


            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect('admin/sakramen/baptis/' . $request->type);
    }

    public function sakramenDewasa($request, $id = null)
    {
        // dd($request->all());
        //validate

        if ($request->_method == 'PUT' && $id != null) //only -edit
            $sb = DB::table('m_sakramen_baptis')->where('id', $id)->first();

        DB::beginTransaction();
        try {

            $pathImage = null;
            $pathImageSuratNikah = null;
            $namePath = null;
            $namePathSuratNikah = null;

            //editmode-delete
            if ($request->hasFile('image')) {
                $pathImage = date("ymdHis") . $request->nama_diri . ".jpg";
                $destinationPath = 'upload/sakramen-baptis/dewasa';
                $request->image->move($destinationPath, $pathImage);
                $namePath = $destinationPath.'/'.$pathImage;

                if ($request->_method == 'PUT')
                    File::delete($sb->image_baptis); //delete old images
            }

            if($request->sudah_menikah == "belum"){
                $ada_ijin_pasangan = "";
                $nama_pasangan = "";
                $agama_pasangan = "";
                $tanggal_pernikahan = "";
                $tempat_pernikahan = "";
                $pernikahan_agama = "";
                $pathImageSuratNikah = null;
            }else{
                $ada_ijin_pasangan = $request->ada_ijin_pasangan;
                $nama_pasangan = $request->nama_pasangan;
                $agama_pasangan = $request->agama_pasangan;
                $tanggal_pernikahan = date('Y-m-d', strtotime($request->tanggal_pernikahan));
                $tempat_pernikahan = $request->tempat_pernikahan;
                $pernikahan_agama = $request->pernikahan_agama;

                if ($request->hasFile('image3')) {
                    $pathImageSuratNikah = date("ymdHis") ."suratnikah_". $request->nama_diri . ".jpg";
                    $destinationPathSuratNikah = 'upload/sakramen-baptis/dewasa';
                    $request->image3->move($destinationPathSuratNikah, $pathImageSuratNikah);
                    $namePathSuratNikah = $destinationPathSuratNikah.'/'.$pathImageSuratNikah;

                    if ($request->_method == 'PUT')
                        File::delete($sb->surat_nikah); //delete old images
                }
            }

            $request->merge([
                'tanggal_lahir' => ($request->tanggal_lahir != "") ? date("Y-m-d", strtotime($request->tanggal_lahir)) : null,
                'tanggal_pernikahan' => ($tanggal_pernikahan != "") ? $tanggal_pernikahan : null,
                "image_baptis" => $namePath,
                "surat_nikah" => $namePathSuratNikah,
                "user_id" => Auth::user()->id,
                "createuser" => Auth::user()->name,
                'status' => 'in process',
                'created_at' => date('Y-m-d H:i:s'),
                'ada_ijin_pasangan' => $ada_ijin_pasangan,
                'nama_pasangan' => $nama_pasangan,
                'agama_pasangan' => $agama_pasangan,
                'tempat_pernikahan' => $tempat_pernikahan,
                'pernikahan_agama' => $pernikahan_agama,
            ]);

            $req = $request->except(
                '_token',
                '_method',
                'image',
                'image3'

            );

            //dd($req);

            if ($request->_method == "PUT") {
                DB::table('m_sakramen_baptis')->where('id', $id)->update($req);
            } else {
                DB::table('m_sakramen_baptis')->insert($req);
            }


            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect('admin/sakramen/baptis/' . $request->type);
    }

    public function sakramenKomuni(Request $request, $id = null)
    {
        // dd($request->all());
        //validate


        if ($request->_method == 'PUT' && $id != null) //only -edit
            $km = DB::table('m_sakramen_komuni')->where('id', $id)->first();

        DB::beginTransaction();
        try {

            $pathImage = null;
            $pathImageSuratBaptis = null;
            $pathImageSuratNikahOrtu = null;
            $namePath = null;
            $namePathSuratBaptis = null;
            $namePathSuratNikahOrtu = null;

            //editmode-delete
            if ($request->hasFile('image')) {
                $pathImage = date("ymdHis") . $request->nama_diri . ".jpg";
                $destinationPath = 'upload/sakramen-komuni';
                $request->image->move($destinationPath, $pathImage);
                $namePath = $destinationPath.'/'.$pathImage;

                if ($request->_method == 'PUT')
                    File::delete($km->image_komuni); //delete old images
            }

            if ($request->hasFile('image1')) {
                $pathImageSuratBaptis = date("ymdHis") ."suratbaptis_". $request->nama_diri . ".jpg";
                $destinationPathSuratBaptis = 'upload/sakramen-komuni';
                $request->image1->move($destinationPathSuratBaptis, $pathImageSuratBaptis);
                $namePathSuratBaptis = $destinationPathSuratBaptis.'/'.$pathImageSuratBaptis;

                if ($request->_method == 'PUT')
                    File::delete($km->image_surat_baptis); //delete old images
            }

            if ($request->hasFile('image2')) {
                $pathImageSuratNikahOrtu = date("ymdHis") ."suratnikahortu_". $request->nama_diri . ".jpg";
                $destinationPathSuratNikahOrtu = 'upload/sakramen-komuni';
                $request->image2->move($destinationPathSuratNikahOrtu, $pathImageSuratNikahOrtu);
                $namePathSuratNikahOrtu = $destinationPathSuratNikahOrtu.'/'.$pathImageSuratNikahOrtu;

                if ($request->_method == 'PUT')
                    File::delete($km->image_surat_nikah_ortu); //delete old images
            }

            $request->merge([
                'tanggal_lahir' => ($request->tanggal_lahir != null) ? date("Y-m-d", strtotime($request->tanggal_lahir)) : null,
                "image_komuni" => $namePath,
                "image_surat_baptis" => $namePathSuratBaptis,
                "image_surat_nikah_ortu" => $namePathSuratNikahOrtu,
                "user_id" => Auth::user()->id,
                "createuser" => Auth::user()->name,
                'status' => 'approved',
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $req = $request->except(
                '_token',
                '_method',
                'type',
                'image',
                'image1',
                'image2'
            );

            //dd($req);

            if ($request->_method == "PUT") {
                DB::table('m_sakramen_komuni')->where('id', $id)->update($req);
            } else {
                $komuni = DB::table('m_sakramen_komuni')->insertGetId($req);
                $this->sendEmail($komuni, "komuni");
            }


            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect('admin/sakramen/komuni/');
    }

    public function sakramenKrisma(Request $request, $id = null)
    {
        // dd($request->all());
        //validate


        if ($request->_method == 'PUT' && $id != null) //only -edit
            $krs = DB::table('m_sakramen_krisma')->where('id', $id)->first();

        DB::beginTransaction();
        try {

            $pathImage = null;
            $pathImageSuratNikah = null;
            $pathImageSuratBaptis = null;
            $namePath = null;
            $namePathSuratNikah = null;
            $namePathSuratBaptis = null;

            //editmode-delete
            if ($request->hasFile('image')) {
                $pathImage = date("ymdHis") . $request->nama_diri . ".jpg";
                $destinationPath = 'upload/sakramen-krisma';
                $request->image->move($destinationPath, $pathImage);
                $namePath = $destinationPath.'/'.$pathImage;

                if ($request->_method == 'PUT')
                    File::delete($krs->image_krisma); //delete old images
            }

            if ($request->hasFile('image1')) {
                $pathImageSuratNikah = date("ymdHis") ."suratnikah__". $request->nama_diri . ".jpg";
                $destinationPathSuratNikah = 'upload/sakramen-krisma';
                $request->image1->move($destinationPathSuratNikah, $pathImageSuratNikah);
                $namePathSuratNikah = $destinationPathSuratNikah.'/'.$pathImageSuratNikah;

                if ($request->_method == 'PUT')
                    File::delete($krs->image_surat_nikah); //delete old images
            }

            if ($request->hasFile('image2')) {
                $pathImageSuratBaptis = date("ymdHis") ."suratbaptis_". $request->nama_diri . ".jpg";
                $destinationPathSuratBaptis = 'upload/sakramen-krisma';
                $request->image2->move($destinationPathSuratBaptis, $pathImageSuratBaptis);
                $namePathSuratBaptis = $destinationPathSuratBaptis.'/'.$pathImageSuratBaptis;

                if ($request->_method == 'PUT')
                    File::delete($krs->image_surat_baptis); //delete old images
            }

            $request->merge([
                'tanggal_lahir' => ($request->tanggal_lahir != null) ? date("Y-m-d", strtotime($request->tanggal_lahir)) : null,
                'tanggal_baptis' => ($request->tanggal_baptis != null) ? date("Y-m-d", strtotime($request->tanggal_baptis)) : null,
                "image_krisma" => $namePath,
                "image_surat_nikah" => $namePathSuratNikah,
                "image_surat_baptis" => $namePathSuratBaptis,
                "user_id" => Auth::user()->id,
                "createuser" => Auth::user()->name,
                'status' => 'approved',
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $req = $request->except(
                '_token',
                '_method',
                'type',
                'image',
                'image1',
                'image2'
            );

            //dd($req);

            if ($request->_method == "PUT") {
                DB::table('m_sakramen_krisma')->where('id', $id)->update($req);
            } else {
                $krisma = DB::table('m_sakramen_krisma')->insertGetId($req);
                $this->sendEmail($krisma, "krisma");
            }



            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect('admin/sakramen/krisma/');
    }

    public function pernikahan($request, $id = null)
    {
        // return Response::json(['data' => [
        //     'status' => 200,
        //     'request' => $request->ajax(),
        //     'response' => $request->all(),
        // ]]);

        // dd($request->all());

        $userid = ($request->ajax()) ? $request->userid : Auth::user()->id;
        //validate
        if ($request->_method != 'PUT' && $id == null) {

            $cek = DB::table('m_agenda')->where('id_user',$userid)->where("type_agenda","pernikahan")->whereIn('status',['approved','in process','in approval'])->get();

            // $hasil = count($cek);
            if($cek->isNotEmpty())
                return ($request->ajax())
                        ?  Response::json([
                                "code" => 403,
                                'description' => "Anda sudah membuat pendaftaran sakramen pernikahan",
                            ],403)
                        : redirect()->back()->with('message', 'Anda sudah membuat pendaftaran sakramen pernikahan');
        }

        if ($request->_method == 'PUT' && $id != null) //only -edit
            $ag = DB::table('m_agenda')->where('id_agenda', $id)->first();

        DB::beginTransaction();
        try {

            $pathPP = null;
            $pathPW = null;
            $pathSuratBaptisPria = null;
            $pathSuratBaptisWanita = null;
            $namePathPP = null;
            $namePathPW = null;
            $namePathSuratBaptisPria = null;
            $namePathSuratBaptisWanita = null;
            $awal = date("H:i:s", strtotime($request->waktu_agenda));
            $akhir = date("H:i:s", strtotime($request->end_agenda));

            if ($request->tempat_menikah != "on") {
                if($request->_method == 'PUT'){
                    $agenda = DB::table('m_agenda')
                    ->where('date_agenda', date("Y-m-d", strtotime($request->date_agenda)))
                    ->where('tempat_menikah', 1)
                    ->where('id_agenda','!=',$ag->id_agenda)
                    ->where('type_agenda', 'pernikahan')
                    ->get();

                }else{
                    $agenda = DB::table('m_agenda')
                        ->where('date_agenda', date("Y-m-d", strtotime($request->date_agenda)))
                        ->where('tempat_menikah', 1)
                        ->where('type_agenda', 'pernikahan')
                        ->get();
                }

                $count = 0;
                foreach ($agenda as $key => $value) {
                    if ($value->waktu_agenda > $awal && $value->end_agenda <= $akhir) {
                        $count = $count + 1;
                    } else if ($value->waktu_agenda <= $awal && $value->end_agenda >= $akhir) {
                        $count = $count + 1;
                    } else if ($value->waktu_agenda <= $awal && $value->end_agenda > $akhir) {
                        $count = $count + 1;
                    }
                }
                if ($count > 0) {
                    return ($request->ajax())
                        ?  Response::json([
                                "code" => 403,
                                'description' => "Waktu/Tempat bertabrakan dengan jadwal agenda lain",
                            ],403)
                        : redirect()->back()->with('message', 'Waktu/Tempat bertabrakan dengan jadwal agenda lain');
                }
            }

            //editmode-delete
            if ($request->hasFile('image_pengantin_pria')) {
                $pathPP = date("ymdHis").'image_pengantin_pria_' . $request->nama_pengantin_pria . ".jpg";
                $destinationPathPP = 'upload/wedding';
                $request->image_pengantin_pria->move($destinationPathPP, $pathPP);
                $namePathPP = $destinationPathPP.'/'.$pathPP;

                if ($request->_method == 'PUT')
                    File::delete($ag->image_nikah_pengantin_pria); //delete old images
            }

            if ($request->hasFile('image_pengantin_wanita')) {
                $pathPW = date("ymdHis") .'image_pengantin_wanita_' . $request->nama_pengantin_wanita . ".jpg";
                $destinationPathPW = 'upload/wedding';
                $request->image_pengantin_wanita->move($destinationPathPW, $pathPW);
                $namePathPW = $destinationPathPW.'/'.$pathPW;

                if ($request->_method == 'PUT')
                    File::delete($ag->image_nikah_pengantin_wanita); //delete old images
            }

            if($request->agama_pengantin_pria == "Katolik"){
                if ($request->hasFile('image2')) {
                    $pathSuratBaptisPria = date("ymdHis") ."suratbaptispria_". $request->nama_pengantin_pria . ".jpg";
                    $destinationPathSuratBaptisPria = 'upload/wedding';
                    $request->image2->move($destinationPathSuratBaptisPria, $pathSuratBaptisPria);
                    $namePathSuratBaptisPria = $destinationPathSuratBaptisPria.'/'.$pathSuratBaptisPria;

                    if ($request->_method == 'PUT')
                        File::delete($ag->surat_baptis_pengantin_pria); //delete old images
                }
            }

            if($request->agama_pengantin_wanita == "Katolik"){
                if ($request->hasFile('image3')) {
                    $pathSuratBaptisWanita = date("ymdHis") ."suratbaptiswanita_". $request->nama_pengantin_wanita . ".jpg";
                    $destinationPathSuratBaptisWanita = 'upload/wedding';
                    $request->image3->move($destinationPathSuratBaptisWanita, $pathSuratBaptisWanita);
                    $namePathSuratBaptisWanita = $destinationPathSuratBaptisWanita.'/'.$pathSuratBaptisWanita;

                    if ($request->_method == 'PUT')
                        File::delete($ag->surat_baptis_pengantin_wanita); //delete old images
                }
            }

            if ($request->hari_libur == "on") {
                $libur = 1;
            } else {
                $libur = 0;
            }

            $request->merge([
                'type_agenda' => 'pernikahan',
                'date_agenda' => ($request->date_agenda != null) ? date("Y-m-d", strtotime($request->date_agenda)) : null,
                'tanggal_lahir_pengantin_pria' => ($request->tanggal_lahir_pengantin_pria != null) ? date("Y-m-d", strtotime($request->tanggal_lahir_pengantin_pria)) : null,
                'tanggal_lahir_pengantin_wanita' => ($request->tanggal_lahir_pengantin_wanita != null) ? date("Y-m-d", strtotime($request->tanggal_lahir_pengantin_wanita)) : null,
                'image_nikah_pengantin_pria' => $namePathPP,
                'image_nikah_pengantin_wanita' => $namePathPW,
                'surat_baptis_pengantin_pria' => $namePathSuratBaptisPria,
                'surat_baptis_pengantin_wanita' => $namePathSuratBaptisWanita,
                "waktu_agenda" => $awal,
                "end_agenda" => $akhir,
                "id_user" => $userid,
                "alternative_tempat_menikah" => ($request->tempat_menikah == 'on') ? $request->alternative_tempat_menikah : '',
                "tempat_menikah" => ($request->tempat_menikah == 'on') ? 2 : 1,
                'hari_libur' => $libur,
                'status' => 'in process',
                'created_at' => date('Y-m-d H:i:s'),
                'image_mempelai' => "default_foto_pasangan.jpg",
            ]);

            $req = $request->except(
                '_token',
                '_method',
                'type',
                'image_pengantin_pria',
                'image_pengantin_wanita',
                'image2',
                'image3',
                'userid',
            );

            // dd($req,$request->_method);

            if ($request->_method == "PUT" && $id != null) { //only -edit
                DB::table('m_agenda')->where('id_agenda', $id)->update($req);
            } else {
                DB::table('m_agenda')->insert($req);
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return ($request->ajax())
            ? Response::json([
                    "code" => 200,
                    'description' => "success",
                ])
            : redirect('admin/sakramen/nikah/' . $request->type);
    }


    public function edit($type, $uniqid, $nama = null)
    {
        switch ($type) {
            case 'nikah':
                return $this->editSNikah($uniqid);
                break;

            case 'baptis':
                return $this->editBaptis($uniqid, $nama);
                break;

            case 'komuni':
                return $this->editKomuni($uniqid);
                break;

            default:
                # code...
                break;
        }
    }

    public function editSNikah($id)
    {
        $lingkungan = DB::table('m_kategori_user')->where('id_bidang', 10)->where('type', 'lingkungan')->where('status', 'active')->get();
        $agenda = AgendaModel::where('id_agenda', $id)->first();

        // return Response::json($agenda);

        return view('admin.sakramen.nikah.create', [
            'editmode' => true,
            'agenda' => $agenda->toJson(),
            'id' => $id,
            'url' => url('/admin/sakramen-update/' . $id),
            'lingkungan' => $lingkungan,
        ]);
    }

    public function editBaptis($id, $type)
    {
        $date = date("Y-m-d");
        $lingkungan = DB::table('m_kategori_user')->where('id_bidang', 10)->where('type', 'lingkungan')->where('status', 'active')->get();
        $periode = DB::table('m_periode')
            ->where('type_periode', "baptis_" . $type)
            ->where('start_date', '<=', $date)
            ->where('end_date', '>', $date)
            ->where('status_periode', 'active')
            ->get();

        $baptis = BaptisModel::where('id', $id)->first();

        // return Response::json($agenda);

        return view('admin.sakramen.baptis.create', [
            'editmode' => true,
            'baptis' => $baptis->toJson(),
            'id' => $id,
            'url' => url('/admin/sakramen-update/' . $id),
            'lingkungan' => $lingkungan,
            'periode' => $periode,
            'type' => $type
        ]);
    }

    public function editKomuni($id)
    {
        $date = date("Y-m-d");
        $lingkungan = DB::table('m_kategori_user')->where('id_bidang', 10)->where('type', 'lingkungan')->where('status', 'active')->get();
        $periode = DB::table('m_periode')
            ->where('type_periode', "komuni")
            ->where('start_date', '<=', $date)
            ->where('end_date', '>', $date)
            ->where('status_periode', 'active')
            ->get();

        $komuni = KomuniModel::where('id', $id)->first();

        // return Response::json($agenda);

        return view('admin.sakramen.komuni.create', [
            'editmode' => true,
            'komuni' => $komuni->toJson(),
            'id' => $id,
            'url' => url('/admin/sakramen-update/' . $id),
            'lingkungan' => $lingkungan,
            'periode' => $periode
        ]);
    }

    public function delete($type, $id)
    {
        if ($type == 'nikah') {
            $del = DB::table('m_agenda')->where('id_agenda', $id)->first();

            if ($del == null) {
                return back();
            }

            DB::table('m_agenda')->where('id_agenda', $id)->delete();
        } elseif ($type == 'baptis') {
            $del = DB::table('m_sakramen_baptis')->where('id', $id)->delete();
            if ($del == null) {
                return back();
            }
            DB::table('m_sakramen_baptis')->where('id', $id)->delete();
        } elseif ($type == 'komuni') {
            $del = DB::table('m_sakramen_komuni')->where('id', $id)->delete();
            if ($del == null) {
                return back();
            }
            DB::table('m_sakramen_komuni')->where('id', $id)->delete();
        }

        return back();
    }

    public function insertTra(array $options)
    {
        DB::beginTransaction();
        try {
            SakramenTransaction::create([
                'code' => SakramenTransaction::GetCode(),
                'sakramen_id' => $options['sakramen_id'],
                'periode_id' => $options['periode_id'],
                'sakramen_name' => $options['sakramen_name'],
                'sakramen_type' => $options['sakramen_type'],
                'name' => $options['name'],
                'place' => $options['place'],
                'price' => $options['price'],
                'date' => $options['date'],
                'date_implementation' => $options['date_implementation'],
                'createuser' => $options['createuser'],
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        DB::commit();
    }

    public function transactions()
    {
        $model = SakramenTransaction::orderBy('id', 'desc');

        return DataTables::of($model)
            ->editColumn('status', function ($s) {
                return ($s->status == 'debt')
                    ? "<span class=\"label label-danger\">Hutang</span>"
                    : "<span class=\"label label-primary\">Lunas</span>";
            })
            ->editColumn('price', function ($s) {
                return "Rp." . number_format($s->price, 0, '.', '.');
            })
            ->addColumn('action', function ($s) {
                if ($s->status == 'debt') {
                    return "<a href=\"" . url('skn-upto-by-admin/tra/' . $s->id . '/paid') . "\" class=\"btn btn-primary btn-xs\"> <span class=\"fa fa-check\"></span> </a> &nbsp"
                        .
                        "<a href=\"" . url('skn-upto-by-admin/tra/' . $s->id . '/reject') . "\" class=\"btn btn-danger btn-xs\"> <span class=\"fa fa-close\"></span> </a>";
                } else {
                    return '-';
                }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function upStatusTra($id)
    {
        $sk = SakramenTransaction::where('id', $id)->where('status', 'debt')->firstOrFail();
        $sk->update([
            'status' => 'paid'
        ]);

        return response()->json('ok', 200);
    }

    public function toInApproval($model, $id)
    {
        // dd($model,$id);
        if ($model == 'pernikahan') {
            $ag = DB::table('m_agenda')->where('id_agenda', $id)->first();
            if ($ag == null)
                abort(404);

            DB::table('m_agenda')->where('id_agenda', $id)->update([
                'status' => 'in approval'
            ]);
        } else if ($model == 'baptis') {
            $skr = DB::table('m_sakramen_baptis')->where('id', $id)->first();
            if ($skr == null)
                abort(404);

            DB::table('m_sakramen_baptis')->where('id', $id)->update([
                'status' => 'in approval'
            ]);
        }

        return back();
    }

    public function toUp($model, $id, $status)
    {
        // dd($model, $id, ucfirst($status));

        if (!in_array(($status), ['approve', 'paid', 'reject'])) abort(404);

        if ($model == 'pernikahan') {

            $ag = DB::table('m_agenda')->where('id_agenda', $id)->first();

            if ($ag == null)
                abort(404);

            if ($ag->id_kategori_user_pengantin_pria != 0) {
                $nikah = [
                    'id_kategori_user' => $ag->id_kategori_user_pengantin_pria,
                    'judul' => $ag->nama_agenda,
                    'id' => $ag->id_agenda,
                    'section' => "pria",
                ];
                $this->sendEmail($nikah, "nikah");
            }

            if ($ag->id_kategori_user_pengantin_wanita != 0) {
                $nikah = [
                    'id_kategori_user' => $ag->id_kategori_user_pengantin_wanita,
                    'judul' => $ag->nama_agenda,
                    'id' => $ag->id_agenda,
                    'section' => "wanita",
                ];
                $this->sendEmail($nikah, "nikah");
            }

            $this->sendEmail($ag, "email_nikah");


            //insert-transaction
            $arr = [
                'sakramen_id' => $ag->id_agenda,
                'periode_id' => 0,
                'date_implementation' => date('Y-m-d',strtotime($ag->date_agenda)),
                'date' => $ag->created_at,
                'sakramen_name' => "Nikah $ag->nama_pengantin_pria - $ag->nama_pengantin_wanita",
                'sakramen_type' => 'pernikahan',
                'place' => ($ag->tempat_menikah == 1)
                            ? "Gereja Paroki"
                            : $ag->alternative_tempat_menikah,
                'name' => $ag->nama_pengantin_pria." - ". $ag->nama_pengantin_wanita,
                'price' => 50000,
                'createuser' => Auth::user()->name,
            ];

            $this->insertTra($arr);

            DB::table('m_agenda')->where('id_agenda', $id)->update([
                'status' => 'approved'
            ]);
        } else if ($model == 'baptis') {
            $skr = DB::table('m_sakramen_baptis')->where('id', $id)->first();
            if ($skr == null)
                abort(404);


            //send-email
            $this->sendEmail($skr, "sakramen");

            $periode = DB::table('m_periode')->where('id_periode',$skr->id_periode)->first();
            //insert-transaction
            $arr = [
                'sakramen_id' => $skr->id,
                'sakramen_name' => $periode->nama_periode,
                'sakramen_type' => "baptis $skr->type",
                'periode_id' => $skr->id_periode,
                'date_implementation' => $periode->tgl_pelaksanaan,
                'date' => $skr->created_at,
                'name' => $skr->nama_diri,
                'place' => 'RK',
                'price' => 10000,
                'createuser' => Auth::user()->name,
            ];

            $this->insertTra($arr);

            //up-status
            DB::table('m_sakramen_baptis')->where('id', $id)->update([
                'status' => 'approved'
            ]);
        } else if ($model == 'tra') {
            SakramenTransaction::where('id', $id)->update([
                'status' => strtolower($status)
            ]);
        }

        return back();
    }

    public function table($param, $uniq = null, $typebaptis = null)
    {
        switch ($param) {
            case 'nikah':
                return $this->apiKegiatanNikah($uniq);
                break;
            case 'baptis':
                return $this->apiKegiatanBaptis($uniq, $typebaptis);
                break;
            case 'transaction':
                return $this->transactions();
                break;

            case 'komuni':
                return $this->apiKegiatanKomuni($uniq);
                break;
            case 'krisma':
                return $this->apiKegiatanKrisma($uniq);
                break;
            default:
                # code...
                break;
        }
    }

    public function tableTTD($param, $uniq = null, $typebaptis = null)
    {
        switch ($param) {
            case 'nikah':
                return $this->apiKegiatanNikahTTD($uniq);
                break;
            case 'baptis':
                return $this->apiKegiatanBaptisTTD($uniq, $typebaptis);
                break;

            case 'komuni':
                return $this->apiKegiatanKomuniTTD($uniq);
                break;
            case 'krisma':
                return $this->apiKegiatanKrismaTTD($uniq);
                break;
            default:
                # code...
                break;
        }
    }

    public function apiKegiatanNikah($iduser = null)
    {
        $user  = DB::table('m_user')->where('id', $iduser)->first();

        $post = DB::table('m_agenda')
            ->select('m_agenda.*')
            ->where('type_agenda', 'pernikahan')
            ->orderBy('id_agenda', 'desc');


        if (in_array($user->role, [4])) {
            $post->where('id_user', $iduser);
        }

        $post = $post;

        return Datatables::of($post)
            ->addColumn('action', function ($post) use ($user) {
                $html = null;
                if(($post->signature_pengantin_pria == null || $post->signature_pengantin_wanita == null) && in_array($user->role, [4])){
                    $html = '<a href="' . url('sakramen-signature-ummat/' .$post->id_agenda .'/nikah') . '" class="btn btn-default  btn-xs"><i class="fas fa-signature"></i></a>&nbsp';
                }else{
                    $html = "";
                }

                if ($post->status == 'in process' && in_array($user->role, [4])) {
                // if ($post->status == 'in process') {
                    $html .= '&nbsp <a href="' . url('sakramen-nikah/wizard/'. $post->step_view) . '" class="btn btn-warning  btn-xs"><i class="fa fa-edit"></i></a>' .
                        '&nbsp' .
                        '<a href="' . url('admin/skn-upto/pernikahan/' . $post->id_agenda) . '" class="btn btn-primary  btn-xs"><i class="fa fa-send"></i></a>' .
                        '&nbsp' .
                        '<a href="' . url('/admin/sakramen-delete/nikah/' . $post->id_agenda) . '" class="btn btn-danger  btn-xs" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Menghapus ?'" . ')"><i class="fa fa-trash"></i></a>';
                } else if ($post->status == 'in approval' && in_array($user->role, [1, 2])) {
                    $html .=
                        '<a href="' . url('skn-upto-by-admin/pernikahan/' . $post->id_agenda . '/approve') . '" class="btn btn-success  btn-xs"><i class="fa fa-check"></i></a> &nbsp' .
                        '<a href="' . url('skn-upto-by-admin/pernikahan/' . $post->id_agenda) . '/cancel" class="btn btn-danger  btn-xs"><i class="fa fa-close"></i></a> &nbsp';

                }


                    $html .= '<a href="' . url('/admin-sakramen-print/pernikahan/' . $post->id_agenda . '/printout') . '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>';


                return $html;
            })
            ->addColumn('pasangan', function ($post) {
                $nama = $post->nama_pengantin_pria . ' & ' . $post->nama_pengantin_wanita;

                return $nama;
            })
            ->editColumn('tempat_menikah', function ($post) {
                $tempat = "";
                if ($post->tempat_menikah == 1) {
                    $tempat = "Gereja Paroki";
                } else { }

                return $tempat;
            })
            ->editColumn('date_agenda', function ($post) {
                $tempat = "Gereja Paroki";
                if ($post->tempat_menikah == 2) {
                    $tempat = $post->alternative_tempat_menikah;
                }
                return date('d M Y', strtotime($post->date_agenda)) . '<br/>' . $tempat;
            })
            ->editColumn('waktu', function ($post) {
                return date('H.i', strtotime($post->waktu_agenda)) . '-' . date('H.i', strtotime($post->end_agenda)) . ' WIB';
            })
            ->editColumn('nama_agenda', function ($post) {
                $cek = '<a href="' . url('/admin/kegiatan-nikah/' . $post->id_agenda . '/show') . '">' . $post->nama_agenda . '</a>';
                return $cek;
            })
            ->filterColumn('date_agenda', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(date_agenda,'%d %M %Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('tempat_menikah', function ($query, $keyword) {
                $query->whereRaw("IF(tempat_menikah = 1, 'Gereja Paroki', 'm_agenda.alternative_tempat_menikah') like ?", ["%$keyword%"]);
            })


            ->editColumn('status', function ($post) {
                if ($post->status == 'in process') {
                    $cl = 'label label-default';
                } elseif ($post->status == 'in approval') {
                    $cl = 'label label-primary';
                } elseif ($post->status == 'approved') {
                    $cl = 'label label-success';
                } elseif ($post->status == 'cancel') {
                    $cl = 'label label-danger';
                }

                return "<span class=\"" . $cl . "\">" . ucfirst($post->status) . "</span>";
            })

            ->editColumn('status_ttd', function ($post) {
                if ($post->status_ttd == 'incomplete') {
                    $cl = 'label label-default';
                } elseif ($post->status_ttd == 'complete') {
                    $cl = 'label label-success';
                }

                return "<span class=\"" . $cl . "\">" . ucfirst($post->status_ttd) . "</span>";
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'date_agenda', 'waktu', 'pasangan', 'nama_agenda', 'tempat_menikah', 'status','status_ttd'])
            ->make(true);
    }

    public function apiKegiatanNikahTTD($iduser = null)
    {
        $user  = DB::table('m_user')->where('id', $iduser)->first();

        $post = DB::table('sakramen_signature')
            ->select('m_agenda.*','sakramen_signature.id','sakramen_signature.sequence','sakramen_signature.section')
            ->join('m_agenda','m_agenda.id_agenda','sakramen_signature.id_agenda')
            ->where('sakramen_signature.type', 'nikah')
            ->where('sakramen_signature.user_id', $iduser)
            ->where('m_agenda.status', "approved")
            ->orderBy('sakramen_signature.id', 'desc');

        $post = $post;

        return Datatables::of($post)
            ->addColumn('action', function ($post) use ($user) {


                $hasil = '<table id="tabel-in-opsi">' .
                    '<tr>' .
                    '<td>' .
                    '<a href="' . url('/admin/sakramen-signature/' .$post->id_agenda.'/nikah/'.$post->section) . '" class="btn btn-default  btn-sm"><i class="fas fa-signature"></i></a>' .
                    '&nbsp' .
                    '<a href="' . url('/admin-sakramen-print/pernikahan/' . $post->id_agenda . '/printout') . '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>'.
                    '</td>' .
                    '</tr>' .
                    '</table>';

                return $hasil;
            })
            ->addColumn('pasangan', function ($post) {
                $nama = $post->nama_pengantin_pria . ' & ' . $post->nama_pengantin_wanita;

                return $nama;
            })
            ->editColumn('tempat_menikah', function ($post) {
                $tempat = "";
                if ($post->tempat_menikah == 1) {
                    $tempat = "Gereja Paroki";
                } else { }

                return $tempat;
            })
            ->editColumn('date_agenda', function ($post) {
                $tempat = "Gereja Paroki";
                if ($post->tempat_menikah == 2) {
                    $tempat = $post->alternative_tempat_menikah;
                }
                return date('d M Y', strtotime($post->date_agenda)) . '<br/>' . $tempat;
            })
            ->editColumn('waktu', function ($post) {
                return date('H.i', strtotime($post->waktu_agenda)) . '-' . date('H.i', strtotime($post->end_agenda)) . ' WIB';
            })
            ->editColumn('nama_agenda', function ($post) {
                $cek = '<a href="' . url('/admin/kegiatan-nikah/' . $post->id_agenda . '/show') . '">' . $post->nama_agenda . '</a>';
                return $cek;
            })
            ->filterColumn('date_agenda', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(date_agenda,'%d %M %Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('tempat_menikah', function ($query, $keyword) {
                $query->whereRaw("IF(tempat_menikah = 1, 'Gereja Paroki', 'm_agenda.alternative_tempat_menikah') like ?", ["%$keyword%"]);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'date_agenda', 'waktu', 'pasangan', 'nama_agenda', 'tempat_menikah'])
            ->make(true);
    }

    public function apiKegiatanBaptis($iduser = null, $typebaptis = null)
    {

        $user  = DB::table('m_user')->where('id', $iduser)->first();

        $skr = DB::table('m_sakramen_baptis')
            ->select('m_sakramen_baptis.*','m_kategori_user.nama_kategori_user')
            ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_sakramen_baptis.id_kategori_user')
            ->where('m_sakramen_baptis.type', $typebaptis)
            ->orderBy('m_sakramen_baptis.id', 'desc');

        if (in_array($user->role, [4])) {
            $skr->where('user_id', $iduser);
        }

        $skr = $skr;

        return Datatables::of($skr)
            ->addColumn('action', function ($skr) use ($typebaptis, $user) {
                $html = null;
                if(!in_array($typebaptis,['balita'])){

                    if( $typebaptis == 'dewasa' && $skr->signature_dewasa == null && $user->role == 4  ){
                        $html = '<a href="' . url('sakramen-signature-ummat/' .$skr->id .'/baptis/'.$typebaptis) . '" class="btn btn-default  btn-xs"><i class="fas fa-signature"></i></a>&nbsp';
                    }else if( ($typebaptis == 'anak' && ($skr->signature_anak == null || $skr->signature_orang_tua == null)) && $user->role == 4 ){
                        $html = '<a href="' . url('sakramen-signature-ummat/' .$skr->id .'/baptis/'.$typebaptis) . '" class="btn btn-default  btn-xs"><i class="fas fa-signature"></i></a>&nbsp';
                    }else{
                        $html = "";
                    }
                }


                if ($skr->status == 'in process' && in_array($user->role, [4])) {
                    $html .=  '&nbsp <a href="' . url('/admin/sakramen-edit/baptis/' . $skr->id . '/' . $typebaptis) . '" class="btn btn-warning  btn-xs"><i class="fa fa-edit"></i></a>' .
                        '&nbsp' .
                        '<a href="' . url('admin/skn-upto/baptis/' . $skr->id) . '" class="btn btn-primary  btn-xs"><i class="fa fa-send"></i></a>' .
                        '&nbsp' .
                        '<a href="' . url('/admin/sakramen-delete/baptis/' . $skr->id) . '" class="btn btn-danger  btn-xs"><i class="fa fa-trash" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Menghapus ?'" . ')"></i></a>&nbsp';
                } else if ($skr->status == 'in approval' && in_array($user->role, [1, 2])) {
                    $html .= '<a href="' . url('skn-upto-by-admin/baptis/' . $skr->id . '/approve') . '" class="btn btn-success  btn-xs"><i class="fa fa-check"></i></a> &nbsp' .
                        '<a href="' . url('skn-upto-by-admin/baptis/' . $skr->id) . '/cancel" class="btn btn-danger  btn-xs"><i class="fa fa-close"></i></a> &nbsp';
                }

                // if(in_array($user->role, [1, 2]) && $post->status == 'approved'){
                     $html .= '<a href="' . url('/admin-sakramen-print/'.$typebaptis.'/' . $skr->id . '/printout') . '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>';
                // }

                return $html;
            })

            ->editColumn('status', function ($skr) {
                if ($skr->status == 'in process') {
                    $cl = 'label label-default';
                } elseif ($skr->status == 'in approval') {
                    $cl = 'label label-primary';
                } elseif ($skr->status == 'approved') {
                    $cl = 'label label-success';
                } elseif ($skr->status == 'cancel') {
                    $cl = 'label label-danger';
                }

                return "<span class=\"" . $cl . "\">" . ucfirst($skr->status) . "</span>";
            })

            ->editColumn('status_ttd', function ($skr) {
                if ($skr->status_ttd == 'incomplete') {
                    $cl = 'label label-default';
                } elseif ($skr->status_ttd == 'complete') {
                    $cl = 'label label-success';
                }

                return "<span class=\"" . $cl . "\">" . ucfirst($skr->status_ttd) . "</span>";
            })

            ->addIndexColumn()
            ->rawColumns(['action','status','status_ttd'])
            ->make(true);
    }

    public function apiKegiatanBaptisTTD($iduser = null, $typebaptis = null)
    {

        $user  = DB::table('m_user')->where('id', $iduser)->first();

        $skr = DB::table('sakramen_signature')
            ->select('m_sakramen_baptis.*','sakramen_signature.id as id_signature','sakramen_signature.sequence')
            ->join('m_sakramen_baptis','m_sakramen_baptis.id','sakramen_signature.id_sakramen')
            ->where('sakramen_signature.type', 'baptis_'.$typebaptis)
            ->where('sakramen_signature.user_id', $iduser)
            ->where('m_sakramen_baptis.status', "approved")
            ->orderBy('sakramen_signature.id', 'desc');


        $skr = $skr;

        return Datatables::of($skr)
            ->addColumn('action', function ($skr) use ($typebaptis, $user) {

                $hasil = '<table id="tabel-in-opsi">' .
                    '<tr>' .
                    '<td>' .
                    '<a href="' . url('/admin/sakramen-signature/' .$skr->id.'/baptis/'.$typebaptis) . '" class="btn btn-default btn-sm"><i class="fas fa-signature"></i></a>' .
                    '&nbsp' .
                    '<a href="' . url('/admin-sakramen-print/'.$typebaptis.'/' . $skr->id . '/printout') . '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>'.
                    '</td>' .
                    '</tr>' .
                    '</table>';

                return $hasil;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function apiKegiatanKomuni($user = null)
    {
        $user_cek  = DB::table('m_user')->where('id', $user)->first();

        $skr = DB::table('m_sakramen_komuni')
            ->select('m_sakramen_komuni.*','m_kategori_user.nama_kategori_user')
            ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_sakramen_komuni.id_kategori_user')
            ->orderBy('id', 'desc');


        if (in_array($user_cek->role, [4])) {
            $skr->where('user_id', $user);
        }

        $skr = $skr;

        return Datatables::of($skr)
            ->addColumn('action', function ($skr) use ($user_cek) {

                if(($skr->signature_ayah == null || $skr->signature_ibu == null) && $user_cek->role == 4  ){
                    $html = '<a href="' . url('sakramen-signature-ummat/' .$skr->id .'/komuni') . '" class="btn btn-default  btn-xs"><i class="fas fa-signature"></i></a>&nbsp';
                }else{
                    $html = "";
                }

                if($skr->status == 'in process'){
                    $html .=
                        '&nbsp <a href="' . url('/admin/sakramen-edit/komuni/' . $skr->id) . '" class="btn btn-warning  btn-xs"><i class="fa fa-edit"></i></a>&nbsp' .
                        '&nbsp <a href="' . url('/admin/sakramen-delete/komuni/' . $skr->id) . '" class="btn btn-danger  btn-xs"><i class="fa fa-trash" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Menghapus ?'" . ')"></i></a>&nbsp';
                }

                // if(in_array($user_cek->role, [1, 2])){
                    $html .= '<a href="' . url('/admin-sakramen-print/komuni/' . $skr->id . '/printout') . '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>';
                // }

                return $html;
            })

            ->editColumn('status', function ($skr) {
                if ($skr->status == 'in process') {
                    $cl = 'label label-default';
                } elseif ($skr->status == 'in approval') {
                    $cl = 'label label-primary';
                } elseif ($skr->status == 'approved') {
                    $cl = 'label label-success';
                } elseif ($skr->status == 'cancel') {
                    $cl = 'label label-danger';
                }

                return "<span class=\"" . $cl . "\">" . ucfirst($skr->status) . "</span>";
            })

            ->editColumn('status_ttd', function ($skr) {
                if ($skr->status_ttd == 'incomplete') {
                    $cl = 'label label-default';
                } elseif ($skr->status_ttd == 'complete') {
                    $cl = 'label label-success';
                }

                return "<span class=\"" . $cl . "\">" . ucfirst($skr->status_ttd) . "</span>";
            })

            ->addIndexColumn()
            ->rawColumns(['action','status','status_ttd'])
            ->make(true);
    }

    public function apiKegiatanKomuniTTD($user = null)
    {
        $user_cek  = DB::table('m_user')->where('id', $user)->first();

        $skr = DB::table('sakramen_signature')
            ->select('m_sakramen_komuni.*','sakramen_signature.id as id_signature','sakramen_signature.sequence')
            ->join('m_sakramen_komuni','m_sakramen_komuni.id','sakramen_signature.id_sakramen')
            ->where('sakramen_signature.type', 'komuni')
            ->where('sakramen_signature.user_id', $user)
            ->where('m_sakramen_komuni.status', "approved")
            ->orderBy('sakramen_signature.id', 'desc');


        $skr = $skr;


        return Datatables::of($skr)
            ->addColumn('action', function ($skr) {

                $hasil = '<table id="tabel-in-opsi">' .
                    '<tr>' .
                    '<td>' .
                    '<a href="' . url('/admin/sakramen-signature/' .$skr->id.'/komuni') . '" class="btn btn-default btn-sm"><i class="fas fa-signature"></i></a>' .
                    '&nbsp'.
                    '<a href="' . url('/admin-sakramen-print/komuni/' . $skr->id . '/printout') . '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>'.
                    '</td>' .
                    '</tr>' .
                    '</table>';
                return $hasil;
            })
            ->editColumn('status', function ($skr) {
                return ucfirst($skr->status);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function apiKegiatanKrisma($user = null)
    {
        $krs = DB::table('m_sakramen_krisma')
            ->select('m_sakramen_krisma.*','m_kategori_user.nama_kategori_user')
            ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_sakramen_krisma.id_kategori_user')
            ->orderBy('id', 'desc');

        $user_cek  = DB::table('m_user')->where('id', $user)->first();

        if (in_array($user_cek->role, [4])) {
            $krs->where('user_id', $user);
        }

        $krs = $krs;

        return Datatables::of($krs)
            ->addColumn('action', function ($krs) use ($user_cek) {
                if($krs->signature_umat == null && $user_cek->role == 4 ){
                    $html = '<a href="' . url('sakramen-signature-ummat/' .$krs->id .'/krisma') . '" class="btn btn-default  btn-xs"><i class="fas fa-signature"></i></a>&nbsp';
                }else{
                    $html = "";
                }

                if($krs->status == 'in process'){
                    $html .=
                    '&nbsp; <a href="' . url('/admin/sakramen-edit/krisma/' . $krs->id) . '" class="btn btn-warning  btn-xs"><i class="fa fa-edit"></i></a>&nbsp' .

                    '&nbsp; <a href="' . url('/admin/sakramen-delete/krisma/' . $krs->id) . '" class="btn btn-danger  btn-xs"><i class="fa fa-trash" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Menghapus ?'" . ')"></i></a>&nbsp';
                }

                // if(in_array($user_cek->role, [1, 2])){
                    $html .= '<a href="' . url('/admin-sakramen-print/krisma/' . $krs->id . '/printout') . '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>';
                // }

                return $html;
            })
            ->editColumn('status', function ($krs) {
                $cl = null;
                if ($krs->status == 'in process') {
                    $cl = 'label label-default';
                } elseif ($krs->status == 'in approval') {
                    $cl = 'label label-primary';
                } elseif ($krs->status == 'approved') {
                    $cl = 'label label-success';
                } elseif ($krs->status == 'cancel') {
                    $cl = 'label label-danger';
                }

                return "<span class=\"" . $cl . "\">" . ucfirst($krs->status) . "</span>";
            })

             ->editColumn('status_ttd', function ($krs) {
                $cl1 = null;
                if ($krs->status_ttd == 'incomplete') {
                    $cl1 = 'label label-default';
                } elseif ($krs->status_ttd == 'complete') {
                    $cl1 = 'label label-success';
                }

                return "<span class=\"" . $cl1 . "\">" . ucfirst($krs->status_ttd) . "</span>";
            })
            ->addIndexColumn()
            ->rawColumns(['action','status','status_ttd'])
            ->make(true);
    }

    public function apiKegiatanKrismaTTD($user = null)
    {
        $user_cek  = DB::table('m_user')->where('id', $user)->first();

        $krs = DB::table('sakramen_signature')
            ->select('m_sakramen_krisma.*','sakramen_signature.id as id_signature','sakramen_signature.sequence')
            ->join('m_sakramen_krisma','m_sakramen_krisma.id','sakramen_signature.id_sakramen')
            ->where('sakramen_signature.type', 'krisma')
            ->where('sakramen_signature.user_id', $user)
            ->orderBy('sakramen_signature.id', 'desc');

        $krs = $krs;

        return Datatables::of($krs)
            ->addColumn('action', function ($krs) {

                $hasil = '<table id="tabel-in-opsi">' .
                    '<tr>' .
                    '<td>' .
                    '<a href="' . url('/admin/sakramen-signature/' .$krs->id.'/krisma') . '" class="btn btn-default btn-sm"><i class="fas fa-signature"></i></a>' .
                    '&nbsp'.
                    '<a href="' . url('/admin-sakramen-print/krisma/' . $krs->id . '/printout') . '" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-print"></i></a>'.
                    '</td>' .
                    '</tr>' .
                    '</table>';
                return $hasil;
            })
            ->editColumn('status', function ($krs) {
                return ucfirst($krs->status);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function tahapan($type)
    {
        if ($type == "nikah") {
            $header = "Pernikahan";
            $side = "pernikahan";
            $tahapan = DB::table('m_tahapan_detail')
                ->select('m_tahapan.*','m_agenda.nama_agenda as nama_diri','m_tahapan_detail.id_tahapan_detail','m_agenda.date_agenda')
                ->leftjoin('m_tahapan', 'm_tahapan.id_tahapan', 'm_tahapan_detail.id_tahapan')
                ->leftjoin('m_agenda', 'm_agenda.id_agenda', 'm_tahapan_detail.id_agenda')
                ->where('m_tahapan.type', $type)
                ->where('m_tahapan.status', 'approved')
                ->where('m_agenda.id_user', Auth::user()->id)
                ->get();
        } else {

            if ($type == "baptis_balita" || $type == "baptis_anak" || $type == "baptis_dewasa") {
                $tipe = explode("_", $type);
                $header = ucfirst($tipe[0]) . " " . ucfirst($tipe[1]);
                $side = $type;
                $tahapan = DB::table('m_tahapan_detail')
                    ->select('m_tahapan.nama_tahapan', 'm_periode.nama_periode', 'm_periode.start_date', 'm_periode.end_date', 'm_tahapan_detail.id_tahapan_detail','m_tahapan.tanggal_tahapan','m_tahapan.upload_file','m_sakramen_baptis.nama_diri')
                    ->leftjoin('m_tahapan', 'm_tahapan.id_tahapan', 'm_tahapan_detail.id_tahapan')
                    ->leftjoin('m_periode', 'm_periode.id_periode', 'm_tahapan.id_periode')
                    ->leftjoin('m_sakramen_baptis', 'm_sakramen_baptis.id', 'm_tahapan_detail.id_sakramen')
                    ->where('m_tahapan.type', $type)
                    ->where('m_sakramen_baptis.type', $tipe[1])
                    ->where('m_tahapan.status', 'approved')
                    ->where('m_sakramen_baptis.user_id', Auth::user()->id)
                    ->get();
            } else if ($type == "komuni") {
                $header = "Sakramen " . ucfirst($type);
                $side = $type;
                $tahapan = DB::table('m_tahapan_detail')
                    ->select('m_tahapan.nama_tahapan', 'm_periode.nama_periode', 'm_periode.start_date', 'm_periode.end_date', 'm_tahapan_detail.id_tahapan_detail','m_tahapan.tanggal_tahapan','m_tahapan.upload_file','m_sakramen_komuni.nama_diri')
                    ->leftjoin('m_tahapan', 'm_tahapan.id_tahapan', 'm_tahapan_detail.id_tahapan')
                    ->leftjoin('m_periode', 'm_periode.id_periode', 'm_tahapan.id_periode')
                    ->leftjoin('m_sakramen_komuni', 'm_sakramen_komuni.id', 'm_tahapan_detail.id_sakramen')
                    ->where('m_tahapan.type', $type)
                    ->where('m_tahapan.status', 'approved')
                    ->where('m_sakramen_komuni.user_id', Auth::user()->id)
                    ->get();
            } else if ($type == "krisma") {
                $header = "Sakramen " . ucfirst($type);
                $side = $type;
                $tahapan = DB::table('m_tahapan_detail')
                    ->select('m_tahapan.nama_tahapan', 'm_periode.nama_periode', 'm_periode.start_date', 'm_periode.end_date', 'm_tahapan_detail.id_tahapan_detail','m_tahapan.tanggal_tahapan','m_tahapan.upload_file','m_sakramen_krisma.nama_diri')
                    ->leftjoin('m_tahapan', 'm_tahapan.id_tahapan', 'm_tahapan_detail.id_tahapan')
                    ->leftjoin('m_periode', 'm_periode.id_periode', 'm_tahapan.id_periode')
                    ->leftjoin('m_sakramen_krisma', 'm_sakramen_krisma.id', 'm_tahapan_detail.id_sakramen')
                    ->where('m_tahapan.type', $type)
                    ->where('m_tahapan.status', 'approved')
                    ->where('m_sakramen_krisma.user_id', Auth::user()->id)
                    ->get();
            }
        }

        foreach ($tahapan as $key => $value) {
            $image_tahapan = DB::table('m_tahapan_image')->where('id_tahapan_detail', $value->id_tahapan_detail)->get();

            $value->image = $image_tahapan;
        }

        //dd($tahapan);

        return view('admin.sakramen.tahapan', compact('header', 'side', 'tahapan', 'type'));
    }

    public function updateImage(Request $request, $id, $type)
    {
        // dd($request->all());
        $pathImage = null;
        if ($type != "nikah") {
            if ($type == "baptis_balita" || $type == "baptis_anak" || $type == "baptis_dewasa") {
                $tahapan = DB::table('m_tahapan_detail')
                    ->join('m_tahapan', 'm_tahapan.id_tahapan', 'm_tahapan_detail.id_tahapan')
                    ->join('m_sakramen_baptis', 'm_sakramen_baptis.id', 'm_tahapan_detail.id_sakramen')
                    ->where('id_tahapan_detail', $id)
                    ->first();
            } else if ($type == "komuni") {
                $tahapan = DB::table('m_tahapan_detail')
                    ->join('m_tahapan', 'm_tahapan.id_tahapan', 'm_tahapan_detail.id_tahapan')
                    ->join('m_sakramen_komuni', 'm_sakramen_komuni.id', 'm_tahapan_detail.id_sakramen')
                    ->where('id_tahapan_detail', $id)
                    ->first();
            } else if ($type == "krisma") {
                $tahapan = DB::table('m_tahapan_detail')
                    ->join('m_tahapan', 'm_tahapan.id_tahapan', 'm_tahapan_detail.id_tahapan')
                    ->join('m_sakramen_krisma', 'm_sakramen_krisma.id', 'm_tahapan_detail.id_sakramen')
                    ->where('id_tahapan_detail', $id)
                    ->first();
            }
            $nama = $tahapan->nama_diri;
        } else {
            $tahapan = DB::table('m_tahapan_detail')
                ->join('m_tahapan', 'm_tahapan.id_tahapan', 'm_tahapan_detail.id_tahapan')
                ->join('m_agenda', 'm_agenda.id_agenda', 'm_tahapan_detail.id_agenda')
                ->where('id_tahapan_detail', $id)
                ->first();

            $nama = $tahapan->nama_agenda;
        }

        DB::beginTransaction();
        try {

            //editmode-delete
            if ($request->hasFile('image')) {
                $pathImage = date("ymdHis") . $tahapan->nama_tahapan . "_" . $nama . ".jpg";
                $destinationPath = 'upload/tahapan';
                $request->image->move($destinationPath, $pathImage);


            }
            DB::table('m_tahapan_image')
                ->insert([
                    'image_tahapan' => $pathImage,
                    'id_tahapan_detail' => $tahapan->id_tahapan_detail,
                    'created_at' => date("Y-m-d H:i:s"),
                ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
        return redirect()->back();
    }

    public function deleteImage($id)
    {
        DB::beginTransaction();
        try {

            //editmode-delete


            $image = DB::table('m_tahapan_image')
                ->where('id',$id)
                ->first();

            DB::table('m_tahapan_image')->where('id',$id)->delete();

            File::delete('upload/tahapan/' . $image->image_tahapan); //delete old images
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
        return redirect()->back();

    }

    public function sendEmail($skr, $type = null)
    {
        $muser = new MUserController();

        //dd($skr);

        if ($type == "sakramen") {
            $this->usersUpline =  $muser->getUpline($skr->id_kategori_user, $type);
            $jenis = ucfirst($skr->type);

            $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
                    karena ada permohonan pengajuan Sakramen Baptis $jenis dari $skr->createuser a/n $skr->nama_diri";

            $this->signature($skr->id, 'baptis_' . $skr->type);

            $text1 = "Terima kasih telah melakukan pendaftaran Sakramen. Silahkan menunggu hingga status Tanda Tanda lengkap. Jika tanda tangan sudah lengkap, silahkan melakukan pengecekan secara berkala pada tahapan Sakramen atau email Anda. Terima kasih.";


            Mail::to($skr->email)->send(new ApproveProposal($text1,"Pendaftaran Sakramen Baptis ".$jenis));

        } else if ($type == "nikah") {

            $this->usersUpline =  $muser->getUpline($skr['id_kategori_user'], $type);
            $nama = $skr['judul'];
            $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
                    karena ada permohonan pengajuan Sakramen Pernikahan a/n $nama ";

            $this->signature($skr['id'], $type, $skr['section']);


        } else if ($type == "komuni") {

            $komuni = DB::table('m_sakramen_komuni')->where('id',$skr)->first();

            $this->usersUpline =  $muser->getUpline($komuni->id_kategori_user, $type);
            $jenis = ucfirst($type);

            $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
                    karena ada permohonan pengajuan Sakramen Komuni dari $komuni->createuser a/n $komuni->nama_diri";

            $this->signature($komuni->id, $type);

            $text1 = "Terima kasih telah melakukan pendaftaran Sakramen. Silahkan menunggu hingga status Tanda Tanda lengkap. Jika tanda tangan sudah lengkap, silahkan melakukan pengecekan secara berkala pada tahapan Sakramen atau email Anda. Terima kasih.";


            Mail::to($komuni->email)->send(new ApproveProposal($text1,"Pendaftaran Sakramen Komuni "));
        } else if ($type == "krisma") {

            $krisma = DB::table('m_sakramen_krisma')->where('id',$skr)->first();

            $this->usersUpline =  $muser->getUpline($krisma->id_kategori_user, $type);
            $jenis = ucfirst($type);

            $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
                    karena ada permohonan pengajuan Sakramen Krisma dari $krisma->createuser a/n $krisma->nama_diri";

            $this->signature($krisma->id, $type);

            $text1 = "Terima kasih telah melakukan pendaftaran Sakramen. Silahkan menunggu hingga status Tanda Tanda lengkap. Jika tanda tangan sudah lengkap, silahkan melakukan pengecekan secara berkala pada tahapan Sakramen atau email Anda. Terima kasih.";


            Mail::to($krisma->email)->send(new ApproveProposal($text1,"Pendaftaran Sakramen Krisma "));
        }else if($type == 'email_nikah'){

            $text1 = "Terima kasih telah melakukan pendaftaran Sakramen. Silahkan menunggu hingga status Tanda Tanda lengkap. Jika tanda tangan sudah lengkap, silahkan melakukan pengecekan secara berkala pada tahapan Sakramen atau email Anda. Terima kasih.";
            Mail::to($skr->email)->send(new ApproveProposal($text1,"Pendaftaran Sakramen Nikah "));
        }

        //dd($this->usersUpline);
        if($type != 'email_nikah'){
            foreach ($this->usersUpline as $upline) {
                Mail::to($upline['email'])->send(new ApproveProposal($text,"Reminder Sakramen"));
            }
        }
    }

    public function signature($id, $type = null, $section = null)
    {
        $signatures = [];
        if ($type == "baptis_balita" || $type == "baptis_anak" || $type == "baptis_dewasa") {
            foreach ($this->usersUpline as $key => $us) {
                $signatures[] = [
                    'sequence' => ($key + 1),
                    'id_sakramen' => $id,
                    'type' => $type,
                    'user_id' => $us['id'],
                    'username' => $us['name'],
                    'signature_path' => null,
                    'created_at' => now(),
                ];
            }
        } else if ($type == "krisma" || $type == "komuni") {
            foreach ($this->usersUpline as $key => $us) {
                $signatures[] = [
                    'sequence' => ($key + 1),
                    'id_sakramen' => $id,
                    'type' => $type,
                    'user_id' => $us['id'],
                    'username' => $us['name'],
                    'signature_path' => null,
                    'created_at' => now(),
                ];
            }
        } else if ($type == "nikah") {
            foreach ($this->usersUpline as $key => $us) {
                $signatures[] = [
                    'sequence' => ($key + 1),
                    'id_agenda' => $id,
                    'type' => "nikah",
                    'section' => $section,
                    'user_id' => $us['id'],
                    'username' => $us['name'],
                    'signature_path' => null,
                    'created_at' => now(),
                ];
            }
        }

        DB::table('sakramen_signature')->insert($signatures);
    }

    public function addSignature($id, $type = null, $type_baptis = null)
    {
        if($type == "nikah"){
            $signature = DB::table('sakramen_signature')
                    ->where('id_agenda',$id)
                    ->where('section',$type_baptis)
                    ->where('user_id',Auth::user()->id)
                    ->where('status','pending')->first();

            if(!empty($signature)){

                if($signature->sequence == 1 ){
                    $options = [
                        'permission' => true,
                        'message' => null,
                    ];
                }
                else{
                    $cek = DB::table('sakramen_signature')->where('id_agenda',$id)
                            ->where('sequence',($signature->sequence - 1))
                            ->where('section',$type_baptis)
                            ->where('status','complete')->first();
                    if(!empty($cek)){
                        $options = [
                            'permission' => true,
                            'message' => null,
                        ];
                    }else{
                        $options = [
                            'permission' => false,
                            'message' => "User sebelum anda belum menyetujui",
                        ];
                    }
                }
            }else{
                $options = [
                    'permission' => false,
                    'message' => "TIdak punya akses",
                ];
            }
        }else{
            $signature = DB::table('sakramen_signature')
                    ->where('id_sakramen',$id)
                    ->where('user_id',Auth::user()->id)
                    ->where('status','pending')->first();

            if(!empty($signature)){

                if($signature->sequence == 1 ){
                    $options = [
                        'permission' => true,
                        'message' => null,
                    ];
                }
                else{
                    $cek = DB::table('sakramen_signature')->where('id_sakramen',$id)
                            ->where('sequence',($signature->sequence - 1))
                            ->where('status','complete')->first();
                    if(!empty($cek)){
                        $options = [
                            'permission' => true,
                            'message' => null,
                        ];
                    }else{
                        $options = [
                            'permission' => false,
                            'message' => "User sebelum anda belum menyetujui",
                        ];
                    }
                }
            }else{
                $options = [
                    'permission' => false,
                    'message' => "TIdak punya akses",
                ];
            }
        }

        $user = (!empty($signature)) ? $signature->user_id : '';
        //dd($signature);

        // dd($signature,($signature->sequence - 1));
        if($type_baptis == null){
            return view('admin/sakramen/'.$type.'/create_ttd', ['id' => $id, 'options' => $options, 'user' => $user, 'type' => $type, 'type_baptis' => $type_baptis ]);
        }else{
            if($type != "nikah"){
                return view('admin/sakramen/'.$type.'/create_ttd_'.$type_baptis, ['id' => $id, 'options' => $options, 'user' => $user, 'type' => $type, 'type_baptis' => $type_baptis ]);
            }else{
                return view('admin/sakramen/'.$type.'/create_ttd', ['id' => $id, 'options' => $options, 'user' => $user, 'type' => $type, 'type_baptis' => $type_baptis ]);
            }
        }
    }

    public function addSignaturePost(Request $request)
    {
        $imageFile = Image::make($request->signature);
        $imageFile->resize(500, 500);

        if (!\File::isDirectory(public_path('upload/signature'))) {
            \File::makeDirectory(public_path('upload/signature'), 493, true);
        }

        $path = "upload/signature/signature_".$request->type. "_" . Carbon::now()->format('his') . ".png";

        $save = $imageFile->save(public_path($path));

        $request->merge([
            'signature_path' => $path,
            'status' => ($path != null) ? "complete" : 'pending',
        ]);

        if($request->type == "nikah"){
            DB::table('sakramen_signature')
                    ->where('user_id',$request->user_id)
                    ->where('id_agenda',$request->id)
                    ->where('section',$request->type_baptis)
                    ->update($request->except('_token', 'signature','type','type_baptis','user_id','id_agenda','id'));

            $cek_status_all = DB::table('sakramen_signature')
                            ->where('id_agenda',$request->id)
                            ->where('status','pending')
                            ->get();

            if(count($cek_status_all) == 0){
                $cek_status = DB::table('sakramen_signature')
                                ->where('id_agenda',$request->id)
                                ->get();
                DB::table('m_agenda')->where('id_agenda',$cek_status[0]->id_agenda)
                ->update([
                    "status_ttd" => 'complete'
                ]);
            }

            $url = url("admin/sakramen_ttd/nikah");
        }else{
            DB::table('sakramen_signature')->where('user_id',$request->user_id)->where('id_sakramen',$request->id)
                ->update($request->except('_token', 'signature','type','type_baptis','id','user_id'));

            $cek_status_all = DB::table('sakramen_signature')->where('id_sakramen',$request->id)->where('status','pending')->get();

            if(count($cek_status_all) == 0){
                if($request->type == "baptis"){
                    $cek_status = DB::table('sakramen_signature')->whereNotIn('type',['komuni','krisma','nikah'])->where('id_sakramen',$request->id)->get();
                    DB::table('m_sakramen_baptis')->where('id',$cek_status[0]->id_sakramen)
                    ->update([
                        "status_ttd" => 'complete'
                    ]);
                    $url = url("admin/sakramen_ttd/baptis/".$request->type_baptis);
                }else if($request->type == "komuni"){
                    $cek_status = DB::table('sakramen_signature')->where('type','komuni')->where('id_sakramen',$request->id)->get();
                    DB::table('m_sakramen_komuni')->where('id',$cek_status[0]->id_sakramen)
                    ->update([
                        "status_ttd" => 'complete'
                    ]);
                    $url = url("admin/sakramen_ttd/komuni/");
                }else if($request->type == "krisma"){
                    $cek_status = DB::table('sakramen_signature')->where('type','krisma')->where('id_sakramen',$request->id)->get();
                    DB::table('m_sakramen_krisma')->where('id',$cek_status[0]->id_sakramen)
                    ->update([
                        "status_ttd" => 'complete'
                    ]);
                    $url = url("admin/sakramen_ttd/krisma/");
                }
            }
        }

        // return response()->json(['success' => true, 'url' => route('sakramenttd.index', [$request->type,Auth::user()->id, $request->type_baptis])]);
        return response()->json(['success' => true, 'url' => $url ]);
    }

    public function addSignatureUmmat($id, $type = null, $type_baptis = null)
    {
        if($type == "nikah"){
            $ck = DB::table('m_agenda')->where('id_agenda',$id)
                    ->whereNull('signature_pengantin_pria')
                    ->whereNull('signature_pengantin_wanita')
                    ->first();
            (!empty($ck) ) ?
                $options = [ 'permission' => true, 'message' => null]
                : $options = [ 'permission' => false, 'message' => null];

            return view('admin/sakramen/nikah/ttd-ummat',compact('options','id'));

        }elseif($type == "baptis"){
            if($type_baptis == "anak"){
                $ck = DB::table('m_sakramen_baptis')->where('id',$id)
                        ->whereNull('signature_anak')
                        ->whereNull('signature_orang_tua')
                        ->first();
            }elseif($type_baptis == "dewasa"){
                $ck = DB::table('m_sakramen_baptis')->where('id',$id)
                        ->whereNull('signature_dewasa')
                        ->first();
            }
            (!empty($ck) ) ?
                $options = [ 'permission' => true, 'message' => null]
                : $options = [ 'permission' => false, 'message' => null];

            return view('admin/sakramen/baptis/'.$type_baptis.'-ttd-umat',compact('options','id'));
        }elseif($type == "komuni"){
            $ck = DB::table('m_sakramen_komuni')->where('id',$id)
                    ->whereNull('signature_ayah')
                    ->whereNull('signature_ibu')
                    ->first();

            (!empty($ck) ) ?
                $options = [ 'permission' => true, 'message' => null]
                : $options = [ 'permission' => false, 'message' => null];

                return view('admin/sakramen/komuni/ttd-ummat',compact('options','id'));
        }elseif($type == "krisma"){
            $ck = DB::table('m_sakramen_krisma')->where('id',$id)
            ->whereNull('signature_umat')
            ->first();

            (!empty($ck) ) ?
                $options = [ 'permission' => true, 'message' => null]
                : $options = [ 'permission' => false, 'message' => null];

                return view('admin/sakramen/krisma/ttd-ummat',compact('options','id'));
        }
    }

    public function ummatSignature(Request $req)
    {
        // return $req->all();

        if($req->type == 'nikah'){
            $spp = Image::make($req->signature_pria);
            $spp->resize(500, 500);
            $pathsPp = "upload/signature/signature_".$req->type. "_ummatpria_" . Carbon::now()->format('his') . ".png";
            $spp->save(public_path($pathsPp));

            $spw = Image::make($req->signature_wanita);
            $spw->resize(500, 500);
            $pathsPw = "upload/signature/signature_".$req->type. "_ummatwanita_" . Carbon::now()->format('his') . ".png";
            $spw->save(public_path($pathsPw));

            DB::table('m_agenda')->where('id_agenda',$req->id)->update([
                'signature_pengantin_pria' => $pathsPp,
                'signature_pengantin_wanita' => $pathsPw,
            ]);

            return response()->json(['success' => true, 'url' => url('admin/sakramen/nikah')]);
        }elseif($req->type == 'anak'){
            $spp = Image::make($req->anak);
            $spp->resize(500, 500);
            $pathsAnak = "upload/signature/signature_".$req->type. "_ummatanak_" . Carbon::now()->format('his') . ".png";
            $spp->save(public_path($pathsAnak));

            $spw = Image::make($req->orangtua);
            $spw->resize(500, 500);
            $pathsOrtu = "upload/signature/signature_".$req->type. "_ummatortu_" . Carbon::now()->format('his') . ".png";
            $spw->save(public_path($pathsOrtu));

            DB::table('m_sakramen_baptis')
                ->where('id',$req->id)->update([
                'signature_anak' => $pathsAnak,
                'signature_orang_tua' => $pathsOrtu,
            ]);

            return response()->json(['success' => true, 'url' => url('admin/sakramen/baptis/anak')]);
        }elseif($req->type == 'dewasa'){
            $spp = Image::make($req->umat);
            $spp->resize(500, 500);
            $pathsUmat = "upload/signature/signature_".$req->type. "_ummatdewasa_" . Carbon::now()->format('his') . ".png";
            $spp->save(public_path($pathsUmat));

            DB::table('m_sakramen_baptis')->where('id',$req->id)->update([
                'signature_dewasa' => $pathsUmat,
            ]);

            return response()->json(['success' => true, 'url' => url('admin/sakramen/baptis/dewasa')]);
        }elseif($req->type == 'komuni'){
            $spp = Image::make($req->sigayah);
            $spp->resize(500, 500);
            $sigayah = "upload/signature/signature_".$req->type. "_ummatkomuni_ayah_" . Carbon::now()->format('his') . ".png";
            $spp->save(public_path($sigayah));

            $spw = Image::make($req->sigibu);
            $spw->resize(500, 500);
            $sigibu = "upload/signature/signature_".$req->type. "_ummatkomuni_ibu_" . Carbon::now()->format('his') . ".png";
            $spw->save(public_path($sigibu));

            DB::table('m_sakramen_komuni')->where('id',$req->id)->update([
                'signature_ayah' => $sigayah,
                'signature_ibu' => $sigibu,
            ]);

            return response()->json(['success' => true, 'url' => url('admin/sakramen/komuni')]);
        }elseif($req->type == 'krisma'){

            $sig = Image::make($req->signature);
            $sig->resize(500, 500);
            $signature = "upload/signature/signature_".$req->type. "_ummatkrisma_" . Carbon::now()->format('his') . ".png";
            $sig->save(public_path($signature));

            DB::table('m_sakramen_krisma')->where('id',$req->id)->update([
                'signature_umat' => $signature,
            ]);

            return response()->json(['success' => true, 'url' => url('admin/sakramen/krisma')]);
        }

    }

    public function trx(){
        $cek_baptis = DB::table('m_sakramen_baptis')
                        ->select('m_sakramen_baptis.*','m_sakramen_transaction.price','m_sakramen_transaction.status')
                        ->join('m_sakramen_transaction','m_sakramen_transaction.sakramen_id','m_sakramen_baptis.id')
                        ->where('m_sakramen_transaction.sakramen_type','!=','pernikahan')
                        ->where('m_sakramen_baptis.user_id',Auth::user()->id)
                        ->where('m_sakramen_baptis.status', "approved")
                        ->get();

        $cek_nikah  = DB::table('m_agenda')
                        ->select('m_agenda.*','m_sakramen_transaction.price','m_sakramen_transaction.status')
                        ->join('m_sakramen_transaction','m_sakramen_transaction.sakramen_id','m_agenda.id_agenda')
                        ->where('m_sakramen_transaction.sakramen_type','pernikahan')
                        ->where('m_agenda.id_user',Auth::user()->id)
                        ->where('m_agenda.status', "approved")
                        ->get();

        return view('admin.sakramen.transaksi', compact('cek_baptis','cek_nikah'));
    }

    public function ttdUmat(){
        return view('admin.sakramen.nikah.create_ttd_umat');
    }

    public function download($type, $id){

        if($type == "pernikahan"){

            $sakramen = DB::table('m_agenda')
                        ->where('m_agenda.id_agenda',$id)
                        ->first();


            $signature = DB::table('sakramen_signature')
                            ->orderBy('sakramen_signature.sequence','asc')
                            ->where('sakramen_signature.id_agenda',$sakramen->id_agenda)
                            ->get();
            $sakramen->signature = $signature;
            $type = ucfirst($type);

        }else {
            if($type == "dewasa" || $type == "anak" || $type == "balita"){
                $sakramen = DB::table('m_sakramen_baptis')
                        ->select('m_sakramen_baptis.*','m_kategori_user.nama_kategori_user')
                        ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_sakramen_baptis.id_kategori_user')
                        ->where('m_sakramen_baptis.id',$id)
                        ->first();


                $signature = DB::table('sakramen_signature')
                                ->orderBy('sakramen_signature.sequence','asc')
                                ->where('sakramen_signature.id_sakramen',$sakramen->id)
                                ->get();
                $sakramen->signature = $signature;

                $type = "Baptis ".ucfirst($type);

            }elseif($type == "komuni"){

                $sakramen = DB::table('m_sakramen_komuni')
                        ->select('m_sakramen_komuni.*','m_kategori_user.nama_kategori_user')
                        ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_sakramen_komuni.id_kategori_user')
                        ->where('m_sakramen_komuni.id',$id)
                        ->first();


                $signature = DB::table('sakramen_signature')
                                ->orderBy('sakramen_signature.sequence','asc')
                                ->where('sakramen_signature.id_sakramen',$sakramen->id)
                                ->get();
                $sakramen->signature = $signature;
                $type = ucfirst($type);

            }elseif ($type == "krisma") {

                $sakramen = DB::table('m_sakramen_krisma')
                        ->select('m_sakramen_krisma.*','m_kategori_user.nama_kategori_user')
                        ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_sakramen_krisma.id_kategori_user')
                        ->where('m_sakramen_krisma.id',$id)
                        ->first();


                $signature = DB::table('sakramen_signature')
                                ->orderBy('sakramen_signature.sequence','asc')
                                ->where('sakramen_signature.id_sakramen',$sakramen->id)
                                ->get();
                $sakramen->signature = $signature;
                $type = ucfirst($type);

            }
        }
        //dd($sakramen);
        if($type == "Pernikahan"){
            return PDF::loadView('admin.sakramen.printout', ['sakramen' => $sakramen,'type' => $type])
                        ->setPaper("A4","landscape")->stream();

        }else{
             return PDF::loadView('admin.sakramen.printout', ['sakramen' => $sakramen,'type' => $type])->stream();
        }
        //dd($sakramen);
    }

    public function wizardForm(Request $req,$step)
    {
        if(!in_array($step,[1,2,3,4,5,6,7])) abort(404);

        $pernikahan = $req->session()->get('pernikahan');

        if($req->isMethod('GET')){

            $lingkungan = \DB::table('m_kategori_user')->where('id_bidang', 10)->where('type', 'lingkungan')->where('status', 'active')->get();

            $editmode = ($req->session()->get('pernikahan') != null) ? true : false;

            return view('admin/sakramen/nikah/wizard',['lingkungan' => $lingkungan,'pernikahan' => $pernikahan,'step' => $step,'editmode' => $editmode]);
        }elseif($req->isMethod('POST')){
            // dd($req->all(),$step,$pernikahan);

            if($step == 1){
                    //validate-agenda

                    //do check user is created pernikahan

                    //do check agenda exist or not
                    $count = 0;
                    $awal = date("H:i:s", strtotime($req->waktu_agenda));

                    if($req->waktu_agenda == '10:00'){
                        $akhir = '11:30:00';
                    }else if($req->waktu_agenda == '12:00'){
                        $akhir = '13:30:00';
                    }else if($req->waktu_agenda == '14:00'){
                        $akhir = '15:30:00';
                    }

                    if ($req->tempat_menikah != "on") {

                        $agenda = AgendaModel::where('type_agenda', 'pernikahan')
                            ->where('date_agenda', date("Y-m-d", strtotime($req->date_agenda)))
                            ->where('tempat_menikah', 1)
                            ->get();

                        foreach ($agenda as $key => $value) {
                            if ($value->waktu_agenda > $awal && $value->end_agenda <= $akhir) {
                                $count = $count + 1;
                            } else if ($value->waktu_agenda <= $awal && $value->end_agenda >= $akhir) {
                                $count = $count + 1;
                            } else if ($value->waktu_agenda <= $awal && $value->end_agenda > $akhir) {
                                $count = $count + 1;
                            }
                        }

                        if ($count > 0) {
                            return redirect()->back()->with('message', 'Waktu/Tempat bertabrakan dengan jadwal agenda lain');
                        }
                    }

                    $req->merge([
                        'type_agenda' => 'pernikahan',
                        'date_agenda' => ($req->date_agenda != null) ? date("Y-m-d", strtotime($req->date_agenda)) : null,
                        "waktu_agenda" => $awal,
                        "end_agenda" => $akhir,
                        "id_user" => Auth::user()->id,
                        "alternative_tempat_menikah" => ($req->tempat_menikah == 'on') ? $req->alternative_tempat_menikah : '',
                        "tempat_menikah" => ($req->tempat_menikah == 'on') ? 2 : 1,
                        'hari_libur' => ($req->hari_libur == 'on') ? 1 : 0,
                        'status' => 'in process',
                        'image_mempelai' => "default_foto_pasangan.jpg",
                    ]);

                //save and put to session
                if(empty($pernikahan)){

                    $pernik = new AgendaModel;
                    $pernik->fill($req->except('_token'));
                    $pernik->save();

                    $pernikahan = AgendaModel::where('id_agenda',$pernik->id_agenda)->first();
                }else{
                    $pernikahan->update($req->except('_token'));
                }

            }else if(in_array($step,[2,3,4,5,6,7])){
                // dd($req->all(),$step,$pernikahan);

                if($step == 2){
                    $req->merge([
                        'tanggal_lahir_pengantin_pria' => ($req->tanggal_lahir_pengantin_pria != null) ? date("Y-m-d", strtotime($req->tanggal_lahir_pengantin_pria)) : null,
                        'tanggal_lahir_pengantin_wanita' => ($req->tanggal_lahir_pengantin_wanita != null) ? date("Y-m-d", strtotime($req->tanggal_lahir_pengantin_wanita)) : null,
                    ]);
                }

                //file-condition
                if ($req->hasFile('image_pengantin_pria')) {

                    $pathPP = date("ymdHis").'image_pengantin_pria_' . $req->nama_pengantin_pria . ".jpg";
                    $destinationPathPP = 'upload/wedding';
                    $req->image_pengantin_pria->move($destinationPathPP, $pathPP);
                    $namePathPP = $destinationPathPP.'/'.$pathPP;

                    $req->merge([
                        'image_nikah_pengantin_pria' => $namePathPP,
                    ]);

                    if ($req->image_nikah_pengantin_pria != null)
                        File::delete($pernikahan->image_nikah_pengantin_pria); //delete old images
                }

                if ($req->hasFile('image_pengantin_wanita')) {
                    $pathPW = date("ymdHis") .'image_pengantin_wanita_' . $req->nama_pengantin_wanita . ".jpg";
                    $destinationPathPW = 'upload/wedding';
                    $req->image_pengantin_wanita->move($destinationPathPW, $pathPW);
                    $namePathPW = $destinationPathPW.'/'.$pathPW;

                    $req->merge([
                        'image_nikah_pengantin_wanita' => $namePathPW,
                    ]);

                    if ($req->image_nikah_pengantin_wanita != null)
                        File::delete($pernikahan->image_nikah_pengantin_wanita); //delete old images
                }

                if($req->agama_pengantin_pria == "Katolik"){
                    if ($req->hasFile('image2')) {
                        $pathSuratBaptisPria = date("ymdHis") ."suratbaptispria_". $req->nama_pengantin_pria . ".jpg";
                        $destinationPathSuratBaptisPria = 'upload/wedding';
                        $req->image2->move($destinationPathSuratBaptisPria, $pathSuratBaptisPria);
                        $namePathSuratBaptisPria = $destinationPathSuratBaptisPria.'/'.$pathSuratBaptisPria;

                        $req->merge([
                            'surat_baptis_pengantin_pria' => $namePathSuratBaptisPria,
                        ]);

                        if ($req->surat_baptis_pengantin_pria != null)
                            File::delete($pernikahan->surat_baptis_pengantin_pria); //delete old images
                    }
                }

                if($req->agama_pengantin_wanita == "Katolik"){
                    if ($req->hasFile('image3')) {
                        $pathSuratBaptisWanita = date("ymdHis") ."suratbaptiswanita_". $req->nama_pengantin_wanita . ".jpg";
                        $destinationPathSuratBaptisWanita = 'upload/wedding';
                        $req->image3->move($destinationPathSuratBaptisWanita, $pathSuratBaptisWanita);
                        $namePathSuratBaptisWanita = $destinationPathSuratBaptisWanita.'/'.$pathSuratBaptisWanita;

                        $req->merge([
                            'surat_baptis_pengantin_wanita' => $namePathSuratBaptisWanita,
                        ]);

                        if ($req->surat_baptis_pengantin_wanita != null)
                            File::delete($pernikahan->surat_baptis_pengantin_wanita); //delete old images
                    }
                }

                //global-merge
                $req->merge([
                    'step_view' => $step
                ]);

                $pernikahan->update($req->except([
                    'type',
                    'image_pengantin_pria',
                    'image_pengantin_wanita',
                    'image2',
                    'image3',
                ]));
            }

            // dd('out',$req->all(),$pernikahan,$step);

            $req->session()->put('pernikahan', $pernikahan);

            if($step == 7){
                $req->session()->forget('pernikahan');
                return redirect('admin/sakramen/nikah');
            }else{
                return redirect('sakramen-nikah/wizard/'.($step+1));
            }
        }
    }
}
