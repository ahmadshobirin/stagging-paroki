<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;
use Mail;
use App\Mail\ApproveProposal;

class MTahapanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahapan = DB::table('m_tahapan')
                    ->select('m_tahapan.*','m_periode.*','m_tahapan.status as status_tahapan','m_agenda.date_agenda')
                    ->leftjoin('m_periode','m_periode.id_periode','m_tahapan.id_periode')
                    ->leftjoin('m_agenda','m_agenda.id_agenda','m_tahapan.id_agenda')
                    ->where('m_tahapan.status','!=','deleted')
                    ->orderBy('m_tahapan.id_tahapan','DESC')
                    ->get();

        //dd($tahapan);

        return view('admin.master.tahapan.index', compact('tahapan'));
    }

     public function getPeriode($type)
    {
        $table = null;

        switch ($type) {
            case 'baptis_anak':
            case 'baptis_dewasa':
            case 'baptis_balita':
                $table = 'm_sakramen_baptis';
                break;
            case 'komuni':
                 $table = 'm_sakramen_komuni';
                break;
            case 'krisma':
                 $table = 'm_sakramen_krisma';
                break;
            default:
                # code...
                break;
        }

        if($type == 'nikah'){

            $periode = DB::table('m_agenda')
                    ->where('type_agenda', 'pernikahan')
                    ->where('status_ttd', 'complete')
                    ->where('id_user','!=', 0)
                    ->select('id_agenda as id',DB::raw("CONCAT(nama_agenda,' ', DATE_FORMAT(date_agenda,'%d %M %Y')) as periode_tampil"))
                    ->get();

            return Response::json($periode);
        }

        $periode = DB::table('m_periode')
                ->where('type_periode', $type)
                ->whereIn('id_periode',function($q) use($table){
                    $q->select('id_periode')->where('status_ttd','=','complete')->from($table);
                })
                ->where('status_periode', 'active')
                ->select('m_periode.id_periode as id',DB::raw("CONCAT(nama_periode,' ', DATE_FORMAT(start_date,'%d %M %Y'), ' s/d ', DATE_FORMAT(end_date,'%d %M %Y')) as periode_tampil"))
                ->get();

                // ->where('id', function($z) {
                //         $z->select(DB::raw('count(1) as status_ttd'))->where('status','pending')->from('sakramen_signature');

        return Response::json($periode);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.master.tahapan.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $this->validate($request,[
            'nama_tahapan' => 'required|max:100',
            'type' => 'required',
        ]);

        DB::beginTransaction();
        try{
            if($request->type != "nikah"){
                DB::table('m_tahapan')->insert([
                    'nama_tahapan' => $request->nama_tahapan,
                    'type' => $request->type,
                    'id_periode' => $request->id_periode,
                    'created_at' => date('Y-m-d H:i:s'),
                    'tanggal_tahapan' => date('Y-m-d', strtotime($request->tanggal_tahapan)),
                    'status' => "in process",
                    'upload_file' => $request->upload_file,
                ]);
            }else{
                DB::table('m_tahapan')->insert([
                    'nama_tahapan' => $request->nama_tahapan,
                    'type' => $request->type,
                    'id_agenda' => $request->id_periode,
                    'created_at' => date('Y-m-d H:i:s'),
                    'tanggal_tahapan' => date('Y-m-d', strtotime($request->tanggal_tahapan)),
                    'status' => "in process",
                    'upload_file' => $request->upload_file,
                ]);
            }

            DB::commit();
            return redirect('admin/tahapan')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editmode = true;
        $tahapan = DB::table('m_tahapan')->where('id_tahapan',$id)->first();

        if($tahapan->type == 'nikah'){

            $periode = DB::table('m_agenda')
                    ->where('id_agenda', $tahapan->id_agenda)
                    ->select('id_agenda as id',DB::raw("CONCAT(nama_agenda,' ', DATE_FORMAT(date,'%d %M %Y')) as periode_tampil"))
                    ->get();
        }else{

            switch ($tahapan->type) {
                case 'baptis_anak':
                case 'baptis_dewasa':
                case 'baptis_balita':
                    $table = 'm_sakramen_baptis';
                    break;
                case 'komuni':
                     $table = 'm_sakramen_komuni';
                    break;
                case 'krisma':
                     $table = 'm_sakramen_krisma';
                    break;
                default:
                    # code...
                    break;
            }

            $periode = DB::table('m_periode')
                    ->where('type_periode', $tahapan->type )
                    ->whereIn('id_periode',function($q) use($table){
                        $q->select('id_periode')->where('status_ttd','=','complete')->from($table);
                    })
                    ->where('status_periode', 'active')
                    ->select('m_periode.id_periode as id',DB::raw("CONCAT(nama_periode,' ', DATE_FORMAT(start_date,'%d %M %Y'), ' s/d ', DATE_FORMAT(end_date,'%d %M %Y')) as periode_tampil"))
                    ->get();
        }

        return view('admin.master.tahapan.create',compact('tahapan','periode','editmode','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_tahapan' => 'required|max:100',
        ]);

        DB::beginTransaction();
        try{
            $tahapan = DB::table('m_tahapan')->where('id_tahapan',$id)->first();

            // dd($request->all(),$tahapan);

            if($tahapan->type == 'nikah')
                DB::table('m_tahapan')->where('id_tahapan',$id)->update([
                    'nama_tahapan' => $request->nama_tahapan,
                    'type' => $request->type,
                    'id_agenda' => $request->id_periode,
                    'id_periode' => 0,
                    'tanggal_tahapan' => date('Y-m-d', strtotime($request->tanggal_tahapan)),
                    'upload_file' => $request->upload_file,
                ]);
            else{
                DB::table('m_tahapan')->where('id_tahapan',$id)->update([
                    'nama_tahapan' => $request->nama_tahapan,
                    'type' => $request->type,
                    'id_agenda' => 0,
                    'id_periode' => $request->id_periode,
                    'tanggal_tahapan' => date('Y-m-d', strtotime($request->tanggal_tahapan)),
                    'upload_file' => $request->upload_file,
                ]);
            }

            DB::commit();
            return redirect('admin/tahapan')->with('message-success', 'Data Berhasil Ditambah');
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }
    }

    public function approve($id)
    {
        DB::beginTransaction();
        try{

            $tahapan = DB::table('m_tahapan')
                            ->where('id_tahapan',$id)
                            ->update([
                                'status' => "approved",
                            ]);
            $cek_tahapan = DB::table('m_tahapan')
                            ->where('id_tahapan',$id)
                            ->first();

            if($cek_tahapan->id_periode != 0){
                if($cek_tahapan->type == "baptis_balita" || $cek_tahapan->type == "baptis_anak" || $cek_tahapan->type == "baptis_dewasa"){
                    $cek_sakramen = DB::table('m_sakramen_baptis')
                                        ->where('id_periode',$cek_tahapan->id_periode)
                                        ->where('status_ttd', "complete")
                                        ->get();
                }else if($cek_tahapan->type == "komuni"){
                    $cek_sakramen = DB::table('m_sakramen_komuni')
                                        ->where('id_periode',$cek_tahapan->id_periode)
                                        ->where('status_ttd', "complete")
                                        ->get();
                }else if($cek_tahapan->type == "krisma"){
                    $cek_sakramen = DB::table('m_sakramen_krisma')
                                        ->where('id_periode',$cek_tahapan->id_periode)
                                        ->where('status_ttd', "complete")
                                        ->get();
                }

                //dd($cek_sakramen);

                foreach ($cek_sakramen as $key => $value) {

                    DB::table('m_tahapan_detail')
                        ->insert([
                        "id_tahapan" => $cek_tahapan->id_tahapan,
                        "id_sakramen" => $value->id,
                        "created_at" => date('Y-m-d H:i:s')
                        ]);

                    $date = date('d M Y', strtotime($cek_tahapan->tanggal_tahapan));

                    $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
                    karena ada permohonan Tahapan $cek_tahapan->nama_tahapan pada tanggal $date";
                    Mail::to($value->email)->send(new ApproveProposal($text,"Tahapan Sakramen"));


                }
            }else{


                $cek_sakramen = DB::table('m_agenda')
                                ->where('id_agenda',$cek_tahapan->id_agenda)
                                ->where('status_ttd', "complete")
                                ->first();
                
                //$user = DB::table('m_user')->where('id',$cek_sakramen->id_user)->first();


                DB::table('m_tahapan_detail')
                    ->insert([
                    "id_tahapan" => $cek_tahapan->id_tahapan,
                    "id_agenda" => $cek_tahapan->id_agenda,
                    "created_at" => date('Y-m-d H:i:s')
                    ]);

                $date = date('d M Y', strtotime($cek_tahapan->tanggal_tahapan));

                $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
                    karena ada permohonan Tahapan $cek_tahapan->nama_tahapan pada tanggal $date";
                    Mail::to($cek_sakramen->email)->send(new ApproveProposal($text,"Tahapan Sakramen"));

            }
            DB::commit();
            return redirect()->back()->with('message-success', 'Berhasil Disetujui');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message', 'Data Gagal Disetujui');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{

            $delete = DB::table('m_tahapan')
                            ->where('id_tahapan',$id)
                            ->update([
                                'status' => "deleted",
                            ]);
            DB::commit();

            return redirect()->back()->with('message-success', 'Berhasil Dihapus');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message', 'Data Gagal Dihapus');
        }
    }

    public function showUpload($type, $id){
        $judul = DB::table('m_tahapan')->where('id_tahapan',$id)->first();

        if($type == "nikah"){

            $cek_upload = DB::table('m_tahapan_detail')
                        ->select("m_agenda.nama_agenda",'m_tahapan_detail.status','m_tahapan_detail.id_tahapan_detail')
                        ->where('m_tahapan_detail.id_tahapan',$id)
                        ->join('m_agenda','m_agenda.id_agenda','m_tahapan_detail.id_agenda')
                        ->join('m_tahapan_image','m_tahapan_image.id_tahapan_detail','m_tahapan_detail.id_tahapan_detail')
                        ->get();

            foreach ($cek_upload as $key => $value) {
                $image_tahapan = DB::table('m_tahapan_image')
                                ->where('id_tahapan_detail',$value->id_tahapan_detail)
                                ->get();

                $value->image = $image_tahapan;
            }

        }else{
            if($type == "baptis_dewasa" || $type == "baptis_anak" || $type == "baptis_balita"){
                $table = "m_sakramen_baptis";
            }elseif($type == "komuni"){
                $table = "m_sakramen_komuni";
            }elseif($type == "krisma"){
                $table = "m_sakramen_krisma";
            }

            $cek_upload = DB::table('m_tahapan_detail')
                        ->select($table.".nama_diri",'m_tahapan_detail.status','m_tahapan_detail.id_tahapan_detail')
                        ->where('m_tahapan_detail.id_tahapan',$id)
                        ->join($table,$table.'.id','m_tahapan_detail.id_sakramen')
                        ->get();

            foreach ($cek_upload as $key => $value) {
                $image_tahapan = DB::table('m_tahapan_image')
                                ->where('id_tahapan_detail',$value->id_tahapan_detail)
                                ->get();

                $value->image = $image_tahapan;
            }
        }

        //dd($cek_upload);
        

        return view('admin.master.tahapan.upload', compact('cek_upload','judul'));
    }

}
