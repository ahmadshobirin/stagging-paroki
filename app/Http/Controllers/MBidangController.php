<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;

class MBidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataBidang = DB::table('m_bidang')->orderBy('id_bidang','DESC')->get();

        return view('admin.master.bidang.index', compact('dataBidang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.master.bidang.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_bidang' => 'required|max:100|unique:m_bidang',
        ]);

       

        DB::beginTransaction();
        try{

            DB::table('m_bidang')->insert([
                'nama_bidang' => $request->nama_bidang,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();
            return redirect('admin/bidang')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $dataBidang = DB::table('m_bidang')->where('id_bidang',$id)->first();
        return view('admin.master.bidang.update',compact('dataBidang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_bidang' => 'required|max:100|unique:m_bidang,nama_bidang,'.$id.',id_bidang',
        ]);

        

        DB::beginTransaction();
        try{

            DB::table('m_bidang')->where('id_bidang',$id)->update([
                'nama_bidang' => $request->nama_bidang,
            ]);

            DB::commit();
            return redirect('admin/bidang')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cek = DB::table('m_produk')->where('id_kategori', $id)->count();
        if ($cek > 0) {
            return redirect()->back()->with('message', 'Data Tidak Bisa Dihapus Karena Sudah Dipakai Untuk Master Produk');
        }

        $delete = MKategoriProdukModel::where('id',$id)->delete();

        return redirect()->back()->with('message-success', 'Berhasil Dihapus');
    }
}
