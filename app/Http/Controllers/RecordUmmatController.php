<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Auth;
use Response;
use Carbon\Carbon;
use App\Models\RecordUmmat;
use Illuminate\Http\Request;
use App\Mail\ApproveProposal;
use Yajra\DataTables\DataTables;

class RecordUmmatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = null)
    {
        $view = ($type == 'sakit') ? 'index' : 'in-kn';

        return view('admin/pencatatan/' . $view, compact('type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        if (!in_array($type, ['kunjungan', 'sakit'])) abort(404);

        $users = DB::table('m_user')->where('role', 4)->orderBy('name', 'desc')->get();
        $action = route('pencatatan.store', $type);

        return view('admin/pencatatan/form', compact('users', 'action', 'type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'user_id' => Auth::user()->id,
                'date' => Carbon::parse($request->date)->format('Y-m-d'),
                'record_type' => ($type == 'sakit') ? 'sakit' : 'kunjungan',
            ]);

            $exceptField = $this->exceptField($request);
            $record = RecordUmmat::create($exceptField);

            $this->sendEmail($record);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
        return redirect("/admin/pencatatan-$type");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($type, $id)
    {
        $users = DB::table('m_user')->where('role', 4)->orderBy('name', 'desc')->get();
        $action = route('pencatatan.update', $id);
        $edit = RecordUmmat::where('id', $id)->first();

        return view('admin/pencatatan/form', [
            'users' => $users,
            'action' => $action,
            'editmode' => true,
            'record' => $edit->toJson(),
            'type' => $type
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'date' => Carbon::parse($request->date)->format('Y-m-d')
            ]);

            $exceptField = $this->exceptField($request);
            $rs = RecordUmmat::where('id', $id)->first();
            $rs->update($exceptField);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect("/admin/pencatatan-$rs->record_type");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = RecordUmmat::where('id', $id)->firstOrFail();

        $del->delete();

        return Response::json([
            'code' => 200,
            'description' => 'delete succes'
        ], 200);
    }

    public  function exceptField($request)
    {
        return $request->except(['_token', '_method']);
    }

    public function table($type,$userid)
    {
        $user = DB::table('m_user')->where('id',$userid)->first();
        if(in_array($user->role,[1,2])){
            $record = RecordUmmat::where('record_type', $type)->orderBy('id', 'desc');
        }else{
            $record = RecordUmmat::where('record_type', $type)->where('user_id',$userid)->orderBy('id', 'desc');
        }

        return DataTables::of($record)
            ->editColumn('ummat_name', function ($self) {
                return ucwords($self->ummat_name);
            })
            ->editColumn('date', function ($self) {
                return date('d M Y', strtotime($self->date));
            })
            ->addColumn('action', function ($self) {
                return "<a href=\"" . url('/admin/pencatatan-' . $self->record_type . '/' . $self->id . '/edit') . "\" class=\"btn btn-warning  btn-xs\"><i class=\"fa fa-edit\"></i></a> &nbsp
                <a href=\"javasript:void(0)\" url=\"" . route('pencatatan.destroy', $self->id) . "\" class=\"btn btn-danger  btn-xs\"><i class=\"fa fa-trash\"></i></a>";
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'ummat_name', 'date', 'note_type'])
            ->make(true);
    }

    public function sendEmail($record)
    {
        $userUpline =  DB::table('m_kategori_user')->where('id_kategori_user', Auth::user()->id_header_user)->first();

        $upline = new MUserController();
        $cek = $upline->getUpline(Auth::user()->id,'record');
        $last = end($cek);

        $text = ($record->record_type == 'kunjungan')
            ?
            "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
            karena telah dilakukan pencatatan kunjungan di lingkungan $userUpline->nama_kategori_user oleh $record->pengunjung
                pada tanggal" . date('d-m-Y', strtotime($record->date)) . ""
            : "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
        karena telah dilakukan pencatatan Sakramen orang sakit di lingkungan $userUpline->nama_kategori_user, atas nama Bpk/Ibu/Sdr/i $record->nama_lengkap, oleh Romo $record->romo, pada tanggal " . date('d-m-Y', strtotime($record->date)) . "";

        // dd(Auth::user()->email,$last['email'],$text);

        Mail::to($last['email'])->send(new ApproveProposal($text, "Reminder Pencatatan"));
    }
}
