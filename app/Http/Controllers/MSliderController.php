<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use App\Models\MUserModel;
use App\Models\MRoleModel;

class MSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $dataSlider = DB::table('m_slider')
        //             ->select('m_slider.*','m_produk.name as name_produk')
        //             ->leftjoin('m_produk','m_produk.id','m_slider.produk_related')
        //             ->get();
        //dd($dataSlider);
        return view("admin.master.slider.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getMRole = DB::table('m_role')->where('name','!=','Customer')->get();
        return view('admin.master.user.create', compact('getMRole'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'username' => 'unique:m_user',
            'email' => 'required|email|unique:m_user',
            'role' => 'required',
            'password' => 'required',
        ]);

        //dd($request->all());
        //$roleSales  = MRoleModel::where('name', 'Sales')->first();
        $newSales = new MUserModel;
        $newSales->name = $request->nama;
        $newSales->username = $request->username;
        $newSales->email = $request->email;
        $newSales->address = $request->alamat;
        $newSales->birthdate = date('Y-m-d', strtotime($request->birthdate));
        $newSales->password =  bcrypt(str_replace(' ', '', $request->password));
        $newSales->role = $request->role;
        $newSales->status = "active";
        $newSales->save();

        return redirect('admin/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataSlider = DB::table('m_slider')
                        ->select('m_slider.*','m_produk.name as name_produk')
                        ->leftjoin('m_produk','m_produk.id','m_slider.produk_related')
                        ->where('m_slider.id', $id)
                        ->first();
        $dataProduk = DB::table('m_produk')->where('status','active')->get();
        return view('admin.master.slider.update',compact('dataSlider','dataProduk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'email' => 'email|unique:m_user,email,'.$id,
            'role' => 'required',
            'username' => 'unique:m_user,username,'.$id,
        ]);

         $updateSales = MUserModel::where('id', '=', $id)->update([
            'name' => $request->nama,
            'address' => $request->alamat,
            'username' => $request->username,
            'email' => $request->email,
            'birthdate' => date('Y-m-d', strtotime($request->birthdate)),
            'role' => $request->role,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return redirect('admin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apiSlider()
    {
        //$users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $dataSlider = DB::table('m_slider')
                    ->select('m_slider.*','m_produk.name as name_produk')
                    ->leftjoin('m_produk','m_produk.id','m_slider.produk_related')
                    ->get();

        return Datatables::of($dataSlider)
            ->editColumn('name_produk', function($dataSlider)
                {
                    if($dataSlider->name_produk == "" || $dataSlider->name_produk == null){
                        return "-";
                    }else{
                        return ucfirst($dataSlider->name_produk);
                    }
                })
            ->addColumn('action', function ($dataSlider) {
                return '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/slider/'.$dataSlider->id.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                            '&nbsp'.

                        '</td>'.
                    '</tr>'.
                '</table>';

            })
            ->addIndexColumn()
            ->rawColumns(['action','name_produk'])
            ->make(true);
    }
}
