<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class MRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataRole = DB::table('m_role')->get();
        return view("admin.master.role.index", compact('dataRole'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->Validate($request, [
            'name' => 'required|max:50'
        ]);

        $role = 0;
        $approval = 0;
        $master_produk = 0;
        $trx = 0;

        if ($request->role == 'on') {
            $role = 1;
        }
        if ($request->approval == 'on') {
            $approval = 1;
        }
        if ($request->produk == 'on') {
            $master_produk = 1;
        }
        if ($request->trx == 'on') {
            $trx = 1;
        }

        DB::table('m_role')->insert([
            'name' => $request->name,
            'status_role' => $role,
            'status_approval' => $approval,
            'status_master_produk' => $master_produk,
            'status_trx' => $trx,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id,
        ]);

        return redirect('admin/userrole');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataRole = DB::table('m_role')->where('id',$id)->first();
        return view('admin.master.role.update',compact('dataRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->Validate($request, [
            'name' => 'required|max:50'
        ]);

        $role = 0;
        $approval = 0;
        $master_produk = 0;
        $trx = 0;

        if ($request->role == 'on') {
            $role = 1;
        }
        if ($request->approval == 'on') {
            $approval = 1;
        }
        if ($request->produk == 'on') {
            $master_produk = 1;
        }
        if ($request->trx == 'on') {
            $trx = 1;
        }

        DB::table('m_role_user')->where('id', $id)->update([
            'role' => $request->name,
            'status_role' => $role,
            'status_approval' => $approval,
            'status_master_produk' => $master_produk,
            'status_trx' => $trx,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id,
        ]);

        return redirect('admin/userrole');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
