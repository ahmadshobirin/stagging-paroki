<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;

class MKategoriUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$getDataKategori = DB::table('m_kategori_user')orderBy('id','DESC')->get();

        return view('admin.master.user.kategori.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bidang = DB::table('m_bidang')->get();
        return view('admin.master.user.kategori.create', compact('bidang'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_kategori_user' => 'required|max:50|unique:m_kategori_user',
            'id_bidang' => 'required',
        ]);

        DB::beginTransaction();
        try{


            DB::table('m_kategori_user')->insert([
                'nama_kategori_user' => $request->nama_kategori_user,
                'id_bidang' => $request->id_bidang,
                'id_parent' => ($request->id_parent != null)
                                ? (int) $request->id_parent
                                 : null,
                'type' => ($request->bidang == null)
                                ? (int) $request->type
                                 : null,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();
            return redirect('admin/katuser')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataKategori = DB::table('m_kategori_user')
                        ->where('id_kategori_user',$id)
                        // ->where('status','active')
                        ->first();

        $bidang = DB::table('m_bidang')->get();

        $upline = DB::table('m_kategori_user')
                ->where('status','active')
                // ->where('id_bidang',$dataKategori->id_bidang)
                ->get();

        return view('admin.master.user.kategori.update',compact('dataKategori','bidang','upline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'nama_kategori_user' => 'required|max:50|unique:m_kategori_user,nama_kategori_user,'.$id.',id_kategori_user',
            'id_bidang' => 'required'
        ]);



        DB::beginTransaction();
        try{

            DB::table('m_kategori_user')->where('id_kategori_user',$id)->update([
                'nama_kategori_user' => $request->nama_kategori_user,
                'id_bidang' => $request->id_bidang,
                'id_parent' => ($request->id_parent != null)
                        ? (int) $request->id_parent
                        : null,
            ]);


            DB::commit();
            return redirect('admin/katuser')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cek = DB::table('m_user')->where('id_header_user', $id)->where('status','active')->count();
        if ($cek > 0) {
            return redirect()->back()->with('message', 'Data Tidak Bisa Dihapus Karena Sudah Dipakai Untuk Master User');
        }

        $delete = DB::table('m_kategori_user')->where('id_kategori_user',$id)->update(["status" => "inactive" ]);

        return redirect()->back()->with('message-success', 'Berhasil Dihapus');
    }

    public function apiKategori()
    {
        $Kategori = DB::table('m_kategori_user')
                        ->select('m_kategori_user.*','m_bidang.nama_bidang')
                        ->leftjoin('m_bidang','m_bidang.id_bidang','m_kategori_user.id_bidang')
                        ->where('status','active')
                        ->orderBy('m_kategori_user.id_kategori_user','DESC')
                        ->get();

        return Datatables::of($Kategori)
        ->editColumn('nama_bidang', function($Kategori){
                if($Kategori->id_bidang == 0){
                    $bidang = "Admin";
                }else{
                    $bidang = $Kategori->nama_bidang;
                }
                return $bidang;
            })
        ->addColumn('action', function ($Kategori) {
            return '<a href="'.url('admin/katuser/'.$Kategori->id_kategori_user.'/edit').'" data-toggle="tooltip" data-placement="top" title="Ubah" class="btn btn-warning pull-left btn-sm"><i class="fa fa-edit"></i></a>'.'&nbsp;'.
            '<a href="'.url('/admin/katuser/delete/'.$Kategori->id_kategori_user).'" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>';
            })
        ->addColumn('name_parent',function($kategori){
            if($kategori->id_parent == null || $kategori->id_parent == 0){
                return '-';
            }

            return DB::table('m_kategori_user')
                ->where('id_kategori_user',$kategori->id_parent)
                ->first()->nama_kategori_user;
        })
        ->addIndexColumn()
        ->rawColumns(['action','nama_bidang','name_parent'])
        ->make(true);
    }

    public function showKategoriFitur($id)
    {
        $kat_fitur = DB::table('m_kategori_fitur')->get();
        $kategori = DB::table('m_kategori_produk')->where('id', $id)->first();
        $det_kat = DB::table('d_kategori_produk')->where('id_kategori_produk', $id)->get();

        $result = [];
        $result['fitur'] = $kat_fitur;
        $result['det_kat'] = $det_kat;
        $result['kategori'] = $kategori;

        return Response::json($result);
    }

    public function saveKategoriFitur(Request $request)
    {
        $kat_fitur = DB::table('m_kategori_fitur')->get();
        $result = [];
        $cek = 0;
        foreach ($kat_fitur as $key => $value) {
            $nama = $value->name_kategori_fitur;
            if($request->$nama == "on"){
                $result[$cek]['id_kategori_fitur'] = $value->id;

                $cek++;
            }
        }

        DB::beginTransaction();
        try{
            DB::table('d_kategori_produk')->where('id_kategori_produk',$request->id_kategori_produk)->delete();

            if(count($result) > 0){
                foreach ($result as $key1 => $value1) {
                    DB::table('d_kategori_produk')
                        ->insert([
                            'id_kategori_produk' => $request->id_kategori_produk,
                            'id_kategori_fitur'  => $value1['id_kategori_fitur'],
                            'created_at'         => date('Y-m-d H:i:s'),
                            'updated_at'         => date('Y-m-d H:i:s'),
                            'created_by'         => Auth::user()->id,
                            'updated_by'         => Auth::user()->id,
                        ]);
                }
            }

            DB::commit();
            return redirect('admin/kategori-produk')->with('message-success', 'Data Berhasil Disimpan');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect('admin/kategori-produk')->with('message-error', 'Data Gagal Disimpan');
        }

        //return Response::json($result);
    }

    public function uplineUser($id)
    {
        if($id == null) return Response::json('id kosong',403);

        $kategori = DB::table('m_kategori_user')
                ->select('id_kategori_user','nama_kategori_user')
                // ->where('id_bidang',$id)
                ->where('status','active')
                ->get();

        return Response::json($kategori,200);
    }
}
