<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;

class MPeriodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periode = DB::table('m_periode')->where('m_periode.status_periode','active')->orderBy('id_periode','DESC')->get();

        return view('admin.master.periode.index', compact('periode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.master.periode.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tglmulai = substr($request->periode,0,10);
        $tglsampai = substr($request->periode,13,10);


        $this->validate($request,[
            'nama_periode' => 'required|max:100',
        ]);



        DB::beginTransaction();
        try{

            DB::table('m_periode')->insert([
                'nama_periode' => $request->nama_periode,
                'status_periode' => "active",
                'type_periode' => $request->type_periode,
                'start_date' => date('Y-m-d', strtotime($tglmulai)),
                'end_date' => date('Y-m-d', strtotime($tglsampai)),
                'tgl_pelaksanaan' => date('Y-m-d', strtotime($request->tgl_pelaksanaan)),
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();
            return redirect('admin/periode')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $dataPeriode = DB::table('m_periode')->where('id_periode',$id)->first();
        return view('admin.master.periode.update',compact('dataPeriode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tglmulai = substr($request->periode,0,10);
        $tglsampai = substr($request->periode,13,10);


        $this->validate($request,[
            'nama_periode' => 'required|max:100',
        ]);



        DB::beginTransaction();
        try{

            DB::table('m_periode')->where('id_periode',$id)->update([
                'nama_periode' => $request->nama_periode,
                'start_date' => date('Y-m-d', strtotime($tglmulai)),
                'end_date' => date('Y-m-d', strtotime($tglsampai)),
                'tgl_pelaksanaan' => date('Y-m-d', strtotime($request->tgl_pelaksanaan)),
            ]);

            DB::commit();
            return redirect('admin/periode')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{

            $delete = DB::table('m_periode')
                            ->where('id_periode',$id)
                            ->update([
                                'status_periode' => "inactive",
                            ]);
            DB::commit();

            return redirect()->back()->with('message-success', 'Berhasil Dihapus');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message', 'Data Gagal Dihapus');
        }
    }

}
