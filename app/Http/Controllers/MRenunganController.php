<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;
use File;

class MRenunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataRenungan = DB::table('m_renungan')->select('m_renungan.*','m_romo.nama_romo')->leftjoin('m_romo','m_romo.id_romo','m_renungan.author_renungan')->orderBy('id_renungan','DESC')->get();

        return view('admin.master.renungan.index', compact('dataRenungan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $romo = DB::table('m_romo')->where('status_romo','on')->get();
        return view('admin.master.renungan.create', compact('romo'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'renungan' => 'required',
            'romo' => 'required',
        ]);

       $file   = $request->image;
        if(!empty($file)){
            $filename_path_bukti = date("ymdHis").$request->title.".jpg";
            $destinationPath = 'upload/renungan';   
        }else{
            $filename_path_bukti = "no_image.png";
        }

        DB::beginTransaction();
        try{

            DB::table('m_renungan')->insert([
                'title_renungan' => $request->title,
                'isi_renungan' => $request->renungan,
                'author_renungan' => $request->romo,
                'image_renungan' => $filename_path_bukti,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

             if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }

            DB::commit();
            return redirect('admin/gereja/renungan')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $dataRenungan = DB::table('m_renungan')->where('id_renungan',$id)->first();
       $romo = DB::table('m_romo')->where('status_romo','on')->get();
        return view('admin.master.renungan.update',compact('dataRenungan','romo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'renungan' => 'required',
            'romo' => 'required',
        ]);

        $file   = $request->image;
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->title.".jpg";
                $destinationPath = 'upload/renungan';   
                $update_img = 2;
        }else{
            if($request->value_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/renungan'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/renungan'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/renungan'; 
                $update_img = 1;
            }
        }


        

        DB::beginTransaction();
        try{

             DB::table('m_renungan')->where('id_renungan',$id)
                ->update([
                    'title_renungan' => $request->title,
                    'isi_renungan' => $request->renungan,
                    'author_renungan' => $request->romo,
                    'image_renungan' => $filename_path_bukti,
                ]);
            if(!empty($file)){
                $file->move($destinationPath, $filename_path_bukti);
            }
            if($update_img == 2){
                File::delete('upload/renungan/'.$request->image_hidden);
            }

            DB::commit();
            return redirect('admin/gereja/renungan')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{

            $delete = DB::table('m_renungan')
                            ->where('id_renungan',$id)
                            ->delete();
            DB::commit();

            return redirect()->back()->with('message-success', 'Berhasil Dihapus');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message', 'Data Gagal Dihapus');
        }
    }
}
