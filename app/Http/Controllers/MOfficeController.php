<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class MOfficeController extends Controller
{
	public function VisiMisi()
	{
        
        $vimi = DB::table('m_office')->select('visi','misi')->first();
       
        return view('admin.master.office.visi_misi',compact('vimi'));
	}

    public function storeVisiMisi(Request $request)
    {
    
            DB::beginTransaction();
                    try{
                            DB::table('m_office')
                                ->where('id_office',1)
                                ->update([
                                    'visi' => $request->visi,
                                    'misi' => $request->misi,
                                ]);
                            DB::commit();

                    }catch(\Exception $e) {
                        $success = false;
                        DB::rollback();
                        dd($e);  
                    }
            return redirect()->back()->with('message-success','Data berhasil diubah');
    }

    public function edit(){
        $office = DB::table('m_office')->first();
        $prov = DB::table('m_provinsi')->get();
        $kota = DB::table('m_kota_kab')->where('provinsi', $office->id_provinsi)->get();
        $kec = DB::table('m_kecamatan')->where('kota_kab', $office->id_kota_kab)->get();
        $kel = DB::table('m_kelurahan_desa')->where('kecamatan', $office->id_kecamatan)->get();

        return view('admin.master.office.update', compact('office','prov','kota','kec','kel'));
    }

    public function update(Request $request){

        DB::beginTransaction();
                    try{
                            DB::table('m_office')
                                ->where('id_office',1)
                                ->update([
                                    'email_office' => $request->email,
                                    'alamat_office' => $request->alamat,
                                    'telp_office' => $request->telp,
                                    'iframe_office' => $request->map,
                                    'url_fb' => $request->fb,
                                    'url_twitter' => $request->twitter,
                                    'url_instagram' => $request->ig,
                                    'url_youtube' => $request->youtube,
                                ]);
                            DB::commit();

                    }catch(\Exception $e) {
                        $success = false;
                        DB::rollback();
                        dd($e);  
                    }
            return redirect()->back()->with('message-success','Data berhasil diubah');

    }

       
}
?>