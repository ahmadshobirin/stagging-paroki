<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;
use DataTables;
use File;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class MAgendaController extends Controller
{
    public function indexUmum()
    {
        return view('admin.posting.agenda.index');
    }


    public function createUmum()
    {
        $bidang = DB::table('m_bidang')->get();
        $tempat = DB::table('m_tempat')->where('status', 'on')->get();


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.posting.agenda.create', compact('bidang', 'tempat'));
    }

    public function storeUmum(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:50',
            'date_agenda' => 'required',
            'waktu_agenda' => 'required',
            'end_agenda' => 'required',
            'tempat_agenda' => 'required',
            'bidang' => 'required',
        ]);

        $awal = date("H:i:00", strtotime($request->waktu_agenda));
        $akhir = date("H:i:00", strtotime($request->end_agenda));
        $tanggal = date("Y-m-d 00:00:00", strtotime($request->date_agenda));

        if ($request->libur == "on") {
            $libur = 1;
        } else {
            $libur = 0;
        }

        $cek_agenda = DB::table('m_agenda')
            ->where('tempat_agenda', $request->tempat_agenda)
            ->where('date_agenda', $tanggal)
            ->get();

        //dd($cek_agenda);
        if (count($cek_agenda) > 0) {
            $count = 0;
            //$s="";
            foreach ($cek_agenda as $key => $value) {
                if ($value->waktu_agenda > $awal && $value->end_agenda <= $akhir) {
                    $count = $count + 1;
                    //$s="1";
                } else if ($value->waktu_agenda <= $awal && $value->end_agenda >= $akhir) {
                    $count = $count + 1;
                    //$s="2";
                } else if ($value->waktu_agenda <= $awal && $value->end_agenda > $akhir) {
                    $count = $count + 1;
                    //$s="3";
                }
            }
            //dd($s);
            //dd($count);
            if ($count > 0) {
                return redirect()->back()->with('message', 'Waktu/Tempat bertabrakan dengan jadwal agenda lain');
            }
        }

        DB::beginTransaction();
        try {
            DB::table('m_agenda')
                ->insert([
                    'nama_agenda' => $request->title,
                    'date_agenda' => $tanggal,
                    'hari_libur' => $libur,
                    'tempat_agenda' => $request->tempat_agenda,
                    'waktu_agenda' => $awal,
                    'end_agenda' => $akhir,
                    'type_agenda' => "umum",
                    'id_bidang' => $request->bidang,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
            dd($e);
        }



        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return redirect('admin/kegiatan-umum');
    }

    public function editUmum($id)
    {
        $type = "edit";
        $agenda = DB::table('m_agenda')->where('id_agenda', $id)->first();
        $bidang = DB::table('m_bidang')->get();
        $tempat = DB::table('m_tempat')->where('status', 'on')->get();


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.posting.agenda.update', compact('bidang', 'tempat', 'agenda', 'type'));
    }

    public function showUmum($id)
    {
        $type = "show";
        $agenda = DB::table('m_agenda')->where('id_agenda', $id)->first();
        $bidang = DB::table('m_bidang')->get();
        $tempat = DB::table('m_tempat')->where('status', 'on')->get();


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.posting.agenda.update', compact('bidang', 'tempat', 'agenda', 'type'));
    }

    public function updateUmum(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:50',
            'date_agenda' => 'required',
            'waktu_agenda' => 'required',
            'end_agenda' => 'required',
            'tempat_agenda' => 'required',
            'bidang' => 'required',
        ]);

        $awal = date("H:i:s", strtotime($request->waktu_agenda));
        $akhir = date("H:i:s", strtotime($request->end_agenda));
        $tanggal = date("Y-m-d 00:00:00", strtotime($request->date_agenda));

        if ($request->libur == "on") {
            $libur = 1;
        } else {
            $libur = 0;
        }

        $cek_agenda = DB::table('m_agenda')
            ->where('tempat_agenda', $request->tempat_agenda)
            ->where('date_agenda', $tanggal)
            ->where('id_agenda', '!=', $id)
            ->get();


        if (count($cek_agenda) > 0) {
            $count = 0;
            foreach ($cek_agenda as $key => $value) {
                if ($value->waktu_agenda > $awal && $value->end_agenda <= $akhir) {
                    $count = $count + 1;
                } else if ($value->waktu_agenda <= $awal && $value->end_agenda >= $akhir) {
                    $count = $count + 1;
                } else if ($value->waktu_agenda <= $awal && $value->end_agenda > $akhir) {
                    $count = $count + 1;
                }
            }
            if ($count > 0) {
                return redirect()->back()->with('message', 'Waktu/Tempat bertabrakan dengan jadwal agenda lain');
            }
        }

        DB::beginTransaction();
        try {
            DB::table('m_agenda')
                ->where('id_agenda', $id)
                ->update([
                    'nama_agenda' => $request->title,
                    'date_agenda' => $tanggal,
                    'hari_libur' => $libur,
                    'tempat_agenda' => $request->tempat_agenda,
                    'waktu_agenda' => $awal,
                    'end_agenda' => $akhir,
                    'type_agenda' => "umum",
                    'id_bidang' => $request->bidang,
                ]);

            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
            dd($e);
        }



        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return redirect('admin/kegiatan-umum');
    }

    public function destroyUmum($id)
    {
        $cek = DB::table('m_posting')->where('id_agenda', $id)->where('status_post', '!=', 'deleted')->get();
        if (count($cek) > 0) {
            return redirect()->back()->with('message', 'Agenda ini tidak bisa dihapus, karena sudah dibuat pada Kronik');
        } else {
            DB::beginTransaction();
            try {
                DB::table('m_agenda')
                    ->where('id_agenda', $id)
                    ->delete();
                DB::commit();
            } catch (\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }
            return redirect()->back()->with('message-success', 'Agenda berhasil dihapus');
        }
    }

    public function apiKegiatanUmum()
    {
        $post = DB::table('m_agenda')
            ->select('m_agenda.*', 'm_tempat.nama_tempat', 'm_bidang.nama_bidang')
            ->join('m_tempat', 'm_tempat.id_tempat', 'm_agenda.tempat_agenda')
            ->leftjoin('m_bidang', 'm_bidang.id_bidang', 'm_agenda.id_bidang')
            ->where('type_agenda', 'umum')
            ->orderBy('id_agenda', 'desc');

        return Datatables::of($post)
            ->addColumn('action', function ($post) {

                $hasil = '<table id="tabel-in-opsi">' .
                    '<tr>' .
                    '<td>' .
                    '<a href="' . url('/admin/kegiatan-umum/' . $post->id_agenda . '/edit') . '" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>' .

                    '&nbsp' .
                    '<a href="' . url('/admin/kegiatan-umum/' . $post->id_agenda . '/delete') . '" class="btn btn-danger  btn-sm"><i class="fa fa-trash" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Menghapus ?'" . ')"></i></a>' .

                    '</td>' .
                    '</tr>' .
                    '</table>';
                return $hasil;
            })
            ->editColumn('nama_bidang', function ($post) {
                $nama = "";
                if ($post->id_bidang == 0) {
                    $nama = "Umum";
                } else {
                    $nama = $post->nama_bidang;
                }
                return $nama;
            })
            ->editColumn('nama_agenda', function ($post) {
                $cek = '<a href="' . url('/admin/kegiatan-umum/' . $post->id_agenda . '/show') . '">' . $post->nama_agenda . '</a>';
                return $cek;
            })
            ->editColumn('date_agenda', function ($post) {
                return date('d M Y', strtotime($post->date_agenda)) . '<br/>' . $post->nama_tempat;
            })
            ->editColumn('waktu', function ($post) {
                return date('H.i', strtotime($post->waktu_agenda)) . '-' . date('H.i', strtotime($post->end_agenda)) . ' WIB';
            })
            ->filterColumn('date_agenda', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(date_agenda,'%d %M %Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('m_agenda.id_bidang', function ($query, $keyword) {
                $query->whereRaw("IF(m_agenda.id_bidang = 0, 'Umum', 'm_bidang.nama_bidang') like ?", ["%$keyword%"]);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'date_agenda', 'waktu', 'nama_bidang', 'nama_agenda', 'id_bidang'])
            ->make(true);
    }

    public function indexNikah()
    {

        return view('admin.posting.nikah.index');
    }


    public function createNikah()
    {

        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.posting.nikah.create');
    }

    public function storeNikah(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:50',
            'date_agenda' => 'required',
            'waktu_agenda' => 'required',
            'end_agenda' => 'required',
            'pria' => 'required',
            'wanita' => 'required',
            'address_pria' => 'required',
            'address_wanita' => 'required',
            'phone_pria' => 'required',
            'phone_wanita' => 'required',
            'romo' => 'required',
            'tempat_menikah' => 'required',

        ]);

        $awal = date("H:i:s", strtotime($request->waktu_agenda));
        $akhir = date("H:i:s", strtotime($request->end_agenda));
        if ($request->tgl_kanonik != "") {
            $tgl_kanonik = date("Y-m-d", strtotime($request->tgl_kanonik));
        } else {
            $tgl_kanonik = null;
        }
        $tanggal = date("Y-m-d 00:00:00", strtotime($request->date_agenda));

        $file   = $request->image;
        if (!empty($file)) {
            $filename_path_bukti = date("ymdHis") . $request->title . ".jpg";
            $destinationPath = 'upload/wedding';
        } else {
            $filename_path_bukti = "no_image.png";
        }

        if ($request->id_tempat_menikah == "on") {
            $tempat_menikah = 2;
            $alternative_tempat_menikah = $request->tempat_menikah;
        } else {
            $tempat_menikah = 1;
            $alternative_tempat_menikah = "";
            $cek_agenda = DB::table('m_agenda')
                ->where('date_agenda', $tanggal)
                ->where('tempat_menikah', 1)
                ->where('type_agenda', 'pernikahan')
                ->get();


            if (count($cek_agenda) > 0) {
                $count = 0;
                foreach ($cek_agenda as $key => $value) {
                    if ($value->waktu_agenda > $awal && $value->end_agenda <= $akhir) {
                        $count = $count + 1;
                    } else if ($value->waktu_agenda <= $awal && $value->end_agenda >= $akhir) {
                        $count = $count + 1;
                    } else if ($value->waktu_agenda <= $awal && $value->end_agenda > $akhir) {
                        $count = $count + 1;
                    }
                }
                if ($count > 0) {
                    return redirect()->back()->with('message', 'Waktu/Tempat bertabrakan dengan jadwal agenda lain');
                }
            }
        }

        if ($request->libur == "on") {
            $libur = 1;
        } else {
            $libur = 0;
        }

        DB::beginTransaction();
        try {
            DB::table('m_agenda')
                ->insert([
                    'nama_agenda' => $request->title,
                    'date_agenda' => $tanggal,
                    'hari_libur' => $libur,
                    'waktu_agenda' => $awal,
                    'end_agenda' => $akhir,
                    'type_agenda' => "pernikahan",
                    'nama_pengantin_pria' => $request->pria,
                    'nama_pengantin_wanita' => $request->wanita,
                    'alamat_pengantin_pria' => $request->address_pria,
                    'alamat_pengantin_wanita' => $request->address_wanita,
                    'image_mempelai' => $filename_path_bukti,
                    'telp_pengantin_pria' => $request->phone_pria,
                    'telp_pengantin_wanita' => $request->phone_wanita,
                    'romo_penerima_pendaftaran' => $request->romo,
                    'tempat_menikah' => $tempat_menikah,
                    'alternative_tempat_menikah' => $alternative_tempat_menikah,
                    'tanggal_kanonik' => $tgl_kanonik,
                    'romo_kanonik' => $request->romo_kanonik,
                    'memberkati_perkawinan' => $request->berkat_kawin,
                    'keterangan' => $request->keterangan,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

            if (!empty($file)) {
                $file->move($destinationPath, $filename_path_bukti);
            }

            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
            dd($e);
        }


        return redirect('admin/kegiatan-nikah');
    }

    public function editNikah($id)
    {
        $type = "edit";
        $agenda = DB::table('m_agenda')->where('id_agenda', $id)->first();
        $romo = DB::table('m_romo')->get();


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.posting.nikah.update', compact('agenda', 'romo', 'type'));
    }

    public function showNikah($id)
    {
        $type = "show";
        $agenda = DB::table('m_agenda')->where('id_agenda', $id)->first();
        $romo = DB::table('m_romo')->get();


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.posting.nikah.update', compact('agenda', 'romo', 'type'));
    }

    public function updateNikah(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:50',
            'date_agenda' => 'required',
            'waktu_agenda' => 'required',
            'end_agenda' => 'required',
            'pria' => 'required',
            'wanita' => 'required',
            'address_pria' => 'required',
            'address_wanita' => 'required',
            'phone_pria' => 'required',
            'phone_wanita' => 'required',
            'romo' => 'required',
            'tempat_menikah' => 'required',

        ]);

        $namePathImage1 = "";
        $namePathImage2 = "";
        $filename_path_bukti = "";
        $awal = date("H:i:s", strtotime($request->waktu_agenda));
        $akhir = date("H:i:s", strtotime($request->end_agenda));
        if ($request->tgl_kanonik != "") {
            $tgl_kanonik = date("Y-m-d", strtotime($request->tgl_kanonik));
        } else {
            $tgl_kanonik = null;
        }
        $tanggal = date("Y-m-d 00:00:00", strtotime($request->date_agenda));

        $file   = $request->image;
        $file1   = $request->image1;
        $file2   = $request->image2;
        if (!empty($file)) {
            $filename_path_bukti = date("ymdHis") . $request->title . ".jpg";
            $destinationPath = 'upload/wedding';
            $update_img = 2;
        } else {
            if ($request->value_image_hidden == "false") {
                if ($request->image_hidden != "no_image.png") {
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/wedding';
                    $update_img = 2;
                } else {
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/wedding';
                    $update_img = 1;
                }
            } else {
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/wedding';
                $update_img = 1;
            }
        }


        if (!empty($file1)) {
            $filename_path_bukti_image1 = date("ymdHis")."pengantinpria_". $request->title . ".jpg";
            $destinationPath_image1 = 'upload/wedding';
            $update_img_1 = 2;
            $namePathImage1 = $destinationPath_image1."/".$filename_path_bukti_image1;
        } else {
            if ($request->value_image_hidden_pria == "false") {
                if ($request->image_hidden_pria != null) {
                    $filename_path_bukti_image1 = "no_image.png";
                    $destinationPath_image1 = 'upload/wedding';
                    $update_img_1 = 2;
                    $namePathImage1 = $destinationPath_image1."/".$filename_path_bukti_image1;
                } else {
                    $filename_path_bukti_image1 = $request->image_hidden_pria;
                    $destinationPath_image1 = 'upload/wedding';
                    $update_img_1 = 1;
                    $namePathImage1 = $destinationPath_image1."/".$filename_path_bukti_image1;
                }
            } else {
                $filename_path_bukti_image1 = $request->image_hidden_pria;
                $destinationPath_image1 = 'upload/wedding';
                $update_img_1 = 1;
                $namePathImage1 = $destinationPath_image1."/".$filename_path_bukti_image1;
            }
        }

        if (!empty($file2)) {
            $filename_path_bukti_image2 = date("ymdHis")."pengantinwanita_" . $request->title . ".jpg";
            $destinationPath_image2 = 'upload/wedding';
            $update_img_2 = 2;
            $namePathImage2 = $destinationPath_image2."/".$filename_path_bukti_image2;
        } else {
            if ($request->value_image_hidden_wanita == "false") {
                if ($request->image_hidden_wanita != null) {
                    $filename_path_bukti_image2 = "no_image.png";
                    $destinationPath_image2 = 'upload/wedding';
                    $update_img_2 = 2;
                    $namePathImage2 = $destinationPath_image2."/".$filename_path_bukti_image2;
                } else {
                    $filename_path_bukti_image2 = $request->image_hidden_wanita;
                    $destinationPath_image2 = 'upload/wedding';
                    $update_img_2 = 1;
                    $namePathImage2 = $destinationPath_image2."/".$filename_path_bukti_image2;
                }
            } else {
                $filename_path_bukti_image2 = $request->image_hidden_wanita;
                $destinationPath_image2 = 'upload/wedding';
                $update_img_2 = 1;
                $namePathImage2 = $destinationPath_image2."/".$filename_path_bukti_image2;
            }
        }

        if ($request->libur == "on") {
            $libur = 1;
        } else {
            $libur = 0;
        }

        if ($request->id_tempat_menikah == "on") {
            $tempat_menikah = 2;
            $alternative_tempat_menikah = $request->tempat_menikah;
        } else {
            $tempat_menikah = 1;
            $alternative_tempat_menikah = "";
            $cek_agenda = DB::table('m_agenda')
                ->where('date_agenda', $tanggal)
                ->where('tempat_menikah', 1)
                ->where('type_agenda', 'pernikahan')
                ->where('id_agenda', '!=', $id)
                ->get();


            if (count($cek_agenda) > 0) {
                $count = 0;
                foreach ($cek_agenda as $key => $value) {
                    if ($value->waktu_agenda > $awal && $value->end_agenda <= $akhir) {
                        $count = $count + 1;
                    } else if ($value->waktu_agenda <= $awal && $value->end_agenda >= $akhir) {
                        $count = $count + 1;
                    } else if ($value->waktu_agenda <= $awal && $value->end_agenda > $akhir) {
                        $count = $count + 1;
                    }
                }
                if ($count > 0) {
                    return redirect()->back()->with('message', 'Waktu/Tempat bertabrakan dengan jadwal agenda lain');
                }
            }
        }

        DB::beginTransaction();
        try {
            DB::table('m_agenda')
                ->where('id_agenda', $id)
                ->update([
                    'nama_agenda' => $request->title,
                    'date_agenda' => $tanggal,
                    'hari_libur' => $libur,
                    'waktu_agenda' => $awal,
                    'end_agenda' => $akhir,
                    'type_agenda' => "pernikahan",
                    'nama_pengantin_pria' => $request->pria,
                    'nama_pengantin_wanita' => $request->wanita,
                    'alamat_pengantin_pria' => $request->address_pria,
                    'alamat_pengantin_wanita' => $request->address_wanita,
                    'image_mempelai' => $filename_path_bukti,
                    'image_nikah_pengantin_pria' => $namePathImage1,
                    'image_nikah_pengantin_wanita' => $namePathImage2,
                    'telp_pengantin_pria' => $request->phone_pria,
                    'telp_pengantin_wanita' => $request->phone_wanita,
                    'romo_penerima_pendaftaran' => $request->romo,
                    'tempat_menikah' => $tempat_menikah,
                    'alternative_tempat_menikah' => $alternative_tempat_menikah,
                    'tanggal_kanonik' => $tgl_kanonik,
                    'romo_kanonik' => $request->romo_kanonik,
                    'memberkati_perkawinan' => $request->berkat_kawin,
                    'keterangan' => $request->keterangan,
                    'status' => 'approved',
                ]);

            if (!empty($file)) {
                $file->move($destinationPath, $filename_path_bukti);
            }
            if ($update_img == 2) {
                File::delete('upload/wedding/' . $request->image_hidden);
            }

            if (!empty($file1)) {
                $file1->move($destinationPath_image1, $filename_path_bukti_image1);
            }
            if ($update_img_1 == 2) {
                File::delete($request->image_hidden_pria);
            }

            if (!empty($file2)) {
                $file2->move($destinationPath_image2, $filename_path_bukti_image2);
            }
            if ($update_img_2 == 2) {
                File::delete($request->image_hidden_wanita);
            }

            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
            dd($e);
        }



        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return redirect('admin/kegiatan-nikah');
    }

    public function destroyNikah($id)
    {
        DB::beginTransaction();
        try {
            DB::table('m_agenda')
                ->where('id_agenda', $id)
                ->delete();
            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            DB::rollback();
            dd($e);
        }
        return redirect()->back()->with('message-success', 'Agenda Nikah berhasil dihapus');
    }

    public function apiKegiatanNikah()
    {
        $post = DB::table('m_agenda')
            ->select('m_agenda.*')
            ->where('type_agenda', 'pernikahan')
            ->orderBy('id_agenda', 'desc');

        return Datatables::of($post)
            ->addColumn('action', function ($post) {

                $hasil = '<table id="tabel-in-opsi">' .
                    '<tr>' .
                    '<td>' .
                    '<a href="' . url('/admin/kegiatan-nikah/' . $post->id_agenda . '/edit') . '" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>' .

                    '&nbsp' .
                    '<a href="' . url('/admin/kegiatan-nikah/' . $post->id_agenda . '/delete') . '" class="btn btn-danger  btn-sm"><i class="fa fa-trash" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Menghapus ?'" . ')"></i></a>' .

                    '</td>' .
                    '</tr>' .
                    '</table>';
                return $hasil;
            })
            ->addColumn('pasangan', function ($post) {
                $nama = $post->nama_pengantin_pria . ' & ' . $post->nama_pengantin_wanita;

                return $nama;
            })
            ->editColumn('tempat_menikah', function ($post) {
                $tempat = "";
                if ($post->tempat_menikah == 1) {
                    $tempat = $this->get_tempat($post->tempat_menikah);
                }

                return $tempat;
            })
            ->editColumn('date_agenda', function ($post) {
                $tempat = "Gereja Paroki";
                if ($post->tempat_menikah == 2) {
                    $tempat = $post->alternative_tempat_menikah;
                }
                return date('d M Y', strtotime($post->date_agenda)) . '<br/>' . $tempat;
            })
            ->editColumn('waktu', function ($post) {
                return date('H.i', strtotime($post->waktu_agenda)) . '-' . date('H.i', strtotime($post->end_agenda)) . ' WIB';
            })
            ->editColumn('status', function($post){
                if($post->status == 'in process'){
                    $cl = 'label label-default';
                }elseif($post->status == 'in approval'){
                    $cl = 'label label-primary';
                }elseif($post->status == 'approved'){
                    $cl = 'label label-success';
                } elseif ($post->status == 'cancel') {
                    $cl = 'label label-danger';
                }

                return "<span class=\"".$cl."\">".ucfirst($post->status)."</span>";
            })
            ->editColumn('nama_agenda', function ($post) {
                $cek = '<a href="' . url('/admin/kegiatan-nikah/' . $post->id_agenda . '/show') . '">' . $post->nama_agenda . '</a>';
                return $cek;
            })
            ->filterColumn('date_agenda', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(date_agenda,'%d %M %Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('tempat_menikah', function ($query, $keyword) {
                $query->whereRaw("IF(tempat_menikah = 1, 'Gereja Paroki', 'm_agenda.alternative_tempat_menikah') like ?", ["%$keyword%"]);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'date_agenda', 'waktu', 'pasangan', 'nama_agenda', 'tempat_menikah','status'])
            ->make(true);
    }

    public function get_tempat($id)
    {
        $value = "";
        if ($id == 1) {
            $value = "Gereja Paroki";
        } else { }
        return $value;
    }
}
