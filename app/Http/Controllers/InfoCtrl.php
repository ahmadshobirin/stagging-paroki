<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class InfoCtrl extends Controller
{
	public function index()
	{
       
        $artikel = DB::table('m_posting')
                    ->select('m_posting.*','m_kategori_user.nama_kategori_user')
                    ->join('m_user','m_user.id','m_posting.id_user')
                    ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_user.id_header_user')
                    ->where('status_post', 'approved')
                    ->where('published', 1)
                    ->whereIn('type_post', ['info','artikel'])
                    ->orderBy('id_posting','desc')
                    ->paginate(10);
        $slider = DB::table('m_banner')->where('page_banner','detail')->first();

        // $artikel_bidang = DB::table('m_posting')
        //                 ->select('m_posting.*','m_bidang.nama_bidang')
        //                 ->join('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
        //                 ->where('type_post','info')
        //                 ->where('m_posting.status_post', 'approved')
        //                 ->where('m_posting.id_bidang', $artikel->id_bidang)
        //                 ->where('m_posting.id_posting', '!=' ,$artikel->id_posting)
        //                 ->orderBy('created_at','desc')
        //                 ->limit(5)
        //                 ->get();

       
        return view('frontend.info',compact('artikel','slider'));
	}

       
}
?>