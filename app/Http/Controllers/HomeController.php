<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Response;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = $this->tableCount(Auth::user()->id);
        // unset($datas['komuni'],$datas['baptis']);
        return view('admin.dashboard', compact('datas'));
    }

    public function tableCount($userId = null)
    {
        $user = DB::table('m_user')->where('id',$userId)->first();

        $info = DB::table('m_posting')->where('id_bidang','!=', 0)->where('type_post','info')->where('status_post', 'in approval')->count();
        $artikel = DB::table('m_posting')->where('id_bidang','!=', 0)->where('type_post','artikel')->where('status_post', 'in approval')->count();

        if(in_array($user->role,[1,2])){
            $proposal = DB::table('proposal')->where('status', 'in approval')->count();
            $lpj = DB::table('lpj')->where('status', 'in approval')->count();
            $kronik = DB::table('m_posting')->where('id_bidang','!=', 0)->where('type_post','kronik')->where('status_post', 'in approval')->count();
            $krisma = DB::table('m_sakramen_krisma')->where('status', 'in approval')->count();
            $komuni = DB::table('m_sakramen_komuni')->where('status', 'in approval')->count();
            $baptis = DB::table('m_sakramen_baptis')->where('status', 'in approval')->count();
            $nikah = DB::table('m_agenda')->where('type_agenda','pernikahan')
            ->where('status', 'in approval')->count();
        }elseif(in_array($user->role,[3])){
            $proposal = DB::table('proposal_signature')->where('user_id', $user->id)->where('status','pending')->count();
            $lpj = DB::table('lpj_signature')->where('user_id', $user->id)->where('status','pending')->count();
            $krisma = DB::table('sakramen_signature')->where('type','krisma')->where('user_id', $user->id)->where('status', 'pending')->count();
            $kronik = DB::table('sakramen_signature')->where('type','kronik')->where('user_id', $user->id)->where('status', 'pending')->count();
            $komuni = DB::table('sakramen_signature')->where('type','komuni')->where('user_id', $user->id)->where('status', 'pending')->count();
            $nikah = DB::table('sakramen_signature')->where('type','nikah')->where('user_id', $user->id)->where('status', 'pending')->count();
            $baptis = DB::table('sakramen_signature')->whereNotIn('type',['krisma','kronik','komuni'])->where('user_id', $user->id)->where('status', 'pending')->count();
        }elseif(in_array($user->role,[4])){
            $proposal = DB::table('proposal')->where('status', 'approved')->where('user_id',$user->id)->count();
            $lpj = DB::table('lpj')->where('status', 'approved')->where('user_id',$user->id)->count();
            $kronik = DB::table('m_posting')->where('type_post','kronik')->where('status_post', 'approved')->where('id_user',$user->id)->count();
            $krisma = DB::table('m_sakramen_krisma')->where('status', 'approved')->where('user_id',$user->id)->count();
            $komuni = DB::table('m_sakramen_komuni')->where('status', 'approved')->where('user_id',$user->id)->count();
            $baptis = DB::table('m_sakramen_baptis')->where('status', 'approved')->where('user_id',$user->id)->count();
            $nikah = DB::table('m_agenda')->where('type_agenda','pernikahan')
            ->where('status', 'approved')->where('id_user',$user->id)->count();
        }

        $array = [
            'info' => $info,
            'artikel' => $artikel,
            'kronik' => $kronik,
            'krisma' => $krisma,
            'komuni' => $komuni,
            'proposal' => $proposal,
            'lpj' => $lpj,
            'nikah' => $nikah,
            'baptis' => $baptis,
        ];

        if($user->role == 3){
            $katUser = \DB::table('m_kategori_user')->where('id_kategori_user',$user->id_header_user)->first();
            if($katUser->type == 'wilayah'){
                unset($array['baptis'],$array['komuni']);
            }elseif($katUser->type == 'romo'){
              unset($array['baptis'],$array['komuni'],$array['info'],$array['artikel'],$array['krisma'],$array['kronik']);
            }elseif(in_array($katUser->type,['seksi','subseksi','koordinator','subkoordinator','bidang'])){
                unset($array['baptis'],$array['komuni'],$array['krisma'],$array['nikah']);
            }elseif($katUser->type == 'biak'){
                unset($array['baptis'],$array['info'],$array['artikel'],$array['krisma'],$array['kronik'],$array['proposal'],$array['lpj'],$array['nikah']);
            }elseif($katUser->type == 'bidang'){
                unset($array['baptis'],$array['krisma'],$array['nikah'],$array['komuni']);
            }elseif($katUser->type == 'seketaris'){
                unset($array['baptis'],$array['krisma'],$array['nikah'],$array['proposal'],$array['lpj'],$array['krisma'],$array['komuni']);
            }
        }elseif($user->role == 4){
            unset($array['info'],$array['artikel'],$array['kronik'],$array['proposal'],$array['lpj']);
        }

        return $array;
    }

}
