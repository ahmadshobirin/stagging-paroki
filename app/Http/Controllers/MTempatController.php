<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;

class MTempatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tempat = DB::table('m_tempat')->where('status','on')->orderBy('id_tempat','DESC')->get();

        return view('admin.master.tempat.index', compact('tempat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.master.tempat.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_tempat' => 'required|max:100',
        ]);

       

        DB::beginTransaction();
        try{

            DB::table('m_tempat')->insert([
                'nama_tempat' => $request->nama_tempat,
                'desc_tempat' => $request->desc_tempat,
                'status' => "on",
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            DB::commit();
            return redirect('admin/gereja/tempat')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $dataTempat = DB::table('m_tempat')->where('id_tempat',$id)->first();
        return view('admin.master.tempat.update',compact('dataTempat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_tempat' => 'required',
        ]);

        

        DB::beginTransaction();
        try{

            DB::table('m_tempat')->where('id_tempat',$id)->update([
                'nama_tempat' => $request->nama_tempat,
                'desc_tempat' => $request->desc_tempat,
            ]);

            DB::commit();
            return redirect('admin/gereja/tempat')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{

            $delete = DB::table('m_tempat')
                            ->where('id_tempat',$id)
                            ->update([
                                'status' => "off",
                            ]);
            DB::commit();

            return redirect()->back()->with('message-success', 'Berhasil Dihapus');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message', 'Data Gagal Dihapus');
        }
    }

}
