<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use App\Models\MUserModel;
use App\Models\MRoleModel;

class MCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$dataRole = DB::table('m_user')->where('role','!=',2)->get();
        return view("admin.master.customer.index");
    }

    public function detail($id)
    {
        $dataCustomer = DB::table('m_user')->where('id',$id)->first();
         $dataSales = DB::table('m_transaksi')
                    ->select('m_user.name as customer_name','m_transaksi.created_at as trx_date','m_transaksi.status_produk','m_transaksi.code','m_transaksi.expired_date','m_transaksi.id','m_produk.name as product_name','m_subdomain.name as subdomain','m_produk.masa_aktif','m_transaksi.nama_perusahaan')
                    //->leftjoin('m_user','m_user.id','t_sales_order.sales')
                    ->join('m_user','m_user.id','m_transaksi.id_user')
                    ->join('m_produk','m_produk.id','m_transaksi.id_produk')
                    ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
                    ->where('id_user','=', $id)
                    ->orderBy('m_transaksi.code','desc')
                    ->get();
        return view("admin.master.customer.detail",compact('dataCustomer','dataSales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$getMRole = DB::table('m_role')->where('name','!=','Customer')->get();
        return view('admin.master.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'username' => 'unique:m_user',
            'email' => 'required|email|unique:m_user',
            'role' => 'required',
            'password' => 'required',
        ]);

        //dd($request->all());
        //$roleSales  = MRoleModel::where('name', 'Sales')->first();
        $newSales = new MUserModel;
        $newSales->name = $request->nama;
        $newSales->username = $request->username;
        $newSales->email = $request->email;
        $newSales->address = $request->alamat;
        $newSales->birthdate = date('Y-m-d', strtotime($request->birthdate));
        $newSales->password =  bcrypt(str_replace(' ', '', $request->password));
        $newSales->role = $request->role;
        $newSales->status = "active";
        $newSales->save();

        return redirect('admin/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataUser = MUserModel::where('id', '=', $id)->first();
        //$getMRole = DB::table('m_role')->where('name','!=','Customer')->get();
        return view('admin.master.customer.update',compact('dataUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'email' => 'email|unique:m_user,email,'.$id,
            'role' => 'required',
            'username' => 'unique:m_user,username,'.$id,
        ]);

         $updateSales = MUserModel::where('id', '=', $id)->update([
            'name' => $request->nama,
            'address' => $request->alamat,
            'username' => $request->username,
            'email' => $request->email,
            'birthdate' => date('Y-m-d', strtotime($request->birthdate)),
            'role' => $request->role,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return redirect('admin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apiUser()
    {
        // $users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $user = DB::table('m_user')
        ->leftjoin('m_role','m_role.id','m_user.role')
        ->select('m_user.id','m_user.name','m_user.username','m_user.email','m_user.address','m_user.status')
        ->where('m_user.role','=', 2)
        ->get();

        return Datatables::of($user)
            ->editColumn('status', function($user)
                {
                    return ucfirst($user->status);
                })
            ->addColumn('action', function ($user) {
                // return '<a href="#edit-'.$supplier->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                return '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/customer/'.$user->id).'" class="btn btn-primary  btn-sm"><i class="fa fa-external-link" data-toggle="tooltip" data-placement="top" title="Detail Customer '.$user->name.'" ></i></a>'.

                            '&nbsp'.

                        '</td>'.
                    '</tr>'.
                '</table>';

            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
