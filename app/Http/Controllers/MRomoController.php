<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;
use File;

class MRomoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $romo = DB::table('m_romo')->where('status_romo','on')->orderBy('id_romo','DESC')->get();

        return view('admin.master.romo.index', compact('romo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.master.romo.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_romo' => 'required|max:100',
            'tempat_romo' => 'required',
            'tanggal_lahir' => 'required',
            'tempat_tahbisan_romo' => 'required',
            'tanggal_tahbisan' => 'required',
            'periode' => 'required',
            'tipe_romo' => 'required',
        ]);

        $tglmulai = substr($request->periode,0,10);
        $tglsampai = substr($request->periode,13,10);

        $tanggal_lahir = date('Y-m-d', strtotime($request->tanggal_lahir));
        $tanggal_tahbisan = date('Y-m-d', strtotime($request->tanggal_tahbisan));
        if($request->misa_perdana == ""){
            $misa_perdana = null;
        }else{
            $misa_perdana = date('Y-m-d', strtotime($request->misa_perdana));
        }

        $file   = $request->image;
        if(!empty($file)){
            $filename_path_bukti = date("ymdHis").$request->nama_romo.".jpg";
            $destinationPath = 'upload/romo';   
        }else{
            $filename_path_bukti = "no_image.png";
        }

       

        DB::beginTransaction();
        try{

            DB::table('m_romo')->insert([
                'nama_romo' => $request->nama_romo,
                'image_romo' => $filename_path_bukti,
                'tempat_lahir' => $request->tempat_romo,
                'tanggal_lahir' => $tanggal_lahir,
                'tempat_tahbisan_imam' => $request->tempat_tahbisan_romo,
                'tanggal_tahbisan_imam' => $tanggal_tahbisan,
                'awal_masa_tugas' => date('Y-m-d', strtotime($tglmulai)),
                'akhir_masa_tugas' => date('Y-m-d', strtotime($tglsampai)),
                'tipe' => $request->tipe_romo,
                'status' => $request->status_romo,
                'misa_perdana' => $misa_perdana,
                'desc_romo' => $request->desc_romo,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            if(!empty($file)){
                $file->move($destinationPath, $filename_path_bukti);
            }

            DB::commit();
            return redirect('admin/gereja/romo')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $dataRomo = DB::table('m_romo')->where('id_romo',$id)->first();
        return view('admin.master.romo.update',compact('dataRomo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_romo' => 'required|max:100',
            'tempat_romo' => 'required',
            'tanggal_lahir' => 'required',
            'tempat_tahbisan_romo' => 'required',
            'tanggal_tahbisan' => 'required',
            'periode' => 'required',
            'tipe_romo' => 'required',
        ]);

        $tglmulai = substr($request->periode,0,10);
        $tglsampai = substr($request->periode,13,10);

        $tanggal_lahir = date('Y-m-d', strtotime($request->tanggal_lahir));
        $tanggal_tahbisan = date('Y-m-d', strtotime($request->tanggal_tahbisan));
        if($request->misa_perdana == ""){
            $misa_perdana = null;
        }else{
            $misa_perdana = date('Y-m-d', strtotime($request->misa_perdana));
        }

        $file   = $request->image;
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->nama_romo.".jpg";
                $destinationPath = 'upload/romo';   
                $update_img = 2;
        }else{
            if($request->value_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/romo'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/romo'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/romo'; 
                $update_img = 1;
            }
        }

        

        DB::beginTransaction();
        try{

            DB::table('m_romo')
                ->where('id_romo',$id)
                ->update([
                'nama_romo' => $request->nama_romo,
                'image_romo' => $filename_path_bukti,
                'tempat_lahir' => $request->tempat_romo,
                'tanggal_lahir' => $tanggal_lahir,
                'tempat_tahbisan_imam' => $request->tempat_tahbisan_romo,
                'tanggal_tahbisan_imam' => $tanggal_tahbisan,
                'awal_masa_tugas' => date('Y-m-d', strtotime($tglmulai)),
                'akhir_masa_tugas' => date('Y-m-d', strtotime($tglsampai)),
                'tipe' => $request->tipe_romo,
                'status' => $request->status_romo,
                'misa_perdana' => $misa_perdana,
                'desc_romo' => $request->desc_romo,
            ]);

                if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }
                if($update_img == 2){
                    File::delete('upload/romo/'.$request->image_hidden);
                }

            DB::commit();
            return redirect('admin/gereja/romo')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{

            $delete = DB::table('m_romo')
                            ->where('id_romo',$id)
                            ->update([
                                'status_romo' => "off",
                            ]);
            DB::commit();

            return redirect()->back()->with('message-success', 'Berhasil Dihapus');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message', 'Data Gagal Dihapus');
        }
    }

}
