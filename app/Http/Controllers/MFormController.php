<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;
use File;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class MFormController extends Controller
{
	public function index($nama)
	{
        $form = DB::table('m_form')->where('tipe_form',$nama)->get();


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.master.form.index',compact('form','nama'));
	}


    public function create($nama)
    {


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.master.form.create',compact('nama'));
    }

    public function store(Request $request, $nama)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'periode' => 'required',
            'pdf' => 'required',
        ]);

        //dd($request->pdf);

        $file   = $request->image;
        if(!empty($file)){
            $filename_path_bukti = date("ymdHis").$request->nama.".jpg";
            $destinationPath = 'upload/image-form';   
        }else{
            $filename_path_bukti = "no_image.png";
        }

        $pdf   = $request->pdf;
        if(!empty($pdf)){
            $file_name_pdf = date("ymdHis").$request->nama.".pdf";
            $destinationPathPDF = 'upload/form';   
        }

         DB::beginTransaction();
            try{
                 DB::table('m_form')
                        ->insert([
                            'nama_form' => $request->nama,
                            'periode_form' => date('Y-m-01', strtotime($request->periode)),
                            'image_form' => $filename_path_bukti,
                            'file_form' => $file_name_pdf,
                            'tipe_form' => $nama,
                            'created_at' => date('Y-m-d H:i:s'),
                        ]);
                if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }

                if(!empty($pdf)){
                    $pdf->move($destinationPathPDF, $file_name_pdf);
                }

                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return redirect('admin/download/'.$nama);
    }

    

    public function destroy($id)
    {
        $form = DB::table('m_form')->where('id_form', $id)->first();

        DB::beginTransaction();
            try{
                 DB::table('m_form')
                        ->where('id_form', $id)
                        ->delete();

                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return redirect('admin/download/'.$form->tipe_form)->with('message-success', "Data Berhasil di hapus");
    }
       
}
?>