<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MKategoriProdukModel;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;

class MKategoriProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getDataKategori = MKategoriProdukModel::orderBy('id','DESC')->get();

        return view('admin.master.produk.kategori-produk.index',compact('getDataKategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.produk.kategori-produk.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required|max:50|unique:m_kategori_produk'
        ]);

        $file   = $request->image;
        //$file   = Input::file('image');
        if(!empty($file)){
            $filename_path_bukti = date("ymdHis").$request->nama.".jpg";
            $destinationPath = 'upload/kategori-produk'; 
        }else{
           $filename_path_bukti = "no_image.png";
        }

        $hero   = $request->hero;
        if(!empty($hero)){
                $heroname_path_bukti = date("ymdHis")."_hero_".$request->nama.".jpg";
                $destinationPathHero = 'upload/kategori-produk/hero';
        }else{
            $heroname_path_bukti = "no_image.png";
        }

        $request->merge([
            'nama'              => $request->nama,
            'kategori_tag'      => $request->kategori_tag,
            'title_hero'        => $request->title_hero,
            'desc_hero'         => $request->desc_hero,
            'kategori_image'    => $filename_path_bukti,
            'image_hero'        => $heroname_path_bukti,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s'),
            'created_by'        => Auth::user()->id,

        ]);

        DB::beginTransaction();
        try{

            MKategoriProdukModel::create($request->except('_token','_method','produk_image_hidden','image_hidden','hero_image_hidden','image_hidden_hero'));

            return redirect('admin/kategori-produk')->with('message-success', 'Data Berhasil Ditambah');

            DB::commit();
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataKategori = MKategoriProdukModel::find($id);
        return view('admin.master.produk.kategori-produk.update',compact('dataKategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama' => 'required|max:50|unique:m_kategori_produk,nama,'.$id
        ]);

        $file   = $request->image;
        //$file   = Input::file('image');
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->nama.".jpg";
                $destinationPath = 'upload/kategori-produk';   
                $update_img = 2;
        }else{
            if($request->produk_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/kategori-produk'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/kategori-produk'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/kategori-produk'; 
                $update_img = 1;
            }
        }

        $hero   = $request->hero;
        if(!empty($hero)){
                $heroname_path_bukti = date("ymdHis")."_hero_".$request->nama.".jpg";
                $destinationPathHero = 'upload/kategori-produk/hero';   
                $update_hero = 2;
        }else{
            if($request->hero_image_hidden == "false"){
                if($request->image_hidden_hero != "no_image.png"){
                    $heroname_path_bukti = "no_image.png";
                    $destinationPathHero = 'upload/kategori-produk/hero'; 
                    $update_hero = 2;   
                }else{
                    $heroname_path_bukti = $request->image_hidden_hero;
                    $destinationPathHero = 'upload/kategori-produk/hero'; 
                    $update_hero = 1;
                }
            }else{
                $heroname_path_bukti = $request->image_hidden_hero;
                $destinationPathHero = 'upload/kategori-produk/hero'; 
                $update_hero = 1;
            }
        }

        $request->merge([
            'nama'              => $request->nama,
            'kategori_tag'      => $request->kategori_tag,
            'title_hero'        => $request->title_hero,
            'desc_hero'         => $request->desc_hero,
            'kategori_image'    => $filename_path_bukti,
            'image_hero'        => $heroname_path_bukti,
            'updated_at'        => date('Y-m-d H:i:s'),
            'updated_by'        => Auth::user()->id,

        ]);

        DB::beginTransaction();
        try{

            MKategoriProdukModel::where('id',$id)->update($request->except('_token','_method','produk_image_hidden','image_hidden','hero_image_hidden','image_hidden_hero'));

            if(!empty($file)){
                $file->move($destinationPath, $filename_path_bukti);
            }
            if($update_img == 2){
                File::delete('upload/kategori-produk/'.$request->image_hidden);
            }

            if(!empty($hero)){
                $file->move($destinationPathHero, $heroname_path_bukti);
            }
            if($update_hero == 2){
                File::delete('upload/kategori-produk/hero/'.$request->image_hidden_hero);
            }

            DB::commit();
            return redirect('admin/kategori-produk')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cek = DB::table('m_produk')->where('id_kategori', $id)->count();
        if ($cek > 0) {
            return redirect()->back()->with('message', 'Data Tidak Bisa Dihapus Karena Sudah Dipakai Untuk Master Produk');
        }

        $delete = MKategoriProdukModel::where('id',$id)->delete();

        return redirect()->back()->with('message-success', 'Berhasil Dihapus');
    }

    public function apiKategori()
    {
        $Kategori = MKategoriProdukModel::orderBy('id','DESC')->get();

        return Datatables::of($Kategori)
        ->addColumn('action', function ($Kategori) {
            return '<a href="'.url('admin/kategori-produk/'.$Kategori->id.'/edit').'" data-toggle="tooltip" data-placement="top" title="Ubah" class="btn btn-warning pull-left btn-sm"><i class="fa fa-edit"></i></a>'.'&nbsp;'.
            '<a href="'.url('/admin/kategori-produk/delete/'.$Kategori->id).'" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>&nbsp;'.
            '<a href="#" onclick="detailFitur('.$Kategori->id.')" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal_fitur" data-placement="top" title="Show Kategori Fitur"><i class="fa fa-user"></i></a>';
            })

        ->addIndexColumn()
        ->rawColumns(['action'])
        ->make(true);
    }

    public function showKategoriFitur($id)
    {
        $kat_fitur = DB::table('m_kategori_fitur')->get();
        $kategori = DB::table('m_kategori_produk')->where('id', $id)->first();
        $det_kat = DB::table('d_kategori_produk')->where('id_kategori_produk', $id)->get();

        $result = [];
        $result['fitur'] = $kat_fitur;
        $result['det_kat'] = $det_kat;
        $result['kategori'] = $kategori;

        return Response::json($result);
    }

    public function saveKategoriFitur(Request $request)
    {
        $kat_fitur = DB::table('m_kategori_fitur')->get();
        $result = [];
        $cek = 0;
        foreach ($kat_fitur as $key => $value) {
            $nama = $value->name_kategori_fitur;
            if($request->$nama == "on"){
                $result[$cek]['id_kategori_fitur'] = $value->id;

                $cek++;
            }
        }

        DB::beginTransaction();
        try{
            DB::table('d_kategori_produk')->where('id_kategori_produk',$request->id_kategori_produk)->delete();

            if(count($result) > 0){
                foreach ($result as $key1 => $value1) {
                    DB::table('d_kategori_produk')
                        ->insert([
                            'id_kategori_produk' => $request->id_kategori_produk,
                            'id_kategori_fitur'  => $value1['id_kategori_fitur'],
                            'created_at'         => date('Y-m-d H:i:s'),
                            'updated_at'         => date('Y-m-d H:i:s'),
                            'created_by'         => Auth::user()->id,
                            'updated_by'         => Auth::user()->id,
                        ]);
                }
            }

            DB::commit();
            return redirect('admin/kategori-produk')->with('message-success', 'Data Berhasil Disimpan');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect('admin/kategori-produk')->with('message-error', 'Data Gagal Disimpan');
        }

        //return Response::json($result);
    }
}
