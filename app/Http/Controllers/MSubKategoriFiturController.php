<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MKategoriFiturModel;
use App\Models\MSubKategoriFiturModel;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use File;
use Storage;

class MSubKategoriFiturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $getDataKategori = MKategoriFiturModel::orderBy('id','DESC')->get();

        return view('admin.master.produk.subkategori-fitur.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = MKategoriFiturModel::orderBy('name_kategori_fitur')->get();
        return view('admin.master.produk.subkategori-fitur.create', compact('kategori'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file   = $request->image;
        //$file   = Input::file('image');
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->name_subkategori_fitur.".jpg";
                $destinationPath = 'upload/subkategori-fitur';   
                $update_img = 2;
            // }else{
            //     $filename_path_bukti = $request->image_hidden;
            //     $destinationPath = 'upload/kategori-fitur'; 
            //     $update_img = 1;
            // }
        }else{
            if($request->produk_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/subkategori-fitur'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/subkategori-fitur'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/subkategori-fitur'; 
                $update_img = 1;
            }
        }
        $this->validate($request,[
            'name_subkategori_fitur' => 'required|max:50|',
            'id_kategori_fitur' => 'required',
        ]);
        DB::beginTransaction();
        try{

            $request->merge([
                'name_subkategori_fitur'       => $request->name_subkategori_fitur,
                'id_kategori_fitur'            => $request->id_kategori_fitur,
                'youtube_video'                => $request->youtube_video,
                'image_subkategori_fitur'      => $filename_path_bukti,
                'created_at'                   => date('Y-m-d H:i:s'),
                'updated_at'                   => date('Y-m-d H:i:s'),
                'created_by'                   => Auth::user()->id,

            ]);

            MSubKategoriFiturModel::create($request->except('produk_image_hidden','image_hidden','image'));
            if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }
            DB::commit();

            return redirect('admin/subkategori-fitur')->with('message-success', 'Data Berhasil Ditambah');
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataSub = MSubKategoriFiturModel::find($id);
        $kategori = MKategoriFiturModel::orderBy('name_kategori_fitur')->get();
        return view('admin.master.produk.subkategori-fitur.update',compact('dataSub','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file   = $request->image;
        //$file   = Input::file('image');
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->name_subkategori_fitur.".jpg";
                $destinationPath = 'upload/subkategori-fitur';   
                $update_img = 2;
            // }else{
            //     $filename_path_bukti = $request->image_hidden;
            //     $destinationPath = 'upload/subkategori-fitur'; 
            //     $update_img = 1;
            // }
        }else{
            if($request->produk_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/subkategori-fitur'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/subkategori-fitur'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/subkategori-fitur'; 
                $update_img = 1;
            }
        }
        $this->validate($request,[
            'name_subkategori_fitur' => 'required|max:50',
            'id_kategori_fitur' => 'required',
        ]);
        DB::beginTransaction();
        try{
            $request->merge([
                'name_subkategori_fitur'       => $request->name_subkategori_fitur,
                'id_kategori_fitur'            => $request->id_kategori_fitur,
                'youtube_video'                => $request->youtube_video,
                'image_subkategori_fitur'      => $filename_path_bukti,
                'updated_at'                   => date('Y-m-d H:i:s'),
                'updated_by'                   => Auth::user()->id,
            ]);

            if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }
                if($update_img == 2){
                    File::delete('upload/subkategori-fitur/'.$request->image_hidden);
                }

            MSubKategoriFiturModel::where('id',$id)->update($request->except('_token','_method','produk_image_hidden','image_hidden','image'));

            DB::commit();

            return redirect('admin/subkategori-fitur')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cek = DB::table('m_subkategori_fitur')->where('id_kategori_fitur', $id)->count();
        if ($cek > 0) {
            return redirect()->back()->with('message', 'Data Tidak Bisa Dihapus Karena Sudah Dipakai Untuk Master Subkategori');
        }

        $delete = MKategoriFiturModel::where('id',$id)->delete();

        return redirect()->back()->with('message-success', 'Berhasil Dihapus');
    }

    public function apiSubKategori()
    {
        $SubDataKategori = MSubKategoriFiturModel::with('kategoriRelation')->orderBy('id','DESC')->get();
        //dd($SubDataKategori);

        return Datatables::of($SubDataKategori)
        ->addColumn('action', function ($SubDataKategori) {
            return '<a href="'.url('/admin/subkategori-fitur/'.$SubDataKategori->id.'/edit').'" data-toggle="tooltip" data-placement="top" title="Ubah" class="btn btn-warning pull-left btn-sm"><i class="fa fa-edit"></i></a>'.'&nbsp;'.
            '<a href="'.url('/admin/subkategori-fitur/delete/'.$SubDataKategori->id).'" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>';
            })
        ->editColumn('kategori',function($data){
            return  $data['kategoriRelation']['name_kategori_fitur'];
        })
        ->addIndexColumn()
        ->rawColumns(['action','kategori'])
        ->make(true);
    }
}
