<?php

namespace App\Http\Controllers;

use DB;
use carbon;
use Mail;
use Auth;
use Share;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SakramenCtrl extends Controller
{
	public function viewsakramenbaptis()
	{
        $slider = DB::table('m_banner')->where('page_banner','sakramen')->first();
        $bidang = DB::table('m_bidang')->get();
				return view('frontend.sakramenbaptis',compact('slider','bidang'));
	}
	public function viewsakramenkomuni()
	{
        $slider = DB::table('m_banner')->where('page_banner','sakramen')->first();
        $bidang = DB::table('m_bidang')->get();
				return view('frontend.sakramenkomuni',compact('slider','bidang'));
	}
	public function viewsakramenkrisma()
	{
        $slider = DB::table('m_banner')->where('page_banner','sakramen')->first();
        $bidang = DB::table('m_bidang')->get();
				return view('frontend.sakramenkrisma',compact('slider','bidang'));
	}
	public function viewsakramenpernikahan()
	{
        $slider = DB::table('m_banner')->where('page_banner','sakramen')->first();
        $bidang = DB::table('m_bidang')->get();
				return view('frontend.sakramenpernikahan',compact('slider','bidang'));
	}
}
