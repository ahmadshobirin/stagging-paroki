<?php

namespace App\Http\Controllers;

use DB;
use Response;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use File;
use Storage;
use Auth;


date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class MProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $getData = DB::table('m_produk')
        //     ->leftjoin('m_kategori_produk', 'm_kategori_produk.id', '=', 'm_produk.kategori')
        //     ->leftjoin('m_sub_kategori_produk', 'm_sub_kategori_produk.id', '=', 'm_produk.sub_kategori')
        //     ->leftjoin('m_merek_produk', 'm_merek_produk.id', '=', 'm_produk.merek')
        //     ->select('m_produk.id','m_produk.code','m_produk.name','m_produk.lebar','m_produk.panjang','m_produk.tinggi','m_produk.berat','m_kategori_produk.name as kategori','m_sub_kategori_produk.name as sub_kategori','m_merek_produk.name as merek','m_produk.harga_regular','m_produk.harga_sale','m_produk.ukuran','m_produk.satuan_ukuran','m_produk.class','m_produk.warna','m_produk.name_tampil')
        //     ->orderBy('m_produk.code', 'DESC')
        //     ->get();

            // foreach ($getData as $key) {
            //     $nama = $key->name;
            //     if($key->ukuran != '' || $key->ukuran != null ){
            //         if($key->satuan_ukuran != '' || $key->satuan_ukuran != null ){
            //             $nama = $nama.' UKURAN '.$key->ukuran.' '.$key->satuan_ukuran;
            //         }else{
            //             $nama = $nama.' UKURAN '.$key->ukuran;   
            //         }
            //     }
            //     if($key->merek != '' || $key->merek != null ){
            //         $nama = $nama.' MERK '.$key->merek;
            //     }
            //     if($key->class != '' || $key->class != null){
            //         $nama = $nama.' CLASS '.$key->class;
            //     }
            //     if($key->warna != '' || $key->warna != null ){
            //         $nama = $nama.' WARNA '.$key->warna;
            //     }

            //     $nama_produk_tampil = $nama;
            //     if($key->name_tampil == '' || $key->name_tampil == NULL){
            //         DB::table('m_produk')
            //             ->where('id',"=", $key->id)
            //             ->update([
            //                 "name_tampil"           => $nama_produk_tampil,
            //                 "updated_at"            => date('Y-m-d H:i:s'),
            //             ]);
            //     }
            // }

        $getData = DB::table('m_produk')
            ->leftjoin('m_kategori_produk','m_produk.id_kategori','m_kategori_produk.id')
            ->select('m_produk.*','m_kategori_produk.nama')
            ->where('m_produk.status','!=','inactive')
            ->orderBy('m_produk.id', 'DESC')
            ->get();

        // dd($getData);

        return view('admin.master.produk.index');
    }

    public function indexData()
    {
         $getData = DB::table('m_produk')
            ->leftjoin('m_kategori_produk','m_produk.id_kategori','m_kategori_produk.id')
            ->select('m_produk.*','m_kategori_produk.nama')
            ->where('m_produk.status','!=','inactive')
            ->orderBy('m_produk.id', 'DESC')
            ->get();

        return DataTables::make($getData)
                ->editColumn('masa_aktif', function($getData)
                {
                    if( $getData->masa_aktif == 0 ){
                        return "Selamanya";
                    }else{
                        return $getData->masa_aktif.' Bulan';
                    }
                })
                ->editColumn('harga', function($getData)
                {
                    if( $getData->masa_aktif == 0 && $getData->harga == 0 ){
                        return "-";
                    }else{
                        return 'Rp. '.number_format($getData->harga, 0,'.','.');
                    }
                })
                ->addColumn('action', function ($getData){
                return '<a href="'.url("/admin/produk/".$getData->id."/edit").'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a> &nbsp;
                        <a href="'.url("/admin/produk/delete/".$getData->id).'" class="btn btn-danger btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>';
            })->addIndexColumn()->rawColumns(['action'])->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $setCodeProduct = $this->setProdukCode();

        //$getMerek = MMerekProdukModel::get();
        $getKategori = DB::table('m_kategori_produk')->get();
        // $getSubKategori = MSubKategoriModel::get();
        //$satuanKemasan = MSatuanKemasanProdukModel::get();
        //$satuanUnit = MSatuanUnitModel::get();
        return view('admin.master.produk.create', compact('setCodeProduct', 'getKategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'max:20'
        ]);

        $setCodeProduct = $this->setProdukCode();
        $file   = $request->image;
        //$file   = Input::file('image');
        if(!empty($file)){
            $filename_path_bukti = date("ymdHis").$setCodeProduct.".jpg";
            $destinationPath = 'upload/produk-photo';   
        }else{
            $filename_path_bukti = "no_image.png";
        }

        //dd($file);

        //set value request code
        $request->merge(['code' => $setCodeProduct]);


        //dd($nama);
    DB::beginTransaction();
        try{

            DB::table('m_produk')
                ->insert([
                    "name"                  => $request->name,
                    "id_kategori"           => $request->kategori,
                    "fitur"                 => $request->fitur,
                    "respon_cs"             => $request->respon_cs,
                    "masa_aktif"            => $request->masa_aktif,
                    "periode_aktif"         => $request->periode_aktif,
                    "biaya_per_transaksi"   => $request->biaya_per_transaksi,
                    "harga"                 => $request->harga,
                    "jumlah_account"        => $request->jumlah_account,
                    "jumlah_produk"         => $request->jumlah_produk,
                    "image"                 => $filename_path_bukti,
                    "code"                  => $request->code,
                    "tagline_produk"        => $request->tagline,
                    "storage"               => $request->storage,
                    "created_by"            => Auth::user()->id,
                    "created_at"            => date('Y-m-d H:i:s'),
                    "updated_at"            => date('Y-m-d H:i:s'),
                ]);

           

            if(!empty($file)){
                $file->move($destinationPath, $filename_path_bukti);
            }
            
            DB::commit();
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
        }

        return redirect('admin/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataProduk = DB::table('m_produk')
            // ->leftjoin('m_kategori_produk', 'm_kategori_produk.id', '=', 'm_produk.kategori')
            // ->leftjoin('m_sub_kategori_produk', 'm_sub_kategori_produk.id', '=', 'm_produk.sub_kategori')
            // ->leftjoin('m_merek_produk', 'm_merek_produk.id', '=', 'm_produk.merek')
            ->select('m_produk.*'
            //     'm_kategori_produk.id as kategori_id','m_kategori_produk.name as kategori',
            // 'm_sub_kategori_produk.name as sub_kategori','m_sub_kategori_produk.id as sub_kategori_id','m_merek_produk.name as merek','m_merek_produk.id as merek_id'
            )
            ->where('m_produk.id',$id)
            ->first();
        // $getMerek = MMerekProdukModel::get();
        $getKategori = DB::table('m_kategori_produk')->get();
        // $getSubKategori = MSubKategoriModel::get();
        // $satuanKemasan = MSatuanKemasanProdukModel::get();
        // $satuanUnit = MSatuanUnitModel::get();

        return view('admin.master.produk.update', compact('dataProduk','getKategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file   = $request->image;
        //$file   = Input::file('image');
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->code_produk.".jpg";
                $destinationPath = 'upload/produk-photo';   
                $update_img = 2;
            // }else{
            //     $filename_path_bukti = $request->image_hidden;
            //     $destinationPath = 'upload/produk-photo'; 
            //     $update_img = 1;
            // }
        }else{
            if($request->produk_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/produk-photo'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/produk-photo'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/produk-photo'; 
                $update_img = 1;
            }
        }

        
        DB::beginTransaction();
        try{
            
                DB::table('m_produk')
                ->where('id',"=", $id)
                ->update([
                    "name"                  => $request->name,
                    "id_kategori"           => $request->kategori,
                    "fitur"                 => $request->fitur,
                    "respon_cs"             => $request->respon_cs,
                    "masa_aktif"            => $request->masa_aktif,
                    "periode_aktif"         => $request->periode_aktif,
                    "biaya_per_transaksi"   => $request->biaya_per_transaksi,
                    "harga"                 => $request->harga,
                    "jumlah_account"        => $request->jumlah_account,
                    "tagline_produk"        => $request->tagline,
                    "jumlah_produk"         => $request->jumlah_produk,
                    "image"                 => $filename_path_bukti,
                    "storage"               => $request->storage,
                    "updated_by"            => Auth::user()->id,
                    "updated_at"            => date('Y-m-d H:i:s'),
                ]);
            
            // $store                  = new MProdukModel;
            // $store->name            = $nama;
            // $store->kategori        = $request->kategori;
            // $store->sub_kategori    = $request->sub_kategori;
            // $store->merek           = $request->merek;
            // $store->class           = $request->class;
            // $store->ukuran          = $request->ukuran;
            // $store->warna           = $request->warna;
            // $store->berat           = $request->berat;
            // $store->harga_regular   = $request->harga;
            // $store->harga_sale      = $request->harga_sale;
            // $store->date_start_sale = date('Y-m-d', strtotime($tglmulai));
            // $store->date_end_sale   = date('Y-m-d', strtotime($tglsampai));
            // $store->min_order       = $request->min_order;
            // if($update_img > 0){
            //     $store->image           = $filename_path_bukti;
            // }
            // $store->satuan_harga    = $request->satuan_harga;
            // $store->satuan_kemasan  = $request->satuan_kemasan;
            // $store->deskripsi       = $request->desc_barang;
            // $store->satuan_berat    = $request->satuan_berat;
            // $store->satuan_ukuran   = $request->satuan_ukuran;
            // $store->updated_at      = date("Y-m-d H:i:s");
            // // $store->gh_code = $request->gh_code;
            // // $store->date_start = date('Y-m-d', strtotime($tglmulai));
            // // $store->date_end = date('Y-m-d', strtotime($tglsampai));
            // // $store->price = $request->price;
            // $store->save();

            if(!empty($file)){
                $file->move($destinationPath, $filename_path_bukti);
            }
            if($update_img == 2){
                File::delete('upload/produk-photo/'.$request->image_hidden);
            }
            DB::commit();
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
        }
        //MProdukModel::where('id',$id)->update($request->except('_token','_method'));

        return redirect('admin/produk');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $cek = DB::table('d_sales_order')->where('produk', '=', $id)->count();
        // if ($cek > 0) {
        //     return redirect()->back()->with('message', 'Data Tidak Bisa Dihapus Karena Dipakai Untuk Transaksi');
        // }

        DB::table('m_produk')
                ->where('id', '=', $id)
                ->update([
                    'status'        => 'inactive',
                    'updated_by'    => Auth::user()->id,
                    "updated_at"    => date('Y-m-d H:i:s'),
                ]);
        return redirect()->back()->with('message-success', 'Data Berhasil Dihapus');
    }

    public function history($id)
    {
        $history = DB::table('m_harga_produk')
                    ->select('m_harga_produk.*','m_user.username')
                    ->join('m_user','m_user.id','=','m_harga_produk.created_by')
                    ->where('produk', '=', $id)
                    ->get();
        $produk = DB::table('m_produk')->select('name_tampil')->where('id', '=', $id)->first();
        $name_produk = $produk->name_tampil;

        return view('admin.produk.history', compact('history','name_produk'));
    }

    protected function setProdukCode()
    {
        $getLastCode = DB::table('m_produk')
                ->select('id')
                ->orderBy('id', 'desc')
                ->pluck('id')
                ->first();
        $getLastCode = $getLastCode +1;

        $nol = null;
        if(strlen($getLastCode) == 1){
            $nol = "000";
        }elseif(strlen($getLastCode) == 2){
            $nol = "00";
        }elseif(strlen($getLastCode) == 3){
            $nol = "0";
        }else{
            $nol = null;
        }

        // return $setCodeProduct = 'PRD'.$nol.$getLastCode;
        return $setCodeProduct = 'BZTPRD'.$nol.$getLastCode;

    }

    public function getSubKategorProdukByKategori($id)
    {
        $result = MSubKategoriModel::where('kategori',$id)->get();

        return Response::json($result);
    }


    public function apiProduk()
    {
        // $users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $produk = DB::table('m_produk')
        ->leftjoin('m_kategori_produk','m_kategori_produk.id','m_produk.kategori')
        ->leftjoin('m_sub_kategori_produk','m_sub_kategori_produk.id','m_produk.sub_kategori')
         ->leftjoin('m_merek_produk','m_merek_produk.id','m_produk.merek')
        ->select('m_produk.id as produk_id','m_produk.code','m_produk.name','m_kategori_produk.name as kategori','m_sub_kategori_produk.name as sub_kat','m_merek_produk.name as merek',
         'm_produk.lebar','m_produk.panjang','m_produk.tinggi','m_produk.berat','m_produk.harga_regular','m_produk.harga_sale','m_produk.ukuran','m_produk.satuan_ukuran','m_produk.warna','m_produk.class','m_produk.date_start_sale','m_produk.date_end_sale','m_produk.name_tampil')
        ->orderBy('m_produk.id','desc')
        ->get();

        foreach ($produk as $key) {
            $nama = $key->name;
            if($key->ukuran != "" || $key->ukuran != null ){
                $nama = $nama." UKURAN ".$key->ukuran." ".$key->satuan_ukuran;
            }
            if($key->merek != "" || $key->merek != null ){
                $nama = $nama." MERK ".$key->merek;
            }
            if($key->class != "" || $key->class != null){
                $nama = $nama." CLASS ".$key->class;
            }
            if($key->warna != "" || $key->warna != null ){
                $nama = $nama." WARNA ".$key->warna;   
            }
            $key->nama_tampil = $key->name_tampil;
            $key->harga_regular_tampil = "Rp. ".number_format($key->harga_regular, 0, ".",".");
            $key->harga_sale_tampil = "Rp. ".number_format($key->harga_sale, 0, ".",".");
            if($key->date_start_sale != null && $key->date_end_sale != null){
                $key->periode_sale_tampil = date('d-m-Y', strtotime($key->date_start_sale))."-".date('d-m-Y', strtotime($key->date_end_sale));
            }else{
                $key->periode_sale_tampil = "-";
            }
        }

        return Datatables::of($produk)
            ->addColumn('action', function ($produk) {
                // return '<a href="#edit-'.$supplier->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
                return '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/produk/'.$produk->produk_id.'/edit').'" data-toggle="tooltip" data-placement="top" title="Ubah" class="btn btn-warning pull-left btn-sm"><i class="fa fa-edit"></i></a>'.

                            '&nbsp'.

                            '<a href="'.url('/admin/produk-delete/'.$produk->produk_id).'" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')" data-toggle="tooltip" data-placement="top" title="Hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>'.

                            '&nbsp'.

                            '<a href="'.url('/admin/produk-history-price/'.$produk->produk_id).'" data-toggle="tooltip" data-placement="top" title="Lihat History Harga" class="btn btn-success btn-sm"><i class="fa fa-money"></i></a>'.

                            '&nbsp'.

                        '</td>'.
                    '</tr>'.
                '</table>';

            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
