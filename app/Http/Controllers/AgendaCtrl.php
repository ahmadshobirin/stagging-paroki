<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class AgendaCtrl extends Controller
{
	public function indexAgenda()
	{
        $type =  "index";
        $periode =  date('Y-m-d 00:00:00');
        $slider = DB::table('m_banner')->where('page_banner','agenda')->first();
        
        $agenda = DB::table('m_agenda')
                    ->select('date_agenda','hari_libur')
                    ->whereDate('date_agenda','>=', date(today()))
                    ->whereMonth('date_agenda',date('m', strtotime($periode)))
                    ->whereYear('date_agenda',date('Y', strtotime($periode)))
                    ->where('tempat_menikah','!=', 2)
                    ->orderBy('date_agenda','asc')
                    ->groupBy('date_agenda','hari_libur')
                    ->get();
        foreach ($agenda as $key_agenda => $value_agenda) {
            $agenda_det = DB::table('m_agenda')
                    ->select('m_agenda.*','m_tempat.nama_tempat')
                    ->leftjoin('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                    ->where('date_agenda', $value_agenda->date_agenda)
                    ->where('tempat_menikah','!=', 2)
                    ->orderBy('waktu_agenda','asc')
                    ->get();
            
            if(count($agenda_det) > 0){
                $value_agenda->detail_agenda = $agenda_det;
            }else{
                unset($agenda[$key_agenda]);
            }

        }

        $bread = "Kegiatan Gereja";

        //dd($agenda);


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('frontend.event',compact('agenda','slider','bread','type'));
	}

    public function filterAgenda(Request $request){
        $type =  "filter";
        $tglmulai = substr($request->periode,0,10);
        $tglsampai = substr($request->periode,13,10);
        $bulan = $request->month;

        $cek_awal = date('Y-m-d', strtotime($tglmulai));
        $cek_akhir = date('Y-m-d', strtotime($tglsampai));
        $slider = DB::table('m_banner')->where('page_banner','agenda')->first();
        
        $agenda = DB::table('m_agenda')
                    ->select('date_agenda','hari_libur')
                    ->where('date_agenda','>=',$cek_awal)
                    ->where('date_agenda','<=',$cek_akhir)
                    ->where('tempat_menikah','!=', 2)
                    ->orderBy('date_agenda','asc')
                    ->groupBy('date_agenda','hari_libur')
                    ->get();
        
        foreach ($agenda as $key_agenda => $value_agenda) {
            $agenda_det = DB::table('m_agenda')
                    ->select('m_agenda.*','m_tempat.nama_tempat')
                    ->leftjoin('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                    ->where('date_agenda', $value_agenda->date_agenda)
                    ->where('tempat_menikah','!=', 2)
                    ->orderBy('waktu_agenda','asc')
                    ->get();
            
            if(count($agenda_det) > 0){
                $value_agenda->detail_agenda = $agenda_det;
            }else{
                unset($agenda[$key_agenda]);
            }

        }

        $bread = "Kegiatan Gereja";

        //dd($agenda);


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('frontend.event',compact('agenda','slider','bread','type','tglmulai','tglsampai','bulan'));
    }


    public function indexNikah()
    {
        $type =  "index";
        $periode =  date('Y-m-d 00:00:00');
        $slider = DB::table('m_banner')->where('page_banner','agenda')->first();
        
        $agenda = DB::table('m_agenda')
                    ->select('date_agenda','hari_libur')
                    ->whereMonth('date_agenda',date('m', strtotime($periode)))
                    ->whereYear('date_agenda',date('Y', strtotime($periode)))
                    ->orderBy('date_agenda','asc')
                    ->groupBy('date_agenda','hari_libur')
                    ->get();
        foreach ($agenda as $key_agenda => $value_agenda) {
            $agenda_det = DB::table('m_agenda')
                    ->select('m_agenda.*')
                    ->where('date_agenda', $value_agenda->date_agenda)
                    ->where('type_agenda', 'pernikahan')
                    ->orderBy('waktu_agenda','asc')
                    ->get();

            if(count($agenda_det) > 0){
                $value_agenda->detail_agenda = $agenda_det;
            }else{
                unset($agenda[$key_agenda]);
            }
        }


        $bread = "Pernikahan";

        //dd($agenda);


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('frontend.event',compact('agenda','slider','bread','type'));
    }

    public function filterNikah(Request $request){
        $type =  "filter";
        $tglmulai = substr($request->periode,0,10);
        $tglsampai = substr($request->periode,13,10);
        $bulan = $request->month;

        $cek_awal = date('Y-m-d', strtotime($tglmulai));
        $cek_akhir = date('Y-m-d', strtotime($tglsampai));

        $slider = DB::table('m_banner')->where('page_banner','agenda')->first();
        
        $agenda = DB::table('m_agenda')
                    ->select('date_agenda','hari_libur')
                    ->where('date_agenda','>=',$cek_awal)
                    ->where('date_agenda','<=',$cek_akhir)
                    ->orderBy('date_agenda','asc')
                    ->groupBy('date_agenda','hari_libur')
                    ->get();
        foreach ($agenda as $key_agenda => $value_agenda) {
            $agenda_det = DB::table('m_agenda')
                    ->select('m_agenda.*')
                    ->where('date_agenda', $value_agenda->date_agenda)
                    ->where('type_agenda', 'pernikahan')
                    ->orderBy('waktu_agenda','asc')
                    ->get();

            if(count($agenda_det) > 0){
                $value_agenda->detail_agenda = $agenda_det;
            }else{
                unset($agenda[$key_agenda]);
            }
        }


        $bread = "Pernikahan";
        

        //dd($agenda);


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('frontend.event',compact('agenda','slider','bread','type','tglmulai','tglsampai','bulan'));
    }

       
}
?>