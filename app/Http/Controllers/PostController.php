<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use DataTables;
use App\Http\Controllers\Controller;
use File;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class PostController extends Controller
{
	public function indexApprovalInfo(){
       
        return view('admin.approval.index_info');
	}

    public function editApprovalInfo($id){
        $type = "edit";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
   
        return view('admin.approval.edit_info', compact('cek_post','type'));
    }

    public function showApprovalInfo($id){
        $type = "show";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        //dd($type);
   
        return view('admin.approval.edit_info', compact('cek_post','type'));
    }

    public function updateApprovalInfo(Request $request, $id){
        $this->validate($request,[
            'info_publish' => 'required|max:500'
        ]);

        DB::beginTransaction();
            try{
                 DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'isi_post' => $request->info_publish,
                    ]);


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/approval/info');
    }

    public function approveApprovalInfo($id){

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post != "in approval"){
                return redirect()->back()->with('message','Tidak Bisa Menyetujui Info');
        }else{
                DB::beginTransaction();
                        try{
                                DB::table('m_posting')
                                    ->where('id_posting',$id)
                                    ->update([
                                        'status_post' => "approved",
                                        'approved_by' => Auth::user()->id,
                                    ]);
                                DB::commit();

                        }catch(\Exception $e) {
                            $success = false;
                            DB::rollback();
                            dd($e);  
                        }
                return redirect('admin/approval/info');
        }
   
    }

    public function deleteApprovalInfo($id){
   
        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post == "in approval" ||  $cek_post->status_post == "approved"){
                DB::beginTransaction();
                        try{
                                DB::table('m_posting')
                                    ->where('id_posting',$id)
                                    ->update([
                                        'status_post' => "deleted",
                                    ]);
                                DB::commit();

                        }catch(\Exception $e) {
                            $success = false;
                            DB::rollback();
                            dd($e);  
                        }
                return redirect('admin/approval/info');
        }else{
                return redirect()->back()->with('message','Tidak Bisa Menghapus Info');
                
        }
    }



    public function apiApprovalInfo(){
        //$users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $post = DB::table('m_posting')
        ->select('m_posting.*','m_user.name','m_bidang.nama_bidang as bidang')
        ->join('m_user','m_user.id','m_posting.id_user')
        ->join('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
        ->where('type_post','info')
        ->whereIn('status_post',['in approval','approved'])
        ->orderBy('id_posting','desc');

        return Datatables::of($post)
            ->addColumn('action', function ($post) {
                if($post->status_post == "in approval"){
                        $hasil = '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/approval/info/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                            '&nbsp'.
                            '<a href="'.url('/admin/approval/info/'.$post->id_posting.'/approve').'" class="btn btn-success  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menyetujui ?'".')"><i class="fa fa-check"></i></a>'.
                            '&nbsp'.
                            '<a href="'.url('/admin/approval/info/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                        '</td>'.
                    '</tr>'.
                '</table>';
                }else if($post->status_post == "approved"){
                        $hasil = '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/approval/info/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                            '&nbsp'.
                            '<a href="'.url('/admin/approval/info/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                        '</td>'.
                    '</tr>'.
                '</table>';
                }
                return $hasil;

            })
            ->editColumn('created_at', function($post){
                return date('d M Y', strtotime($post->created_at));
            }) 
            ->editColumn('status_post', function($post){
                return ucwords($post->status_post);
            })
            ->editColumn('title_post', function($post){
                $cek = '<a href="'.url('/admin/approval/info/'.$post->id_posting.'/show').'">'.$post->title_post.'</a>';
                return $cek;
            })
            ->addIndexColumn()
            ->rawColumns(['action','created_at','status_post','title_post'])
            ->make(true);
    }

    public function indexInfo(){
       
        return view('admin.posting.info.index');
    }

    public function createInfo(){
       
        return view('admin.posting.info.create');
    }

     public function storeInfo(Request $request){
        $this->validate($request,[
            'info_publish' => 'required|max:500',
            'title' => 'required|max:100',
        ]);

        DB::beginTransaction();
            try{
                if(Auth::user()->role < 3){
                    DB::table('m_posting')
                        ->insert([
                            'isi_post' => $request->info_publish,
                            'title_post' => $request->title,
                            'id_bidang' => 0,
                            'id_user' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_post' => 'info',
                            'status_post' => 'approved',
                            'published' => 1,
                            'approved_by' => Auth::user()->id,
                        ]);
                }else{
                    $cek_kat_user = DB::table('m_kategori_user')->where('id_kategori_user', Auth::user()->id_header_user)->first();
                    $cek_bidang = DB::table('m_bidang')->where('id_bidang', $cek_kat_user->id_bidang)->first();
                    DB::table('m_posting')
                        ->insert([
                            'isi_post' => $request->info_publish,
                            'title_post' => $request->title,
                            'id_bidang' => $cek_bidang->id_bidang,
                            'id_user' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_post' => 'info',
                            'status_post' => 'draft',
                            'published' => 1,
                        ]);
                }


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/info');
    }


    public function editInfo($id){

        $type = "edit";
        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
   
        return view('admin.posting.info.update', compact('cek_post','type'));
    }

    public function showInfo($id){

        $type = "show";
        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
   
        return view('admin.posting.info.update', compact('cek_post','type'));
    }

    public function updateInfo(Request $request, $id){
        $this->validate($request,[
            'info_publish' => 'required|max:500',
            'title' => 'required|max:100',
        ]);

        DB::beginTransaction();
            try{
                 DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'isi_post' => $request->info_publish,
                        'title_post' => $request->title,
                    ]);


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/info');
    }

    public function approvalInfo($id){
        DB::beginTransaction();
            try{
                 DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'status_post' => 'in approval',
                    ]);


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/info');
    }

    public function deleteInfo($id){
   
        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post == "in approval" ||  $cek_post->status_post == "approved"){
                DB::beginTransaction();
                        try{
                                DB::table('m_posting')
                                    ->where('id_posting',$id)
                                    ->update([
                                        'status_post' => "deleted",
                                    ]);
                                DB::commit();

                        }catch(\Exception $e) {
                            $success = false;
                            DB::rollback();
                            dd($e);  
                        }
                return redirect('admin/info');
        }else{
                return redirect()->back()->with('message','Tidak Bisa Menghapus Info');
                
        }
    }



    public function apiInfo(){
        //$users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $post = DB::table('m_posting')
        ->select('m_posting.*','m_user.name')
        ->join('m_user','m_user.id','m_posting.id_user')
        ->where('type_post','info')
        ->where('status_post','!=','deleted')
        ->where('m_posting.id_user', Auth::user()->id)
        ->orderBy('id_posting','desc');

        return Datatables::of($post)
            ->addColumn('action', function ($post) {
                if(Auth::user()->role < 3){
                    $hasil = '<table id="tabel-in-opsi">'.
                            '<tr>'.
                                '<td>'.
                                    '<a href="'.url('/admin/info/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                                    '&nbsp'.
                                    '<a href="'.url('/admin/info/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                                '</td>'.
                            '</tr>'.
                        '</table>';
                }else{
                    if($post->status_post == "draft"){
                            $hasil = '<table id="tabel-in-opsi">'.
                        '<tr>'.
                            '<td>'.
                                '<a href="'.url('/admin/info/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                                '&nbsp'.
                                '<a href="'.url('/admin/info/'.$post->id_posting.'/send-approval').'" class="btn btn-success  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Memposting ?'".')"><i class="fa fa-paper-plane"></i></a>'.
                                '&nbsp'.
                                '<a href="'.url('/admin/info/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm"  onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                            '</td>'.
                        '</tr>'.
                    '</table>';
                    }else{
                        $hasil = '<table id="tabel-in-opsi">'.
                            '<tr>'.
                                '<a href="'.url('/admin/info/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm"><i class="fa fa-trash" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"></i></a>'.

                                '</td>'.
                            '</tr>'.
                        '</table>';
                    }

                }
                return $hasil;

            })
            ->editColumn('created_at', function($post){
                return date('d M Y', strtotime($post->created_at));
            })
            ->editColumn('status_post', function($post){
                return ucwords($post->status_post);
            })
            ->editColumn('title_post', function($post){
                $cek = '<a href="'.url('/admin/info/'.$post->id_posting.'/show').'">'.$post->title_post.'</a>';
                return $cek;
            })
            ->addIndexColumn()
            ->rawColumns(['action','created_at','status_post','title_post'])
            ->make(true);
    }

    public function indexApprovalArtikel(){
   
        return view('admin.approval.index_artikel');
    }

    public function editApprovalArtikel($id){
        $type = "edit";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
   
        return view('admin.approval.edit_artikel', compact('cek_post','type'));
    }

     public function showApprovalArtikel($id){

        $type = "show";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        //dd($type);
   
        return view('admin.approval.edit_artikel', compact('cek_post','type'));
    }

    public function updateApprovalArtikel(Request $request, $id){

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
        
        if($cek_post->published == 1){
            $this->validate($request,[
                'info_publish' => 'required|max:500',
                'artikel' => 'required',
            ]);

            DB::beginTransaction();
            try{
                 DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'isi_publish_post' => $request->info_publish,
                        'isi_post' => $request->artikel,
                    ]);

                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }
        }else{
            $this->validate($request,[
                'artikel' => 'required',
            ]);

            DB::beginTransaction();
            try{
                 DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'isi_post' => $request->artikel,
                    ]);


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }
        }

        return redirect('admin/approval/artikel');
    }

    public function approveApprovalArtikel($id){

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post != "in approval"){
                return redirect()->back()->with('message','Tidak Bisa Menyetujui Artikel');
        }else{
                DB::beginTransaction();
                        try{
                                DB::table('m_posting')
                                    ->where('id_posting',$id)
                                    ->update([
                                        'status_post' => "approved",
                                        'approved_by' => Auth::user()->id,
                                    ]);
                                DB::commit();

                        }catch(\Exception $e) {
                            $success = false;
                            DB::rollback();
                            dd($e);  
                        }
                return redirect('admin/approval/artikel');
        }
   
    }

    public function deleteApprovalArtikel($id){
   
        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post != "deleted"){
                DB::beginTransaction();
                        try{
                                DB::table('m_posting')
                                    ->where('id_posting',$id)
                                    ->update([
                                        'status_post' => "deleted",
                                    ]);
                                DB::commit();

                        }catch(\Exception $e) {
                            $success = false;
                            DB::rollback();
                            dd($e);  
                        }
                return redirect('admin/approval/artikel');
        }else{
                return redirect()->back()->with('message','Tidak Bisa Menyetujui Info');
                
        }
    }



    public function apiApprovalArtikel(){
        //$users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $post = DB::table('m_posting')
        ->select('m_posting.*','m_user.name','m_bidang.nama_bidang as bidang')
        ->join('m_user','m_user.id','m_posting.id_user')
        ->join('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
        ->where('type_post','artikel')
        ->whereIn('status_post',['in approval','approved'])
        ->orderBy('id_posting','desc');

        return Datatables::of($post)
            ->addColumn('action', function ($post) {
                if($post->status_post == "in approval"){
                        $hasil = '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/approval/artikel/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                            '&nbsp'.
                            '<a href="'.url('/admin/approval/artikel/'.$post->id_posting.'/approve').'" class="btn btn-success  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menyetujui ?'".')"><i class="fa fa-check"></i></a>'.
                            '&nbsp'.
                            '<a href="'.url('/admin/approval/artikel/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                        '</td>'.
                    '</tr>'.
                '</table>';
                }else if($post->status_post == "approved"){
                        $hasil = '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/approval/artikel/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                            '&nbsp'.
                            '<a href="'.url('/admin/approval/artikel/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                        '</td>'.
                    '</tr>'.
                '</table>';
                }
                return $hasil;

            })
            ->editColumn('created_at', function($post){
                return date('d M Y', strtotime($post->created_at));
            }) 
            ->editColumn('status_post', function($post){
                return ucwords($post->status_post);
            })
            ->editColumn('isi_publish_post', function($post){
                return html_entity_decode($post->isi_publish_post);
            })
            ->editColumn('title_post', function($post){
                $cek = '<a href="'.url('/admin/approval/artikel/'.$post->id_posting.'/show').'">'.$post->title_post.'</a>';
                return $cek;
            })
            ->addIndexColumn()
            ->rawColumns(['action','created_at','status_post','title_post','isi_publish_post'])
            ->make(true);
    }

    public function indexArtikel(){
       
        return view('admin.posting.artikel.index');
    }

    public function createArtikel(){
       
        return view('admin.posting.artikel.create');
    }

     public function storeArtikel(Request $request){
        
        if($request->publish == "on"){
            $this->validate($request,[
                'info_publish' => 'required|max:500',
                'title' => 'required|max:100',
                'artikel' => 'required',
            ]);
        }else{
            $this->validate($request,[
                'title' => 'required|max:100',
                'artikel' => 'required',
            ]);
        }

        $file   = $request->image;
        if(!empty($file)){
            $filename_path_bukti = date("ymdHis").$request->title.".jpg";
            $destinationPath = 'upload/artikel';   
        }else{
            $filename_path_bukti = "no_image.png";
        }

        DB::beginTransaction();
            try{
                $cek_kat_user = DB::table('m_kategori_user')->where('id_kategori_user', Auth::user()->id_header_user)->first();
                $cek_bidang = DB::table('m_bidang')->where('id_bidang', $cek_kat_user->id_bidang)->first();
                if($request->publish == "on"){
                    DB::table('m_posting')
                        ->insert([
                            'isi_publish_post' => $request->info_publish,
                            'isi_post' => $request->artikel,
                            'title_post' => $request->title,
                            'id_bidang' => $cek_bidang->id_bidang,
                            'id_user' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_post' => 'artikel',
                            'status_post' => 'draft',
                            'published' => 1,
                            'image_post' => $filename_path_bukti,
                        ]);
                }else{
                   
                    DB::table('m_posting')
                        ->insert([
                            'isi_post' => $request->artikel,
                            'title_post' => $request->title,
                            'id_bidang' => $cek_bidang->id_bidang,
                            'id_user' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_post' => 'artikel',
                            'status_post' => 'draft',
                            'published' => 0,
                            'image_post' => $filename_path_bukti,
                        ]);
                }

                if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/artikel');
    }


    public function editArtikel($id){

        $type = "edit";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
   
        return view('admin.posting.artikel.update', compact('cek_post','type'));
    }

     public function showArtikel($id){

        $type = "show";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
   
        return view('admin.posting.artikel.update', compact('cek_post','type'));
    }

    public function updateArtikel(Request $request, $id){
        if($request->publish == "on"){
            $this->validate($request,[
                'info_publish' => 'required|max:500',
                'title' => 'required|max:100',
                'artikel' => 'required',
            ]);
        }else{
            $this->validate($request,[
                'title' => 'required|max:100',
                'artikel' => 'required',
            ]);
        }

        $file   = $request->image;
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->title.".jpg";
                $destinationPath = 'upload/artikel';   
                $update_img = 2;
        }else{
            if($request->value_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/artikel'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/artikel'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/artikel'; 
                $update_img = 1;
            }
        }

        //dd($request);

        DB::beginTransaction();
            try{
                // $cek_kat_user = DB::table('m_kategori_user')->where('id_kategori_user', Auth::user()->id_header_user)->first();
                // $cek_bidang = DB::table('m_bidang')->where('id_bidang', $cek_kat_user->id_bidang)->first();
                if($request->publish == "on"){
                    DB::table('m_posting')
                        ->where('id_posting',$id)
                        ->update([
                            'isi_publish_post' => $request->info_publish,
                            'isi_post' => $request->artikel,
                            'title_post' => $request->title,
                            'published' => 1,
                            'image_post' => $filename_path_bukti,
                        ]);
                }else{
                   
                    DB::table('m_posting')
                        ->where('id_posting',$id)
                        ->update([
                            'isi_post' => $request->artikel,
                            'title_post' => $request->title,
                            'published' => 0,
                            'image_post' => $filename_path_bukti,
                        ]);
                }

                if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }
                if($update_img == 2){
                    File::delete('upload/artikel/'.$request->image_hidden);
                }



                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/artikel');
    }

    public function approvalArtikel($id){
        DB::beginTransaction();
            try{
                 DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'status_post' => 'in approval',
                    ]);


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/artikel');
    }

    public function deleteArtikel($id){
   
        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post == "in approval" ||  $cek_post->status_post == "approved"){
                DB::beginTransaction();
                        try{
                                DB::table('m_posting')
                                    ->where('id_posting',$id)
                                    ->update([
                                        'status_post' => "deleted",
                                    ]);
                                DB::commit();

                        }catch(\Exception $e) {
                            $success = false;
                            DB::rollback();
                            dd($e);  
                        }
                return redirect('admin/artikel');
        }else{
                return redirect()->back()->with('message','Tidak Bisa Menghapus Artikel');
                
        }
    }



    public function apiArtikel(){
        //$users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $post = DB::table('m_posting')
        ->select('m_posting.*','m_user.name')
        ->join('m_user','m_user.id','m_posting.id_user')
        ->where('type_post','artikel')
        ->where('status_post','!=','deleted')
        ->where('m_posting.id_user', Auth::user()->id)
        ->orderBy('id_posting','desc');

        return Datatables::of($post)
            ->addColumn('action', function ($post) {
                if(Auth::user()->role < 3){
                    $hasil = '<table id="tabel-in-opsi">'.
                            '<tr>'.
                                '<td>'.
                                    '<a href="'.url('/admin/artikel/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                                    '&nbsp'.
                                    '<a href="'.url('/admin/artikel/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                                '</td>'.
                            '</tr>'.
                        '</table>';
                }else{
                    if($post->status_post == "draft"){
                            $hasil = '<table id="tabel-in-opsi">'.
                        '<tr>'.
                            '<td>'.
                                '<a href="'.url('/admin/artikel/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                                '&nbsp'.
                                '<a href="'.url('/admin/artikel/'.$post->id_posting.'/send-approval').'" class="btn btn-success  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Memposting ?'".')"><i class="fa fa-paper-plane"></i></a>'.
                                '&nbsp'.
                                '<a href="'.url('/admin/artikel/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                            '</td>'.
                        '</tr>'.
                    '</table>';
                    }else{
                        $hasil = '<table id="tabel-in-opsi">'.
                            '<tr>'.
                                '<a href="'.url('/admin/artikel/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                                '</td>'.
                            '</tr>'.
                        '</table>';
                    }

                }
                return $hasil;

            })
            ->editColumn('created_at', function($post){
                return date('d M Y', strtotime($post->created_at));
            }) 
            ->editColumn('title_post', function($post){
                $cek = '<a href="'.url('/admin/artikel/'.$post->id_posting.'/show').'">'.$post->title_post.'</a>';
                return $cek;
            })
            ->editColumn('isi_publish_post', function($post){
                return html_entity_decode($post->isi_publish_post);
            })
            ->editColumn('status_post', function($post){
                return ucwords($post->status_post);
            })
            ->addIndexColumn()
            ->rawColumns(['action','created_at','title_post','isi_publish_post','status_post'])
            ->make(true);
    }

    public function indexApprovalKronik(){
   
        return view('admin.approval.index_kronik');
    }

    public function editApprovalKronik($id){

        $type = "edit";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
        $user = DB::table('m_user')->select('id_header_user')->where('id', $cek_post->id_user)->first();
        $bidang = DB::table('m_kategori_user')->select('id_bidang')->where('id_kategori_user', $user->id_header_user)->first();
        $agenda = DB::table('m_agenda')->where('id_bidang', $bidang->id_bidang)->get();
        
        foreach ($agenda as $key => $value) {
            $m_posting = DB::table('m_posting')->where('id_posting','!=',$id)->where('status_post','!=','deleted')->where('id_agenda',$value->id_agenda)->get();
            if(count($m_posting) > 0){
                unset($agenda[$key]);
            }
        }

        $agenda = array_values($agenda->toArray());
        //dd($cek_post);
        //dd($agenda);
   
        return view('admin.approval.edit_kronik', compact('cek_post','agenda','type'));
    }

    public function showApprovalKronik($id){

        $type = "show";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();
        $user = DB::table('m_user')->select('id_header_user')->where('id', $cek_post->id_user)->first();
        $bidang = DB::table('m_kategori_user')->select('id_bidang')->where('id_kategori_user', $user->id_header_user)->first();
        $agenda = DB::table('m_agenda')->where('id_bidang', $bidang->id_bidang)->get();
        
        foreach ($agenda as $key => $value) {
            $m_posting = DB::table('m_posting')->where('id_posting','!=',$id)->where('status_post','!=','deleted')->where('id_agenda',$value->id_agenda)->get();
            if(count($m_posting) > 0){
                unset($agenda[$key]);
            }
        }

        $agenda = array_values($agenda->toArray());
        //dd($cek_post);
        //dd($agenda);
   
        return view('admin.approval.edit_kronik', compact('cek_post','agenda','type'));
    }

    public function updateApprovalKronik(Request $request, $id){

        
        $this->validate($request,[
            'title' => 'required|max:100',
            'kronik' => 'required',
            'agenda' => 'required',
        ]);

        $file   = $request->image;
        $file2   = $request->image2;
        $file3   = $request->image3;


        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->title.".jpg";
                $destinationPath = 'upload/kronik';   
                $update_img = 2;
        }else{
            if($request->value_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/kronik'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/kronik'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/kronik'; 
                $update_img = 1;
            }
        }

        if(!empty($file2)){
                $filename_path_bukti2 = date("ymdHis").$request->title."_2.jpg";
                $destinationPath2 = 'upload/kronik';   
                $update_img2 = 2;
        }else{
            if($request->value_image_hidden2 == "false"){
                if($request->image_hidden2 != "no_image.png"){
                    $filename_path_bukti2 = "no_image.png";
                    $destinationPath2 = 'upload/kronik'; 
                    $update_img2 = 2;   
                }else{
                    $filename_path_bukti2 = $request->image_hidden2;
                    $destinationPath2 = 'upload/kronik'; 
                    $update_img2 = 1;
                }
            }else{
                $filename_path_bukti2 = $request->image_hidden2;
                $destinationPath2 = 'upload/kronik'; 
                $update_img2 = 1;
            }
        }

        if(!empty($file3)){
                $filename_path_bukti3 = date("ymdHis").$request->title."_3.jpg";
                $destinationPath3 = 'upload/kronik';   
                $update_img3 = 2;
        }else{
            if($request->value_image_hidden3 == "false"){
                if($request->image_hidden3 != "no_image.png"){
                    $filename_path_bukti3 = "no_image.png";
                    $destinationPath3 = 'upload/kronik'; 
                    $update_img3 = 2;   
                }else{
                    $filename_path_bukti3 = $request->image_hidden3;
                    $destinationPath3 = 'upload/kronik'; 
                    $update_img3 = 1;
                }
            }else{
                $filename_path_bukti3 = $request->image_hidden3;
                $destinationPath3 = 'upload/kronik'; 
                $update_img3 = 1;
            }
        }



        //dd($request);

        DB::beginTransaction();
            try{

                DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'isi_post' => $request->kronik,
                        'title_post' => $request->title,
                        'image_post' => $filename_path_bukti,
                        'image2_post' => $filename_path_bukti2,
                        'image3_post' => $filename_path_bukti3,
                        'id_agenda' => $request->agenda,
                    ]);

                if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }
                if(!empty($file2)){
                    $file2->move($destinationPath2, $filename_path_bukti2);
                }

                if(!empty($file3)){
                    $file3->move($destinationPath3, $filename_path_bukti3);
                }

                if($update_img == 2){
                    File::delete('upload/kronik/'.$request->image_hidden);
                }
                if($update_img2 == 2){
                    File::delete('upload/kronik/'.$request->image_hidden2);
                }
                if($update_img3 == 2){
                    File::delete('upload/kronik/'.$request->image_hidden3);
                }



                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/approval/kronik');
    }

    public function approveApprovalKronik($id){

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post != "in approval"){
            return redirect()->back()->with('message','Tidak Bisa Menyetujui Kronik');
        }else{
            DB::beginTransaction();
                    try{
                            DB::table('m_posting')
                                ->where('id_posting',$id)
                                ->update([
                                    'status_post' => "approved",
                                    'approved_by' => Auth::user()->id,
                                ]);
                            DB::commit();

                    }catch(\Exception $e) {
                        $success = false;
                        DB::rollback();
                        dd($e);  
                    }
            return redirect('admin/approval/kronik');
        }
   
    }

    public function deleteApprovalKronik($id){
   
        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post != "deleted"){
            DB::beginTransaction();
                    try{
                            DB::table('m_posting')
                                ->where('id_posting',$id)
                                ->update([
                                    'status_post' => "deleted",
                                ]);
                            DB::commit();

                    }catch(\Exception $e) {
                        $success = false;
                        DB::rollback();
                        dd($e);  
                    }
            return redirect('admin/approval/kronik');
        }else{
            return redirect()->back()->with('message','Tidak Bisa menghapus Kronik');
                
        }
    }



    public function apiApprovalKronik(){
        //$users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $post = DB::table('m_posting')
        ->select('m_posting.*','m_user.name','m_bidang.nama_bidang as bidang')
        ->join('m_user','m_user.id','m_posting.id_user')
        ->join('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
        ->where('type_post','kronik')
        ->where('m_posting.id_bidang','!=',0)
        ->whereIn('status_post',['in approval','approved'])
        ->orderBy('id_posting','desc');

        return Datatables::of($post)
            ->addColumn('action', function ($post) {
                if($post->status_post == "in approval"){
                        $hasil = '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/approval/kronik/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                            '&nbsp'.
                            '<a href="'.url('/admin/approval/kronik/'.$post->id_posting.'/approve').'" class="btn btn-success  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menyetujui ?'".')"><i class="fa fa-check"></i></a>'.
                            '&nbsp'.
                            '<a href="'.url('/admin/approval/kronik/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                        '</td>'.
                    '</tr>'.
                '</table>';
                }else if($post->status_post == "approved"){
                        $hasil = '<table id="tabel-in-opsi">'.
                    '<tr>'.
                        '<td>'.
                            '<a href="'.url('/admin/approval/kronik/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                            '&nbsp'.
                            '<a href="'.url('/admin/approval/kronik/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                        '</td>'.
                    '</tr>'.
                '</table>';
                }
                return $hasil;

            })
            ->editColumn('created_at', function($post){
                return date('d M Y', strtotime($post->created_at));
            }) 
            ->editColumn('status_post', function($post){
                return ucwords($post->status_post);
            })
            ->editColumn('title_post', function($post){
                $cek = '<a href="'.url('/admin/approval/kronik/'.$post->id_posting.'/show').'">'.$post->title_post.'</a>';
                return $cek;
            })
            ->addIndexColumn()
            ->rawColumns(['action','created_at','status_post','title_post'])
            ->make(true);
    }

    public function indexKronik(){
       
        return view('admin.posting.kronik.index');
    }

    public function createKronik(){
        $bidang = DB::table('m_kategori_user')->select('id_bidang')->where('id_kategori_user', Auth::user()->id_header_user)->first();
        $agenda = DB::table('m_agenda')->where('id_bidang', $bidang->id_bidang)->get();
        foreach ($agenda as $key => $value) {
            $m_posting = DB::table('m_posting')->where('status_post','!=','deleted')->where('id_agenda',$value->id_agenda)->get();
            if(count($m_posting) > 0){
                unset($agenda[$key]);
            }
        }

        $agenda = array_values($agenda->toArray());

        //dd($agenda);
       
        return view('admin.posting.kronik.create', compact('agenda'));
    }

     public function storeKronik(Request $request){
        
        
        $this->validate($request,[
            'title' => 'required|max:100',
            'kronik' => 'required',
            'agenda' => 'required',
        ]);

        $file   = $request->image;
        $file2   = $request->image2;
        $file3   = $request->image3;
        
        if(!empty($file)){
            $filename_path_bukti = date("ymdHis").$request->title.".jpg";
            $destinationPath = 'upload/kronik';   
        }else{
            $filename_path_bukti = "no_image.png";
        }

        if(!empty($file2)){
            $filename_path_bukti2 = date("ymdHis").$request->title."_2.jpg";
            $destinationPath2 = 'upload/kronik';   
        }else{
            $filename_path_bukti2 = "no_image.png";
        }

        if(!empty($file3)){
            $filename_path_bukti3 = date("ymdHis").$request->title."_3.jpg";
            $destinationPath3 = 'upload/kronik';   
        }else{
            $filename_path_bukti3 = "no_image.png";
        }

        DB::beginTransaction();
            try{

                $cek_kat_user = DB::table('m_kategori_user')->where('id_kategori_user', Auth::user()->id_header_user)->first();
                $cek_bidang = DB::table('m_bidang')->where('id_bidang', $cek_kat_user->id_bidang)->first();
                if($cek_bidang == null){
                    $cek_bidang = 0;
                }else{
                    $cek_bidang = $cek_bidang->id_bidang;
                }
                //dd($cek_bidang);
                if($cek_bidang == 0){
                    DB::table('m_posting')
                        ->insert([
                            'isi_post' => $request->kronik,
                            'title_post' => $request->title,
                            'id_bidang' => $cek_bidang,
                            'id_agenda' => $request->agenda,
                            'id_user' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_post' => 'kronik',
                            'status_post' => 'approved',
                            'published' => 0,
                            'image_post' => $filename_path_bukti,
                            'image2_post' => $filename_path_bukti2,
                            'image3_post' => $filename_path_bukti3,
                        ]);
                }else{
                    DB::table('m_posting')
                        ->insert([
                            'isi_post' => $request->kronik,
                            'title_post' => $request->title,
                            'id_bidang' => $cek_bidang,
                            'id_agenda' => $request->agenda,
                            'id_user' => Auth::user()->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'type_post' => 'kronik',
                            'status_post' => 'draft',
                            'published' => 0,
                            'image_post' => $filename_path_bukti,
                            'image2_post' => $filename_path_bukti2,
                            'image3_post' => $filename_path_bukti3,
                        ]);
                }
                   

                if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }

                if(!empty($file2)){
                    $file2->move($destinationPath2, $filename_path_bukti2);
                }

                if(!empty($file3)){
                    $file3->move($destinationPath3, $filename_path_bukti3);
                }


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/kronik');
    }


    public function editKronik($id){

        $type = "edit";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        $bidang = DB::table('m_kategori_user')->select('id_bidang')->where('id_kategori_user', Auth::user()->id_header_user)->first();
        $agenda = DB::table('m_agenda')->where('id_bidang', $bidang->id_bidang)->get();
        foreach ($agenda as $key => $value) {
            $m_posting = DB::table('m_posting')->where('id_posting','!=',$id)->where('status_post','!=','deleted')->where('id_agenda',$value->id_agenda)->get();
            if(count($m_posting) > 0){
                unset($agenda[$key]);
            }
        }

        $agenda = array_values($agenda->toArray());
   
        return view('admin.posting.kronik.update', compact('cek_post','agenda','type'));
    }

     public function showKronik($id){

        $type = "show";

        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        $bidang = DB::table('m_kategori_user')->select('id_bidang')->where('id_kategori_user', Auth::user()->id_header_user)->first();
        $agenda = DB::table('m_agenda')->where('id_bidang', $bidang->id_bidang)->get();
        foreach ($agenda as $key => $value) {
            $m_posting = DB::table('m_posting')->where('id_posting','!=',$id)->where('status_post','!=','deleted')->where('id_agenda',$value->id_agenda)->get();
            if(count($m_posting) > 0){
                unset($agenda[$key]);
            }
        }

        $agenda = array_values($agenda->toArray());
   
        return view('admin.posting.kronik.update', compact('cek_post','agenda','type'));
    }

    public function updateKronik(Request $request, $id){
        $this->validate($request,[
            'title' => 'required|max:100',
            'kronik' => 'required',
            'agenda' => 'required',
        ]);

        $file   = $request->image;
        $file2   = $request->image2;
        $file3   = $request->image3;


        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->title.".jpg";
                $destinationPath = 'upload/kronik';   
                $update_img = 2;
        }else{
            if($request->value_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/kronik'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/kronik'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/kronik'; 
                $update_img = 1;
            }
        }

        if(!empty($file2)){
                $filename_path_bukti2 = date("ymdHis").$request->title."_2.jpg";
                $destinationPath2 = 'upload/kronik';   
                $update_img2 = 2;
        }else{
            if($request->value_image_hidden2 == "false"){
                if($request->image_hidden2 != "no_image.png"){
                    $filename_path_bukti2 = "no_image.png";
                    $destinationPath2 = 'upload/kronik'; 
                    $update_img2 = 2;   
                }else{
                    $filename_path_bukti2 = $request->image_hidden2;
                    $destinationPath2 = 'upload/kronik'; 
                    $update_img2 = 1;
                }
            }else{
                $filename_path_bukti2 = $request->image_hidden2;
                $destinationPath2 = 'upload/kronik'; 
                $update_img2 = 1;
            }
        }

        if(!empty($file3)){
                $filename_path_bukti3 = date("ymdHis").$request->title."_3.jpg";
                $destinationPath3 = 'upload/kronik';   
                $update_img3 = 2;
        }else{
            if($request->value_image_hidden3 == "false"){
                if($request->image_hidden3 != "no_image.png"){
                    $filename_path_bukti3 = "no_image.png";
                    $destinationPath3 = 'upload/kronik'; 
                    $update_img3 = 2;   
                }else{
                    $filename_path_bukti3 = $request->image_hidden3;
                    $destinationPath3 = 'upload/kronik'; 
                    $update_img3 = 1;
                }
            }else{
                $filename_path_bukti3 = $request->image_hidden3;
                $destinationPath3 = 'upload/kronik'; 
                $update_img3 = 1;
            }
        }



        //dd($request);

        DB::beginTransaction();
            try{

                DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'isi_post' => $request->kronik,
                        'title_post' => $request->title,
                        'image_post' => $filename_path_bukti,
                        'image2_post' => $filename_path_bukti2,
                        'image3_post' => $filename_path_bukti3,
                        'id_agenda' => $request->agenda,
                    ]);

                if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }
                if(!empty($file2)){
                    $file2->move($destinationPath2, $filename_path_bukti2);
                }

                if(!empty($file3)){
                    $file3->move($destinationPath3, $filename_path_bukti3);
                }

                if($update_img == 2){
                    File::delete('upload/kronik/'.$request->image_hidden);
                }
                if($update_img2 == 2){
                    File::delete('upload/kronik/'.$request->image_hidden2);
                }
                if($update_img3 == 2){
                    File::delete('upload/kronik/'.$request->image_hidden3);
                }



                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/kronik');
    }

    public function approvalKronik($id){
        DB::beginTransaction();
            try{
                 DB::table('m_posting')
                    ->where('id_posting',$id)
                    ->update([
                        'status_post' => 'in approval',
                    ]);


                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }

        return redirect('admin/kronik');
    }

    public function deleteKronik($id){
   
        $cek_post = DB::table('m_posting')->where('id_posting', $id)->first();

        if($cek_post->status_post == "in approval" ||  $cek_post->status_post == "approved"){
                DB::beginTransaction();
                        try{
                                DB::table('m_posting')
                                    ->where('id_posting',$id)
                                    ->update([
                                        'status_post' => "deleted",
                                    ]);
                                DB::commit();

                        }catch(\Exception $e) {
                            $success = false;
                            DB::rollback();
                            dd($e);  
                        }
                return redirect('admin/kronik');
        }else{
                return redirect()->back()->with('message','Tidak Bisa Menghapus Kronik');
                
        }
    }



    public function apiKronik(){
        //$users = User::select(['id', 'name', 'email', 'password', 'created_at', 'updated_at']);
        $post = DB::table('m_posting')
        ->select('m_posting.*','m_user.name','m_agenda.nama_agenda')
        ->join('m_user','m_user.id','m_posting.id_user')
        ->join('m_agenda','m_agenda.id_agenda','m_posting.id_agenda')
        ->where('type_post','kronik')
        ->where('status_post','!=','deleted')
        ->where('m_posting.id_user', Auth::user()->id)
        ->orderBy('id_posting','desc');

        return Datatables::of($post)
            ->addColumn('action', function ($post) {
                if(Auth::user()->role < 3){
                    $hasil = '<table id="tabel-in-opsi">'.
                            '<tr>'.
                                '<td>'.
                                    '<a href="'.url('/admin/kronik/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                                    '&nbsp'.
                                    '<a href="'.url('/admin/kronik/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm"><i class="fa fa-trash" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"></i></a>'.

                                '</td>'.
                            '</tr>'.
                        '</table>';
                }else{
                    if($post->status_post == "draft"){
                            $hasil = '<table id="tabel-in-opsi">'.
                        '<tr>'.
                            '<td>'.
                                '<a href="'.url('/admin/kronik/'.$post->id_posting.'/edit').'" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>'.

                                '&nbsp'.
                                '<a href="'.url('/admin/kronik/'.$post->id_posting.'/send-approval').'" class="btn btn-success  btn-sm"  onclick="return confirm('."'Apakah Anda Yakin Untuk Memposting ?'".')"><i class="fa fa-paper-plane"></i></a>'.
                                '&nbsp'.
                                '<a href="'.url('/admin/kronik/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                            '</td>'.
                        '</tr>'.
                    '</table>';
                    }else{
                        $hasil = '<table id="tabel-in-opsi">'.
                            '<tr>'.
                                '<a href="'.url('/admin/kronik/'.$post->id_posting.'/delete').'" class="btn btn-danger  btn-sm" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')"><i class="fa fa-trash"></i></a>'.

                                '</td>'.
                            '</tr>'.
                        '</table>';
                    }

                }
                return $hasil;

            })
            ->editColumn('created_at', function($post){
                return date('d M Y', strtotime($post->created_at));
            }) 
            ->editColumn('title_post', function($post){
                $cek = '<a href="'.url('/admin/kronik/'.$post->id_posting.'/show').'">'.$post->title_post.'</a>';
                return $cek;
            })
            ->editColumn('status_post', function($post){
                return ucwords($post->status_post);
            })
            ->addIndexColumn()
            ->rawColumns(['action','created_at','title_post','status_post'])
            ->make(true);
    }



       
}

?>