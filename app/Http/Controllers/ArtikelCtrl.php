<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;
use Share;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class ArtikelCtrl extends Controller
{
	public function index($id)
	{
       
        $artikel = DB::table('m_posting')
                        ->select('m_posting.*','m_bidang.nama_bidang')
                        ->join('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
                        ->where('type_post','artikel')
                        ->where('m_posting.status_post', 'approved')
                        ->where('m_posting.id_posting', $id)
                        ->first();
        $slider = DB::table('m_banner')->where('page_banner','detail')->first();

        $artikel_bidang = DB::table('m_posting')
                        ->select('m_posting.*','m_bidang.nama_bidang')
                        ->join('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
                        ->where('type_post','artikel')
                        ->where('m_posting.status_post', 'approved')
                        ->where('m_posting.id_bidang', $artikel->id_bidang)
                        ->where('m_posting.id_posting', '!=' ,$artikel->id_posting)
                        ->orderBy('created_at','desc')
                        ->limit(5)
                        ->get();
        $web = 'http://google.com';
        $fb = Share::page($web, 'Renungan Gereja Paroki Roh Kudus', ['class' => 'pr-page-detail__content-share-link'],'<ul>','</ul>')
                ->facebook()
                ->twitter()
                ->googlePlus();
        
        $date_now =  date('Y-m-d 00:00:00');
        $date_end = date('Y-m-d 23:59:59');

        $agenda = DB::table('m_agenda')
                    ->select('m_agenda.*','m_tempat.nama_tempat')
                    ->join('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                    ->where('date_agenda','>=', $date_now)
                    ->where('date_agenda','<=', $date_end)
                    ->where('type_agenda','umum')
                    ->limit(5)
                    ->orderBy('id_agenda','desc')
                    ->get();
        $agenda_lain = [];
        if(count($agenda) == 0){
            $agenda_lain = DB::table('m_agenda')
                    ->select('date_agenda')
                    ->where('date_agenda','>=', $date_end)
                    ->orderBy('date_agenda','asc')
                    ->groupBy('date_agenda')
                    ->get();
            $cek_event = 0;
            foreach ($agenda_lain as $key_agenda => $value_agenda) {
                
                if($cek_event < 6){
                    $agenda_det = DB::table('m_agenda')
                            ->select('m_agenda.*','m_tempat.nama_tempat')
                            ->join('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                            ->where('date_agenda', $value_agenda->date_agenda)
                            ->where('type_agenda', "umum")
                            ->orderBy('waktu_agenda','asc')
                            ->get();

                    $value_agenda->detail_agenda = $agenda_det;
                    $cek_event = $cek_event + count($agenda_det);
                }else{
                    $value_agenda->detail_agenda = "";                    
                }
                
                if($value_agenda->detail_agenda == ""){
                    unset($agenda_lain[$key_agenda]);
                }
            }
        }

       
        return view('frontend.post_detail',compact('artikel','slider','artikel_bidang','fb','agenda','agenda_lain'));
	}

       
}
?>