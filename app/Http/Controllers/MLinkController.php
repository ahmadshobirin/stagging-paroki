<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class MLinkController extends Controller
{
	public function index($nama)
	{
        $link = DB::table('m_link')->where('category_link',$nama)->orderBy('id_link','desc')->get();


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.master.link.index',compact('link','nama'));
	}


    public function create($nama)
    {


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.master.link.create',compact('nama'));
    }

    public function store(Request $request, $nama)
    {
        $this->validate($request, [
            'judul' => 'required|max:50',
            'url' => 'required',
            'desc' => 'required',
        ]);

         DB::beginTransaction();
            try{
                 DB::table('m_link')
                        ->insert([
                            'title_link' => $request->judul,
                            'url_link' => $request->url,
                            'desc_link' => $request->desc,
                            'category_link' => $nama,
                            'created_at' => date('Y-m-d H:i:s'),
                        ]);

                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return redirect('admin/link/'.$nama);
    }

    public function edit($id)
    {
        $link = DB::table('m_link')->where('id_link', $id)->first();

        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return view('admin.master.link.update',compact('link'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required|max:50',
            'url' => 'required',
            'desc' => 'required',
        ]);

        $link = DB::table('m_link')->where('id_link', $id)->first();

         DB::beginTransaction();
            try{
                 DB::table('m_link')
                        ->where('id_link', $id)
                        ->update([
                            'title_link' => $request->judul,
                            'url_link' => $request->url,
                            'desc_link' => $request->desc,
                        ]);

                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return redirect('admin/link/'.$link->category_link);
    }

    public function destroy($id)
    {
        $link = DB::table('m_link')->where('id_link', $id)->first();

        DB::beginTransaction();
            try{
                 DB::table('m_link')
                        ->where('id_link', $id)
                        ->delete();

                DB::commit();
            }catch(\Exception $e) {
                $success = false;
                DB::rollback();
                dd($e);
            }


        // return view('frontend.home', compact('slider','about','kategori_fitur','produk1','id_laris','kategori'));
        return redirect('admin/link/'.$link->category_link);
    }
       
}
?>