<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Image;
use PDF;
use File;
use Mail;
use Carbon\Carbon;
use App\Models\Proposal;
use Illuminate\Http\Request;
use App\Helpers\CollectDetail;
use App\Mail\ApproveProposal;
use Yajra\DataTables\DataTables;
use App\Models\ProposalBudgetIn;
use App\Models\ProposalBudgetOut;
use App\Models\ProposalTempAgenda;
use App\Models\ProposalSignature;

class ProposalController extends Controller
{
    protected $usersUpline;

    public function index($type = null)
    {
        $type = is_null($type) ? 'default' : 'submission';

        return view('admin/posting/proposal/index',compact('type'));
    }

    public function upload()
    {
        return view('admin/posting/proposal/upload', [
            'editmode' => null
        ]);
    }

    public function create()
    {
        $tempat = DB::table('m_tempat')->where('status', 'on')->get();

        return view('admin.posting.proposal.form', compact('tempat'));
    }

    public function store(Request $request)
    {
        if ($request->hasFile('file_upload'))
            return $this->storeUpload($request);

        $this->validations($request);

        if($request->hasFile('lapkeu')){
            $file = $request->file('lapkeu');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $nameLapK = $file->getClientOriginalName() . time() . '.' . $extension;
            $file->move('upload/proposal/', $nameLapK);

            $request->merge([
                'laporan_keuangan' => "upload/proposal/$nameLapK",
            ]);
        }

        $file2 = $request->file('proker');
        $extension = $file2->getClientOriginalExtension(); // getting image extension
        $nameProker = $file2->getClientOriginalName() . time() . '.' . $extension;
        $file2->move('upload/proposal/', $nameProker);

        $request->merge([
            'user_id' => Auth::user()->id,
            'createuser' => AUth::user()->name,
            'bidang_user_id' => Auth::user()->id_header_user,
            'program_kerja' => "upload/proposal/$nameProker",
        ]);

        $exceptReq = $this->exceptFeld();

        DB::beginTransaction();
        try {
            $pro = Proposal::create($request->except($exceptReq));

            $dtl = $this->detailcollections($request); //collect-pengeluaran-detail
            // dd($pro,$dtl);
            //create-many-with-relations
            $pro->budget_in()->createMany($dtl['in']);
            $pro->budget_out()->createMany($dtl['out']);
            $pro->temp_agenda()->createMany($dtl['tempAgenda']);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('proposal.index');
    }

    public function storeUpload($request)
    {
        $request->validate([
            'judul' => 'required',
            'file_upload' => 'required',
        ]);

        $file = $request->file('file_upload');
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $name = $file->getClientOriginalName() . time() . '.' . $extension;
        $file->move('upload/proposal/', $name);

        $request->merge([
            'user_id' => Auth::user()->id,
            'createuser' => AUth::user()->name,
            'bidang_user_id' => Auth::user()->id_header_user,
            'file' => "upload/proposal/$name",
            'type' => 'upload',
        ]);

        $exceptReq = $this->exceptFeld();

        DB::beginTransaction();
        try {
            $pro = Proposal::create($request->except($exceptReq));

            $dtl = $this->detailcollections($request); //collect-pengeluaran-detail

            //create-many-with-relations
            $pro->budget_in()->createMany($dtl['in']);
            $pro->budget_out()->createMany($dtl['out']);
            $pro->temp_agenda()->createMany($dtl['tempAgenda']);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('proposal.index');
    }

    public function show($id)
    {
        // dd(Proposal::with(['budget_in', 'budget_out', 'signature' => function($q){
        //         $q->where('status','complete')->orderBy('id','asc');
        // },'temp_agenda','temp_agenda.place'])->find($id));
        // return view('admin.posting.proposal.printout', ['proposal' => Proposal::with(['budget_in', 'budget_out', 'signature' => function($q){
        //             $q->where('status','complete')->orderBy('id','asc');
        //     }, 'temp_agenda', 'temp_agenda.place'])->find($id)]);

        return PDF::loadView('admin.posting.proposal.printout', [
            'proposal' => Proposal::with(['budget_in', 'budget_out', 'temp_agenda', 'temp_agenda.place','signature' => function($q){
                $q->where('status','complete')->orderBy('id','asc');
            }])->find($id),
        ])->stream();
    }

    public function edit($id)
    {
        $data = Proposal::with(['budget_in', 'budget_out', 'temp_agenda'])->where('id', $id)->first();
        $tempat = DB::table('m_tempat')->where('status', 'on')->get();

        $pathView = ($data->type == 'form')
            ? 'admin.posting.proposal.form'
            : 'admin.posting.proposal.upload';

        return view($pathView, [
            'data' => $data->toJson(),
            'id' => $id,
            'editmode' => true,
            'tempat' => $tempat,
        ]);
    }

    public function update(Request $request, $id)
    {
        // $dtl = $this->detailcollections($request);
        // dd($request->all(),$dtl,Proposal::where('id',$id)->first());

        // $this->validations($request);
        $pro = Proposal::where('id', $id)->first();

        if ($pro->type == 'upload')
            return $this->updateUpload($request, $id);

        DB::beginTransaction();
        try {
            $exceptReq = $this->exceptFeld();

            if($request->hasFile('lapkeu')){
                $file = $request->file('lapkeu');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $nameLapK = $file->getClientOriginalName() . time() . '.' . $extension;
                $file->move('upload/proposal/', $nameLapK);

                $request->merge(['program_kerja' => "upload/proposal/$nameLapK"]);
                File::delete($pro->program_kerja); //delete old images
            }

            if($request->hasFile('proker')){
                $file2 = $request->file('proker');
                $extension = $file2->getClientOriginalExtension(); // getting image extension
                $nameProker = $file2->getClientOriginalName() . time() . '.' . $extension;
                $file2->move('upload/proposal/', $nameProker);

                $request->merge(['laporan_keuangan' => "upload/proposal/$nameProker"]);
                File::delete($pro->laporan_keuangan); //delete old images
            }

            $pro->update($request->except($exceptReq)); //update-header

            ProposalBudgetIn::where('proposal_id', $id)->delete(); //detete-detail
            ProposalBudgetOut::where('proposal_id', $id)->delete();
            ProposalTempAgenda::where('proposal_id', $id)->delete();

            //insert-detail
            $dtl = $this->detailcollections($request); //collect-pengeluaran-detail
            $pro->budget_in()->createMany($dtl['in']);  //create-many-with-relations
            $pro->budget_out()->createMany($dtl['out']);
            $pro->temp_agenda()->createMany($dtl['tempAgenda']);

            DB::commit();
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
        }

        return redirect()->route('proposal.index');
    }


    public function updateUpload($request, $id)
    {
        $pro = Proposal::where('id', $id)->first();
        // dd($request->all());
        DB::beginTransaction();
        try {

            if ($request->hasFile('file_upload')) {
                $file = $request->file('file_upload');
                $extension = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName() . time() . '.' . $extension;
                $file->move('upload/proposal/', $name);

                $request->merge(['file' => "upload/proposal/$name"]);
                File::delete($pro->file); //delete old images
            }

            $exceptReq = $this->exceptFeld();
            $pro->update($request->except($exceptReq)); //update-header

            ProposalBudgetIn::where('proposal_id', $id)->delete(); //detete-detail
            ProposalBudgetOut::where('proposal_id', $id)->delete();

            //insert-detail
            $dtl = $this->detailcollections($request); //collect-pengeluaran-detail
            $pro->budget_in()->createMany($dtl['in']);  //create-many-with-relations
            $pro->budget_out()->createMany($dtl['out']);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('proposal.index');
    }

    public function destroy($id)
    {
        $pro = Proposal::where('id', $id)->first();
        if ($pro == null) {
            return response()->json('data not found', 404);
        }

        $pro->delete();
    }

    public function upStatus($oldstatus, $id, $changestatus, Request $request)
    {
        if ($changestatus == "add-signature") {
            return view('admin/posting/proposal/signature');
        }

        $pro = Proposal::with(['temp_agenda'])->where('id', $id)->where('status', $oldstatus)->first();
        if ($pro == null) {
            return response()->json('data not found', 404);
        }


        if ($pro->type == 'form' && $changestatus == 'approved') {
            $decisision = null;
            foreach ($pro->temp_agenda as $key => $value) {
                $cek = DB::table('m_agenda')
                    ->where('tempat_agenda', $value->place_id)
                    ->whereDate('date_agenda', Carbon::parse($value->date)->format('Y-m-d'))
                    ->whereTime('waktu_agenda', Carbon::parse($value->time_start)->format('H:i:00'))
                    ->whereTime('end_agenda', Carbon::parse($value->time_end)->format('H:i:00'))
                    ->first();

                if ($cek !== null) {
                    $decisision = true;
                }
            }

            if ($decisision == true) {
                return response()->json([
                    'data' => [
                        'status' => 403,
                        'description' => 'Data agenda sudah ada'
                    ]
                ], 403);
            } else {

                $bd = DB::table('m_kategori_user')->where('id_kategori_user', DB::table('m_user')->where('id', $pro->user_id)->first()->id_header_user)->first();
                $agenda = [];
                foreach ($pro->temp_agenda as $key => $value) {
                    $agenda[] = [
                        'nama_agenda' => $value->name,
                        'date_agenda' => $value->date,
                        'hari_libur' => $value->holiday,
                        'waktu_agenda' => $value->time_start,
                        'end_agenda' => $value->time_end,
                        'alternative_tempat_menikah' => $value->other_place,
                        'created_at' => $value->created_at,
                        'tempat_agenda' => ($value->place_id == null) ? 0 : $value->place_id,
                        'id_bidang' => $bd->id_bidang,
                        'proposal_id' => $pro->id
                    ];
                }

                DB::table('m_agenda')->insert($agenda);
            }
        }

        if ($changestatus == 'approved') {
            $this->sendEmail($pro,"proposal");
            $this->signature($pro->id);
        }

        $pro->update([
            'status' => $changestatus
        ]);

        return response()->json([
            'data' => [
                'status' => 200,
                'description' => 'Berhasil',
                'data' => $this->usersUpline,
            ]
        ], 200);
    }

    public function addSignature($id)
    {
        $signature = ProposalSignature::where('proposal_id', $id)->where('user_id', Auth::user()->id)->where('status', 'pending')->first();

        // dd($signature,($signature->sequence - 1));

        if (!empty($signature)) {

            if ($signature->sequence == 1) {
                $options = [
                    'permission' => true,
                    'message' => null,
                ];
            } else {
                $cek = ProposalSignature::where('proposal_id', $id)
                    ->where('sequence', ($signature->sequence - 1))
                    ->where('status', 'complete')->first();
                if (!empty($cek)) {
                    $options = [
                        'permission' => true,
                        'message' => null,
                    ];
                } else {
                    $options = [
                        'permission' => false,
                        'message' => "User sebelum anda belum menyetujui",
                    ];
                }
            }
        } else {
            $options = [
                'permission' => false,
                'message' => "TIdak punya akses",
            ];
        }

        $user = (!empty($signature)) ? $signature->user_id : '';

        return view('admin/posting/proposal/signature', ['id' => $id, 'options' => $options, 'user' => $user]);
    }

    public function addSignaturePost(Request $request)
    {
        $imageFile = Image::make($request->signature);
        $imageFile->resize(240, 240);

        if (!\File::isDirectory(public_path('upload/signature'))) {
            \File::makeDirectory(public_path('upload/signature'), 493, true);
        }

        $path = "upload/signature/signature." . Carbon::now()->format('his') . ".png";

        $save = $imageFile->save(public_path($path));

        $request->merge([
            'signature_path' => $path,
            'status' => ($path != null) ? "complete" : 'pending',
        ]);

        ProposalSignature::where('user_id', $request->user_id)->where('proposal_id', $request->id)
            ->update($request->except('_token', 'signature','id','user_id'));

        $cekSignature = ProposalSignature::where('proposal_id',$request->id)->where('status','pending')->get();

        if(count($cekSignature) == 0){
            Proposal::where('id',$request->id)->update([
                'status' => 'finish'
            ]);
        }

        return response()->json(['success' => true, 'url' => route('proposal.index')]);
    }

    public function signatureReject($id)
    {
        $cekSignature = ProposalSignature::where('proposal_id',$id)
                    ->where('user_id',Auth::user()->id)
                    ->first();

        if($cekSignature != null){
            DB::beginTransaction();

            try {
                $cekSignature->update([
                    'status' => 'reject'
                ]);

                $pro = Proposal::where('id',$id)->first();

                $userCreated = DB::table('m_user')->where('id',$pro->user_id)->first();
                $note = ($pro->note != null) ? "dengan alasan ".$pro->note : '';

                $pro->update([
                    'status' => 'reject'
                ]);

                //send-email-created
                Mail::to($userCreated->email)->send(new ApproveProposal("Mohon maaf proposal Anda ditolak $note", "Reminder Cancel Proposal"));

                //delete agenda proposal
                DB::table('m_agenda')->where('proposal_id',$id)->delete();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                dd($e);
            }

            return back();
        }

        dd($cekSignature);
    }

    public function download($id)
    {
        $pro = Proposal::where('id', $id)->first();
        return response()->download(public_path($pro->file));
    }

    protected function validations($request)
    {
        $request->validate([
            'judul' => 'required',
            'nama_kegiatan' => 'required',
            'latar_belakang' => 'required',
            'tujuan_kegiatan' => 'required',
            'bentuk_kegiatan' => 'required',
            'susunan_acara' => 'required',
            'subjek_sasaran' => 'required',
            'indikator_keberhasilan' => 'required',
            'susunan_panitia' => 'required',
            'penutup' => 'required',
            'nameagenda' => 'required',
            'proker' => 'required',
        ]);
    }

    protected function exceptFeld()
    {
        return [
            '_token', '_method', 'pemasukan', 'pemasukan_total',
            'pengeluaran', 'pengeluaran_price', 'pengeluaran_qty', 'pengeluaran_total', 'file_upload',
            'nameagenda', 'place_id', 'date', 'time_start', 'time_end', 'holiday', 'other_place','lapkeu','proker'
        ];
    }

    protected function detailcollections($request)
    {
        $in = CollectDetail::get(
            $request->only([
                'pemasukan', 'pemasukan_total'
            ]),
            null,
            [
                'pemasukan_total' => 'integer',
            ]
        );

        $out = CollectDetail::get(
            $request->only([
                'pengeluaran', 'pengeluaran_price', 'pengeluaran_qty', 'pengeluaran_total',
            ]),
            null,
            [
                'pengeluaran_price' => 'integer',
                'pengeluaran_qty' => 'integer',
                'pengeluaran_total' => 'integer',
            ]
        );

        $tempAgenda = CollectDetail::get(
            $request->only([
                'date', 'time_start', 'time_end', 'holiday'
            ]),
            [
                'place_id' => $request->place_id,
                'other_place' => $request->other_place,
                'name' => $request->nameagenda
            ],
            [
                'date' => 'date',
                'time_start' => 'time',
                'time_end' => 'time',
            ]
        );
        return [
            'in' => $in,
            'out' => $out,
            'tempAgenda' => $tempAgenda
        ];
    }

    public function table($userId = null,$type = null)
    {
        $user = DB::table('m_user')->where('id', $userId)->first();

        if ($type == 'submission') {
            $proposal = ProposalSignature::join('proposal','proposal_signature.proposal_id','proposal.id')
            ->select('proposal.id as id','proposal.user_id','proposal.createuser',
            'proposal.judul', 'proposal.type', 'proposal.file', 'proposal.status', 'proposal.created_at')
            ->where('proposal_signature.user_id',$user->id)
            ->orderBy('proposal.id','desc');
        }else{

            $proposal =  Proposal::select('id', 'user_id', 'createuser', 'judul', 'type', 'file', 'status', 'created_at')
            ->orderBy('proposal.id', 'desc');

            if (!in_array($user->role, [1, 2])) {
                $proposal->where('user_id',$user->id);
            }
        }

        $proposal = $proposal;

        return DataTables::of($proposal)
            ->editColumn('createuser', function ($self) {
                return ucwords($self->createuser);
            })
            ->editColumn('judul', function ($self) use ($user) {
                    return '<a href="' . route('proposal.show', $self->id) . '">' . ucwords($self->judul) . '</a>';
            })
            ->editColumn('status', function ($self) {
                return $this->tablecheckstatus($self->status);
            })
            ->editColumn('created_at', function ($self) {
                return date('d M Y', strtotime($self->created_at));
            })
            ->editColumn('type', function ($self) {
                $class = ($self->type == 'form') ? 'default' : 'primary';
                return "<span class='label label-$class'>" . ucfirst($self->type) . "</span>";
            })
            ->addColumn('action', function ($self) use ($user) {
                return view('admin/posting/proposal/_actions', [
                    'data' => $self,
                    'user' => $user,
                    'links' => [
                        'show' => route('proposal.show', $self->id),
                        'edit' => route('proposal.edit', $self->id),
                        'note' => route('proposal.note', $self->id),
                        'destroy' => route('proposal.destroy', $self->id),
                    ]
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'created_at', 'status', 'judul', 'type'])
            ->make(true);
    }

    protected function tablecheckstatus($status, $class = null)
    {

        if ($status == 'in-approval') {
            $class = 'warning';
        } elseif ($status == 'approved') {
            $class = 'primary';
        } elseif ($status == 'cancel') {
            $class = 'danger';
        } else {
            $class = 'default';
        }

        return "<span class='label label-$class'>" . ucwords($status) . "</span>";
    }

    public function sendEmail($proposal)
    {
        $muser = new MUserController();
        $this->usersUpline  =  $muser->getUpline($proposal->user_id);
        // $this->usersUpline =  $muser->getUpline(171);

        $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
                karena ada permohonan pengajuan Proposal $proposal->judul dari $proposal->createuser";

        foreach ($this->usersUpline as $us) {
            Mail::to($us['email'])->send(new ApproveProposal($text, "Reminder Proposal"));
        }
    }

    public function signature($proposalId)
    {
        $signatures = [];
        foreach ($this->usersUpline as $key => $us) {
            $signatures[] = [
                'sequence' => ($key + 1),
                'proposal_id' => $proposalId,
                'user_id' => $us['id'],
                'username' => $us['name'],
                'signature_path' => null,
                'created_at' => now(),
            ];
        }

        ProposalSignature::insert($signatures);
    }

    public function note(Request $request,$id)
    {
        $pro = Proposal::select('note')->where('id',$id)->first();

        return ($request->isMethod('GET')) ? view("admin/posting/proposal/form-note",[ 'id' => $id,'pro' => $pro]) : $this->notePost($request,$id);
    }

    public function notePost($req,$id)
    {
        Proposal::where('id',$id)->update([
            'note' => $req->note,
            'updateuser' => Auth::user()->name,
        ]);

        return redirect()->route('proposal.index');
    }
}
