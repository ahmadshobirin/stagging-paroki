<?php

namespace App\Http\Controllers;

use Mail;
use DB;
use App\Mail\ApproveProposal;
use Auth;
use Response;
use App\Models\MUserModel;
use App\Models\MRoleModel;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class MUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $users = [];

    public function index($type = null)
    {
        if ($type == 'ummat') return view("admin.master.user.index-ummat");

        //$dataRole = DB::table('m_user')->where('role','!=',2)->get();
        $cek_tanggal = date('Y-m-d 00:00:00');
        $user = DB::table('m_user')->where('status', 'active')->get();

        DB::beginTransaction();
        try {
            foreach ($user as $key => $value) {
                if ($value->role == 3 ) {
                    if ($value->periode_aktif_end < $cek_tanggal) {
                        DB::table('m_user')->where('id', $value->id)->update([
                            "status" => 'inactive',
                        ]);
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return view("admin.master.user.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $getMRole = DB::table('m_role')->where('id', '!=', 4)->get();
        // $getMKatUser = DB::table('m_kategori_user')->get();
        return view('admin.master.user.create', compact('getMRole'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
            'email' => 'required|email',
            'role' => 'required',
            'password' => 'required',
            'katuser' => 'required',
        ]);

        $tglmulai = substr($request->periode, 0, 10);
        $tglsampai = substr($request->periode, 13, 10);

        if ($request->katuser != 0) {
            $cek_user = DB::table('m_user')
                ->where('id_header_user', $request->katuser)
                ->join('m_kategori_user', 'm_kategori_user.id_kategori_user', 'm_user.id_header_user')
                ->where('m_user.status', 'active')
                ->get();
            if (count($cek_user) > 0) {
                return redirect()->back()->with('message-error', 'User ' . $cek_user[0]->nama_kategori_user . ' masih ada yang aktif');
            }
        }

        //dd($request->all());
        //$roleSales  = MRoleModel::where('name', 'Sales')->first();
        $newSales = new MUserModel;
        $newSales->name = $request->nama;
        $newSales->email = $request->email;
        $newSales->address = $request->alamat;
        $newSales->birthdate = date('Y-m-d', strtotime($request->birthdate));
        $newSales->password =  bcrypt(str_replace(' ', '', $request->password));
        $newSales->role = $request->role;
        $newSales->id_header_user = $request->katuser;
        $newSales->status = "active";
        if ($request->role < 3) {
            $newSales->periode_aktif_start = null;
            $newSales->periode_aktif_end = null;
        } else {
            $newSales->periode_aktif_start = date('Y-m-d 00:00:00', strtotime($tglmulai));
            $newSales->periode_aktif_end = date('Y-m-d 23:59:59', strtotime($tglsampai));
        }
        $newSales->status = "active";
        $newSales->save();

        return redirect('admin/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataUser = MUserModel::where('id', '=', $id)->first();
        $getMRole = DB::table('m_role')->get();

        if ($dataUser->role < 3) {
            $getMKatUser = DB::table('m_kategori_user')->where('id_bidang', 0)->get();
        } else {
            $getMKatUser = DB::table('m_kategori_user')->where('id_bidang', '!=', 0)->get();
        }
        return view('admin.master.user.update', compact('dataUser', 'getMRole', 'getMKatUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required|max:50',
        ]);

        $cek_user = DB::table('m_user')->where('id', $id)->first();

        DB::beginTransaction();
        try {
            if ($cek_user->role < 3) {
                $updateSales = MUserModel::where('id', '=', $id)->update([
                    'name' => $request->nama,
                    'email' => $request->email,
                    'address' => $request->alamat,
                    'birthdate' => date('Y-m-d', strtotime($request->birthdate)),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            } else {
                $tglmulai = substr($request->periode, 0, 10);
                $tglsampai = substr($request->periode, 13, 10);
                $updateSales = MUserModel::where('id', '=', $id)->update([
                    'name' => $request->nama,
                    'address' => $request->alamat,
                    'birthdate' => date('Y-m-d', strtotime($request->birthdate)),
                    'periode_aktif_start' => date('Y-m-d 00:00:00', strtotime($tglmulai)),
                    'periode_aktif_end' => date('Y-m-d 23:59:59', strtotime($tglsampai)),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            $deadline = "";
            DB::rollback();
            dd($e);
        }

        return redirect('admin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $status = null)
    {
        $cek_user = DB::table('m_user')->where('id', $id)->first();

        if ($cek_user == null)
            // return redirect()->back()->with('message-success',"User berhasil di hapus");
            abort(404);

        DB::beginTransaction();
        try {
            MUserModel::where('id', '=', $id)
                ->update([
                    'status' => $status
                ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->back()->with('message-success', "User berhasil di update");
    }

    public function apiUser($type = null)
    {

        if ($type == 'ummat')
            return $this->apiuserummat();

        $user = DB::table('m_user')
            ->select('m_user.id', 'm_user.name', 'm_user.periode_aktif_start', 'm_user.periode_aktif_end', 'm_role.name as role', 'm_kategori_user.nama_kategori_user', 'm_bidang.nama_bidang', 'm_user.role as id_role', 'm_user.status')
            ->leftjoin('m_role', 'm_role.id', 'm_user.role')
            ->leftjoin('m_kategori_user', 'm_kategori_user.id_kategori_user', 'm_user.id_header_user')
            ->leftjoin('m_bidang', 'm_bidang.id_bidang', 'm_kategori_user.id_bidang')
            ->where('m_user.status', 'active');


        return Datatables::of($user)
            ->addColumn('periode', function ($user) {
                $periode = "";
                if ($user->id_role < 3) {
                    $periode = "-";
                } else {
                    $periode = date('d M Y', strtotime($user->periode_aktif_start)) . '-' . date('d M Y', strtotime($user->periode_aktif_end));
                }
                return $periode;
            })
            ->addColumn('action', function ($user) {
                if ($user->status == "active" || $user->id_role < 3) {
                    return '<table id="tabel-in-opsi">' .
                        '<tr>' .
                        '<td>' .
                        '<a href="' . url('/admin/user/' . $user->id . '/edit') . '" class="btn btn-warning  btn-sm"><i class="fa fa-edit"></i></a>' .

                        '&nbsp' .
                        '<a href="' . url('/admin/user/' . $user->id . '/reset-password') . '" class="btn btn-default  btn-sm" data-toggle="tooltip" data-placement="top" title="Reset Password"><i class="fa fa-question"></i></a>' .

                        '&nbsp' .
                        '<a href="' . url('/admin/user-delete/' . $user->id . '/inactive') . '" class="btn btn-danger  btn-sm" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Menghapus ?'" . ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>' .

                        '&nbsp' .

                        '</td>' .
                        '</tr>' .
                        '</table>';
                } else { }
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'periode'])
            ->make(true);
    }

    public function getKategoriUser()
    {
        $cek_kat = DB::table('m_kategori_user')
            ->select('m_kategori_user.*', 'm_bidang.nama_bidang')
            ->join('m_bidang', 'm_bidang.id_bidang', 'm_kategori_user.id_bidang')
            ->get();
        foreach ($cek_kat as $key => $value) {
            $cek_user = DB::table('m_user')->where('id_header_user', $value->id_kategori_user)->where('status', 'active')->get();
            if (count($cek_user) > 0) {
                unset($cek_kat[$key]);
            }
        }

        $cek_kat = array_values($cek_kat->toArray());

        return Response::json($cek_kat);
    }

    public function apiuserummat()
    {
        $us = DB::table('m_user')->where('id_header_user', 0);

        return Datatables::of($us)
            ->addColumn('action', function ($user) {
                return '<table id="tabel-in-opsi">' .
                    '<tr>' .
                    '<td>' .
                    '<a href="' . url('/admin/user-delete/' . $user->id . '/active') . '" class="btn btn-warning  btn-sm" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Merubah ?'" . ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Aktifkan"><i class="fa fa-check"></i></a>' .
                    '&nbsp' .
                    '<a href="' . url('/admin/user-delete/' . $user->id . '/inactive') . '" class="btn btn-danger  btn-sm" onclick="return confirm(' . "'Apakah Anda Yakin Untuk Menghapus ?'" . ')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Non Aktif"><i class="fa fa-remove"></i></a>' .

                    '&nbsp' .

                    '</td>' .
                    '</tr>' .
                    '</table>';
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    public function getKategoriAdmin()
    {
        $cek_kat = DB::table('m_kategori_user')
            ->select('m_kategori_user.*')
            ->where('id_bidang', 0)
            ->get();

        foreach ($cek_kat as $key => $value) {
            $cek_user = DB::table('m_user')->where('id_header_user', $value->id_kategori_user)->where('status', 'active')->get();
            if (count($cek_user) > 0) {
                unset($cek_kat[$key]);
            }
        }

        $cek_kat = array_values($cek_kat->toArray());

        return Response::json($cek_kat);
    }

    public function resetPassword($id)
    {

        return view('admin.master.user.reset', compact('id'));
    }

    public function storePassword(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $updateSales = MUserModel::where('id', '=', $id)
                ->update([
                    'password' => bcrypt(str_replace(' ', '', $request->password))
                ]);
            DB::commit();
        } catch (\Exception $e) {
            $success = false;
            $deadline = "";
            DB::rollback();
            dd($e);
        }
        return redirect('admin/user')->with('message-success', "Password berhasil di reset");
    }

    public function showForm()
    {
        return view('auth/register-umat');
    }

    public function register(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'no_telp' => 'required|string|max:25',
            'email' => 'required|string|email|max:255|unique:m_user',
            'password' => 'required|string|min:6|confirmed',
        ]);

        DB::table('m_user')->insert([
            'name' => $request->name,
            'address' => $request->address,
            'no_telp' => $request->no_telp,
            'email' => $request->email,
            'id_header_user' => 0,
            'password' => Hash::make($request->password),
            'role' => 4,
            'status' => 'inactive',
            'created_at' => now(),
        ]);

        return redirect()->back()->with('message-success', 'Anda Sudah Terdaftar. Silahkan tunggu 1x24jam agar akun anda dapat digunakan');
    }

    public function getUpline($id, $type = null)
    {
        if(in_array($type,['nikah','komuni','krisma','sakramen'])){
            $user = DB::table('m_user')->where('id_header_user',$id)->first();

            $checkUpline = DB::table('m_kategori_user')
            ->where('id_kategori_user', $user->id_header_user)
            ->first();
        }else{

            $user = DB::table('m_user')->where('id',$id)->first();

            $checkUpline = DB::table('m_kategori_user')
                ->where('id_kategori_user', $user->id_header_user)
                ->first();

        }

        $this->users[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                ];
        // if(in_array($type,['nikah','komuni','krisma'])){
        //     $user = DB::table('m_user')->where('id_header_user',$id)->first();

        //     $checkUpline = DB::table('m_kategori_user')
        //     ->where('id_kategori_user', $user->id_header_user)
        //     ->first();
        
        // }elseif($type == 'sakramen'){
        //     $user = DB::table('m_user')->where('id_header_user',$id)->first();

        //     $this->users[] = [
        //         'id' => $user->id,
        //         'name' => $user->name,
        //         'email' => $user->email,
        //     ];

        //     //cumasyarat untuk if dibawah (tidak bergunaaa)
        //     $checkUpline = DB::table('m_kategori_user')
        //     ->where('id_kategori_user', $user->id_header_user)
        //     ->first();

        // }else{
        //     $user = DB::table('m_user')->where('id',$id)->first();

        //     $checkUpline = DB::table('m_kategori_user')
        //         ->where('id_kategori_user', $user->id_header_user)
        //         ->first();

        //         $this->users[] = [
        //             'id' => $user->id,
        //             'name' => $user->name,
        //             'email' => $user->email,
        //         ];
        // }
        if ($checkUpline->id_parent >= 0) {
            if($type != "sakramen"){
                $this->lastUplineUser($checkUpline->id_parent, $type);
            }
        }

        //dd($this->users);

        return $this->users;
    }

    public function lastUplineUser($idparent, $type = null)
    {
        $lastUplineUser =  DB::table('m_kategori_user')
            ->where('id_kategori_user', $idparent)
            ->first();

        if (!empty($lastUplineUser)) {
            if ($lastUplineUser->id_parent >= 0) {

                if($type == "komuni"){
                    if($lastUplineUser->type != "wilayah" && $lastUplineUser->id_parent > 0){
                        $us = DB::table('m_user')
                            ->where('id_header_user', $lastUplineUser->id_kategori_user)
                            ->where('status', 'active')
                            ->first();

                        if (!empty($us)) {
                            // array_push($this->emailupline, $us->email); //add string email
                            $this->users[] = [
                                'id' => $us->id,
                                'name' => $us->name,
                                'email' => $us->email,
                            ];
                        }
                    }
                }elseif($type == "krisma"){
                    if($lastUplineUser->type != "biak" && $lastUplineUser->id_parent != 0){
                        $us = DB::table('m_user')
                            ->where('id_header_user', $lastUplineUser->id_kategori_user)
                            ->where('status', 'active')
                            ->first();

                        if (!empty($us)) {
                            // array_push($this->emailupline, $us->email); //add string email
                            $this->users[] = [
                                'id' => $us->id,
                                'name' => $us->name,
                                'email' => $us->email,
                            ];
                        }
                    }
                }else{
                    if($lastUplineUser->type != "biak"){
                        $us = DB::table('m_user')
                            ->where('id_header_user', $lastUplineUser->id_kategori_user)
                            ->where('status', 'active')
                            ->first();

                        if (!empty($us)) {
                            // array_push($this->emailupline, $us->email); //add string email
                            $this->users[] = [
                                'id' => $us->id,
                                'name' => $us->name,
                                'email' => $us->email,
                            ];
                        }
                    }
                }
                $this->lastUplineUser($lastUplineUser->id_parent, $type);
            }
        }
    }


    public function sendEmail($iduser,$type = null)
    {

        $data  =  $this->getUpline($iduser, $type);

        $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
        karena ada permohonan pengajuan Proposal HARDCODE dari HARDCODEUSER";

        $mailsend = null;
        foreach ($data as $us) {
            $mailsend .= $us['email']." <br>";
            //Mail::to($us['email'])->send(new ApproveProposal($text, "Reminder Proposal"));
        }

        echo $mailsend;
     }
}
