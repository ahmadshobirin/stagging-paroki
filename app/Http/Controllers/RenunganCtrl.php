<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Response;
use carbon;
use Mail;
use Auth;
use App\Http\Controllers\Controller;
use Share;
use Illuminate\Database\QueryException;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class RenunganCtrl extends Controller
{
	public function index()
	{
       
        
        $slider = DB::table('m_banner')->where('page_banner','renungan')->first();

        $renungan = DB::table('m_renungan')->orderBy('id_renungan','desc')->paginate(10);

        $tahun = DB::table('m_renungan')
        			->select(DB::raw("YEAR(created_at) as tahun"))
        			->groupBy(DB::raw("YEAR(created_at)"))
        			->get();

        
        foreach ($tahun as $key => $value) {
        	$bulan = DB::table('m_renungan')
        			->select(DB::raw("MONTH(created_at) as bulan"))
                                ->whereYear('created_at', $value->tahun)
                                ->groupBy((DB::raw("MONTH(created_at)")))
                                ->get();
        	$value->bulan = $bulan;
        }

        //dd($tahun);

        $bln = "";

       
        return view('frontend.renungan',compact('renungan','slider','tahun','bln'));
	}

        public function indexBulan($bln)
        {
       
        
        $slider = DB::table('m_banner')->where('page_banner','renungan')->first();

        $renungan = DB::table('m_renungan')->whereMonth('created_at', $bln)->orderBy('id_renungan','desc')->paginate(10);

        $tahun = DB::table('m_renungan')
                                ->select(DB::raw("YEAR(created_at) as tahun"))
                                ->groupBy(DB::raw("YEAR(created_at)"))
                                ->get();

        
        foreach ($tahun as $key => $value) {
                $bulan = DB::table('m_renungan')
                                ->select(DB::raw("MONTH(created_at) as bulan"))
                                ->whereYear('created_at', $value->tahun)
                                ->groupBy(DB::raw("MONTH(created_at)"))
                                ->get();
                $value->bulan = $bulan;
        }

        //dd($bln);

       
        return view('frontend.renungan',compact('renungan','slider','tahun','bln'));
        }

        public function indexTahun($thn)
        {
       
        
        $slider = DB::table('m_banner')->where('page_banner','renungan')->first();

        $renungan = DB::table('m_renungan')->whereYear('created_at', $thn)->orderBy('id_renungan','desc')->paginate(10);

        $tahun = DB::table('m_renungan')
                                ->select(DB::raw("YEAR(created_at) as tahun"))
                                ->groupBy(DB::raw("YEAR(created_at)"))
                                ->get();

        
        foreach ($tahun as $key => $value) {
                $bulan = DB::table('m_renungan')
                                ->select(DB::raw("MONTH(created_at) as bulan"))
                                ->whereYear('created_at', $value->tahun)
                                ->groupBy((DB::raw("MONTH(created_at)")))
                                ->get();
                $value->bulan = $bulan;
        }

        $bln="";
        //dd($bln);

       
        return view('frontend.renungan',compact('renungan','slider','tahun','thn','bln'));
        }


        public function detailRenungan($id)
        {
                $slider = DB::table('m_banner')->where('page_banner','renungan')->first();
                $renungan = DB::table('m_renungan')
                            ->select('m_renungan.*','m_romo.nama_romo')
                            ->join('m_romo','m_romo.id_romo','m_renungan.author_renungan')
                            ->where('id_renungan',$id)->first();

                $tahun = DB::table('m_renungan')
                                        ->select(DB::raw("YEAR(created_at) as tahun"))
                                        ->groupBy(DB::raw("YEAR(created_at)"))
                                        ->get();

                
                foreach ($tahun as $key => $value) {
                        $bulan = DB::table('m_renungan')
                                        ->select(DB::raw("MONTH(created_at) as bulan"))
                                        ->whereYear('created_at', $value->tahun)
                                        //->groupBy('created_at')
                                        ->groupBy(DB::raw("MONTH(created_at)"))
                                        ->get();
                        $value->bulan = $bulan;
                }

                //dd($tahun);
                //$web = 'http://staging.devkru.com/paroki/';
                $web = 'http://parokirohkudus.or.id/public/renungan/detail/'.$id;
                $fb = Share::page($web, 'Renungan Gereja Paroki Roh Kudus', ['class' => 'pr-page-detail__content-share-link'],'<ul>','</ul>')
                        ->facebook()
                        ->twitter()
                        ->googlePlus();

                $bln = "";

                $date_now =  date('Y-m-d 00:00:00');
                $date_end = date('Y-m-d 23:59:59');

                $agenda = DB::table('m_agenda')
                            ->select('m_agenda.*','m_tempat.nama_tempat')
                            ->join('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                            ->where('date_agenda','>=', $date_now)
                            ->where('date_agenda','<=', $date_end)
                            ->where('type_agenda','umum')
                            ->limit(5)
                            ->orderBy('id_agenda','desc')
                            ->get();
                $agenda_lain = [];
                if(count($agenda) == 0){
                    $agenda_lain = DB::table('m_agenda')
                            ->select('date_agenda')
                            ->where('date_agenda','>=', $date_end)
                            ->orderBy('date_agenda','asc')
                            ->groupBy('date_agenda')
                            ->get();
                    $cek_event = 0;
                    foreach ($agenda_lain as $key_agenda => $value_agenda) {
                        
                        if($cek_event < 6){
                            $agenda_det = DB::table('m_agenda')
                                    ->select('m_agenda.*','m_tempat.nama_tempat')
                                    ->join('m_tempat','m_tempat.id_tempat','m_agenda.tempat_agenda')
                                    ->where('date_agenda', $value_agenda->date_agenda)
                                    ->where('type_agenda', "umum")
                                    ->orderBy('waktu_agenda','asc')
                                    ->get();

                            $value_agenda->detail_agenda = $agenda_det;
                            $cek_event = $cek_event + count($agenda_det);
                        }else{
                            $value_agenda->detail_agenda = "";                    
                        }
                        
                        if($value_agenda->detail_agenda == ""){
                            unset($agenda_lain[$key_agenda]);
                        }
                    }
                }

                 return view('frontend.renungan_detail',compact('renungan','slider','tahun','bln','fb','agenda','agenda_lain'));
        }

        public function indexSearch(Request $request)
        {

       
        
        if(!empty($request->search)){
            $search =  $request->search;
        }else{
            $search = "";
        }

        $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY); 

        $slider = DB::table('m_banner')->where('page_banner','renungan')->first();
        //dd($searchValues);

        $getData = DB::table('m_renungan');
        
        if($search != ""){
            $getData->where(function($q) use ($searchValues){
                foreach ($searchValues as $value) {
                    $q->where('m_renungan.title_renungan','like', '%'.$this->escape_like($value).'%');
                }
            });
        }

        $renungan = $getData->paginate(10);

        //dd($renungan);

        $tahun = DB::table('m_renungan')
                                ->select(DB::raw("YEAR(created_at) as tahun"))
                                ->groupBy(DB::raw("YEAR(created_at)"))
                                ->get();

        
        foreach ($tahun as $key => $value) {
                $bulan = DB::table('m_renungan')
                                ->select(DB::raw("MONTH(created_at) as bulan"))
                                ->whereYear('created_at', $value->tahun)
                                ->groupBy((DB::raw("MONTH(created_at)")))
                                ->get();
                $value->bulan = $bulan;
        }

        $bln="";

       
        return view('frontend.renungan',compact('renungan','slider','tahun','bln'));
    }

    public function escape_like($string)
    {
        $search = array('%', '_','"');
        $replace   = array('\%', '\_','\"');
        return str_replace($search, $replace, $string);
    }

       
}

?>