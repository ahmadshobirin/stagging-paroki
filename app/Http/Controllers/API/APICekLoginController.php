<?php

namespace App\Http\Controllers\API;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class APICekLoginController extends Controller
{
    public function cekDomain(request $request)
    {
        /**
        * Programmer   : Andry
        * Tanggal      : 24-10-2018
        * Fungsi       : cek expired domain
        * Tipe         : read
        */
        $return = [];
        $date_now = date("Y-m-d");
        $data = DB::table('m_subdomain')
                    ->select('id')
                    ->where('m_subdomain.name',$request->subdomain)
                    ->first();
        $cek_expired = DB::table('m_transaksi')
                        ->select('started_date','expired_date')
                        ->where('id_subdomain', $data->id)
                        ->first();
        if($cek_expired->started_date <= $date_now && $date_now <= date("Y-m-d", strtotime($cek_expired->expired_date."+ 1 Days"))){
            $return['success'] = true;
            $return['msgServer'] = $cek_expired;
        }else{
            $return['success'] = false;
            $return['msgServer'] = $cek_expired;
            
        }

        // if (count($data) !== 0) {
        // }else{
        // }

        return response($return);
    }

    protected function resultReturn($success = true ,$msgServer = null, $count = null)
    {
        $return = [];
        $return['success'] = $success;
        $return['msgServer'] = $msgServer;

        if( $count !== null ){
            $return['count'] = $count;
        }

		return response($return);
    }
}
