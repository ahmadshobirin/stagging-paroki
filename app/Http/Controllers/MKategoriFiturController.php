<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MKategoriFiturModel;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use File;
use Storage;

class MKategoriFiturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getDataKategori = MKategoriFiturModel::orderBy('id','DESC')->get();

        return view('admin.master.produk.kategori-fitur.index',compact('getDataKategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.produk.kategori-fitur.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file   = $request->image;
        $icon   = $request->icon;
        //$file   = Input::file('image');
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->name_kategori_fitur.".jpg";
                $destinationPath = 'upload/kategori-fitur';   
                $update_img = 2;
        }else{
            if($request->produk_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/kategori-fitur'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/kategori-fitur'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/kategori-fitur'; 
                $update_img = 1;
            }
        }

        if(!empty($icon)){
                $iconname_path_bukti = date("ymdHis")."ico-".$request->name_kategori_fitur.".jpg";
                $destinationIconPath = 'upload/kategori-fitur/icon';   
                $update_icon = 2;
        }else{
            if($request->produk_icon_hidden == "false"){
                if($request->icon_hidden != "no_image.png"){
                    $iconname_path_bukti = "no_image.png";
                    $destinationIconPath = 'upload/kategori-fitur/icon'; 
                    $update_icon = 2;   
                }else{
                    $iconname_path_bukti  = $request->icon_hidden;
                    $destinationIconPath = 'upload/kategori-fitur/icon';  
                    $update_icon = 1;
                }
            }else{
                $iconname_path_bukti  = $request->icon_hidden;
                $destinationIconPath = 'upload/kategori-fitur/icon';  
                $update_icon = 1;
            }
        }

        $this->validate($request,[
            'name_kategori_fitur' => 'required|max:50|unique:m_kategori_fitur',
            'status_kategori_fitur' => 'required',
            'desc_kategori_fitur' => 'required',
        ]);
        DB::beginTransaction();
        try{

            $request->merge([
                'name_kategori_fitur'       => $request->name_kategori_fitur,
                'status_kategori_fitur'     => $request->status_kategori_fitur,
                'desc_kategori_fitur'       => $request->desc_kategori_fitur,
                'image_kategori_fitur'      => $filename_path_bukti,
                'icon_kategori_fitur'       => $iconname_path_bukti,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id,

            ]);

            MKategoriFiturModel::create($request->except('produk_image_hidden','image_hidden','image','produk_icon_hidden','icon_hidden','icon'));
            if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }
            if(!empty($icon)){
                    $icon->move($destinationIconPath, $iconname_path_bukti);
                }
            DB::commit();

            return redirect('admin/kategori-fitur')->with('message-success', 'Data Berhasil Ditambah');
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataKategori = MKategoriFiturModel::find($id);
        return view('admin.master.produk.kategori-fitur.update',compact('dataKategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file   = $request->image;
        $icon   = $request->icon;
        //$file   = Input::file('image');
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->name_kategori_fitur.".jpg";
                $destinationPath = 'upload/kategori-fitur';   
                $update_img = 2;
            // }else{
            //     $filename_path_bukti = $request->image_hidden;
            //     $destinationPath = 'upload/kategori-fitur'; 
            //     $update_img = 1;
            // }
        }else{
            if($request->produk_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/kategori-fitur'; 
                    $update_img = 2;   
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/kategori-fitur'; 
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/kategori-fitur'; 
                $update_img = 1;
            }
        }

        if(!empty($icon)){
                $iconname_path_bukti = date("ymdHis")."ico-".$request->name_kategori_fitur.".jpg";
                $destinationIconPath = 'upload/kategori-fitur/icon';   
                $update_icon = 2;
        }else{
            if($request->produk_icon_hidden == "false"){
                if($request->icon_hidden != "no_image.png"){
                    $iconname_path_bukti = "no_image.png";
                    $destinationIconPath = 'upload/kategori-fitur/icon'; 
                    $update_icon = 2;   
                }else{
                    $iconname_path_bukti  = $request->icon_hidden;
                    $destinationIconPath = 'upload/kategori-fitur/icon';  
                    $update_icon = 1;
                }
            }else{
                $iconname_path_bukti  = $request->icon_hidden;
                $destinationIconPath = 'upload/kategori-fitur/icon';  
                $update_icon = 1;
            }
        }

        $this->validate($request,[
            'name_kategori_fitur' => 'required|max:50|unique:m_kategori_fitur,name_kategori_fitur,'.$id,
            'status_kategori_fitur' => 'required',
            'desc_kategori_fitur' => 'required',
        ]);
        DB::beginTransaction();
        try{
            $request->merge([
                'name_kategori_fitur'       => $request->name_kategori_fitur,
                'status_kategori_fitur'     => $request->status_kategori_fitur,
                'desc_kategori_fitur'       => $request->desc_kategori_fitur,
                'image_kategori_fitur'      => $filename_path_bukti,
                'icon_kategori_fitur'       => $iconname_path_bukti,
                'updated_at'                => date('Y-m-d H:i:s'),
                'updated_by'                => Auth::user()->id,
            ]);

            if(!empty($file)){
                $file->move($destinationPath, $filename_path_bukti);
            }
            if($update_img == 2){
                File::delete('upload/kategori-fitur/icon'.$request->image_hidden);
            }

            if(!empty($icon)){
                $icon->move($destinationIconPath, $iconname_path_bukti);
            }
            if($update_icon == 2){
                File::delete('upload/kategori-fitur/icon/'.$request->icon_hidden);
            }

            MKategoriFiturModel::where('id',$id)->update($request->except('_token','_method','produk_image_hidden','image_hidden','image','icon','icon_hidden','produk_icon_hidden'));

            DB::commit();

            return redirect('admin/kategori-fitur')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cek = DB::table('m_subkategori_fitur')->where('id_kategori_fitur', $id)->count();
        if ($cek > 0) {
            return redirect()->back()->with('message', 'Data Tidak Bisa Dihapus Karena Sudah Dipakai Untuk Master Subkategori');
        }

        $delete = MKategoriFiturModel::where('id',$id)->delete();

        return redirect()->back()->with('message-success', 'Berhasil Dihapus');
    }

    public function apiKategori()
    {
        $Kategori = MKategoriFiturModel::orderBy('id','DESC')->get();

        return Datatables::of($Kategori)
        ->addColumn('action', function ($Kategori) {
            return '<a href="'.url('admin/kategori-fitur/'.$Kategori->id.'/edit').'" data-toggle="tooltip" data-placement="top" title="Ubah" class="btn btn-warning pull-left btn-sm"><i class="fa fa-edit"></i></a>'.'&nbsp;'.
            '<a href="'.url('/admin/kategori-fitur/delete/'.$Kategori->id).'" onclick="return confirm('."'Apakah Anda Yakin Untuk Menghapus ?'".')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>';
            })

        ->addIndexColumn()
        ->rawColumns(['action'])
        ->make(true);
    }
}
