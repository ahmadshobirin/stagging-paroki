<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\MRoleModel;
use DB;
//use Gloudemans\Shoppingcart\Facades\Cart;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        attemptLogin as attemptLoginAtAuthenticatesUsers;
    }

     /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */

    public function showLoginForm()
    {
        session(['link' => url()->previous()]);
        $cek_tanggal = date('Y-m-d 00:00:00');
        $user = DB::table('m_user')->where('status','active')->get();

        DB::beginTransaction();
        try{
            foreach ($user as $key => $value) {
                if($value->role == 3){
                    if($value->periode_aktif_end < $cek_tanggal){
                        DB::table('m_user')->where('id',$value->id)->update([
                            "status" => 'inactive',
                        ]);
                    }
                }
            }
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        $header = DB::table('m_kategori_user')
                    ->select('m_kategori_user.nama_kategori_user','m_kategori_user.id_kategori_user')
                    ->join('m_user','m_user.id_header_user','m_kategori_user.id_kategori_user')
                    ->where('m_user.status','active')
                    ->groupBy('m_kategori_user.id_kategori_user','m_kategori_user.nama_kategori_user')
                    ->get();
        //dd($header);
        return view('auth.login', compact('header'));
    }

    public function showLoginFormUmat()
    {
        session(['link' => url()->previous()]);
        $cek_tanggal = date('Y-m-d 00:00:00');
        $user = DB::table('m_user')->where('status','active')->get();

        DB::beginTransaction();
        try{
            foreach ($user as $key => $value) {
                if($value->role == 3){
                    if($value->periode_aktif_end < $cek_tanggal){
                        DB::table('m_user')->where('id',$value->id)->update([
                            "status" => 'inactive',
                        ]);
                    }
                }
            }
            DB::commit();
        }catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        //dd($header);
        return view('auth.login_umat');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        if($user->role == 3){
            $today = date('Y-m-d 00:00:00');
            if($user->periode_aktif_start > $today && $user->periode_aktif_end < $today ){
                if($user->status == 'active'){
                    DB::beginTransaction();
                    try{
                        DB::table('m_user')
                            ->where('id', $user->id)
                            ->update([
                                'status' => 'inactive',
                            ]);
                        DB::commit();
                    }catch(\Exception $e){
                        dd($e);
                        DB::rollback();
                    }
                }
            }
        }
        if( $user->status == 'inactive'){
            Auth::logout();

            return redirect('/login')->with('error_message','Akun anda sudah tidak aktif lagi');

        }

        return redirect('/admin');
        // $roleSales = MRoleModel::where('name', 'Admin')->first();
        // $roleCustomer = MRoleModel::where('name', 'Customer')->first();

        // if( $user->role == $roleSales->id ){
        //     return redirect('/admin');
        // }else if($user->role == $roleCustomer->id){
        //     //Cart::restore(Auth::id());
        //     $daftar     = "https://".$_SERVER['SERVER_NAME']."/daftar";
        //     $daftar1    = "localhost:8000/daftar";
        //     $daftar2    = "localhost/biztekweb/public/daftar";
        //     if(session('link') == $daftar || session('link') == $daftar1 || session('link') == $daftar2){
        //         return redirect('/')->with('success_message','Login Berhasil');
        //     }else{
        //         return redirect(session('link'))->with('success_message','Login Berhasil');
        //     }
        // }
    }

    public function username()
    {
        return config('auth.providers.users.field','email');
    }

    protected function attemptLogin(Request $request)
    {
        if ($this->username() === 'email') return $this->attemptLoginAtAuthenticatesUsers($request);
        if ( ! $this->attemptLoginAtAuthenticatesUsers($request)) {
            return $this->attempLoginUsingUsernameAsAnEmail($request);
        }
        return false;
    }

    protected function attempLoginUsingUsernameAsAnEmail(Request $request)
    {
        return $this->guard()->attempt(
            ['email' => $request->input('username'), 'password' => $request->input('password')],
            $request->has('remember'));
    }

    //override login
    protected function credentials(Request $request)
    {
        // dd($request->all());
        // return $request->only($this->username(), 'password');
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'id_header_user';

        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
            'status' => 'active'
        ];
    }

    // public function logout(Request $request)
    // {
    //     if( auth()->user()->role == 3){
    //         if(Cart::count() > 0){
    //             Cart::store(Auth::id());
    //         }
    //     }
    //     $this->guard()->logout();
    //     $request->session()->invalidate();
    //     return redirect('/');
    // }
}
