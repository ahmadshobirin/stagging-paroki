<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;

class MMisaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $misa = DB::table('d_jadwal_misa')
                    ->select('d_jadwal_misa.*','h_jadwal_misa.nama_jadwal_misa')
                    ->join('h_jadwal_misa','h_jadwal_misa.id_jadwal_misa','d_jadwal_misa.id_jadwal_misa')
                    ->orderBy('id_detail_jadwal_misa','DESC')
                    ->get();

        return view('admin.master.misa.index', compact('misa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header_misa = DB::table('h_jadwal_misa')->get();
        return view('admin.master.misa.create', compact('header_misa'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'jadwal_misa' => 'required',
            'hari_misa' => 'required',
            'waktu_misa' => 'required',
        ]);

       

        DB::beginTransaction();
        try{

            DB::table('d_jadwal_misa')->insert([
                'id_jadwal_misa' => $request->jadwal_misa,
                'hari' => $request->hari_misa,
                'waktu' => date("H:i:s", strtotime($request->waktu_misa)),
            ]);

            DB::commit();
            return redirect('admin/gereja/misa')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $dataMisa = DB::table('d_jadwal_misa')->where('id_detail_jadwal_misa',$id)->first();
       $header_misa = DB::table('h_jadwal_misa')->get();
        return view('admin.master.misa.update',compact('dataMisa','header_misa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'jadwal_misa' => 'required',
            'hari_misa' => 'required',
            'waktu_misa' => 'required',
        ]);

       

        DB::beginTransaction();
        try{

            DB::table('d_jadwal_misa')
                    ->where('id_detail_jadwal_misa', $id)
                    ->update([
                        'id_jadwal_misa' => $request->jadwal_misa,
                        'hari' => $request->hari_misa,
                        'waktu' => date("H:i:s", strtotime($request->waktu_misa)),
                    ]);

            DB::commit();
            return redirect('admin/gereja/misa')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{

            $delete = DB::table('d_jadwal_misa')
                            ->where('id_detail_jadwal_misa',$id)
                            ->delete();
            DB::commit();

            return redirect()->back()->with('message-success', 'Berhasil Dihapus');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message', 'Data Gagal Dihapus');
        }
    }

}
