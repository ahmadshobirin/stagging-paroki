<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Response;
use Mail;
use DateTime;
use App\Models\MUserModel;
use App\Mail\ProdukAccMail;
use App\Mail\TransferConfirmMail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

class TTransaksiController extends Controller
{
    public function index()
    {
        $dataSales = DB::table('m_transaksi')
                    ->select('m_user.name as customer_name','m_transaksi.created_at as trx_date','m_transaksi.status_transfer','m_transaksi.status_produk','m_transaksi.code','m_transaksi.expired_date','m_transaksi.id','m_produk.name as product_name','m_subdomain.name as subdomain','m_produk.masa_aktif')
                    //->leftjoin('m_user','m_user.id','t_sales_order.sales')
                    ->join('m_user','m_user.id','m_transaksi.id_user')
                    ->join('m_produk','m_produk.id','m_transaksi.id_produk')
                    ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
                    ->where('id_produk','!=', 1)
                    ->orderBy('m_transaksi.code','desc')
                    ->get();
        $date_now = new DateTime();
        foreach ($dataSales as $key ) {
            $cek_bayar = DB::table('m_pembayaran')->select('status as status_bayar')->where('id_transaksi', $key->id)->where('status','=','unconfirmed')->count();
            $extend = 'false';
            if($key->masa_aktif != 0){
                $exp = new DateTime($key->expired_date);
                $interval = $exp->diff($date_now);
                $days = $interval->format('%a');
                if($days <= 30){
                    $extend = 'true';
                }
                $key->extend = $extend;
            }
            $key->cek_bayar = $cek_bayar;

        }
        $dataSales = $dataSales->sortByDesc('cek_bayar');
        //dd($dataSales);

        return view('admin.transaksi.buy-product',compact('dataSales'));
    }

    public function indexFree()
    {
         $dataSales = DB::table('m_transaksi')
        ->select('m_user.name as customer_name','m_transaksi.created_at as trx_date','m_transaksi.status_transfer','m_transaksi.status_produk','m_transaksi.code','m_transaksi.expired_date','m_transaksi.id')
        //->leftjoin('m_user','m_user.id','t_sales_order.sales')
        ->join('m_user','m_user.id','m_transaksi.id_user')
        ->where('id_produk', 1)
        ->orderBy('m_transaksi.code','desc')
        ->get();

        return view('admin.transaksi.free-trial',compact('dataSales'));
    }

    public function detailFree($code)
    {
        $dataSales = DB::table('m_transaksi')
                            ->select('m_user.name as customer_name','m_transaksi.created_at as trx_date','m_transaksi.status_transfer','m_transaksi.status_produk','m_transaksi.code','m_transaksi.expired_date','m_transaksi.id','m_transaksi.username','m_transaksi.nama_perusahaan')
                            //->leftjoin('m_user','m_user.id','t_sales_order.sales')
                            ->join('m_user','m_user.id','m_transaksi.id_user')
                            ->where('m_transaksi.code', $code)
                            ->orderBy('m_transaksi.code','desc')
                            ->first();

        return Response::json($dataSales);
    }

    public function inApproval()
    {
        $datainApproval = DB::table('t_sales_order')
        ->select('m_user.name as user_name','m_customer.name as customer_name','t_sales_order.so_date','t_sales_order.status_approve','t_sales_order.so_code')
        ->join('m_user','m_user.id','t_sales_order.sales')
        ->join('m_customer','m_customer.id','t_sales_order.customer')
        ->where('status_approve', 'in-approval')
        ->get();

        return view('admin.transaksi.sales-order.inapproval',compact('datainApproval'));
    }

    public function approved()
    {
        $dataApproved = DB::table('t_sales_order')
        ->select('m_user.name as user_name','m_customer.name as customer_name','t_sales_order.so_date','t_sales_order.status_approve','t_sales_order.so_code')
        ->join('m_user','m_user.id','t_sales_order.sales')
        ->join('m_customer','m_customer.id','t_sales_order.customer')
        //->where('status_approve', 'approved')
        ->get();

        return view('admin.transaksi.sales-order.approved',compact('dataApproved'));
    }

    public function freewaitinglist()
    {
        $dataSales = DB::table('m_transaksi')
        ->select('m_user.name as customer_name','m_transaksi.created_at as trx_date','m_transaksi.status_transfer','m_transaksi.status_produk','m_transaksi.code')
        //->leftjoin('m_user','m_user.id','t_sales_order.sales')
        ->join('m_user','m_user.id','m_transaksi.id_user')
        ->where('status_produk', 'inactive')
        ->where('id_produk', 1)
        ->orderBy('m_transaksi.code','desc')
        ->get();

        return view('admin.transaksi.free-waiting-approval',compact('dataSales'));
    }

    public function buywaitinglist()
    {
        $dataSales = DB::table('m_pembayaran')
        ->select('m_pembayaran.name_rekening_asal','m_bank.name as nama_bank','m_pembayaran.tanggal_transfer','m_pembayaran.nominal_transfer','m_transaksi.code','m_produk.name as nama_produk','m_pembayaran.id as id_bayar','m_subdomain.name as subdomain')
        //->leftjoin('m_user','m_user.id','t_sales_order.sales')
        ->join('m_bank','m_bank.id','m_pembayaran.id_bank_asal')
        ->join('m_transaksi','m_transaksi.id','m_pembayaran.id_transaksi')
        ->join('m_produk','m_produk.id','m_transaksi.id_produk')
        ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
        ->where('m_pembayaran.status', 'unconfirmed')
        ->orderBy('m_transaksi.code','desc')
        ->get();

        return view('admin.transaksi.buy-product-waiting-approval',compact('dataSales'));
    }

    public function buywaitinglist1($code)
    {
        $dataSales = DB::table('m_pembayaran')
        ->select('m_pembayaran.name_rekening_asal','m_bank.name as nama_bank','m_pembayaran.tanggal_transfer','m_pembayaran.nominal_transfer','m_transaksi.code','m_produk.name as nama_produk','m_pembayaran.id as id_bayar','m_subdomain.name as subdomain')
        //->leftjoin('m_user','m_user.id','t_sales_order.sales')
        ->join('m_bank','m_bank.id','m_pembayaran.id_bank_asal')
        ->join('m_transaksi','m_transaksi.id','m_pembayaran.id_transaksi')
        ->join('m_produk','m_produk.id','m_transaksi.id_produk')
        ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
        ->where('m_pembayaran.status', 'unconfirmed')
        ->where('m_transaksi.code', $code)
        ->get();

        return view('admin.transaksi.buy-product-waiting-approval',compact('dataSales'));
    }

    public function detailBuyProduct($code)
    {
        $dataSales = DB::table('m_transaksi')
                    ->select('m_user.name as customer_name','m_transaksi.created_at as trx_date','m_transaksi.status_transfer','m_transaksi.status_produk','m_transaksi.code as code_trx','m_transaksi.expired_date','m_transaksi.id','m_produk.name as product_name','m_transaksi.username','m_transaksi.nama_perusahaan','m_subdomain.name as subdomain')
                    //->leftjoin('m_user','m_user.id','t_sales_order.sales')
                    ->join('m_user','m_user.id','m_transaksi.id_user')
                    ->join('m_produk','m_produk.id','m_transaksi.id_produk')
                    ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
                    ->where('m_transaksi.code', $code)
                    ->orderBy('m_transaksi.code','desc')
                    ->first();

        return Response::json($dataSales);
    }

    public function detailTransferBuy($code)
    {
        $dataSales = DB::table('m_pembayaran')
                        ->select('m_pembayaran.name_rekening_asal','m_bank.name as nama_bank','m_pembayaran.tanggal_transfer','m_pembayaran.nominal_transfer','m_transaksi.code as code_trx','m_produk.name as nama_produk','m_pembayaran.id as id_bayar','m_subdomain.name as subdomain','m_pembayaran.image','m_rekening.name as nama_rekening_tujuan','m_rekening.nomer_rekening as no_rekening_tujuan','bank_rekening.name as nama_bank_rekening_tujuan')
                        //->leftjoin('m_user','m_user.id','t_sales_order.sales')
                        ->join('m_bank','m_bank.id','m_pembayaran.id_bank_asal')
                        ->join('m_transaksi','m_transaksi.id','m_pembayaran.id_transaksi')
                        ->join('m_produk','m_produk.id','m_transaksi.id_produk')
                        ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
                        ->join('m_rekening','m_rekening.id','m_pembayaran.id_rekening_tujuan')
                        ->join('m_bank as bank_rekening','bank_rekening.id','m_rekening.id_bank')
                        ->where('m_pembayaran.status', 'unconfirmed')
                        ->where('m_transaksi.code', $code)
                        ->first();

        return Response::json($dataSales);
    }


    public function approveFreewaitinglist($code)
    {
        DB::beginTransaction();
        try{
        // $date_now =  date('Y-m-d');
        // $date_expired = date('Y-m-d', strtotime($date_now.'+ 1 month'));
        

        $cek_user = DB::table('m_transaksi')
                        ->select('m_transaksi.*','m_user.email','m_user.name as name_user','m_produk.name as name_produk','m_subdomain.name as subdomain','m_produk.masa_aktif')
                        ->join('m_user','m_user.id','m_transaksi.id_user')
                        ->join('m_produk','m_produk.id','m_transaksi.id_produk')
                        ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
                        ->where('m_transaksi.code',$code)
                        ->first();

        $date_now =  date('Y-m-d');
        $date_expired = date('Y-m-d', strtotime($date_now.'+ '.$cek_user->masa_aktif.' month'));
        
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $randomstring = '';
        for ($i=0; $i<6; $i++) {
            $randomstring.=$chars{rand(0, strlen($chars)-1)};
        }
        $user = $cek_user->email;
        $pass = bcrypt($randomstring);
        $cc = $cek_user->company_code;

        $dataSales = DB::table('m_transaksi')
                    ->where('code',$code)->update([
                        'username'      => $user,
                        'password'      => $randomstring,
                        'status_produk' => "active",
                        'started_date'  => $date_now,
                        'expired_date'  => $date_expired,
                        'updated_at'    => date('Y-m-d H:i:s'),
                        'updated_by'    => Auth::user()->id,
                    ]);


        ini_set('max_execution_time', 1000);
        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, "trial.biztekweb.com/api/user-trial-create?nama=".urlencode($cek_user->name_user)."&email=".$user."&password=".$randomstring."&company_code=".$cc);

        curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

        $jsonData = json_decode(curl_exec($curlSession));
        // var_dump($jsonData);
        // die();
        //dd($cek_user->name_user, $jsonData,$user, $randomstring, $cc);
        //dd( $jsonData->success);
        curl_close($curlSession);


        Mail::to($user)->send(new ProdukAccMail($cek_user,$randomstring));
        DB::commit();
        //dd($jsonData);

        return redirect()->back()->with('message-success','Free Trial '.$code.' berhasil disetujui');
         }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }

    public function listaccbuywaitinglist(){
        $dataSales = DB::table('m_transaksi')
                    ->select('m_user.name as customer_name','m_transaksi.created_at as trx_date','m_transaksi.status_transfer','m_transaksi.status_produk','m_transaksi.code','m_transaksi.expired_date','m_transaksi.id','m_produk.name as product_name','m_subdomain.name as subdomain','m_produk.masa_aktif')
                    //->leftjoin('m_user','m_user.id','t_sales_order.sales')
                    ->join('m_user','m_user.id','m_transaksi.id_user')
                    ->join('m_produk','m_produk.id','m_transaksi.id_produk')
                    ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
                    ->where('id_produk','!=', 1)
                    ->where('status_produk','=', 'inactive')
                    ->where('status_transfer','=', 'accept')
                    ->orderBy('m_transaksi.code','desc')
                    ->get();

        return view('admin.transaksi.list-acc-buy-product',compact('dataSales'));
    }

    public function accbuywaitinglist(Request $request)
    {
        $this->validate($request,[
            'code_trx' => 'required',
            'username' => 'required',
            'password' => 'required',
            'subdomain' => 'required',
        ]);
        DB::beginTransaction();
        try{

        $cek_user = DB::table('m_transaksi')
                        ->select('m_transaksi.*','m_user.email','m_user.name as name_user','m_produk.name as name_produk','m_subdomain.name as subdomain','m_produk.masa_aktif')
                        ->join('m_user','m_user.id','m_transaksi.id_user')
                        ->join('m_produk','m_produk.id','m_transaksi.id_produk')
                        ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
                        ->where('m_transaksi.code',$request->code_trx)
                        ->first();

        if($cek_user->status_produk == "inactive"){
        
            $date_now =  date('Y-m-d');
            $date_expired = date('Y-m-d', strtotime($date_now.'+ '.$cek_user->masa_aktif.' month'));
            
            // $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            // $randomstring = '';
            // for ($i=0; $i<6; $i++) {
            //     $randomstring.=$chars{rand(0, strlen($chars)-1)};
            // }
            $user = $request->username;
            $pass = $request->password;
            $cc   = $cek_user->company_code;

            $cek_user->username = $user;

            if($cek_user->masa_aktif != 0){

            $dataSales = DB::table('m_transaksi')
                        ->where('code',$request->code_trx)->update([
                            'username'      => $user,
                            'password'      => $pass,
                            'status_produk' => "active",
                            'started_date'  => $date_now,
                            'expired_date'  => $date_expired,
                            'updated_at'    => date('Y-m-d H:i:s'),
                            'updated_by'    => Auth::user()->id,
                        ]);
            }else{
                $dataSales = DB::table('m_transaksi')
                        ->where('code',$request->code_trx)->update([
                            'username'      => $user,
                            'password'      => $pass,
                            'status_produk' => "active",
                            'started_date'  => $date_now,
                            'updated_at'    => date('Y-m-d H:i:s'),
                            'updated_by'    => Auth::user()->id,
                        ]);
            }

            DB::table('m_subdomain')
            ->where('id', $cek_user->id_subdomain)
            ->update([
                'status' => 'active',
                'updated_at'    => date('Y-m-d H:i:s'),
            ]);


            // ini_set('max_execution_time', 1000);
            // $curlSession = curl_init();
            // curl_setopt($curlSession, CURLOPT_URL, "http://trial.biztekweb.com/insert_user?user=".$user."&password=".$pass."&company_code=".$cc);

            // curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
            // curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

            // $jsonData = json_decode(curl_exec($curlSession));
            // curl_close($curlSession);

            Mail::to($cek_user->email)->send(new ProdukAccMail($cek_user,$pass));
            DB::commit();

            return redirect()->back()->with('message-success','Transaksi '.$request->code.' berhasil aktif');
        }else{
            return redirect()->back()->with('message','Transaksi '.$request->code.' sudah diproses sebelumnya');
        }
         }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }

    public function approveTransferBuyProduct($id,$code)
    {
        DB::beginTransaction();
        try{
            $cek_bayar = DB::table('m_pembayaran')->where('id',$id)->first();
            if($cek_bayar->status == "unconfirmed"){
                DB::table('m_pembayaran')->where('id',$id)
                        ->update([
                            'status' => 'accept',
                            'confirmed_by' => Auth::user()->id,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                DB::table('m_transaksi')->where('id',$cek_bayar->id_transaksi)
                        ->update([
                            'status_transfer' => 'accept',
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                $confirm = DB::table('m_pembayaran')
                                ->select('m_user.name as customer_name','m_pembayaran.status','m_transaksi.code as code_trx','m_pembayaran.email_pengirim','bank_rekening_tujuan.name as nama_bank_tujuan','m_rekening.nomer_rekening','m_rekening.name as nama_rekening_tujuan','m_pembayaran.tanggal_transfer','m_pembayaran.nominal_transfer','m_pembayaran.keterangan_admin')
                                ->join('m_transaksi','m_transaksi.id','m_pembayaran.id_transaksi')
                                ->join('m_rekening','m_rekening.id','m_pembayaran.id_rekening_tujuan')
                                ->join('m_user','m_user.id','m_transaksi.id_user')
                                ->join('m_bank as bank_rekening_tujuan','bank_rekening_tujuan.id','m_rekening.id_bank')
                                ->where('m_pembayaran.id',$id)
                                ->first();                
                $telp_office = DB::table('m_office')->select('telp_office')->where('id',1)->first();

                $confirm->telp_office = $telp_office->telp_office;
                
                Mail::to($cek_bayar->email_pengirim)->send(new TransferConfirmMail($confirm));
                DB::commit();
                return redirect()->back()->with('message-success', 'Pembayaran untuk kode Transaksi '.$code.' berhasil disetujui');
            }else{
                return redirect()->back()->with('message','Pembayaran untuk kode Transaksi '.$code.' telah diproses sebelumnya');
            }
         }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }

    public function rejectTransferBuyProduct($id)
    {   
        $cek_bayar = DB::table('m_pembayaran')
        ->select('m_pembayaran.*','m_transaksi.code as code_trx','m_produk.name as nama_produk','m_subdomain.name as subdomain')
        ->join('m_transaksi','m_transaksi.id','m_pembayaran.id_transaksi')
        ->join('m_produk','m_produk.id','m_transaksi.id_produk')
        ->join('m_subdomain','m_subdomain.id','m_transaksi.id_subdomain')
        ->where('m_pembayaran.id',$id)
        ->first();
        return view('admin.transaksi.reject-transfer-buy-product', compact('cek_bayar'));
    }

     public function postrejectTransferBuyProduct(Request $request)
    {   
        DB::beginTransaction();
        try{
            $cek_bayar = DB::table('m_pembayaran')->where('id',$request->id)->first();
            if($cek_bayar->status == "unconfirmed"){
                DB::table('m_pembayaran')->where('id',$request->id)
                        ->update([
                            'status' => 'reject',
                            'keterangan_admin' => $request->reject_reason,
                            'confirmed_by' => Auth::user()->id,
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                $confirm = DB::table('m_pembayaran')
                                ->select('m_user.name as customer_name','m_pembayaran.status','m_transaksi.code as code_trx','m_pembayaran.email_pengirim','bank_rekening_tujuan.name as nama_bank_tujuan','m_rekening.nomer_rekening','m_rekening.name as nama_rekening_tujuan','m_pembayaran.tanggal_transfer','m_pembayaran.nominal_transfer','m_pembayaran.keterangan_admin')
                                ->join('m_transaksi','m_transaksi.id','m_pembayaran.id_transaksi')
                                ->join('m_user','m_user.id','m_transaksi.id_user')
                                ->join('m_rekening','m_rekening.id','m_pembayaran.id_rekening_tujuan')
                                ->join('m_bank as bank_rekening_tujuan','bank_rekening_tujuan.id','m_rekening.id_bank')
                                ->where('m_pembayaran.id',$request->id)
                                ->first();                
                $telp_office = DB::table('m_office')->select('telp_office')->where('id',1)->first();

                $confirm->telp_office = $telp_office->telp_office;
                
                Mail::to($cek_bayar->email_pengirim)->send(new TransferConfirmMail($confirm));
                DB::commit();
                //Mail::to($cek_bayar->email_pengirim)->send(new TransferConfirmMail($confirm));
                return redirect('admin/transaksi/waiting-buy-product')->with('message-success','Pembayaran untuk kode Transaksi '.$request->code_trx.' berhasil ditolak');
            }else{
                return redirect('admin/transaksi/waiting-buy-product')->with('message','Pembayaran kode Transaksi '.$request->code_trx.' telah diproses sebelumnya');
            }
         }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }
}
