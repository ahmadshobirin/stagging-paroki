<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use Auth;
use File;
use Image;
use Mail;
use Response;
use Carbon\Carbon;
use App\Models\LPJ;
use App\Models\Proposal;
use App\Models\LPJBudgetIn;
use Illuminate\Http\Request;
use App\Models\LPJBudgetOut;
use App\Models\LPJSignature;
use App\Mail\ApproveProposal;
use App\Helpers\CollectDetail;
use Yajra\DataTables\DataTables;

class LPJController extends Controller
{
    protected $usersUpline;

    public function index($type = null)
    {
        $type = is_null($type) ? 'default' : 'submission';

        return view('admin/posting/lpj/index',compact('type'));
    }

    public function upload()
    {
        return view('admin/posting/lpj/upload', ['editmode' => null]);
    }

    public function create()
    {
        $proposal = DB::table('proposal')
            ->join('m_agenda', 'm_agenda.proposal_id', 'proposal.id')
            ->whereIn('m_agenda.id_agenda', function ($q) {
                $q->from('m_posting')->where('type_post', 'kronik')->select('id_agenda');
            })
            ->whereNotIn('proposal.id', function ($x) {
                $x->from('lpj')->select('proposal_id')->where('status','!=','reject');
            })
            ->where('proposal.status', 'approved')
            ->where('proposal.type', 'form')
            ->where('user_id', Auth::user()->id)
            ->get();

        return view('admin.posting.lpj.form', compact('proposal'));
    }

    public function getAgenda($id = null)
    {
        $agenda = DB::table('m_agenda')->leftjoin('m_tempat', 'm_tempat.id_tempat', 'm_agenda.tempat_agenda')->where('proposal_id', $id)->get();
        $tempat = "";
        $waktu = "";
        foreach ($agenda as $key => $value) {
            if ($key == 0) {
                $tempat = $value->nama_tempat;
                $waktu = date('d F Y', strtotime($value->date_agenda));
            } else {
                $waktu = $waktu . "</br>" . date('d F Y', strtotime($value->date_agenda));
            }
        }

        $all = $tempat . "</br></br>" . $waktu;
        return Response::json($all);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        if ($request->hasFile('file_upload'))
            return $this->storeUpload($request);

        $this->validations($request);

        $file = $request->file('nota');
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $nota = $file->getClientOriginalName() . time() . '.' . $extension;
        $file->move('upload/lpj/', $nota);

        $file2 = $request->file('kegiatan');
        $extension = $file2->getClientOriginalExtension(); // getting image extension
        $kegiatan = $file2->getClientOriginalName() . time() . '.' . $extension;
        $file2->move('upload/lpj/', $kegiatan);

        $request->merge([
            'user_id' => Auth::user()->id,
            'createuser' => AUth::user()->name,
            'bidang_user_id' => Auth::user()->id_header_user,
            'foto_nota' => "upload/lpj/$nota",
            'foto_kegiatan' => "upload/lpj/$kegiatan",
        ]);

        $exceptReq = $this->exceptFeld();

        $pro = LPJ::create($request->except($exceptReq));

        $dtl = $this->detailcollections($request); //collect-pengeluaran-detail

        //create-many-with-relations
        $pro->budget_in()->createMany($dtl['in']);
        $pro->budget_out()->createMany($dtl['out']);

        return redirect()->route('lpj.index');
    }

    public function storeUpload($request)
    {
        $request->validate([
            'judul' => 'required',
            'file_upload' => 'required'
        ]);

        $file = $request->file('file_upload');
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $name = $file->getClientOriginalName() . time() . '.' . $extension;
        $file->move('upload/lpj/', $name);

        $request->merge([
            'proposal_id' => 0, //set 0
            'user_id' => Auth::user()->id,
            'createuser' => AUth::user()->name,
            'bidang_user_id' => Auth::user()->id_header_user,
            'file' => "upload/lpj/$name",
            'type' => 'upload',
        ]);

        $exceptReq = $this->exceptFeld();

        $lpj = LPJ::create($request->except($exceptReq));

        $dtl = $this->detailcollections($request); //collect-pengeluaran-detail

        //create-many-with-relations
        $lpj->budget_in()->createMany($dtl['in']);
        $lpj->budget_out()->createMany($dtl['out']);

        return redirect()->route('lpj.index');
    }

    public function show($id)
    {
        return PDF::loadView('admin.posting.lpj.printout', [
            'lpj' => LPJ::with(['budget_in', 'budget_out', 'signature' => function($q){
                $q->where('status','complete')->orderBy('id');
            }])->find($id),
        ])->stream();
    }

    public function edit($id)
    {
        $data = LPJ::with(['budget_in', 'budget_out'])->where('id', $id)->first();

        $proposal = DB::table('proposal')
            ->join('m_agenda', 'm_agenda.proposal_id', 'proposal.id')
            ->whereIn('m_agenda.id_agenda', function ($q) {
                $q->from('m_posting')->where('type_post', 'kronik')->select('id_agenda');
            })
            ->whereNotIn('proposal.id', function ($x) use ($id) {
                $x->from('lpj')->select('proposal_id')->where('id', '!=', $id);
            })
            ->where('proposal.status', 'approved')
            ->where('proposal.user_id', Auth::user()->id)->get();

        $pathView = ($data->type == 'form')
            ? 'admin.posting.lpj.form'
            : 'admin.posting.lpj.upload';

        return view($pathView, [
            'data' => $data->toJson(),
            'proposal' => $proposal,
            'id' => $id,
            'editmode' => true
        ]);
    }

    public function update(Request $request, $id)
    {
        // $this->validations($request);

        $pro = LPJ::where('id', $id)->first();

        if ($pro->type == 'upload')
            return $this->updateUpload($request, $id);

        DB::beginTransaction();
        try {
            $exceptReq = $this->exceptFeld();

            if($request->hasFile('nota')){
                $file = $request->file('nota');
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $not = $file->getClientOriginalName() . time() . '.' . $extension;
                $file->move('upload/lpj/', $not);

                $request->merge(['foto_nota' => "upload/proposal/$not"]);
                File::delete($pro->foto_nota); //delete old images
            }

            if($request->hasFile('kegiatan')){
                $file2 = $request->file('kegiatan');
                $extension = $file2->getClientOriginalExtension(); // getting image extension
                $keg = $file2->getClientOriginalName() . time() . '.' . $extension;
                $file2->move('upload/lpj/', $keg);

                $request->merge(['foto_kegiatan' => "upload/proposal/$keg"]);
                File::delete($pro->foto_kegiatan); //delete old images
            }

            $pro->update($request->except($exceptReq)); //update-header

            LPJBudgetIn::where('lpj_id', $id)->delete(); //detete-detail
            LPJBudgetOut::where('lpj_id', $id)->delete();

            //insert-detail
            $dtl = $this->detailcollections($request); //collect-pengeluaran-detail
            $pro->budget_in()->createMany($dtl['in']);  //create-many-with-relations
            $pro->budget_out()->createMany($dtl['out']);

            DB::commit();
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
        }

        return redirect()->route('lpj.index');
    }


    public function updateUpload($request, $id)
    {
        $pro = LPJ::where('id', $id)->first();
        // dd($request->all());
        DB::beginTransaction();
        try {

            if ($request->hasFile('file_upload')) {
                $file = $request->file('file_upload');
                $extension = $file->getClientOriginalExtension();
                $name = $file->getClientOriginalName() . time() . '.' . $extension;
                $file->move('upload/lpj/', $name);

                $request->merge(['file' => "upload/lpj/$name"]);
                File::delete($pro->file); //delete old images
            }

            $exceptReq = $this->exceptFeld();
            $pro->update($request->except($exceptReq)); //update-header

            LPJBudgetIn::where('lpj_id', $id)->delete(); //detete-detail
            LPJBudgetOut::where('lpj_id', $id)->delete();

            //insert-detail
            $dtl = $this->detailcollections($request); //collect-pengeluaran-detail
            $pro->budget_in()->createMany($dtl['in']);  //create-many-with-relations
            $pro->budget_out()->createMany($dtl['out']);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

        return redirect()->route('lpj.index');
    }

    public function destroy($id)
    {
        $pro = LPJ::where('id', $id)->first();
        if ($pro == null) {
            return response()->json('data not found', 404);
        }

        $pro->delete();
    }

    public function upStatus($oldstatus, $id, $changestatus)
    {
        if ($changestatus == "add-signature") {
            return view('admin/posting/lpj/signature');
        }

        $pro = LPJ::where('id', $id)->where('status', $oldstatus)->first();
        if ($pro == null) {
            return response()->json('data not found', 404);
        }

        if ($changestatus == 'approved') {
            $this->sendEmail($pro,"lpj");
            $this->signature($pro->id);
        }

        $pro->update([
            'status' => $changestatus
        ]);

        return back();
    }

    public function addSignature($id)
    {
        $signature = LPJSignature::where('lpj_id', $id)->where('user_id', Auth::user()->id)->where('status', 'pending')->first();
        $user = (!empty($signature)) ? $signature->user_id : '';

        if (!empty($signature)) {

            if ($signature->sequence == 1) {
                $options = [
                    'permission' => true,
                    'message' => null,
                ];
            } else {
                $cek = LPJSignature::where('lpj_id', $id)
                    ->where('sequence', ($signature->sequence - 1))
                    ->where('status', 'complete')->first();
                if (!empty($cek)) {
                    $options = [
                        'permission' => true,
                        'message' => null,
                    ];
                } else {
                    $options = [
                        'permission' => false,
                        'message' => "User sebelum anda belum menyetujui",
                    ];
                }
            }
        } else {
            $options = [
                'permission' => false,
                'message' => "TIdak punya akses",
            ];
        }

        return view('admin/posting/lpj/signature', ['id' => $id, 'options' => $options, 'user' => $user]);
    }

    public function addSignaturePost(Request $request)
    {
        $imageFile = Image::make($request->signature);
        $imageFile->resize(240, 240);

        if (!\File::isDirectory(public_path('upload/lpj/signature'))) {
            \File::makeDirectory(public_path('upload/lpj/signature'), 493, true);
        }

        $path = "upload/lpj/signature/signature." . Carbon::now()->format('his') . ".png";

        $save = $imageFile->save(public_path($path));

        $request->merge([
            'signature_path' => $path,
            'status' => ($path != null) ? "complete" : 'pending',
        ]);

        LPJSignature::where('lpj_id', $request->id)
            ->where('user_id', $request->user_id)->update($request->except('_token', 'signature','user_id','id'));

        $cekSignature = LPJSignature::where('lpj_id',$request->id)->where('status','pending')->get();

        if(count($cekSignature) == 0){
            LPJ::where('id',$request->id)->update([
                'status' => 'finish'
            ]);
        }

        return response()->json(['success' => true, 'url' => route('lpj.index')]);
    }

    public function signatureReject($id)
    {
        $cekSignature = LPJSignature::where('lpj_id',$id)
                    ->where('user_id',Auth::user()->id)
                    ->first();

        if($cekSignature != null){
            DB::beginTransaction();

            try {
                $cekSignature->update([
                    'status' => 'reject'
                ]);

                $lpj = LPJ::where('id',$id)->first();

                $userCreated = DB::table('m_user')->where('id',$lpj->user_id)->first();
                $note = ($lpj->note != null) ? "dengan alasan ".$lpj->note : '';

                $lpj->update([ 'status' => 'reject' ]); //update-status

                Mail::to($userCreated->email)->send(new ApproveProposal("Mohon maaf LPJ Anda ditolak $note", "Reminder Cancel LPJ")); //send email

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                dd($e);
            }

            return back();
        }

        dd($cekSignature);
    }

    public function download($id)
    {
        $pro = LPJ::where('id', $id)->first();
        return response()->download( ($pro->file));
    }

    protected function validations($request)
    {
        $request->validate([

            'proposal_id' => 'required',
            'no_lpj' => 'required',
            'judul' => 'required',
            'nama_kegiatan' => 'required',
            'latar_belakang' => 'required',
            'tujuan_kegiatan' => 'required',
            'jenis_kegiatan' => 'required',
            'tempat_waktu_kegiatan' => 'required',
            'peserta_kegiatan' => 'required',
            'refleksi' => 'required',
            'evaluasi_kegiatan' => 'required',
            'hasil_kegiatan' => 'required',
            'penutup' => 'required',
            'nota' => 'required',
            'kegiatan' => 'required',
        ]);
    }

    protected function exceptFeld()
    {
        return [
            '_token', '_method', 'pemasukan', 'pemasukan_total',
            'pengeluaran', 'pengeluaran_price', 'pengeluaran_qty', 'pengeluaran_total', 'file_upload','nota','kegiatan'
        ];
    }

    protected function detailcollections($request)
    {
        $in = CollectDetail::get(
            $request->only([
                'pemasukan', 'pemasukan_total'
            ]),
            null,
            [
                'pemasukan_total' => 'integer',
            ]
        );

        $out = CollectDetail::get(
            $request->only([
                'pengeluaran', 'pengeluaran_price', 'pengeluaran_qty', 'pengeluaran_total',
            ]),
            null,
            [
                'pengeluaran_price' => 'integer',
                'pengeluaran_qty' => 'integer',
                'pengeluaran_total' => 'integer',
            ]
        );

        return [
            'in' => $in,
            'out' => $out
        ];
    }

    public function table($userid = null,$type = null)
    {
        $user = DB::table('m_user')->where('id', $userid)->first();

        if($type == 'submission'){
            $lpj = DB::table('lpj_signature')->join('lpj','lpj_signature.lpj_id','lpj.id')
                ->select('lpj.id', 'lpj.user_id','lpj.createuser', 'lpj.judul', 'lpj.type', 'lpj.status', 'lpj.created_at', 'lpj.updated_at')
                ->where('lpj_signature.user_id',$user->id)
                ->orderBy('lpj_signature.id','desc');
        }else{
            $lpj = DB::table('lpj')
                ->select('id', 'user_id','createuser', 'judul', 'type', 'status', 'created_at', 'updated_at')
                ->orderBy('id', 'desc');
            if(!in_array($user->role,[1,2])){
                $lpj->where('user_id',$user->id);
            }

        }

        $lpj = $lpj;

        return DataTables::of($lpj)
            ->editColumn('createuser', function ($self) {
                return ucwords($self->createuser);
            })
            ->editColumn('judul', function ($self) use ($user){
                return '<a href="' . route('lpj.show', $self->id) . '">' . ucwords($self->judul) . '</a>';
            })
            ->editColumn('status', function ($self) {
                return $this->tablecheckstatus($self->status);
            })
            ->editColumn('created_at', function ($self) {
                return date('d M Y', strtotime($self->created_at));
            })
            ->editColumn('type', function ($self) {
                $class = ($self->type == 'form') ? 'default' : 'primary';
                return "<span class='label label-$class'>" . ucfirst($self->type) . "</span>";
            })
            ->addColumn('action', function ($self) use ($user) {
                return view('admin/posting/lpj/_actions', [
                    'data' => $self,
                    'user' => $user,
                    'links' => [
                        'show' => route('lpj.show', $self->id),
                        'edit' => route('lpj.edit', $self->id),
                        'destroy' => route('lpj.destroy', $self->id),
                    ]
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'created_at', 'status', 'judul', 'type'])
            ->make(true);
    }

    protected function tablecheckstatus($status, $class = null)
    {

        if ($status == 'in-approval') {
            $class = 'warning';
        } elseif ($status == 'approved') {
            $class = 'primary';
        } elseif ($status == 'cancel') {
            $class = 'danger';
        } else {
            $class = 'default';
        }

        return "<span class='label label-$class'>" . ucwords($status) . "</span>";
    }

    public function sendEmail($lpj)
    {
        $muser = new MUserController();
        $this->usersUpline  =  $muser->getUpline($lpj->user_id);
        // $this->usersUpline =  $muser->getUpline(171);
        // dd($this->usersUpline);

        $text = "Mohon cek akun Anda di website Gereja Katolik Roh Kudus,
                karena ada permohonan pengajuan LPJ $lpj->judul dari $lpj->createuser";

        foreach ($this->usersUpline as $us) {
            Mail::to($us['email'])->send(new ApproveProposal($text, "Reminder LPJ"));
        }
    }

    public function signature($lpjId)
    {
        $signatures = [];
        foreach ($this->usersUpline as $key => $us) {
            $signatures[] = [
                'sequence' => ($key + 1),
                'lpj_id' => $lpjId,
                'user_id' => $us['id'],
                'username' => $us['name'],
                'signature_path' => null,
                'created_at' => now(),
            ];
        }

        LPJSignature::insert($signatures);
    }

    public function note(Request $request,$id)
    {
        $pro = LPJ::select('note')->where('id',$id)->first();

        return ($request->isMethod('GET')) ? view("admin/posting/lpj/form-note",[ 'id' => $id,'pro' => $pro]) : $this->notePost($request,$id);
    }

    public function notePost($req,$id)
    {
        LPJ::where('id',$id)->update([
            'note' => $req->note,
            'updateuser' => Auth::user()->name,
        ]);

        return redirect()->route('lpj.index');
    }
}
