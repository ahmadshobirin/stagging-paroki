<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Auth;
use DB;
use Response;
use File;

class MSejarahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sejarah = DB::table('m_sejarah')->orderBy('id_sejarah','DESC')->get();

        return view('admin.master.sejarah.index', compact('sejarah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.master.sejarah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'tag' => 'required',
            'isi' => 'required',
        ]);

        $file   = $request->image;
        if(!empty($file)){
            $filename_path_bukti = date("ymdHis").$request->title.".jpg";
            $destinationPath = 'upload/sejarah';
        }else{
            $filename_path_bukti = "no_image.png";
        }


        DB::beginTransaction();
        try{

            DB::table('m_sejarah')->insert([
                'title_sejarah' => $request->title,
                'isi_sejarah' => $request->isi,
                'tag_sejarah' => $request->tag,
                'image_sejarah' => $filename_path_bukti,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            if(!empty($file)){
                    $file->move($destinationPath, $filename_path_bukti);
                }

            DB::commit();
            return redirect('admin/gereja/sejarah')->with('message-success', 'Data Berhasil Ditambah');

        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Ditambah');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $dataSejarah = DB::table('m_sejarah')->where('id_sejarah',$id)->first();
        return view('admin.master.sejarah.update',compact('dataSejarah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'title' => 'required',
            'tag' => 'required',
            'isi' => 'required',
        ]);

          $file   = $request->image;
        if(!empty($file)){
                $filename_path_bukti = date("ymdHis").$request->title.".jpg";
                $destinationPath = 'upload/sejarah';
                $update_img = 2;
        }else{
            if($request->value_image_hidden == "false"){
                if($request->image_hidden != "no_image.png"){
                    $filename_path_bukti = "no_image.png";
                    $destinationPath = 'upload/sejarah';
                    $update_img = 2;
                }else{
                    $filename_path_bukti = $request->image_hidden;
                    $destinationPath = 'upload/sejarah';
                    $update_img = 1;
                }
            }else{
                $filename_path_bukti = $request->image_hidden;
                $destinationPath = 'upload/sejarah';
                $update_img = 1;
            }
        }



        DB::beginTransaction();
        try{

            DB::table('m_sejarah')->where('id_sejarah',$id)
            ->update([
                'title_sejarah' => $request->title,
                'isi_sejarah' => $request->isi,
                'tag_sejarah' => $request->tag,
                'image_sejarah' => $filename_path_bukti,
            ]);

            if(!empty($file)){
                $file->move($destinationPath, $filename_path_bukti);
            }
            if($update_img == 2){
                File::delete('upload/sejarah/'.$request->image_hidden);
            }

            DB::commit();
            return redirect('admin/gereja/sejarah')->with('message-success', 'Data Berhasil Diubah');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message-error', 'Data Gagal Diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{

            $delete = DB::table('m_sejarah')
                            ->where('id_sejarah',$id)
                            ->delete();
            DB::commit();

            return redirect()->back()->with('message-success', 'Berhasil Dihapus');
        }catch(\Exception $e){
            dd($e);
            DB::rollback();
            return redirect()->back()->with('message', 'Data Gagal Dihapus');
        }
    }

}
