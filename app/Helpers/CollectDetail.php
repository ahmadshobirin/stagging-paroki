<?php

namespace App\Helpers;

class CollectDetail
{

    public $temp_array;

    public static function get($array, $tambahan = null, $convert = null)
    {
        $baru = new CollectDetail();
        return $baru->start($array, $tambahan, $convert);
    }

    public function start($array, $tambahan = null, $convert = null)
    {
        $array_child = [];

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array_child[][$key] = $value;
            }
        }
        $hitung = 0;
        $akhir = [];
        for ($x = 0; $x <= count($array_child) - 1; $x++) {
            foreach ($array_child[$x] as $key => $value) {
                if ($hitung == 0) {
                    $hitung = count($value);
                }
                for ($i = 0; $i <= $hitung - 1; $i++) {
                    if (!isset($akhir[$i])) {
                        $akhir[$i] = [];
                    }

                    $akhir[$i] += [$key => $value[$i]];
                    if ($tambahan != null) {
                        $akhir[$i] += $tambahan;
                    }
                }
            }
        }

        if ($convert != null) {
            foreach ($convert as $key_convert => $tipe_convert) {
                foreach ($akhir as $array_data_key => $array_data_value) {
                    foreach ($array_data_value as $data_key => $data_value) {
                        if ($key_convert == $data_key) {

                            $akhir[$array_data_key][$data_key] = $this->converter($data_value, $tipe_convert);
                        }
                    }
                }
            }
        }
        // $this->temp_array = $akhir;
        return $akhir;
    }

    public function converter($value, $jenis)
    {
        if ($jenis == 'integer') {
            return str_replace(array('.'), '', $value);
        }else if($jenis == 'date'){
            return date('Y-m-d',strtotime($value));
        }else if($jenis == 'time'){
            return date('H:i:00',strtotime($value));
        }
    }
}
