<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgendaModel extends Model
{
    protected $table = 'm_agenda';

    protected $guarded = ['id_agenda'];

    protected $primaryKey = 'id_agenda';
}
