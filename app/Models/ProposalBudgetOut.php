<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProposalBudgetOut extends Model
{
    protected $table = 'proposal_pengeluaran';

    protected $guarded = ['id'];
}
