<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MSubKategoriFiturModel extends Model
{
    protected $table = 'm_subkategori_fitur';

    protected $guarded = ['id'];

    public function kategoriRelation()
    {
    	return $this->belongsTo('\App\Models\MKategoriFiturModel','id_kategori_fitur','id');
    }

}
