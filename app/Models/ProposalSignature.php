<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProposalSignature extends Model
{
    protected $table = 'proposal_signature';

    protected $guarded = ['id'];

    public function proposal()
    {
        return $this->hasOne(\App\Models\Proposal::class,'proposal_id','id');
    }
}
