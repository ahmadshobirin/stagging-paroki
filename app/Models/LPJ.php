<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LPJ extends Model
{
    protected $table = 'lpj';

    protected $guarded = ['id'];

    //relations
    public function budget_in()
    {
        return $this->hasMany(\App\Models\LPJBudgetIn::class, 'lpj_id', 'id');
    }

    public function budget_out()
    {
        return $this->hasMany(\App\Models\LPJBudgetOut::class, 'lpj_id', 'id');
    }

    // public function temp_agenda()
    // {
    //     return $this->hasMany(\App\Models\LPJTempAgenda::class, 'lpj_id', 'id');
    // }

    public function signature()
    {
        return $this->hasMany(\App\Models\LPJSignature::class, 'lpj_id', 'id');
    }


    public static function boot()
    {
        parent::boot();

        static::deleting(function ($pro) { // before delete() method call this
            $pro->budget_in()->delete();
            $pro->budget_out()->delete();
            $pro->signature()->delete();
        });
    }
}
