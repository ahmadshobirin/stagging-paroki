<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LPJBudgetOut extends Model
{
    protected $table = 'lpj_pengeluaran';

    protected $guarded = ['id'];
}
