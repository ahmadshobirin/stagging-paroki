<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class SakramenTransaction extends Model
{
    protected $table = 'm_sakramen_transaction';

    protected $guarded = ['id'];

    public function scopeGetCode($query)
    {
        $string = "TRA";

        $selectLastCode = DB::raw(" coalesce( MAX( CAST( RIGHT( code, 5) AS UNSIGNED  ))  ,0) as code ");

        $getData = $query->select($selectLastCode)->where('code', 'LIKE', '%' . $string . '%')->first();

        $number = sprintf("%'.05d ", $getData->code + 1);

        return $string . $number;
    }
}
