<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MPeriode extends Model
{
    protected $table = 'm_periode';

    protected $guarded = ['id'];

    public function sakramen_tra()
    {
        return $this->hasMany(\App\Models\SakramenTransaction::class,'periode_id','id_periode');
    }
}
