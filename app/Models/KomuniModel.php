<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KomuniModel extends Model
{
    protected $table = 'm_sakramen_komuni';

    protected $guarded = ['id'];
}
