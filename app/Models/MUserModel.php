<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MailResetPasswordToken;

class MUserModel extends Authenticatable
{
    use SoftDeletes, Notifiable;

    protected $table = 'm_user';
    protected $dates = ['deleted_at'];

    public function roles()
    {
        return $this->belongsToMany('App\Models\MRoleModel')->withTimestamps();
    }

    public function sendPasswordResetNotification($token)
    {
        return $this->notify(new MailResetPasswordToken($token));
    }
}
