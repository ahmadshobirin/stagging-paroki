<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProposalTempAgenda extends Model
{
    protected $table = 'proposal_temp_agenda';

    protected $guarded = ['id'];

    public function place()
    {
        return $this->belongsTo(\App\Models\Place::class,'place_id','id_tempat');
    }
}
