<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $table = 'proposal';

    protected $guarded = ['id'];

    //relations
    public function budget_in()
    {
        return $this->hasMany(\App\Models\ProposalBudgetIn::class, 'proposal_id', 'id');
    }

    public function budget_out()
    {
        return $this->hasMany(\App\Models\ProposalBudgetOut::class, 'proposal_id', 'id');
    }

    public function temp_agenda()
    {
        return $this->hasMany(\App\Models\ProposalTempAgenda::class, 'proposal_id', 'id');
    }

    public function signature()
    {
        return $this->hasMany(\App\Models\ProposalSignature::class, 'proposal_id', 'id');
    }

    public function user()
    {
        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }


    public static function boot()
    {
        parent::boot();

        static::deleting(function ($pro) { // before delete() method call this
            $pro->budget_in()->delete();
            $pro->budget_out()->delete();
            $pro->temp_agenda()->delete();
        });
    }
}
