<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LPJSignature extends Model
{
    protected $table = 'lpj_signature';

    protected $guarded = ['id'];
}
