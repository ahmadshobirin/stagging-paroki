<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaptisModel extends Model
{
    protected $table = 'm_sakramen_baptis';

    protected $guarded = ['id'];
}
