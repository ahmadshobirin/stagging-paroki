<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecordUmmat extends Model
{
    protected $table = 'record_ummat';

    protected $guarded = ['id'];
}
