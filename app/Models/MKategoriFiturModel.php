<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MKategoriFiturModel extends Model
{
    protected $table = 'm_kategori_fitur';

    protected $guarded = ['id'];

    public function subKategoriRelation()
    {
    	return $this->hasMany('\App\Models\MSubKategoriFiturModel','id_kategori_fitur','id');
    }

}
