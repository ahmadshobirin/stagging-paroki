<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProposalBudgetIn extends Model
{
    protected $table = 'proposal_pemasukan';

    protected $guarded = ['id'];
}
