<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LPJBudgetIn extends Model
{
    protected $table = 'lpj_pemasukan';

    protected $guarded = ['id'];
}
