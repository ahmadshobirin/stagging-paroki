<?php

namespace App\Notifications;

use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordToken extends Notification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $customer = DB::table('m_user')->where('id',$notifiable->id)->first();

        $username = $customer->name;
        $token = $this->token;

        return (new MailMessage)
                    ->from('admin@biztekweb.com', "Admin BiztekWeb")
                    ->view('emails.forgot_password', compact('token','username'));

                    
                    // ->subject("Reset your password")
                    // // ->line()
                    // ->line('You are receiving this email because we received a password reset request for your account.')
                    // ->action('Reset Password', url('/password/reset/'.$this->token))
                    // ->line('If you did not request a password reset, no further action is required.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
