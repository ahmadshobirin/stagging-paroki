<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TransactionReport implements FromView
{
    protected $collection;
    protected $options;

    public function __construct($datas, array $options = [])
    {
        $this->collection = $datas;
        $this->options = $options;
    }

    public function view(): View
    {
        return view('admin/sakramen/transaction/export-transaction', [
            'collection' => $this->collection,
            'options' => $this->options
        ]);
    }
}
