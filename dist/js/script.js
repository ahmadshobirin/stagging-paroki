// GLOBAL VARIABLE
var menuStatus=false;
var popup=false;

// GET CLASS
function getElement(attr){
  return document.querySelector(attr);
}

// GET CLASS
function getClass(attr){
  return document.getElementsByClassName(attr);
}

// GET ID
function getId(attr){
  return document.getElementById(attr);
}

// GET WINDOW HEIGHT
var windowHeight = getClass('window_height');
for (var i = 0; i < windowHeight.length; i++) {
  windowHeight[i].style.height=window.innerHeight;
}





// FLOATING MENU
function menuFixed() {
    if (document.body.scrollTop > 1 || document.documentElement.scrollTop > 1) {
        getId("body").classList.add("floating");
        
    } else {
        getId("body").classList.remove("floating");
       
    }
}


// ANIMATE VISIBLE ELEMENT
function checkVisible(elm) {
  var rect = elm.getBoundingClientRect();
  var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
  return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
}


//WINDOW SCROLL EVENT
window.onscroll = function(){
	
    //MENU FIXED
    menuFixed();
    
};



//jquery

//SOFT LINK ONE PAGE
$(function() {  $('a[href*="#"]:not([href="#"])').click(function() {    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {      var target = $(this.hash);      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');      if (target.length) {        $('html, body').animate({          scrollTop: target.offset().top-70}, 1000);        return false;      }    }  });});



$(document).ready(function(){
	menu_mobile();
	back_top();
	inputField();
	hero_slider();
}) 


function hero_slider(){
	$('.pr-hero').slick({
		slidesToScroll: 1,
		dots: true,
		autoplay:true,
		autoplaySpeed: 5000,
		arrows: false,
	});

	$('.pr-home-pray__list').slick({
		slidesToScroll: 1,
		dots: true,
		autoplay:true,
		autoplaySpeed: 5000,
		arrows: false,
	});
}

function inputField(){
	if( $('.pr-page-contact__detail-form').length > 0 ) floatLabels();

	function floatLabels() {
		var inputFields = $('.pr-page-contact__detail-form .pr-input-label').next();
		inputFields.each(function(){
			var singleInput = $(this);
			//check if user is filling one of the form fields 
			checkVal(singleInput);
			singleInput.on('change keyup', function(){
				checkVal(singleInput);	
			});
		});
	}

	function checkVal(inputField) {
		( inputField.val() == '' ) ? inputField.prev('.pr-input-label').removeClass('floating') : inputField.prev('.pr-input-label').addClass('floating');
	}
}

function menu_mobile(){
	$('.pr-nav-bar').click(function(e){
		e.preventDefault();
		if($('.pr-nav-list').hasClass('show')){
			$('.pr-nav-list').removeClass('show');
			$('.pr-nav-overlay').fadeOut(300);
		}else{
			$('.pr-nav-list').addClass('show');
			$('.pr-nav-overlay').fadeIn(300);
		}
	})
	$('.pr-nav-overlay').click(function(e){
		e.preventDefault();
		if($('.pr-nav-list').hasClass('show')){
			$('.pr-nav-list').removeClass('show');
			$('.pr-nav-overlay').fadeOut(300);
		}else{
			$('.pr-nav-list').addClass('show');
			$('.pr-nav-overlay').fadeIn(300);
		}
	})
}



function back_top(){
	$('.pr-btn-floating__totop').on('click', function (e) {
		e.preventDefault();
		$('html,body').animate({
			scrollTop: 0
		}, 700);
	});

}


// function sc(){
// 	$('.pr-btn-floating__search').click(function(e){
// 		e.preventDefault();
// 		if($('.pr-btn-floating__search').hasClass('show')){
// 			$('.pr-btn-floating__search').removeClass('show');
// 			$('.pr-popup-box--search').fadeOut(200);
// 		}else{
// 			$('.pr-btn-floating__search').addClass('show');
// 			$('.pr-popup-box--search').fadeIn(200);
// 		}
// 	})
// 	$('.pr-popup-box__close--search').click(function(e){
// 		e.preventDefault();
// 		if($('.pr-btn-floating__search').hasClass('show')){
// 			$('.pr-btn-floating__search').removeClass('show');
// 			$('.pr-popup-box--search').fadeOut(200);
// 		}else{
// 			$('.pr-btn-floating__search').addClass('show');
// 			$('.pr-popup-box--search').fadeIn(200);
// 		}
// 	})
// }


// function login(){
// 	$('.pr-btn-floating__login').click(function(e){
// 		e.preventDefault();
// 		if($('.pr-btn-floating__login').hasClass('show')){
// 			$('.pr-btn-floating__login').removeClass('show');
// 			$('.pr-popup-box--login').fadeOut(200);
// 		}else{
// 			$('.pr-btn-floating__login').addClass('show');
// 			$('.pr-popup-box--login').fadeIn(200);
// 		}
// 	})
// 	$('.pr-popup-box__close--login').click(function(e){
// 		e.preventDefault();
// 		if($('.pr-btn-floating__login').hasClass('show')){
// 			$('.pr-btn-floating__login').removeClass('show');
// 			$('.pr-popup-box--login').fadeOut(200);
// 		}else{
// 			$('.pr-btn-floating__login').addClass('show');
// 			$('.pr-popup-box--login').fadeIn(200);
// 		}
// 	})
// }