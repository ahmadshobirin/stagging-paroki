<?php

use Illuminate\Http\Request;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('cek-expired','API\APICekLoginController@cekDomain');

//kategori-user
Route::get('/upline-user/{id}','MKategoriUserController@uplineUser');

Route::get('/proposal/{userid?}/{type?}','ProposalController@table');
Route::get('/lpj/{userid?}/{type?}','LPJController@table');
Route::get('/upstatus/{oldstatus}/{id}/{changestatus}','ProposalController@upStatus')->name('proposal.upstatus');
Route::get('/lpjupstatus/{oldstatus}/{id}/{changestatus}','LPJController@upStatus')->name('lpj.upstatus');

Route::get('/pencatatan/{type}/{userid}','RecordUmmatController@table');
Route::get('/sakramen/{type}/{uniq?}/{typename?}','SakramenController@table');
Route::get('/sakramen_ttd/{type}/{uniq?}/{typename?}','SakramenController@tableTTD');
Route::get('/tahapan/get-periode/{type}','MTahapanController@getPeriode');

Route::post('sakramen-wizard/nikah','SakramenController@wizardForm')->name("nikah.wizard");