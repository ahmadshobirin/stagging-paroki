<?php

Route::get('sakramenbaptis', 'SakramenCtrl@viewsakramenbaptis');
Route::get('sakramenkomuni', 'SakramenCtrl@viewsakramenkomuni');
Route::get('sakramenkrisma', 'SakramenCtrl@viewsakramenkrisma');
Route::get('sakramenpernikahan', 'SakramenCtrl@viewsakramenpernikahan');


Route::get('/', 'FrontCtrl@index');
Route::get('agenda/gereja', 'AgendaCtrl@indexAgenda');
Route::get('agenda/gereja/filter', 'AgendaCtrl@filterAgenda');
Route::get('agenda/nikah', 'AgendaCtrl@indexNikah');
Route::get('agenda/nikah/filter', 'AgendaCtrl@filterNikah');
Route::get('bidang/{id}', 'BidangCtrl@index');
Route::get('artikel/{id}', 'ArtikelCtrl@index');
Route::get('info', 'InfoCtrl@index');
Route::get('contact', 'FrontCtrl@contact');
Route::get('kronik', 'KronikCtrl@index');
Route::get('kronik/{id}', 'KronikCtrl@indexKategori');
Route::get('kronik-detail/{id}', 'KronikCtrl@detailKronik');
Route::get('sejarah/{id}', 'SejarahCtrl@index');
Route::get('romo', 'RomoCtrl@index');
Route::get('link/{kat}', 'LinkCtrl@index');
Route::get('renungan', 'RenunganCtrl@index');
Route::get('renungan/search', 'RenunganCtrl@indexSearch');
Route::get('renungan/bulan/{param}', 'RenunganCtrl@indexBulan');
Route::get('renungan/tahun/{param}', 'RenunganCtrl@indexTahun');
Route::get('renungan/detail/{id}', 'RenunganCtrl@detailRenungan');
Route::get('/about', 'FrontCtrl@about');
Route::get('/download/{id}', 'FrontCtrl@download');
Route::get('/form/{name}', 'FrontCtrl@getForm');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/login/umat', 'Auth\LoginController@showLoginFormUmat')->name('loginumat');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/register', 'MUserController@showForm')->name('register');
Route::post('/register', 'MUserController@register')->name('register.post');

Route::post('send-contact', 'FrontCtrl@sendContact');

Route::middleware(['auth', 'customer'])->group(function () { });

Route::middleware(['auth', 'admin'])->group(function () {
	Route::get('/admin', 'HomeController@index')->name('admin');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/admin/get-new-count/{id?}', 'HomeController@tableCount');
	Route::get('/admin/profile/{id}', 'HomeController@profile');

	Route::get('/admin/artikel', 'PostController@indexArtikel');
	Route::get('/admin/artikel/create', 'PostController@createArtikel');
	Route::get('/admin/artikel/{id}/edit', 'PostController@editArtikel');
	Route::get('/admin/artikel/{id}/show', 'PostController@showArtikel');
	Route::post('/admin/artikel/', 'PostController@storeArtikel');
	Route::post('/admin/artikel/{id}', 'PostController@updateArtikel');
	Route::get('/admin/artikel/{id}/send-approval', 'PostController@approvalArtikel');
	Route::get('/admin/artikel/{id}/delete', 'PostController@deleteArtikel');
	Route::get('/admin/api/artikel', 'PostController@apiArtikel');

	Route::get('/admin/kronik', 'PostController@indexKronik');
	Route::get('/admin/kronik/create', 'PostController@createKronik');
	Route::get('/admin/kronik/{id}/edit', 'PostController@editKronik');
	Route::get('/admin/kronik/{id}/show', 'PostController@showKronik');
	Route::post('/admin/kronik/', 'PostController@storeKronik');
	Route::post('/admin/kronik/{id}', 'PostController@updateKronik');
	Route::get('/admin/kronik/{id}/send-approval', 'PostController@approvalKronik');
	Route::get('/admin/kronik/{id}/delete', 'PostController@deleteKronik');
	Route::get('/admin/api/kronik', 'PostController@apiKronik');

	Route::get('/admin/info', 'PostController@indexInfo');
	Route::get('/admin/info/create', 'PostController@createInfo');
	Route::get('/admin/info/{id}/edit', 'PostController@editInfo');
	Route::get('/admin/info/{id}/show', 'PostController@showInfo');
	Route::post('/admin/info/', 'PostController@storeInfo');
	Route::post('/admin/info/{id}', 'PostController@updateInfo');
	Route::get('/admin/info/{id}/send-approval', 'PostController@approvalInfo');
	Route::get('/admin/info/{id}/delete', 'PostController@deleteInfo');
	Route::get('/admin/api/info', 'PostController@apiInfo');


	//RoleController
	Route::resource('admin/userrole', 'MRoleController');
	Route::get('admin/userrole/trash/deleted', 'MRoleController@trash');
	Route::get('admin/userrole/restore/{id}', 'MRoleController@restore');
	Route::get('admin/userrole/deletepermanent/{id}', 'MRoleController@permanentDelete');

	//Kategori User
	Route::resource('admin/katuser', 'MKategoriUserController');
	Route::get('api/katuser', 'MKategoriUserController@apiKategori');
	Route::get('/admin/katuser/delete/{id}', 'MKategoriUserController@destroy');

	//ListUserAdmin
	Route::resource('admin/user', 'MUserController');
	Route::get('admin/users/{type?}', 'MUserController@index');
	Route::get('user/api/user/{type?}', 'MUserController@apiUser');
	Route::get('get-KategoriUser', 'MUserController@getKategoriUser');
	Route::get('get-KategoriAdmin', 'MUserController@getKategoriAdmin');
	Route::get('admin/user-delete/{id}/{status?}', 'MUserController@destroy');
	Route::get('admin/user/{id}/reset-password', 'MUserController@resetPassword');
	Route::post('admin/user/{id}/reset-password', 'MUserController@storePassword');

	//Bidang
	Route::get('/admin/bidang', 'MBidangController@index');
	Route::resource('admin/bidang', 'MBidangController');
	Route::get('admin/bidang-delete/{id}', 'MBidangController@destroy');

	//Agenda Umum
	Route::get('admin/kegiatan-umum', 'MAgendaController@indexUmum');
	Route::get('/admin/api/kegiatan-umum', 'MAgendaController@apiKegiatanUmum');
	Route::get('/admin/kegiatan-umum/create', 'MAgendaController@createUmum');
	Route::get('/admin/kegiatan-umum/{id}/edit', 'MAgendaController@editUmum');
	Route::get('/admin/kegiatan-umum/{id}/show', 'MAgendaController@showUmum');
	Route::get('/admin/kegiatan-umum/{id}/delete', 'MAgendaController@destroyUmum');
	Route::post('/admin/kegiatan-umum', 'MAgendaController@storeUmum');
	Route::post('/admin/kegiatan-umum/{id}', 'MAgendaController@updateUmum');

	Route::get('admin/kegiatan-nikah', 'MAgendaController@indexNikah');
	Route::get('/admin/api/kegiatan-nikah', 'MAgendaController@apiKegiatanNikah');
	Route::get('/admin/kegiatan-nikah/create', 'MAgendaController@createNikah');
	Route::get('/admin/kegiatan-nikah/{id}/edit', 'MAgendaController@editNikah');
	Route::get('/admin/kegiatan-nikah/{id}/show', 'MAgendaController@showNikah');
	Route::get('/admin/kegiatan-nikah/{id}/delete', 'MAgendaController@destroyNikah');
	Route::post('/admin/kegiatan-nikah', 'MAgendaController@storeNikah');
	Route::post('/admin/kegiatan-nikah/{id}', 'MAgendaController@updateNikah');


	Route::get('admin/link/{nama}', 'MLinkController@index');
	Route::get('admin/link/{nama}/create', 'MLinkController@create');
	Route::post('/admin/link/{nama}', 'MLinkController@store');
	Route::post('/admin/link/update/{id}', 'MLinkController@update');
	Route::get('admin/link/{id}/edit', 'MLinkController@edit');
	Route::get('admin/link/{id}/delete', 'MLinkController@destroy');


	Route::get('admin/download/{nama}', 'MFormController@index');
	Route::get('admin/download/{nama}/create', 'MFormController@create');
	Route::post('/admin/download/{nama}', 'MFormController@store');
	Route::post('/admin/download/update/{id}', 'MFormController@update');
	Route::get('admin/download/{id}/edit', 'MFormController@edit');
	Route::get('admin/download/{id}/delete', 'MFormController@destroy');


	Route::get('admin/visi-misi', 'MOfficeController@VisiMisi');
	Route::post('admin/visi-misi', 'MOfficeController@storeVisiMisi');

	Route::get('/admin/gereja/tempat', 'MTempatController@index');
	Route::resource('admin/gereja/tempat', 'MTempatController');
	Route::post('admin/gereja/tempat/{id}', 'MTempatController@update');
	Route::get('admin/gereja/tempat-delete/{id}', 'MTempatController@destroy');

	Route::get('/admin/gereja/romo', 'MRomoController@index');
	Route::resource('admin/gereja/romo', 'MRomoController');
	Route::post('admin/gereja/romo/{id}', 'MRomoController@update');
	Route::get('admin/gereja/romo-delete/{id}', 'MRomoController@destroy');

	Route::get('/admin/gereja/renungan', 'MRenunganController@index');
	Route::resource('admin/gereja/renungan', 'MRenunganController');
	Route::post('admin/gereja/renungan/{id}', 'MRenunganController@update');
	Route::get('admin/gereja/renungan-delete/{id}', 'MRenunganController@destroy');

	Route::get('/admin/gereja/misa', 'MMisaController@index');
	Route::resource('admin/gereja/misa', 'MMisaController');
	Route::post('admin/gereja/misa/{id}', 'MMisaController@update');
	Route::get('admin/gereja/misa-delete/{id}', 'MMisaController@destroy');


	Route::get('admin/office', 'MOfficeController@edit');
	Route::post('admin/office', 'MOfficeController@update');

	Route::get('/admin/gereja/sejarah', 'MSejarahController@index');
	Route::resource('admin/gereja/sejarah', 'MSejarahController');
	Route::post('admin/gereja/sejarah/{id}', 'MSejarahController@update');
	Route::get('admin/gereja/sejarah-delete/{id}', 'MSejarahController@destroy');

	//periode
	Route::get('/admin/periode', 'MPeriodeController@index');
	Route::resource('admin/periode', 'MPeriodeController');
	Route::post('admin/periode/{id}', 'MPeriodeController@update');
	Route::get('admin/periode-delete/{id}', 'MPeriodeController@destroy');

	//tahapan

	// Route::resource('admin/tahapan', 'MTahapanController');
	Route::get('/admin/tahapan', 'MTahapanController@index');
	Route::get('/admin/tahapan/create', 'MTahapanController@create');
	Route::post('/admin/tahapan', 'MTahapanController@store');
	Route::get('admin/tahapan/{id}/edit', 'MTahapanController@update');
	Route::post('admin/tahapan/{id}', 'MTahapanController@update');
	Route::get('admin/tahapan-delete/{id}', 'MTahapanController@destroy');
	Route::get('admin/tahapan-approve/{id}', 'MTahapanController@approve');
	Route::get('admin/tahapan-detail/{type}/{id}', 'MTahapanController@showUpload');

	//Slider
	Route::resource('admin/slider', 'MSliderController');
	Route::get('/api/slider', 'MSliderController@apiSlider');

	Route::middleware(['role'])->group(function () {
		//RoleController
		Route::resource('admin/userrole', 'MRoleController');
		Route::get('admin/userrole/trash/deleted', 'MRoleController@trash');
		Route::get('admin/userrole/restore/{id}', 'MRoleController@restore');
		Route::get('admin/userrole/deletepermanent/{id}', 'MRoleController@permanentDelete');

		//ListUserAdmin
		Route::resource('admin/user', 'MUserController');
		Route::get('user/api/user', 'MUserController@apiUser');
	});

	Route::middleware(['produk'])->group(function () { });

	//Transaksi
	Route::middleware(['transaksi'])->group(function () { });

	Route::middleware(['approval'])->group(function () {
		Route::get('/admin/approval/info', 'PostController@indexApprovalInfo');
		Route::get('/admin/approval/info/{id}/edit', 'PostController@editApprovalInfo');
		Route::get('/admin/approval/info/{id}/show', 'PostController@showApprovalInfo');
		Route::post('/admin/approval/info/{id}', 'PostController@updateApprovalInfo');
		Route::get('/admin/approval/info/{id}/approve', 'PostController@approveApprovalInfo');
		Route::get('/admin/approval/info/{id}/delete', 'PostController@deleteApprovalInfo');
		Route::get('/admin/api/approval/info', 'PostController@apiApprovalInfo');

		Route::get('/admin/approval/artikel', 'PostController@indexApprovalArtikel');
		Route::get('/admin/approval/artikel/{id}/edit', 'PostController@editApprovalArtikel');
		Route::get('/admin/approval/artikel/{id}/show', 'PostController@showApprovalArtikel');
		Route::post('/admin/approval/artikel/{id}', 'PostController@updateApprovalArtikel');
		Route::get('/admin/approval/artikel/{id}/approve', 'PostController@approveApprovalArtikel');
		Route::get('/admin/approval/artikel/{id}/delete', 'PostController@deleteApprovalArtikel');
		Route::get('/admin/api/approval/artikel', 'PostController@apiApprovalArtikel');

		Route::get('/admin/approval/kronik', 'PostController@indexApprovalKronik');
		Route::get('/admin/approval/kronik/{id}/edit', 'PostController@editApprovalKronik');
		Route::get('/admin/approval/kronik/{id}/show', 'PostController@showApprovalKronik');
		Route::post('/admin/approval/kronik/{id}', 'PostController@updateApprovalKronik');
		Route::get('/admin/approval/kronik/{id}/approve', 'PostController@approveApprovalKronik');
		Route::get('/admin/approval/kronik/{id}/delete', 'PostController@deleteApprovalKronik');
		Route::get('/admin/api/approval/kronik', 'PostController@apiApprovalKronik');
	});

	Route::middleware(['kontributor'])->group(function () {

		Route::get('/admin', 'HomeController@index')->name('admin');
		Route::get('/home', 'HomeController@index')->name('home');
		Route::get('/admin/get-new-count/{id?}', 'HomeController@tableCount');
		Route::get('/admin/profile/{id}', 'HomeController@profile');

		Route::get('/admin/artikel', 'PostController@indexArtikel');
		Route::get('/admin/artikel/create', 'PostController@createArtikel');
		Route::get('/admin/artikel/{id}/edit', 'PostController@editArtikel');
		Route::get('/admin/artikel/{id}/show', 'PostController@showArtikel');
		Route::post('/admin/artikel/', 'PostController@storeArtikel');
		Route::post('/admin/artikel/{id}', 'PostController@updateArtikel');
		Route::get('/admin/artikel/{id}/send-approval', 'PostController@approvalArtikel');
		Route::get('/admin/artikel/{id}/delete', 'PostController@deleteArtikel');
		Route::get('/admin/api/artikel', 'PostController@apiArtikel');

		Route::get('/admin/kronik', 'PostController@indexKronik');
		Route::get('/admin/kronik/create', 'PostController@createKronik');
		Route::get('/admin/kronik/{id}/edit', 'PostController@editKronik');
		Route::get('/admin/kronik/{id}/show', 'PostController@showKronik');
		Route::post('/admin/kronik/', 'PostController@storeKronik');
		Route::post('/admin/kronik/{id}', 'PostController@updateKronik');
		Route::get('/admin/kronik/{id}/send-approval', 'PostController@approvalKronik');
		Route::get('/admin/kronik/{id}/delete', 'PostController@deleteKronik');
		Route::get('/admin/api/kronik', 'PostController@apiKronik');

		Route::get('/admin/info', 'PostController@indexInfo');
		Route::get('/admin/info/create', 'PostController@createInfo');
		Route::get('/admin/info/{id}/edit', 'PostController@editInfo');
		Route::get('/admin/info/{id}/show', 'PostController@showInfo');
		Route::post('/admin/info/', 'PostController@storeInfo');
		Route::post('/admin/info/{id}', 'PostController@updateInfo');
		Route::get('/admin/info/{id}/send-approval', 'PostController@approvalInfo');
		Route::get('/admin/info/{id}/delete', 'PostController@deleteInfo');
		Route::get('/admin/api/info', 'PostController@apiInfo');
	});

	Route::resource('admin/proposal', 'ProposalController');
	Route::get('admin/proposal-pengajuan/{pengajuan?}', 'ProposalController@index')->name('proposal.submission');
	Route::get('admin/proposal/create/upload', 'ProposalController@upload')->name('proposal.upload');
	Route::match(['GET', 'POST'], 'admin/proposal-note/{id}', 'ProposalController@note')->name('proposal.note');
	Route::get('admin/proposal/{id}/signaturre', 'ProposalController@addSignature')->name('proposal.signature');
	Route::get('proposal-signature/{id}', 'ProposalController@signatureReject')->name('proposal.signature.reject');
	Route::post('admin/proposal-signature', 'ProposalController@addSignaturePost')->name('proposal.signature.post');
	Route::get('admin/proposal-download/{id}', 'ProposalController@download')->name('proposal.download');

	Route::resource('admin/lpj', 'LPJController');
	Route::get('admin/proposal-lpj/{pengajuan?}', 'LPJController@index')->name('lpj.submission');
	Route::get('admin/lpj/create/upload', 'LPJController@upload')->name('lpj.upload');
	Route::match(['GET', 'POST'], 'admin/lpj-note/{id}', 'LPJController@note')->name('lpj.note');
	Route::get('admin/get-agenda/{id}', 'LPJController@getAgenda');
	Route::get('admin/lpj/{id}/signature', 'LPJController@addSignature')->name('lpj.signature');
	Route::get('lpj-signature/{id}', 'LPJController@signatureReject')->name('lpj.signature.reject');
	Route::post('admin/lpj-signature', 'LPJController@addSignaturePost')->name('lpj.signature.post');
	Route::get('admin/lpj-download/{id}', 'LPJController@download')->name('lpj.download');


	//Sakramen

	//Sakramen Nikah,Baptis,Komuni, Transaksi Sakramen
	Route::get('/admin/sakramen/{tsakramen}/{typebaptis?}', 'SakramenController@index');
	Route::get('/admin/sakramen_ttd/{tsakramen}/{typebaptis?}', 'SakramenController@indexTtd')->name('sakramenttd.index');
	Route::get('/admin/sakramen-create/{tsakramen}/{typebaptis?}', 'SakramenController@create');
	Route::post('/admin/sakramen-store', 'SakramenController@store');
	Route::get('/admin/sakramen-edit/{tsakramen}/{id}/{typebaptis?}', 'SakramenController@edit');
	Route::put('/admin/sakramen-update/{id}', 'SakramenController@store');
	Route::get('/admin/sakramen-delete/{tsakramen}/{id}', 'SakramenController@delete');
	Route::get('admin/skn-upto/{model}/{id}', 'SakramenController@toInApproval')->name('skn.upto');
	Route::get('skn-upto-by-admin/{model}/{id}/{setstatus}', 'SakramenController@toUp')->name('skn.upto');
	Route::get('/admin/tahapan-sakramen/{type}', 'SakramenController@tahapan');
	Route::post('/admin/tahapan-image/{id}/{type}', 'SakramenController@updateImage');
	Route::get('/admin/tahapan-image-delete/{id}', 'SakramenController@deleteImage');
	Route::get('/sakramen-report', 'SakramenController@reportTransaction');
	Route::post('/sakramen-report', 'SakramenController@applyFilterReport');

	Route::get('admin/sakramen-signature/{id}/{tsakramen}/{tbaptis?}', 'SakramenController@addSignature')->name('sakramen.signature');
	Route::post('admin/sakramen-signature', 'SakramenController@addSignaturePost')->name('sakramen.signature.post');
	Route::get('admin-sakramen-print/{type}/{id}/printout', 'SakramenController@download')->name('sakramen.download');

	Route::get('sakramen-signature-ummat/{id}/{tsakramen}/{tbaptis?}', 'SakramenController@addSignatureUmmat')->name('sakramen.signature');
	Route::post('sakramen-signature-ummat', 'SakramenController@ummatSignature')->name('sakramen.signature-ummat.post');


	Route::get('admin/transaksi-sakramen', 'SakramenController@trx');




	// Route::resource('/admin/record-ummat', 'RecordUmmatController');
	Route::get('/admin/pencatatan-{type}', 'RecordUmmatController@index')->name('pencatatan.index');
	Route::get('/admin/pencatatan-{type}/create', 'RecordUmmatController@create')->name('pencatatan.create');
	Route::post('/admin/pencatatan-{type}', 'RecordUmmatController@store')->name('pencatatan.store');
	Route::get('/admin/pencatatan-{type}/{id}/edit', 'RecordUmmatController@edit')->name('pencatatan.edit');
	Route::put('/admin/pencatatan/{id}/update', 'RecordUmmatController@update')->name('pencatatan.update');
	Route::delete('/admin/pencatatan/{id}', 'RecordUmmatController@destroy')->name('pencatatan.destroy');
});


Route::get('upline-user/{id}/{type}', 'MUserController@sendEmail');
Route::get('ttd_umat', 'SakramenController@ttdUmat');
Route::get('download-tahapan/{id}', function($id)
{
	// dd($filename);
	$getFile = \DB::table('m_tahapan_image')->where('id',$id)->first();

    // Check if file exists in app/storage/file folder
    $file_path = public_path() .'/upload/tahapan/'. $getFile->image_tahapan;
    if (file_exists($file_path))
    {
        // Send Download
        return \Response::download($file_path, $getFile->image_tahapan, [
            'Content-Length: '. filesize($file_path)
        ]);
    }
    else
    {
        // Error
        exit('Requested file does not exist on our server!');
    }
});

Route::get('view-image/{filename}', function ($filename)
{
    $path = public_path('upload/tahapan/' . $filename);

    if (!File::exists($path)) {
        return response()->json('file tidak ditemukan', 404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::match(['get','post'],'sakramen-nikah/wizard/{step}','SakramenController@wizardForm');
