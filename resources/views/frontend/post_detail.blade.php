@extends('layout.front')
@section('page')
{{$artikel->title_post}}
@endsection
@section('content')

<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">{{$artikel->title_post}}</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->

<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		<a href="{{ url('bidang/'.$artikel->id_bidang) }}" class="pr-breadcrumb__link">{{ $artikel->nama_bidang }} Artikel</a> /
		<a href="#" class="pr-breadcrumb__link">{{$artikel->title_post}}</a>
	</div>

</section>
<!-- /pr-breadcrumb -->

<section class="pr-page">

	<!-- pr-page-detail -->
	<div class="pr-page-detail">

		<div class="pr-wrapper pr-wrapper--large add-fix">
			
			<!-- pr-page-detail__content -->
			<div class="pr-page-detail__content">

				<!-- pr-page-detail__content-share -->
				<div class="pr-page-detail__content-share">

					<?php 
						$a = str_replace("<ul>", "", $fb);
						$b = str_replace("</ul>", "", $a); 
						$c = str_replace("<li>", "", $b); 
						$d = str_replace("</li>", "", $c);

						echo html_entity_decode($d); 
					?>
					{{-- <a title="Bagikan" href="http://www.facebook.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-facebook-square"></i></a>
					<a title="Bagikan" href="http://www.twitter.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-twitter-square"></i></a>
					<a title="Bagikan" href="http://www.google.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-google-plus-square"></i></a> --}}

				</div>
				<!-- /pr-page-detail__content-share -->
				
				<!-- pr-page-detail__content-figure -->
				<figure class="pr-page-detail__content-figure">
					<img src="{{ URL::asset('upload/artikel/'.$artikel->image_post)}}" alt="Paroki Roh Kudus" title="{{$artikel->title_post}}">
				</figure>
				<!-- /pr-page-detail__content-figure -->

				<h3 class="pr-page-detail__content-title">{{$artikel->title_post}}</h3>

				<div class="pr-page-detail__content-author">
					<span>{{$artikel->nama_bidang}}</span> | <span>{{ date('d M Y', strtotime($artikel->created_at)) }}</span>
				</div>

				<article class="pr-page-text__desc">

					{!! $artikel->isi_post !!}

				</article>

			</div>
			<!-- /pr-page-detail__content -->

			<!-- pr-page-detail__sidebar -->
			<aside class="pr-page-detail__sidebar">
				
				<div class="pr-page-detail__sidebar-wrapper">

					<!-- pr-page-detail__sidebar-list -->
					<div class="pr-page-detail__sidebar-list">

						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item">
							
							<h4 class="pr-page-detail__sidebar-list-item-title">Artikel</h4>

							<!-- pr-page-detail__sidebar-list-item-content -->
							<ul class="pr-page-detail__sidebar-list-item-content">

								@foreach($artikel_bidang as $list_artikel)
								<!-- Max Isi 5 -->
								<li>
									<a href="{{ url('artikel/'.$list_artikel->id_posting) }}" class="pr-page-detail__sidebar-list-item-content-link">{{$list_artikel->title_post}}</a>
									<span class="pr-page-detail__sidebar-list-item-content-author">{{$list_artikel->nama_bidang}} | {{ date('d M Y', strtotime($list_artikel->created_at)) }}</span>
								</li>
								@endforeach
							</ul>
							<!-- /pr-page-detail__sidebar-list-item-content -->

						</div>
						<!-- /pr-page-detail__sidebar-list-item -->

						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item">
							
							<h4 class="pr-page-detail__sidebar-list-item-title">Agenda</h4>

							<!-- pr-page-detail__sidebar-list-item-content -->
							<ul class="pr-page-detail__sidebar-list-item-content">
								@if(count($agenda) > 0)

								@foreach($agenda as $agn)
								<!-- Max Isi 5 -->
								<li>
									<span class="pr-page-detail__sidebar-list-item-content-link">{{ $agn->nama_agenda }}</span>
									<span class="pr-page-detail__sidebar-list-item-content-author">{{ $agn->nama_tempat }}| {{ date('d M Y', strtotime($agn->date_agenda)) }} | {{ date('H.i', strtotime($agn->waktu_agenda)) }} WIB</span>
								</li>
								@endforeach
								@else
								@foreach($agenda_lain as $agn_lain)
									<!-- Max Isi 5 -->
									@foreach($agn_lain->detail_agenda as $agn)
									<li>
										<span class="pr-page-detail__sidebar-list-item-content-link">{{ $agn->nama_agenda }}</span>
										<span class="pr-page-detail__sidebar-list-item-content-author">{{ $agn->nama_tempat }}| {{ date('d M Y', strtotime($agn->date_agenda)) }} | {{ date('H.i', strtotime($agn->waktu_agenda)) }} WIB</span>
									</li>
									@endforeach
								@endforeach
								@endif
							</ul>
						</div>
						

					</div>
					<!-- /pr-page-detail__sidebar-list -->

				</div>

			</aside>
			<!-- /pr-page-detail__sidebar -->


		</div>

	</div>
	<!-- /pr-page-detail -->


</section>
<!-- /pr-page -->


@endsection