@extends('layout.front')
@section('page')
Kontak
@endsection
@section('content')

<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">Kontak Paroki Roh Kudus</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->


<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		<a href="#" class="pr-breadcrumb__link">Kontak</a>
	</div>

</section>
<!-- /pr-breadcrumb -->

<!-- pr-page -->
<section class="pr-page">

	<!-- pr-page-contact -->
	<div class="pr-page-contact">

		<div class="pr-wrapper pr-wrapper--large">

			<!-- pr-page-contact__map -->
			<div class="pr-page-contact__map">

				<iframe src="{{$office->iframe_office}}" frameborder="0" style="border:0" allowfullscreen></iframe>


			</div>
			<!-- /pr-page-contact__map -->

			<!-- pr-page-contact__detail -->
			<div class="pr-page-contact__detail add-fix">

				<!-- pr-page-contact__detail-info -->
				<div class="pr-page-contact__detail-info">

					<h4 class="pr-page-contact__detail-title">Kontak</h4>

					<div class="pr-page-contact__detail-wrapper">
						<p>{{$office->telp_office}}</p>
						<p>{{$office->email_office}}</p>
						<p>{!! $office->alamat_office !!}<br>Kel. {{ $office->nama_kel }}, Kec. {{$office->nama_kec}}<br>{{$office->nama_kota}}, {{$office->nama_prov}} {{$office->zipcode}}</p>
					</div>

				</div>
				<!-- /pr-page-contact__detail-info -->

				<!-- pr-page-contact__detail-form -->
				<div class="pr-page-contact__detail-form">

					<h4 class="pr-page-contact__detail-title">Hubungi Kami</h4>

					<div class="pr-page-contact__detail-wrapper">
						
						<form action="{{url('/send-contact')}}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div>
								
								<!-- pr-page-contact__detail-form-field -->
								<div class="pr-page-contact__detail-form-field pr-page-contact__detail-form-field--half">
									<label class="pr-input-label">Nama</label>
									<input type="text" class="pr-input" placeholder="Nama" name="nama" required>
								</div>
								<!-- /pr-page-contact__detail-form-field -->

								<!-- pr-page-contact__detail-form-field -->
								<div class="pr-page-contact__detail-form-field pr-page-contact__detail-form-field--half">
									<label class="pr-input-label">Telepon (Optional)</label>
									<input type="text" class="pr-input" placeholder="Telepon" name="telp">
								</div>
								<!-- /pr-page-contact__detail-form-field -->

								<!-- pr-page-contact__detail-form-field -->
								<div class="pr-page-contact__detail-form-field pr-page-contact__detail-form-field--half">
									<label class="pr-input-label">Alamat</label>
									<input type="text" class="pr-input" placeholder="Alamat" name="alamat">
								</div>
								<!-- /pr-page-contact__detail-form-field -->

								<!-- pr-page-contact__detail-form-field -->
								<div class="pr-page-contact__detail-form-field pr-page-contact__detail-form-field--half">
									<label class="pr-input-label">Email</label>
									<input type="email" name="email" class="pr-input" placeholder="Email" required>
								</div>
								<!-- /pr-page-contact__detail-form-field -->

								<!-- pr-page-contact__detail-form-field -->
								<div class="pr-page-contact__detail-form-field pr-page-contact__detail-form-field--full">
									<label class="pr-input-label">Pesan</label>
									<textarea class="pr-input pr-input__textarea" placeholder="Pesan" name="pesan" required></textarea>
								</div>
								<!-- /pr-page-contact__detail-form-field -->

								<!-- pr-btn -->
								<input type="submit" name="" value="Submit" class="pr-btn">
								<!-- /pr-btn -->

							</div>

						</form>

					</div>

				</div>
				<!-- /pr-page-contact__detail-form -->

			</div>
			<!-- /pr-page-contact__detail -->

		</div>

	</div>
	<!-- /pr-page-contact -->

</section>
<!-- /pr-page -->

@endsection