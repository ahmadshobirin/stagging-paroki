@extends('layout.front')
@section('page')
Agenda
@endsection
@section('customcss')
<link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
<link href="{{asset('css/select2-bootstrap.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{URL::asset('plugins/datepicker/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('plugins/datepicker/datepicker3.css')}}">
<link href="{{asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
@stop
@section('content')

<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">Kegiatan Gereja</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->

<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<?php $url_bread ="";
			if($bread == "Kegiatan Gereja"){
				$url_bread = "gereja";
			}else{
				$url_bread = "nikah";
			}
		 ?>
		<a href="{{url('/')}}" class="pr-breadcrumb__link">Home</a> /
		<a href="{{url('/agenda/'.$url_bread)}}" class="pr-breadcrumb__link">{{$bread}}</a>
	</div>

</section>
<!-- /pr-breadcrumb -->

<!-- pr-page -->
<section class="pr-page">


	<!-- pr-page-event -->
	<div class="pr-page-event">


		<div class="pr-wrapper pr-wrapper--large">


			<!-- pr-page-event__filter -->
			<div class="pr-page-event__filter">

				<form class="add-fix" @if($url_bread == "gereja") action="{{ url('agenda/gereja/filter') }}" @else action="{{ url('agenda/nikah/filter') }}" @endif method="GET">

					<input type="hidden" name="type" id="type" value="{{$type}}">

					<div class="pr-page-event__filter-item">
						<label class="pr-label">Bulan</label>
						@if($type == "filter")
						<input type="text" id="datepicker1" class="pr-input" readonly name="month" value="{{ $bulan }}" required data-date-end-month="18m">
						@else
						<input type="text" id="datepicker1" class="pr-input" readonly name="month" value="{{ date('M Y') }}" required data-date-end-month="18m">
						@endif
					</div>


					<div class="pr-page-event__filter-item">
                    	<label class="pr-label">Tanggal</label>
                    	<input type="text" class="pr-input" id="periode" name="periode" @if($type == "filter") value="{{ $tglmulai }} - {{ $tglsampai}}" @else value="" @endif>
                    	<input type="submit" class="pr-btn" class="pr-input" value="Cari" name="">
                	</div>
                	
				</form>

			</div>
			<!-- /pr-page-event__filter -->


			<!-- pr-page-event__content -->
			<div class="pr-page-event__content">

				<h4 class="pr-page-event__content-title">
					<?php
						$month = "";
						if($type == "filter"){
							$month = date('F', strtotime($tglmulai));
						}else{
							$month = date('F');
						} 
						$bulan = "";
							if($month == "January"){
								$bulan = "Januari";
							}elseif($month == "February") {
								$bulan = "Februari";
							}elseif($month == "March") {
								$bulan = "Maret";
							}elseif($month == "April") {
								$bulan = "April";
							}elseif($month == "May") {
								$bulan = "Mei";
							}elseif($month == "June") {
								$bulan = "Juni";
							}elseif($month == "July") {
								$bulan = "Juli";
							}elseif($month == "August") {
								$bulan = "Agustus";
							}elseif($month == "September") {
								$bulan = "September";
							}elseif($month == "October") {
								$bulan = "Oktober";
							}elseif($month == "November") {
								$bulan = "November";
							}elseif($month == "December") {
								$bulan = "Desember";
							}  
					?>{{ $bulan }}
				</h4>
				
				<input type="button" class="pr-page-event__content-title" onclick="window.location.href = 'https://wa.me/62318792426?text=Selamat%20pagi%2Fsiang%2Fsore%2Fmalam%20Pak.%20Saya%20mau%20tanya...';" value="Tanya Sekretariat"/>
				
				<!-- pr-page-event__content-list -->
				<div class="pr-page-event__content-list">

					@foreach($agenda as $list_agenda)
					<div class="pr-page-event__content-list-item add-fix">
						<?php 
							$day = date('l', strtotime($list_agenda->date_agenda));
							$hari = "";
							  
							  if($day == "Monday"){
							  	$hari = "Senin";
							  }elseif($day == "Tuesday"){
							  	$hari = "Selasa";
							  }elseif($day == "Wednesday"){
							  	$hari = "Rabu";
							  }elseif($day == "Thursday"){
							  	$hari = "Kamis";
							  }elseif($day == "Friday"){
							  	$hari = "Jumat";
							  }elseif($day == "Saturday"){
							  	$hari = "Sabtu";
							  }elseif($day == "Sunday"){
							  	$hari = "Minggu";
							  }
						?>
						<h5 class="pr-page-event__content-list-item-date @if($hari == "Minggu" || $list_agenda->hari_libur == 1)pr-page-event__content-list-item-date--red @endif">
							{{ date('d', strtotime($list_agenda->date_agenda)) }}
							<span>
								{{$hari}}
							</span>
						</h5>

						@if($bread == "Pernikahan")

						<ul class="pr-page-event__content-list-item-detail pr-page-event__content-list-item-detail--wedding">
							@foreach($list_agenda->detail_agenda as $detail_agenda)
							<li>
									
								<figure class="pr-page-event__content-list-item-detail--wedding-figure">
									<img src="{{ URL::asset('upload/wedding/'.$detail_agenda->image_mempelai)}}" alt="Paroki Roh Kudus" title="{{ $detail_agenda->nama_agenda }}">
								</figure>

								<div class="pr-page-event__content-list-item-detail--wedding-text">
									<h4>{{ $detail_agenda->nama_pengantin_pria }} & {{ $detail_agenda->nama_pengantin_wanita }}</h4>
									<dl>
										<dt>Alamat Pria</dt>
										<dd>{{ $detail_agenda->alamat_pengantin_pria }}</dd>
										<dt>Alamat Wanita</dt>
										<dd>{{ $detail_agenda->alamat_pengantin_wanita }}</dd>
										<dt>Pukul</dt>
										<dd>{{date('H.i', strtotime($detail_agenda->waktu_agenda))}} - {{date('H.i', strtotime($detail_agenda->end_agenda))}} WIB</dd>
										<dt>Tempat</dt>
										<dd>@if($detail_agenda->tempat_menikah == 1)
											Gereja Paroki Roh Kudus
										@else
										{{ $detail_agenda->alternative_tempat_menikah }}
										@endif</dd>
									</dl>
								</div>
							</li>
							@endforeach
						</ul>


						@else

						<ul class="pr-page-event__content-list-item-detail">
							@foreach($list_agenda->detail_agenda as $detail_agenda)
							<li>
								<p>{{ $detail_agenda->nama_agenda }}</p>
								@if($detail_agenda->tempat_agenda == 0)
									@if($detail_agenda->tempat_menikah == 1)
										<span>Gereja<span>
									@else
										<span>{{ $detail_agenda->alternative_tempat_menikah }}<span>
									@endif
								@else
								<span>{{ $detail_agenda->nama_tempat }}</span>
								@endif
								<span>{{ date('H.i', strtotime($detail_agenda->waktu_agenda)) }} - {{date('H.i', strtotime($detail_agenda->end_agenda))}} WIB</span>

							</li>
							@endforeach

						</ul>

						@endif



					</div>
					<!-- /pr-page-event__content-list-item -->
					@endforeach
				</div>
				<!-- /pr-page-event__content-list -->
			</div>
			<!-- /pr-page-event__content -->

		</div>

	</div>
	<!-- /pr-page-event -->


</section>
@endsection

@section('script')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
	var date = new Date();
	var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
	var year_min = today.getFullYear() +1.5;
	var month_min = today.getMonth() + 5;
	var end = new Date(year_min, month_min, date.getDate());
	$(document).ready(function(){

		$('#datepicker1').datepicker({
			autoclose : true,
            format: 'M yyyy',
            viewMode : 'months',
            minViewMode : 'months',
            startDate : today,
            endDate : end,
            
        }).on('changeDate', function(ev){
        	
        	var newDate = new Date(ev.date);
        	var newMonth = newDate.getMonth()+1;

        	var startMonth = moment(newDate).startOf('month');
        	var endMonth = moment(newDate).endOf('month');
        	
				
        	$('#periode').daterangepicker({
        		"startDate" : startMonth,
        		"endDate" : endMonth,
        		"minDate" : startMonth,
        		"maxDate" : endMonth,
	            locale: {
	              format: 'DD-MM-YYYY'
	            },
	        });
			
		});

		var bulan="";
		bulan = $("#select_month option:selected").val();
		var date_awal = new Date();
        var bulan_awal = date_awal.getMonth() + 1;

  //       $("#datepicker1").on('change', function(){
		// 	bulan = $("#datepicker1").val();
		// 	var cek = new Date(bulan.getMonth());
		// 	alert(cek);
		// });
		var type = $("#type").val();
		if(type == "filter"){
			var bulan = $("#datepicker1").val()
			var newDate = new Date(bulan.date);
        	var newMonth = newDate.getMonth()+1;

        	var startMonth = moment(newDate).startOf('month');
        	var endMonth = moment(newDate).endOf('month');
			$('#periode').daterangepicker({
	        		"minDate" : moment(startMonth).startOf('month'),
	        		"maxDate" : moment(endMonth).endOf('month'),
		            locale: {
		              format: 'DD-MM-YYYY'
		            },
		        });			
		}else{
			$('#periode').daterangepicker({
	        		"startDate" :  moment(today).startOf('month'),
	        		"endDate" : moment(today).endOf('month'),
	        		"minDate" : moment(today).startOf('month'),
	        		"maxDate" : moment(today).endOf('month'),
		            locale: {
		              format: 'DD-MM-YYYY'
		            },
		        });			
		}

		


		$("#select_month").on('change', function(){
			bulan = $("#select_month option:selected").val();
		});

	});

	
</script>
@stop