@extends('layout.front')
@section('page')
{{ $bidang->nama_bidang }}
@endsection
@section('content')

<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">{{ $bidang->nama_bidang }}</h2>
	<!-- /pr-hero-page__caption -->


</section>
<!-- /pr-hero-page -->

<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		<a href="#" class="pr-breadcrumb__link">{{ $bidang->nama_bidang }}</a>
	</div>

</section>
<!-- /pr-breadcrumb -->

<!-- pr-page -->
<section class="pr-page">


	<!-- pr-page-cat -->
	<div class="pr-page-catpost">
		
		<div class="pr-wrapper pr-wrapper--large">

			<!-- pr-page-catpost__list -->
			<div class="pr-page-catpost__list">
                @if(count($artikel) > 0)
    				@foreach($artikel as $list_artikel)
    
    				<!-- pr-page-catpost__list-item -->
    				<div class="pr-page-catpost__list-item add-fix">
    
    					<figure class="pr-page-catpost__list-item-figure">
    						<img src="{{ URL::asset('upload/artikel/'.$list_artikel->image_post)}}" alt="Paroki Roh Kudus" title="{{$list_artikel->title_post}}">
    					</figure>
    
    					<div class="pr-page-catpost__list-item-text">
    
    						<h3 class="pr-page-catpost__list-item-text-title">
    							<a href="{{ url('artikel/'.$list_artikel->id_posting) }}">{{$list_artikel->title_post}}</a>
    						</h3>
    
    						<div class="pr-page-catpost__list-item-text-author">
    							<span>{{ $list_artikel->nama_bidang }}</span> | <span>{{ date('d M Y', strtotime($list_artikel->created_at)) }}</span>
    						</div>
    
    						<article class="pr-page-catpost__list-item-text-desc">
    							@if(strlen($list_artikel->isi_post) <= 520)
    								{!! $list_artikel->isi_post !!}
    								@else
    								{!! substr($list_artikel->isi_post, 0, 520) !!}...
    								@endif
    						</article>
    
    						<a href="{{ url('artikel/'.$list_artikel->id_posting) }}" class="pr-btn pr-btn--white">Selengkapnya</a>
    					
    					</div>
    
    				</div>
    				<!-- /pr-page-catpost__list-item -->
    				@endforeach
    			@else


				<!-- Jika Tidak Ada Artikel -->
				<!-- pr-page-catpost__list-item -->
				<div class="pr-page-catpost__list-item add-fix">


					Tidak ada Artikel


				</div>
				<!-- /pr-page-catpost__list-item -->
				@endif


			</div>
			<!-- /pr-page-catpost__list -->

			<!-- pr-page-cat__paging -->
			<div class="pr-page-cat__paging">

				<div class="pr-page-cat__paging-wrapper">
					{{$artikel->links()}}
				</div>

			</div>
			<!-- /pr-page-cat__paging -->

		</div>
	</div>
</section>

@endsection