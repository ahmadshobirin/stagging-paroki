@extends('layout.front')
@section('page')
Visi & Misi
@endsection
@section('content')
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">Visi & Misi</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->


<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		<a href="#" class="pr-breadcrumb__link">Visi & Misi</a>
	</div>

</section>
<!-- /pr-breadcrumb -->

<!-- pr-page -->
<section class="pr-page">

	<!-- pr-page-vision -->
	<div class="pr-page-vision">
		
		<div class="pr-wrapper pr-wrapper--small">
			
			<div class="pr-page-vision__list">
				
				<div class="pr-page-vision__list-item">

					<h4 class="pr-page-vision__list-item-title">Visi</h4>

					<div class="pr-page-vision__list-item-text">

						{!! $data->visi !!}

					</div>

				</div>

				<div class="pr-page-vision__list-item">

					<h4 class="pr-page-vision__list-item-title">Misi</h4>

					<div class="pr-page-vision__list-item-text">
						
						{!! $data->misi !!}
					
					</div>

				</div>

			</div>

		</div>

	</div>
	<!-- /pr-page-vision -->


</section>
<!-- /pr-page -->
@endsection