@extends('layout.front')
@section('content')
<main>
	<!-- bz-hero-about -->
	<section class="bz-hero bz-hero-about" style="background-image:url(images/banner-faq.jpg);">
		<div class="bz-wrapper bz-wrapper--large">

			<!-- bz-hero-about__page -->
			<div class="bz-hero-about__page">

				<!-- bz-hero-about__page-desc -->
				<div class="bz-hero-about__page-desc">

					<h3 class="bz-hero-about__page-desc-title">FAQ</h3>
					<p class="bz-hero-about__page-desc-lead">Pertanyaan yang sering muncul seputar Biztek. Jika Anda menemui kesulitan dalam penggunaan website ini, kami telah menyiapkan beberapa pertanyaan beserta jawabannya. Semoga bisa membantu.</p>

				</div>
				<!-- /bz-hero-about__page-desc -->

				<!-- bz-hero-about__page-images -->
				<figure class="bz-hero-about__page-images">
					<img src="{{ URL::asset('images/'.$about->image_about)}}" alt="" title="">
				</figure>
				<!-- /bz-hero-about__page-images -->

			</div>
			<!-- /bz-hero-about__page -->

		</div>
	</section>
	<!-- /bz-hero-about -->

	<!-- bz-about-faq -->
	<section class="bz-about-faq">

		<div class="bz-wrapper bz-wrapper--large" id="faq">

			<!-- bz-about-faq__desc -->
			<div class="bz-about-faq__desc">

				<h3 class="bz-about-faq__desc-title">FAQ Biztek</h3>
				<p class="bz-about-faq__desc-lead">Jika jawaban atas pertanyaan Anda tidak ada di halaman ini, silakan hubungi kami. <a href="contact">Hubungi Kami</a></p>
			</div>
			<!-- /bz-about-faq__desc -->

			<!-- bz-about-faq__list -->
			<div class="bz-about-faq__list add-fix">
				@foreach($faq as $list_faq)
				<!-- bz-about-faq__list-item -->
				<div class="bz-about-faq__list-item">
					<h4 class="bz-about-faq__list-item-title">{{$list_faq->question}}</h4>
					<p class="bz-about-faq__list-item-text">{{$list_faq->answer}}</p>
				</div>
				<!-- bz-about-faq__list-item -->
				@endforeach
			</div>
		</div>
	</section>
	<!-- /bz-about-faq -->
</main>
@include('layout.section_contact')
@endsection