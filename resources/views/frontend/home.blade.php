@extends('layout.front')
@section('page')
Home
@endsection
@section('content')
<!-- pr-hero -->
<section class="pr-hero">

	<!-- pr-hero-item -->
	@foreach($slider as $slide)
	<div class="pr-hero-item">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/slider_update_oktober2/'.$slide->image_slider)}}" alt="" title="">
	</div>
	@endforeach
	<!-- /pr-hero-item -->

</section>
<!-- /pr-hero -->

<!-- pr-home-schedule -->
<section class="pr-home-schedule">
	<div class="pr-wrapper pr-wrapper--large add-fix">

		<!-- pr-home-schedule-misa -->
		<div class="pr-home-schedule-misa">
			
			<h4 class="pr-home-schedule__title">JADWAL MISA</h4>

			<!-- pr-home-schedule-misa__list -->
			<div class="pr-home-schedule-misa__list">



				<!-- pr-home-schedule-misa__list-item -->
				@foreach($header_misa as $misa)
				<div class="pr-home-schedule-misa__list-item">


					<h5 class="pr-home-schedule-misa__list-item-title">{{$misa->nama_jadwal_misa}}</h5>

					<!-- pr-home-schedule-misa__list-item-day -->
					@foreach($misa->hari_misa as $hari_misa)
					<div class="pr-home-schedule-misa__list-item-day">
						<p>{{$hari_misa->hari}}</p>
						@foreach($hari_misa->waktu_misa as $waktu_misa)
						<span>{{ date('H.i',strtotime($waktu_misa->waktu)) }} WIB</span>
						@endforeach
					</div>
					@endforeach
				</div>
				@endforeach
				<!-- /pr-home-schedule-misa__list-item -->

			</div> 
			<!-- /pr-home-schedule-misa__list -->



		</div>
		<!-- /pr-home-schedule-misa -->

		<!-- pr-home-schedule-event -->
		<div class="pr-home-schedule-event">
			

			<h4 class="pr-home-schedule__title">AGENDA</h4>


			<!-- pr-home-schedule-event__list -->
			<div class="pr-home-schedule-event__list add-fix">


				<div class="pr-home-schedule-event__list-wrapper add-fix">
					@if(count($agenda_lain) > 0)
						@foreach($agenda_lain as $list_agenda)
						<!-- pr-home-schedule-event__list-item -->
							@foreach($list_agenda->detail_agenda as $list)
							<div class="pr-home-schedule-event__list-item">
								<p title="{{$list->nama_agenda}}">{{$list->nama_agenda}}</p>
								<span>{{$list->nama_tempat}} | {{date('d M Y', strtotime($list->date_agenda))}} | {{date('H.i', strtotime($list->waktu_agenda))}} WIB</span>
							</div>
							@endforeach
						@endforeach
					@else
						@foreach($agenda as $list)
						<!-- pr-home-schedule-event__list-item -->
							<div class="pr-home-schedule-event__list-item">
								<p title="{{$list->nama_agenda}}">{{$list->nama_agenda}}</p>
								<span>{{$list->nama_tempat}} | {{date('d M Y', strtotime($list->date_agenda))}} | {{date('H.i', strtotime($list->waktu_agenda))}} WIB</span>
							</div>
						@endforeach
					@endif
					<!-- /pr-home-schedule-event__list-item -->

				</div>


				<!-- pr-home-schedule-event__list-date -->
				{{-- <div class="pr-home-schedule-event__list-date">
					<h4>{{date('d')}}<span>{{date('M')}}</span></h4>
					<a href="{{url('agenda/gereja')}}" class="pr-btn pr-btn--white">Selengkapnya</a>
				</div> --}}
				<!-- /pr-home-schedule-event__list-date -->


			</div>
			<!-- /pr-home-schedule-event__list -->


		</div>
		<!-- /pr-home-schedule-event -->

	</div>
</section>
<!-- /pr-home-event -->

<!-- pr-home-info -->
<section class="pr-home-info">
	
	<div class="pr-wrapper pr-wrapper--large add-fix">

		<!-- pr-home-info__wrapper -->
		<div class="pr-home-info__wrapper">

			<h4 class="pr-home-info__title">Info</h4>


			<!-- pr-home-info__list -->
			<div class="pr-home-info__list add-fix">

				@foreach($posting as $list_post)
				<!-- pr-home-info__list-item -->
				<div class="pr-home-info__list-item add-fix">

					<!-- pr-home-info__list-item-text -->
					<div class="pr-home-info__list-item-text">

						<h3 class="pr-home-info__list-item-text-title">
							{{$list_post->title_post}}
						</h3>

						<div class="pr-home-info__list-item-text-author">
							<span>{{$list_post->nama_kategori_user}}</span> | <span>Info</span> | <span>{{date('d M Y', strtotime($list_post->created_at))}}</span>
						</div>

						<article class="pr-home-info__list-item-text-desc">
							@if($list_post->id_bidang == 0)
							 	{!! $list_post->isi_post !!}
							@else
								@if($list_post->type_post == "info")
									{!! $list_post->isi_post !!}
								@else
									{!! $list_post->isi_publish_post !!}
								@endif
							@endif
						</article>

						

					</div>
					<!-- /pr-home-info__list-item-text -->


				</div>
				<!-- /pr-home-info__list-item -->
				@endforeach

			</div>
			<!-- /pr-home-info__list -->


			<a href="{{ url('info')}}" class="pr-link">Info Lainnya &raquo;</a>

		</div>

		<!-- pr-home-info__wrapper -->
		<div class="pr-home-info__wrapper">

			<h4 class="pr-home-info__title">Artikel</h4>


			<!-- pr-home-info__list -->
			<div class="pr-home-info__list add-fix">
			    @if(count($artikel) <= 0)
			        <div class="pr-home-info__list-item add-fix">
			            Tidak Ada Artikel
			        </div>
			    @else

    				@foreach($artikel as $list_artikel)
    				<!-- pr-home-info__list-item -->
    				<div class="pr-home-info__list-item add-fix">
    
    					<!-- pr-home-info__list-item-text -->
    					<div class="pr-home-info__list-item-text">
    
    						<h3 class="pr-home-info__list-item-text-title">
    							<a href="{{ url('artikel/'.$list_artikel->id_posting) }}">{{$list_artikel->title_post}}</a>
    						</h3>
    
    						<div class="pr-home-info__list-item-text-author">
    							<span>{{$list_artikel->nama_kategori_user}}</span> | <span>Artikel</span> | <span>{{date('d M Y', strtotime($list_artikel->created_at))}}</span>
    						</div>
    
    						<article class="pr-home-info__list-item-text-desc">
    							@if(strlen($list_artikel->isi_post) <= 200)
        							{!! $list_artikel->isi_post !!}
        						@else
        							{!! substr($list_artikel->isi_post, 0, 200) !!}...<a href="{{ url('artikel/'.$list_artikel->id_posting) }}">Selengkapnya</a>
        						@endif
    
    						</article>						
    
    					</div>
    					<!-- /pr-home-info__list-item-text -->
    
    
    				</div>
    				<!-- /pr-home-info__list-item -->
    				@endforeach
    			@endif

			</div>
			<!-- /pr-home-info__list -->

		</div>

		

	</div>
</section>
<!-- /pr-home-info -->

<!-- pr-home-pray -->
<section class="pr-home-pray">
	<div class="pr-wrapper pr-wrapper--small">
		

		<div class="pr-home-pray__list">

			@foreach($renungan as $list_renungan)
			<!-- pr-home-pray__list-item -->
			<div class="pr-home-pray__list-item">
				
				<h3 class="pr-home-pray__list-item-title">
					<a href="{{url('renungan/detail/'.$list_renungan->id_renungan)}}">{{ $list_renungan->title_renungan }}</a>
				</h3>
				<article class="pr-home-pray__list-item-desc">
					{!! substr($list_renungan->isi_renungan, 0, 200) !!}<a href="{{url('renungan/detail/'.$list_renungan->id_renungan)}}"> Selengkapnya...</a>
				</article>
				<div class="pr-home-pray__list-item-author">- {{ $list_renungan->nama_romo }} -</div>
			</div>
			<!-- /pr-home-pray__list-item -->
			@endforeach

		</div>


	</div>
</section>
<!-- /pr-home-pray -->


@endsection