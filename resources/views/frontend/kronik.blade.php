@extends('layout.front')
@section('page')
Kronik
@endsection
@section('customcss')
<link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('plugins/datepicker/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('plugins/datepicker/datepicker3.css')}}">
<link href="{{asset('css/select2-bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
@stop
@section('content')
	<section class="pr-hero-page">

		<!-- pr-hero-page__figure -->
		<figure class="pr-hero-page__figure">
			<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
		</figure>
		<!-- /pr-hero-page__figure -->

		<!-- pr-hero-page__caption -->
		<h2 class="pr-hero-page__caption">Kronik</h2>
		<!-- /pr-hero-page__caption -->


	</section>
	<!-- /pr-hero-page -->
	<!-- pr-breadcrumb -->
	<section class="pr-breadcrumb">

		<div class="pr-wrapper pr-wrapper--large">
			<a href="{{url('/')}}" class="pr-breadcrumb__link">Home</a> /
			<a href="{{url('kronik')}}" class="pr-breadcrumb__link">Kronik</a>
		</div>

	</section>
	<!-- /pr-breadcrumb -->

	<!-- pr-page -->
	<section class="pr-page">

		<!-- pr-page-info -->
		<div class="pr-page-info">
			<div class="pr-wrapper pr-wrapper--large">


				<div class="add-fix">

					<!-- pr-page-kronik__sidebar -->
					<aside class="pr-page-kronik__sidebar">
						
						<div class="pr-page-kronik__sidebar-wrapper">

							<!-- pr-page-kronik__sidebar-list -->
							<div class="pr-page-kronik__sidebar-list">

								<!-- pr-page-kronik__sidebar-list-item -->
								<div class="pr-page-kronik__sidebar-list-item">
									
									<h4 class="pr-page-kronik__sidebar-list-item-title">Kronik</h4>

									<!-- pr-page-kronik__sidebar-list-item-content -->
									<ul class="pr-page-kronik__sidebar-list-item-content">

										<li>
											<a href="{{ url('kronik/0')}}" class="pr-page-kronik__sidebar-list-item-content-link">Agenda Umum</a>
										</li>
										@foreach($bidang as $list_bidang)
										<li>
											<a href="{{ url('kronik/'.$list_bidang->id_bidang)}}" class="pr-page-kronik__sidebar-list-item-content-link">{{$list_bidang->nama_bidang}}</a>
										</li>
										@endforeach

									</ul>
									<!-- /pr-page-kronik__sidebar-list-item-content -->

								</div>
								<!-- /pr-page-kronik__sidebar-list-item -->

							</div>
							<!-- /pr-page-kronik__sidebar-list -->

						</div>

					</aside>
					<!-- /pr-page-kronik__sidebar -->

					<div class="pr-home-info__list pr-home-info__list--kronik add-fix">

						@if(count($kronik) > 0)
							@foreach($kronik as $list_kronik)
							<!-- pr-home-info__list-item -->
							<div class="pr-home-info__list-item">

								<!-- pr-home-info__list-item-text -->
								<div class="pr-home-info__list-item-text">

									<h3 class="pr-home-info__list-item-text-title">
										<a href="{{ url('kronik-detail/'.$list_kronik->id_posting) }}">{{$list_kronik->title_post}}</a>
									</h3>

									<div class="pr-home-info__list-item-text-author">
										<span>@if($list_kronik->id_bidang == 0) Sekretariat Paroki @else{{$list_kronik->nama_bidang}} @endif</span> | <span>Kronik</span> | <span>{{ date('d M Y', strtotime($list_kronik->created_at)) }}</span>
									</div>

									<article class="pr-home-info__list-item-text-desc">
										 {!! substr($list_kronik->isi_post, 0, 200) !!} <a href="{{ url('kronik-detail/'.$list_kronik->id_posting) }}"> Selengkapnya...</a>
									</article>

								</div>
								<!-- /pr-home-info__list-item-text -->

							</div>
							<!-- /pr-home-info__list-item -->
							@endforeach
						@else


						<!-- Jika tidak ada kronik -->
						<!-- pr-home-info__list-item -->
						<div class="pr-home-info__list-item">

							<!-- pr-home-info__list-item-text -->
							<div class="pr-home-info__list-item-text">

								Belum Ada Kronik

							</div>
							<!-- /pr-home-info__list-item-text -->

						</div>
						<!-- /pr-home-info__list-item -->
						@endif



					</div>
					<!-- /pr-home-info__list -->
				
				</div>

				<div class="pr-page-cat__paging">

					<div class="pr-page-cat__paging-wrapper">

						{{$kronik->links()}}

					</div>

				</div>
				<!-- /pr-page-cat__paging -->
			
			</div>
		</div>
	</section>

@endsection