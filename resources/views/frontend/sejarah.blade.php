@extends('layout.front')
@section('page')
{{ $sejarah->title_sejarah }}
@endsection
@section('content')

<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">{{ $sejarah->title_sejarah }}</h2>
	<!-- /pr-hero-page__caption -->


</section>
<!-- /pr-hero-page -->

<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		<a href="#" class="pr-breadcrumb__link">Sejarah</a> /
		<a href="#" class="pr-breadcrumb__link">{{ $sejarah->title_sejarah }}</a>
	</div>

</section>
<!-- /pr-breadcrumb -->

<section class="pr-page">

		<!-- pr-page-text -->
		<div class="pr-page-text">

			<div class="pr-wrapper pr-wrapper--small">

				<h3 class="pr-page-text__title">{{ $sejarah->title_sejarah }}</h3>
				<article class="pr-page-text__desc add-fix">

					<figure class="pr-float-right">
						<img src="{{  URL::asset('upload/sejarah/'.$sejarah->image_sejarah) }}" alt="Paroki Roh Kudus" title="{{ $sejarah->title_sejarah }}">
					</figure>
					
					{!! $sejarah->isi_sejarah !!}
				</article>

			</div>

		</div>
		<!-- /pr-page-text -->

</section>
<!-- /pr-page -->
@endsection