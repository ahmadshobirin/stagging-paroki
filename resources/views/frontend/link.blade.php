@extends('layout.front')
@section('page')
Link
@endsection
@section('content')

<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">{{ ucfirst($kat) }}</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->

<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		<a href="#" class="pr-breadcrumb__link">{{ ucfirst($kat) }}</a>
	</div>

</section>
<!-- /pr-breadcrumb -->

<section class="pr-page">


	<!-- pr-page-link -->
	<div class="pr-page-link">
		
		<div class="pr-wrapper pr-wrapper--small">

			<!-- pr-page-link__list -->
			<div class="pr-page-link__list">
                @if(count($link) > 0)
    				@foreach($link as $list_link)
    				<!-- pr-page-link__list-item -->
    				<div class="pr-page-link__list-item">
    
    					<h3 class="pr-page-link__list-item-title">
    						<a href="http://{{ $list_link->url_link }}" target="_blank" title="{{ $list_link->title_link }}">{{ $list_link->title_link }}</a>
    					</h3>
    
    					<span class="pr-page-link__list-item-link">{{ $list_link->url_link }}</span>
    
    					<p class="pr-page-link__list-item-text">{{ $list_link->desc_link }}</p>
    
    
    				</div>
    				<!-- /pr-page-link__list-item -->
    				@endforeach
    			@else


				<!-- Jika Tidak Ada Link -->
				<!-- pr-page-link__list-item -->
				 <div class="pr-page-link__list-item"> 

					 Tidak Ada Link 


				 </div> 
				<!-- /pr-page-link__list-item -->
				@endif
				

			</div>

			<!-- pr-page-cat__paging -->
			<div class="pr-page-cat__paging">

				<div class="pr-page-cat__paging-wrapper">

					{{$link->links()}}

				</div>

			</div>
			<!-- /pr-page-cat__paging -->

		</div>
	</div>
</section>

@endsection