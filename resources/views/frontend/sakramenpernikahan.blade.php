@extends('layout.front')
@section('page')
Sakramen
@endsection
@section('customcss')
<link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('plugins/datepicker/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('plugins/datepicker/datepicker3.css')}}">
<link href="{{asset('css/select2-bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
@stop
@section('content')
	<section class="pr-hero-page">

		<!-- pr-hero-page__figure -->

		<figure class="pr-hero-page__figure">
			<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="Sejarah Paroki Roh Kudus">
		</figure>



		<!-- /pr-hero-page__figure -->

		<!-- pr-hero-page__caption -->
		<h2 class="pr-hero-page__caption">Pelayanan Sakramen</h2>
		<!-- /pr-hero-page__caption -->


	</section>
	<!-- /pr-hero-page -->
	<!-- pr-breadcrumb -->
	<section class="pr-breadcrumb">

		<div class="pr-wrapper pr-wrapper--large">
			<a href="{{url('/')}}" class="pr-breadcrumb__link">Home</a> /
			<a href="{{url('sakramenpernikahan')}}" class="pr-breadcrumb__link">Pelayanan Sakramen Pernikahan</a>
		</div>

	</section>
	<!-- /pr-breadcrumb -->

	<!-- pr-page -->
	<section class="pr-page">

		<!-- pr-page-info -->
		<div class="pr-page-info">
			<div class="pr-wrapper pr-wrapper--large">


				<div class="add-fix">

					<!-- pr-page-kronik__sidebar -->
					<aside class="pr-page-kronik__sidebar">

						<div class="pr-page-kronik__sidebar-wrapper">

							<!-- pr-page-kronik__sidebar-list -->
							<div class="pr-page-kronik__sidebar-list">

								<!-- pr-page-kronik__sidebar-list-item -->
								<div class="pr-page-kronik__sidebar-list-item">

									<h4 class="pr-page-kronik__sidebar-list-item-title">Pelayanan Sakramen</h4>

									<!-- pr-page-kronik__sidebar-list-item-content -->
									<ul class="pr-page-kronik__sidebar-list-item-content">

										<li>
											<a href="{{ url('sakramenbaptis')}}" class="pr-page-kronik__sidebar-list-item-content-link">Sakramen Baptis</a>
										</li>
										<li>
											<a href="{{ url('sakramenkomuni')}}" class="pr-page-kronik__sidebar-list-item-content-link">Sakramen Komuni</a>
										</li>
										<li>
											<a href="{{ url('sakramenkrisma')}}" class="pr-page-kronik__sidebar-list-item-content-link">Sakramen Krisma</a>
										</li>
										<li>
											<a href="{{ url('sakramenpernikahan')}}" class="pr-page-kronik__sidebar-list-item-content-link">Sakramen Pernikahan</a>
										</li>


									</ul>
									<!-- /pr-page-kronik__sidebar-list-item-content -->

								</div>
								<!-- /pr-page-kronik__sidebar-list-item -->

							</div>
							<!-- /pr-page-kronik__sidebar-list -->

						</div>

					</aside>
					<!-- /pr-page-kronik__sidebar -->

					<div class="pr-home-info__list pr-home-info__list--kronik add-fix">
							<div class="pr-home-info__list-item">

								<div class="pr-home-info__list-item-text">

									<h3 class="pr-home-info__list-item-text-title">

									</h3>

									<div class="pr-home-info__list-item-text-author">
										<span></span> | <span>Sakramen Pernikahan</span> | <span></span>
									</div>

									<!-- <article class="pr-home-info__list-item-text-desc">

									</article> -->

								</div>
							</div>


						<img src="{{ URL::asset('upload/alur_pendaftaran/pendaftaran_nikah.jpg')}}" alt="Paroki Roh Kudus" width="100%" height="auto">
						<!-- Jika tidak ada kronik -->
						<!-- pr-home-info__list-item -->
						<div class="pr-home-info__list-item">

							<!-- pr-home-info__list-item-text -->
							<div class="pr-home-info__list-item-text">
								<div><h3 class="pr-home-info__list-item-text-title"><b>Syarat Pendaftaran Pernikahan</b></h3>
									<ul style="list-style-type: square">
											<li>Pihak Calon mempelai Perempuan adalah Warga Gereja Katolik Roh Kudus (jika Calon mempelai Laki-laki Beragama Katolik / Non Katolik). Atau pihak calon mempelai Laki-laki adalah Warga Gereja Katolik Roh Kudus (jika Calon mempelai Perempuan Bukan Katolik).</li>
											<li>Harus Sudah Menerima Sakramen Krisma.</li>
											<li>Melampirkan Foto Surat Baptis terbaru / tanggal pengesahan terbaru maksimal 6 bulan terakhir dari tanggal menikah.</p>
											<li>Mengikuti Kursus Pernikahan.</li>
											<li>Untuk pernikahan sesama Katolik, pendaftaran paling lambat 3 sebelum tanggal menikah.Sedangkan pernikahan beda gereja atau agama, pendaftaran paling lambat 6 bulan sebelum tanggal menikah.</li>
											<li>Membayar biaya administrasi pernikahan sebesar Rp. 50.000,-.</li></br>
									</ul>
								</div>
							</div>
							<button onclick="location.href='{{ url('register') }}'" type="button" name="button" value="daftar" class="pr-btn">Daftar</button>
							<button onclick="location.href='{{url('/login/umat')}}'" type="button" name="button" value="loginumat" class="pr-btn">Login</button>
						</div>


					</div>
				</div>

				<div class="pr-page-cat__paging">

					<div class="pr-page-cat__paging-wrapper">
					</div>

				</div>
			</div>
		</div>
	</section>

@endsection
