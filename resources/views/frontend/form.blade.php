@extends('layout.front')
@section('customcss')

@stop
@section('content')
<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">@if($name == "majalah") Majalah ERKA @else Warta PAROKI @endif</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->

<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{url('/')}}" class="pr-breadcrumb__link">Home</a> /
		<a href="#" class="pr-breadcrumb__link">@if($name == "majalah") Majalah ERKA @else Warta PAROKI @endif</a>
	</div>

</section>
<!-- /pr-breadcrumb -->
<section class="pr-page">


	<!-- pr-page-cat -->
	<div class="pr-page-cat">
		
		<div class="pr-wrapper pr-wrapper--large">

			<!-- pr-page-cat__list -->
			<div class="pr-page-cat__list add-fix">
				@foreach($data as $form)
				<!-- pr-page-cat__list-erka -->
				<div class="pr-page-cat__list-erka">

					<div class="pr-page-cat__list-erka-wrapper">

						<figure class="pr-page-cat__list-erka-figure">
							<img src="{{ URL::asset('upload/image-form/'.$form->image_form)}}" alt="Paroki Roh Kudus" title="{{ $form->nama_form }}">
						</figure>

						<h3 class="pr-page-cat__list-erka-title" title="{{ $form->nama_form }}">
							{{ $form->nama_form }}
						</h3>

						<span class="pr-page-cat__list-erka-date">{{ date('F Y', strtotime($form->periode_form))}}</span>

						<a href="{{url('/download/'.$form->id_form)}}" class="pr-btn" target="_blank">Download</a>

					</div>

				</div>
				<!-- /pr-page-cat__list-erka -->
				@endforeach
			</div>
		</div>
	</div>
</section>
@endsection