@extends('layout.front')
@section('content')
<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">Renungan</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->

<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/')}}" class="pr-breadcrumb__link">Home</a> /
		<a href="{{url('renungan')}}" class="pr-breadcrumb__link">Renungan</a>
	</div>

</section>

<!-- pr-page -->
<section class="pr-page">

	<!-- pr-page-detail -->
	<div class="pr-page-detail">

		<div class="pr-wrapper pr-wrapper--large add-fix">


			<!-- pr-page-detail__content--pray-search -->
			<form class="pr-page-detail__content--pray-search" action="{{ url('renungan/search') }}" method="GET">
				<input type="text" name="search" class="pr-input" placeholder="Cari...">
				<input type="submit" name="" class="pr-btn" value="Cari">
			</form>
			<!-- /pr-page-detail__content--pray-search -->
			
			<!-- pr-page-detail__content -->
			<div class="pr-page-detail__content">

				<!-- pr-page-detail__content-share -->
				<div class="pr-page-detail__content-share">

					<?php 
						$a = str_replace("<ul>", "", $fb);
						$b = str_replace("</ul>", "", $a); 
						$c = str_replace("<li>", "", $b); 
						$d = str_replace("</li>", "", $c);

						echo html_entity_decode($d); 
					?>

					{{-- <a title="Bagikan" href="http://www.facebook.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-facebook-square"></i></a>
					<a title="Bagikan" href="http://www.twitter.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-twitter-square"></i></a>
					<a title="Bagikan" href="http://www.google.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-google-plus-square"></i></a> --}}

				</div>
				<!-- /pr-page-detail__content-share -->
				
				<!-- pr-page-detail__content-figure -->
				<figure class="pr-page-detail__content-figure">
					<img src="{{ URL::asset('upload/renungan/'.$renungan->image_renungan)}}" alt="Paroki Roh Kudus" title="{{$renungan->title_renungan}}">
				</figure>
				<!-- /pr-page-detail__content-figure -->

				<h3 class="pr-page-detail__content-title">{{$renungan->title_renungan}}</h3>

				<div class="pr-page-detail__content-author">
					<span>Sekretariat Paroki</span> | <span>Renungan</span> | <span>{{ date('d F Y', strtotime($renungan->created_at)) }}</span>
				</div>

				<article class="pr-page-text__desc">

					{!! $renungan->isi_renungan !!}

					<div class="pr-home-pray__list-item-author">- {{ $renungan->nama_romo }} -</div>

				</article>

			</div>
			<!-- /pr-page-detail__content -->

			<!-- pr-page-detail__sidebar -->
			<aside class="pr-page-detail__sidebar">
				
				<div class="pr-page-detail__sidebar-wrapper">

					<!-- pr-page-detail__sidebar-list -->
					<div class="pr-page-detail__sidebar-list">


						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item  pr-page-detail__sidebar-list-item--search">

							<form action="{{ url('renungan/search') }}" method="GET">
								<input type="text" name="search" class="pr-input" placeholder="Cari...">
								<input type="submit" name="" class="pr-btn" value="Cari">
							</form>

						</div>
						<!-- /pr-page-detail__sidebar-list-item -->

						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item">
							
							<h4 class="pr-page-detail__sidebar-list-item-title">Archive</h4>

							<!-- pr-page-detail__sidebar-list-item-content -->
							<ul class="pr-page-detail__sidebar-list-item-content">
								@foreach($tahun as $thn)
								<li>
									<a href="{{ url('renungan/tahun/'.$thn->tahun) }}" class="pr-page-detail__sidebar-list-item-content-link">{{$thn->tahun}}</a>
									<div class="pr-page-detail__sidebar-list-item-content-dd">
										@foreach($thn->bulan as $bln)
										<a href="{{ url('renungan/bulan/'.$bln->bulan) }}" class="pr-page-detail__sidebar-list-item-content-dd-link">
											<?php 	$dateObj   = DateTime::createFromFormat('!m', $bln->bulan);
													$monthName = $dateObj->format('F'); // March ?>
												{{ $monthName }}</a>
										@endforeach
									</div>
								</li>
								@endforeach
							</ul>
							<!-- /pr-page-detail__sidebar-list-item-content -->

						</div>
						<!-- /pr-page-detail__sidebar-list-item -->

						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item">
							
							<h4 class="pr-page-detail__sidebar-list-item-title">Agenda</h4>

							<!-- pr-page-detail__sidebar-list-item-content -->
							<ul class="pr-page-detail__sidebar-list-item-content">
								@if(count($agenda) > 0)

								@foreach($agenda as $agn)
								<!-- Max Isi 5 -->
								<li>
									<span class="pr-page-detail__sidebar-list-item-content-link">{{ $agn->nama_agenda }}</span>
									<span class="pr-page-detail__sidebar-list-item-content-author">{{ $agn->nama_tempat }}| {{ date('d M Y', strtotime($agn->date_agenda)) }} | {{ date('H.i', strtotime($agn->waktu_agenda)) }} WIB</span>
								</li>
								@endforeach
								@else
								@foreach($agenda_lain as $agn_lain)
									<!-- Max Isi 5 -->
									@foreach($agn_lain->detail_agenda as $agn)
									<li>
										<span class="pr-page-detail__sidebar-list-item-content-link">{{ $agn->nama_agenda }}</span>
										<span class="pr-page-detail__sidebar-list-item-content-author">{{ $agn->nama_tempat }}| {{ date('d M Y', strtotime($agn->date_agenda)) }} | {{ date('H.i', strtotime($agn->waktu_agenda)) }} WIB</span>
									</li>
									@endforeach
								@endforeach
								@endif
							</ul>
						</div>

					</div>
					<!-- /pr-page-detail__sidebar-list -->

				</div>

			</aside>
			<!-- /pr-page-detail__sidebar -->
		</div>
	</div>
</section>

@endsection
@section('script')
<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/share.js') }}"></script>
@endsection