@extends('layout.front')
@section('content')

<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">Renungan</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->


<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		@if($bln)
		<a href="{{ url('renungan') }}" class="pr-breadcrumb__link">Renungan</a> /
		<a href="#" class="pr-breadcrumb__link">{{ date('F', strtotime($bln)) }}</a>
		@else
		<a href="{{ url('renungan') }}" class="pr-breadcrumb__link">Renungan</a>
		@endif
	</div>

</section>
<!-- /pr-breadcrumb -->

<!-- pr-page -->
<section class="pr-page">

	<!-- pr-page-detail -->
	<div class="pr-page-detail">

		<div class="pr-wrapper pr-wrapper--large add-fix">
			
			<!-- pr-page-detail__content -->
			<div class="pr-page-detail__content pr-page-detail__content--pray">


				<!-- pr-page-detail__content--pray-search -->
				<form class="pr-page-detail__content--pray-search" action="{{ url('renungan/search') }}" method="GET">
					<input type="text" name="search" class="pr-input" placeholder="Cari...">
					<input type="submit" name="" class="pr-btn" value="Cari">
				</form>
				<!-- /pr-page-detail__content--pray-search -->



				<!-- pr-home-info__list -->
				<div class="pr-home-info__list add-fix">
					@if(count($renungan) > 0)
						@foreach($renungan as $renung)
						<!-- pr-home-info__list-item -->
						<div class="pr-home-info__list-item">

							<!-- pr-home-info__list-item-text -->
							<div class="pr-home-info__list-item-text">

								<h3 class="pr-home-info__list-item-text-title">
									<a href="{{ url('renungan/detail/'.$renung->id_renungan) }}">{{$renung->title_renungan}}</a>
								</h3>

								<div class="pr-home-info__list-item-text-author">
									<span>Sekretariat Paroki</span> | <span>Renungan</span> | <span>{{ date('d M Y', strtotime($renung->created_at)) }}</span>
								</div>

								<article class="pr-home-info__list-item-text-desc">
									{!! substr($renung->isi_renungan, 0, 200) !!} <a href="{{ url('renungan/detail/'.$renung->id_renungan) }}"> Selengkapnya...</a>
								</article>

								

							</div>
							<!-- /pr-home-info__list-item-text -->

						</div>
						<!-- /pr-home-info__list-item -->
						@endforeach
					@else
						<!-- Pencarian Tidak ada -->
					<!-- pr-home-info__list-item -->
					<div class="pr-home-info__list-item">

						<!-- pr-home-info__list-item-text -->
						<div class="pr-home-info__list-item-text">


							<article class="pr-home-info__list-item-text-desc">
								Tidak ada hasil.
							</article>

							

						</div>
						<!-- /pr-home-info__list-item-text -->

					</div>
					<!-- /pr-home-info__list-item -->
					@endif
				</div>

				<!-- pr-page-cat__paging -->
				<div class="pr-page-cat__paging">

					<div class="pr-page-cat__paging-wrapper">

						{{$renungan->links()}}

					</div>

				</div>
				<!-- /pr-page-cat__paging -->
			</div>

			<!-- pr-page-detail__sidebar -->
			<aside class="pr-page-detail__sidebar pr-page-detail__sidebar--pray">
				
				<div class="pr-page-detail__sidebar-wrapper">


					<!-- pr-page-detail__sidebar-list -->
					<div class="pr-page-detail__sidebar-list">


						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item pr-page-detail__sidebar-list-item--search">

							<form action="{{ url('renungan/search') }}" method="GET">
								<input type="text" name="search" class="pr-input" placeholder="Cari...">
								<input type="submit" name="" class="pr-btn" value="Cari">
							</form>

						</div>
						<!-- /pr-page-detail__sidebar-list-item -->

						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item">
							
							<h4 class="pr-page-detail__sidebar-list-item-title">Archive</h4>

							<!-- pr-page-detail__sidebar-list-item-content -->
							<ul class="pr-page-detail__sidebar-list-item-content">
								@foreach($tahun as $thn)
								<li>
									<a href="{{ url('renungan/tahun/'.$thn->tahun) }}" class="pr-page-detail__sidebar-list-item-content-link">{{$thn->tahun}}</a>
									<div class="pr-page-detail__sidebar-list-item-content-dd">
										@foreach($thn->bulan as $bln)
										<a href="{{ url('renungan/bulan/'.$bln->bulan) }}" class="pr-page-detail__sidebar-list-item-content-dd-link">
											<?php 	$dateObj   = DateTime::createFromFormat('!m', $bln->bulan);
													$monthName = $dateObj->format('F'); // March ?>
												{{ $monthName }}</a>
										</a>
										@endforeach
									</div>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</aside>
		</div>
	</div>
</section>

@endsection