@extends('layout.front')
@section('page')
Romo
@endsection
@section('content')
<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">Pastor & Diakon</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->


<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{url('/')}}" class="pr-breadcrumb__link">Home</a> /
		<a href="#" class="pr-breadcrumb__link">Pastor & Diakon</a>
	</div>

</section>
<!-- /pr-breadcrumb -->
<!-- pr-page -->
<section class="pr-page">
	<div class="pr-page-romo">
		<div class="pr-wrapper pr-wrapper--large">

			<!-- pr-page-romo__list -->
			<div class="pr-page-romo__list add-fix">

				@foreach($romo as $list_romo)
				
				<!-- pr-page-romo__list-item -->
				<div class="pr-page-romo__list-item">

					<div class="pr-page-romo__list-item-wrapper add-fix">

						<figure class="pr-page-romo__list-item-figure">
							<img src="{{ URL::asset('upload/romo/'.$list_romo->image_romo)}}" alt="Paroki Roh Kudus" title="{{$list_romo->nama_romo}}">
						</figure>

						<div class="pr-page-romo__list-item-text">
							<h3 class="pr-page-romo__list-item-text-title">{{$list_romo->nama_romo}}</h3>
							<dl class="pr-page-romo__list-item-text-desc">
								<dt>Tahbisan Imam</dt>
								<dd>{{ $list_romo->tempat_tahbisan_imam }}, {{ date('d F Y', strtotime($list_romo->tanggal_tahbisan_imam)) }}</dd>
								<dt>Masa Tugas</dt>
								<dd>{{ date('d M Y', strtotime($list_romo->awal_masa_tugas)) }} – {{ date('d M Y', strtotime($list_romo->akhir_masa_tugas)) }}</dd>
								<dt>Status</dt>
								<dd>{{ $list_romo->status }}</dd>
							</dl>
							{{-- <a href="{{ url('romo/'.$list_romo->id_romo) }}" class="pr-page-romo__list-item-text-link">Selengkapnya</a> --}}
						</div>

					</div>

				</div>
				<!-- /pr-page-romo__list-item -->
				@endforeach
			</div>
		</div>
	</div>
</section>
@endsection