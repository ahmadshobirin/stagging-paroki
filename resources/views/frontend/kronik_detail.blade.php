@extends('layout.front')
@section('page')
{{$kronik->title_post}}
@endsection
@section('customcss')
<link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('plugins/datepicker/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('plugins/datepicker/datepicker3.css')}}">
<link href="{{asset('css/select2-bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
@stop
@section('content')

<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="Sejarah Paroki Roh Kudus">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">{{$kronik->title_post}}</h2>
	<!-- /pr-hero-page__caption -->

</section>
<!-- /pr-hero-page -->

<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		<a href="bidang.html" class="pr-breadcrumb__link">Kronik</a>
	</div>

</section>
<!-- /pr-breadcrumb -->

<section class="pr-page">

	<!-- pr-page-detail -->
	<div class="pr-page-detail">

		<div class="pr-wrapper pr-wrapper--large add-fix">
			
			<!-- pr-page-detail__content -->
			<div class="pr-page-detail__content">

				<!-- pr-page-detail__content-share -->
				<div class="pr-page-detail__content-share">

					<?php 
						$a = str_replace("<ul>", "", $fb);
						$b = str_replace("</ul>", "", $a); 
						$c = str_replace("<li>", "", $b); 
						$d = str_replace("</li>", "", $c);

						echo html_entity_decode($d); 
					?>

					{{-- <a title="Bagikan" href="http://www.facebook.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-facebook-square"></i></a>
					<a title="Bagikan" href="http://www.twitter.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-twitter-square"></i></a>
					<a title="Bagikan" href="http://www.google.com/" target="_blank" class="pr-page-detail__content-share-link"><i class="pr-fa fab fa-google-plus-square"></i></a> --}}

				</div>
				<!-- /pr-page-detail__content-share -->
				
				<!-- pr-page-detail__content-figure -->
				<figure class="pr-page-detail__content-figure">


					@if($kronik->image_post != "no_image.png")
						<img src="{{ URL::asset('upload/kronik/'.$kronik->image_post)}}" alt="Paroki Roh Kudus" title="{{$kronik->title_post}}">
					@endif


				</figure>
				<!-- /pr-page-detail__content-figure -->

				<h3 class="pr-page-detail__content-title">{{$kronik->title_post}}</h3>

				<div class="pr-page-detail__content-author">
					<span>@if($kronik->id_bidang == 0) Sekretariat Paroki @else{{$kronik->nama_bidang}} @endif</span> | <span>Kronik</span> | <span>{{ date('d M Y', strtotime($kronik->created_at)) }}</span>
				</div>

				<article class="pr-page-text__desc">

					<figure class="pr-float-left">
						

						@if($kronik->image2_post != "no_image.png")
						<img src="{{ URL::asset('upload/kronik/'.$kronik->image2_post)}}" alt="Paroki Roh Kudus" title="{{$kronik->title_post}}">
					@endif


					</figure>


					{!! $kronik->isi_post !!}

					<figure class="pr-page-detail__content-figure pr-page-detail__content-figure--bottom">
						

						@if($kronik->image3_post != "no_image.png")
						<img src="{{ URL::asset('upload/kronik/'.$kronik->image3_post)}}" alt="Paroki Roh Kudus" title="{{$kronik->title_post}}">
					@endif


					</figure>

				</article>

			</div>
			<!-- /pr-page-detail__content -->

			<!-- pr-page-detail__sidebar -->
			<aside class="pr-page-detail__sidebar">
				
				<div class="pr-page-detail__sidebar-wrapper">

					<!-- pr-page-detail__sidebar-list -->
					<div class="pr-page-detail__sidebar-list">

						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item">
							
							<h4 class="pr-page-detail__sidebar-list-item-title">Kronik</h4>

							<!-- pr-page-detail__sidebar-list-item-content -->
							<ul class="pr-page-detail__sidebar-list-item-content">
								@foreach($kronik_all as $kron_all)
								<!-- Max Isi 5 -->
								<li>
									<a href="{{ url('kronik-detail/'.$kron_all->id_posting) }}" class="pr-page-detail__sidebar-list-item-content-link">{{ $kron_all->title_post }}</a>
									<span class="pr-page-detail__sidebar-list-item-content-author">@if($kron_all->id_bidang == 0) Sekretariat Paroki @else{{$kron_all->nama_bidang}} @endif | {{ date('d M Y', strtotime($kron_all->created_at)) }}</span>
								</li>
								@endforeach

							</ul>
							<!-- /pr-page-detail__sidebar-list-item-content -->

						</div>
						<!-- /pr-page-detail__sidebar-list-item -->

						<!-- pr-page-detail__sidebar-list-item -->
						<div class="pr-page-detail__sidebar-list-item">
							
							<h4 class="pr-page-detail__sidebar-list-item-title">Agenda</h4>

							<!-- pr-page-detail__sidebar-list-item-content -->
							<ul class="pr-page-detail__sidebar-list-item-content">

								@if(count($agenda) > 0)

								@foreach($agenda as $agn)
								<!-- Max Isi 5 -->
								<li>
									<span class="pr-page-detail__sidebar-list-item-content-link">{{ $agn->nama_agenda }}</span>
									<span class="pr-page-detail__sidebar-list-item-content-author">{{ $agn->nama_tempat }}| {{ date('d M Y', strtotime($agn->date_agenda)) }} | {{ date('H.i', strtotime($agn->waktu_agenda)) }} WIB</span>
								</li>
								@endforeach
								@else
								@foreach($agenda_lain as $agn_lain)
									<!-- Max Isi 5 -->
									@foreach($agn_lain->detail_agenda as $agn)
									<li>
										<span class="pr-page-detail__sidebar-list-item-content-link">{{ $agn->nama_agenda }}</span>
										<span class="pr-page-detail__sidebar-list-item-content-author">{{ $agn->nama_tempat }}| {{ date('d M Y', strtotime($agn->date_agenda)) }} | {{ date('H.i', strtotime($agn->waktu_agenda)) }} WIB</span>
									</li>
									@endforeach
								@endforeach
								@endif

							</ul>
							<!-- /pr-page-detail__sidebar-list-item-content -->

						</div>
						<!-- /pr-page-detail__sidebar-list-item -->

					</div>
					<!-- /pr-page-detail__sidebar-list -->

				</div>

			</aside>
			<!-- /pr-page-detail__sidebar -->


		</div>

	</div>
	<!-- /pr-page-detail -->


</section>
<!-- /pr-page -->

@endsection