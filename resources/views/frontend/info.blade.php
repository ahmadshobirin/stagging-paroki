@extends('layout.front')
@section('content')

<!-- pr-hero-page -->
<section class="pr-hero-page">

	<!-- pr-hero-page__figure -->
	<figure class="pr-hero-page__figure">
		<img class="pr-hero-item__images" src="{{ URL::asset('upload/banner/'.$slider->image_banner)}}" alt="Paroki Roh Kudus" title="{{$slider->title_banner}}">
	</figure>
	<!-- /pr-hero-page__figure -->

	<!-- pr-hero-page__caption -->
	<h2 class="pr-hero-page__caption">Info Paroki</h2>
	<!-- /pr-hero-page__caption -->


</section>
<!-- /pr-hero-page -->

<!-- pr-breadcrumb -->
<section class="pr-breadcrumb">

	<div class="pr-wrapper pr-wrapper--large">
		<a href="{{ url('/') }}" class="pr-breadcrumb__link">Home</a> /
		<a href="#" class="pr-breadcrumb__link">Info</a>
	</div>

</section>
<!-- /pr-breadcrumb -->
<section class="pr-page">

	
	<!-- pr-page-info -->
	<div class="pr-page-info">
		<div class="pr-wrapper pr-wrapper--large">

			<!-- pr-home-info__list -->
			<div class="pr-home-info__list pr-home-info__list--detail add-fix">


				@foreach($artikel as $art)
				<!-- pr-home-info__list-item -->
				<div class="pr-home-info__list-item add-fix">

					<!-- pr-home-info__list-item-text -->
					<div class="pr-home-info__list-item-text">

						<h3 class="pr-home-info__list-item-text-title">
							{{$art->title_post}}
						</h3>

						<div class="pr-home-info__list-item-text-author">
							<span>{{$art->nama_kategori_user}}</span> | <span>Info</span> | <span>{{date('d M Y', strtotime($art->created_at))}}</span>
						</div>

						<article class="pr-home-info__list-item-text-desc">
							 @if($art->id_bidang == 0)
							 	{!! $art->isi_post !!}
							@else
								@if($art->type_post == "info")
									{!! $art->isi_post !!}
								@else
									{!! $art->isi_publish_post !!}
								@endif
							@endif
						</article>

						

					</div>
					<!-- /pr-home-info__list-item-text -->


				</div>
				<!-- /pr-home-info__list-item -->
				@endforeach
			</div>
			<!-- pr-page-cat__paging -->
			<div class="pr-page-cat__paging">

				<div class="pr-page-cat__paging-wrapper">

					{{$artikel->links()}}

				</div>

			</div>
			<!-- /pr-page-cat__paging -->
		</div>
	</div>
</section>
@endsection