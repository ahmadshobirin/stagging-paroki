<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Url Bar Background Mobile -->
	<meta name="theme-color" content="#192532">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"  />
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
	<meta name="description" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">


	<link rel="manifest" href="site.webmanifest">

	<link rel="apple-touch-icon" href="{{ URL::asset('images/favicon.png')}}">
	<!-- Place favicon.ico in the root directory -->

	<title>biztek</title>

	<!-- Slick -->
	<link href="{{ URL::asset('js/front/slick/slick.css') }}" rel="stylesheet" />

	<!-- Style -->
	<link href="{{ URL::asset('css/style.css')}}" rel="stylesheet">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet"> 

	<!-- Font Awesome -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

</head>

<body id="body">
<!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->


<!-- Main -->
<main class="bz-login">

	<!-- bz-login -->
	<section class="bz-wrapper bz-wrapper--small bz-login__wrapper">

		<h1 class="bz-login__content-logo">
			<a href="{!! url('/') !!}" class="bz-login__content-logo-link">
				<img src="{{ URL::asset('images/logo.png')}}" alt="biztek" title="biztek">
			</a>
		</h1>

		<!-- bz-login__content -->
		<div class="bz-login__content add-fix">
			
			<div class="bz-login__content-hero">
				<figure class="bz-login__content-hero-images">
					<img src="{{ URL::asset('images/login.png')}}" alt="" title="">
				</figure>

				<p class="bz-login__content-hero-lead">Bisnis berkembang lebih cepat dan mudah</p>
				<h3 class="bz-login__content-hero-title">Buat dan kembangkan bisnis Anda dengan Biztek.</h3>

			</div>

			<!-- bz-login__content-box -->
			<div class="bz-login__content-box">

				<h3 class="bz-login__content-box-title">Register</h3>
				<p class="bz-login__content-box-register-link">Sudah punya akun Biztek? <a href="{{ url('/login') }}">Masuk</a>.</p>
                @include("admin.displayerror")

					<!-- bz-login__content-box-form -->
					<div class="bz-login__content-box-form">
						
						<form onsubmit="return validateForm()" method="POST" action="{{ url('/daftar') }}">

							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="bz-login__content-box-form-group">
								<label class="bz-input-label">Nama Lengkap</label>
								<input placeholder="Nama Lengkap" type="text" name="name" required class="bz-input">
							</div>

							<div class="bz-login__content-box-form-group">
								<label class="bz-input-label">Email</label>
								<input placeholder="Email" type="email" name="email" required class="bz-input">
							</div>

							<div class="bz-login__content-box-form-group">
								<label class="bz-input-label">Telepon</label>
								<input placeholder="Telepon" type="text" name="phone" required class="bz-input telp">
							</div>

							<div class="bz-login__content-box-form-group">
								<label class="bz-input-label">Password</label>
								<input placeholder="Password" type="password" name="password" class="bz-input">
							</div>

							<div class="bz-login__content-box-form-group">
								<label class="bz-input-label">Confirm Password</label>
								<input placeholder="Password" type="password" name="confirm_password" class="bz-input">
							</div>

							<input type="submit" name="" value="Daftar" class="bz-btn">
						
						</form>
						
					</div>
					<!-- /bz-login__content-box-form -->

			</div>
			<!-- /bz-login__content-box -->

		</div>
		<!-- /bz-login__content -->

	</section>
	<!-- /bz-login -->

</main>
<!-- /Main -->

<!-- Scripts -->
<script src="{{ URL::asset('js/front/jquery.min.js') }}"></script>
<script src="{{ URL::asset('js/front/script.js') }}"></script>
<script>
    function validateForm(){
        var pass = $('#password').val();
        var confirm_pass = $('#confirm_password').val();

        if( pass != confirm_pass){
            alert('Password dan Confirm Password tidak sama');
            return false;
        }else{
            return true;
        }
    }
</script>
<script type="text/javascript">
	$('.telp').keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 32 || e.which > 57 || (32 < e.which && e.which < 40) ||  e.which == 42 || ( 43 < e.which && e.which < 48 ) )) {
          return false;
        }
      });
</script>
</body>
</html>