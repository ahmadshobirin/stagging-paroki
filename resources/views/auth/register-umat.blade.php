<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Url Bar Background Mobile -->
    <meta name="theme-color" content="#E22F35">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="author" content="Paroki Roh Kudus Surabaya">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
    <meta name="description" content="Website Paroki Roh Kudus Surabaya">
    <meta name="og:title" property="og:title" content="Paroki Roh Kudus Surabaya">
    <meta name="og:url" property="og:url" content="https://www.parokirohkudus.or.id/">
    <meta name="og:description" property="og:description" content="Website Paroki Roh Kudus Surabaya">
    <meta name="og:type" property="og:type" content="letter">
    <meta name="og:image" property="og:image" content="https://www.parokirohkudus.or.id/images/logo.png">
    <meta name="robots" content="index, follow">
    <link href="https://www.parokirohkudus.or.id/" rel="canonical">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="manifest" href="site.webmanifest">

    <link rel="apple-touch-icon" href="{{ URL::asset('images/favicon.png') }}">
    <!-- Place favicon.ico in the root directory -->

    <title>Paroki Roh Kudus Surabaya</title>

    <!-- Slick -->
    <link href="{{ URL::asset('js/front/slick/slick.css') }}" rel="stylesheet" />

    <!-- Style -->
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>

<body id="body">
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

    <div class="pr-login-box">
        <div class="pr-login-box__wrapper">
            <form class="pr-login-box__form add-fix" method="POST" action="{{ url('/register') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @if(Session::has('message-success'))
                    <div class="" style="text-align:center">
                        {{Session::get('message-success')}}
                    </div>
                    <br>
                @endif

                @if (count($errors)>0)
                    <div style="text-align:center">
                        @foreach($errors->all() as $error)
                            {{$error}}
                        @endforeach
                    </div>
                @endif

                <i class="pr-fa fas fa-users-cog"></i>

                <h1 class="pr-login-box__logo">
                    <a href="{{ url('/') }}" class="pr-login-box__logo-link">
                        <img src="{{ URL::asset('images/logo.png') }}" alt="Paroki Roh Kudus"
                            title="Paroki Roh Kudus">
                        <h2 class="pr-login-box__logo-link-text">Paroki Roh Kudus<br>Register Umat</h2>
                    </a>
                </h1>

                <input type="text" name="name" placeholder="Nama Lengkap" class="pr-input" value="{{ old('name') }}">
                
                <input type="text" name="address" placeholder="Alamat" class="pr-input" value="{{ old('address') }}">

                <input type="number" name="no_telp" placeholder="Nomor HP" class="pr-input" value="{{ old('no_telp') }}">

                <input type="email" name="email" placeholder="Email" class="pr-input" value="{{ old('email') }}" autocomplete="new-password">

                <input type="password" name="password" placeholder="Password" class="pr-input" autocomplete="new-password">

                <input type="password" name="password_confirmation" placeholder="Confirm Password" class="pr-input">

                <input type="submit" value="Register" class="pr-btn">
            </form>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ URL::asset('js/front/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/front/script.js') }}"></script>
</body>

</html>