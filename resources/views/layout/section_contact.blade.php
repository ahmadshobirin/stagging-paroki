<Section class="bz-contact-section">

    <div class="bz-wrapper bz-wrapper--large">

        <!-- bz-contact-section__link -->
       <!--  <a href="contact.html" class="bz-contact-section__link"> -->

            <h4 class="bz-contact-section__link-title">Dengan Biztek, bisnis berkembang lebih cepat dan mudah</h4>
            <p class="bz-contact-section__link-text">Jalankan proses bisnis Anda secara online sekarang juga.<br>Selalu ada solusi untuk kebutuhan Anda.</p>
            <a href="{{url('/contact')}}" class="bz-btn bz-btn--blue-dark bz-contact-section__link-btn">Hubungi Sekarang</a>

        <!-- </a> -->
        <!-- /bz-contact-section__link -->

    </div>

</Section>
<!-- /bz-contact-section -->