<?php
$bidang = DB::table('m_bidang')->get();
$sejarah = DB::table('m_sejarah')->get();
$office = DB::table('m_office')->first();

 ?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">

	<!-- Url Bar Background Mobile -->
	<meta name="theme-color" content="#E22F35">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"  />
	<meta name="author" content="Paroki Roh Kudus Surabaya">
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
	<meta name="description" content="Website Paroki Roh Kudus Surabaya">
	<meta name="og:title" property="og:title" content="Paroki Roh Kudus Surabaya">
	<meta name="og:url" property="og:url" content="https://www.parokirohkudus.or.id/">
	<meta name="og:description" property="og:description" content="Website Paroki Roh Kudus Surabaya">
	<meta name="og:type" property="og:type" content="letter">
	<meta name="og:image" property="og:image" content="https://www.parokirohkudus.or.id/images/logo.png">
	<meta name="robots" content="index, follow">
	<link href="https://www.parokirohkudus.or.id/" rel="canonical">
	<meta name="csrf-token" content="<?php echo csrf_token(); ?>"/>

	<link rel="manifest" href="site.webmanifest">

	<!-- <link rel="apple-touch-icon" href="icon.png"> -->
	<!-- Place favicon.ico in the root directory -->
	<link rel="shortcut icon" href="{{ URL::asset('images/favicon_1.ico') }}">
	<!--<link rel="apple-touch-icon" href="{{ URL::asset('images/favicon_1.ico')}}">-->

	<title>Paroki Roh Kudus Surabaya - @yield("page")</title>

	<!-- Slick -->
	<link href="{{ URL::asset('js/front/slick/slick.css') }}" rel="stylesheet" />

	<!-- Style -->
	<link href="{{ URL::asset('css/style.css')}}" rel="stylesheet">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<!--<script defer src="https://use.fontawesome.com/releases/v5.7.1/js/all.js" integrity="sha384-eVEQC9zshBn0rFj4+TU78eNA19HMNigMviK/PU/FFjLXqa/GKPgX58rvt5Z8PLs7" crossorigin="anonymous"></script>-->
	@yield("customcss")

</head>

<body id="body" class="">
<!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->


<!-- pr-header -->
<header class="pr-header">

	<div class="pr-wrapper pr-wrapper pr-wrapper--large add-fix">

		<!-- pr-header-logo -->
		<h1 class="pr-header-logo">
			<a href="{{ url('/') }}" class="pr-header-logo__link">
				<img src="{{ URL::asset('images/logo.png')}}" alt="Paroki Roh Kudus" title="Paroki Roh Kudus Surabaya"> <h2 class="pr-header-logo__link-text">Paroki Roh Kudus Surabaya</h2>
			</a>
		</h1>
		<!-- /pr-header-logo -->

		<!-- pr-nav -->
		<nav class="pr-nav">


			<span class="pr-nav-bar">
				<i class="pr-fa fas fa-bars"></i>
			</span>


			<div class="pr-nav-overlay"></div>


			<!-- pr-nav-list -->
			<ul class="pr-nav-list">
				<!-- <li class="pr-nav-list__item">
					<a href="{{url('/')}}" class="pr-nav-list__item-link">Home</a>
				</li> -->
        <li class="pr-nav-list__item">
          <a href="#" class="pr-nav-list__item-link">Tentang</a>
          <div class="pr-nav-list__item-dd">
            <a href="{{url('about')}}" class="pr-nav-list__item-dd-link">Visi & Misi</a>
            <a href="{{ url('romo') }}" class="pr-nav-list__item-dd-link">Pastor & Diakon</a>
            <div class="pr-nav-list__item-dd-link">Sejarah Gereja
              <div class="pr-nav-list__item-dd-link--child">
                @foreach($sejarah as $list_sejarah)
                <a href="{{ url('sejarah/'.$list_sejarah->id_sejarah) }}">{{$list_sejarah->tag_sejarah}}</a>
                @endforeach
              </div>
            </div>
          </div>
        </li>
        <li class="pr-nav-list__item">
					<a href="#" class="pr-nav-list__item-link">Agenda</a>
					<div class="pr-nav-list__item-dd">
						<a href="{{ url('agenda/gereja') }}" class="pr-nav-list__item-dd-link">Kegiatan Gereja</a>
						<a href="{{ url('agenda/nikah') }}" class="pr-nav-list__item-dd-link">Pernikahan</a>
					</div>
				</li>
				<li class="pr-nav-list__item">
					<a href="#" class="pr-nav-list__item-link">Pelayanan Sakramen</a>
					<div class="pr-nav-list__item-dd">
						<a href="{{ url('sakramenbaptis')}}" class="pr-nav-list__item-dd-link">Sakramen Baptis</a>
						<a href="{{ url('sakramenkomuni')}}" class="pr-nav-list__item-dd-link">Sakramen Komuni</a>
						<a href="{{ url('sakramenkrisma')}}" class="pr-nav-list__item-dd-link">Sakramen Krisma</a>
						<a href="{{ url('sakramenpernikahan')}}" class="pr-nav-list__item-dd-link">Sakramen Pernikahan</a>
					</div>
				</li>
				<li class="pr-nav-list__item">
					<a href="#" class="pr-nav-list__item-link">DPP</a>
					<div class="pr-nav-list__item-dd">
						@foreach($bidang as $list_bidang)
						<a href="{{url('bidang/'.$list_bidang->id_bidang)}}" class="pr-nav-list__item-dd-link">{{$list_bidang->nama_bidang}} 
({{$countartikel = DB::table('m_posting')
->select('.m_bidang.nama_bidang','count(m_posting.id_posting) as jumlah')
->join('m_bidang','m_bidang.id_bidang','m_posting.id_bidang')
->where('type_post','artikel')
->where('m_posting.id_bidang', $list_bidang->id_bidang)
->where('m_posting.status_post', 'approved')
->count()}})</a>
						@endforeach
					</div>
				</li>
				<li class="pr-nav-list__item">
					<a href="{{ url('kronik') }}" class="pr-nav-list__item-link">Kronik</a>
				</li>
				<li class="pr-nav-list__item">
					<a href="#" class="pr-nav-list__item-link">Link</a>
					<div class="pr-nav-list__item-dd">
						<a href="{{ url('link/lingkungan')}}" class="pr-nav-list__item-dd-link">Lingkungan</a>
						<a href="{{ url('link/wilayah')}}" class="pr-nav-list__item-dd-link">Wilayah</a>
						<a href="{{ url('link/seksi')}}" class="pr-nav-list__item-dd-link">Seksi</a>
					</div>
				</li>
				<li class="pr-nav-list__item">
					<a href="#" class="pr-nav-list__item-link">Download</a>
					<div class="pr-nav-list__item-dd">
						<a href="{{url('form/warta')}}" class="pr-nav-list__item-dd-link">Warta Paroki</a>
						<a href="{{ url('form/majalah') }}" class="pr-nav-list__item-dd-link">Media eRKa</a>
					</div>
				</li>
				<li class="pr-nav-list__item">
					<a href="{{ url('renungan') }}" class="pr-nav-list__item-link">Renungan</a>
				</li>
				<!-- <li class="pr-nav-list__item">
					<a href="{{ url('contact') }}" class="pr-nav-list__item-link">Kontak</a>
				</li> -->
			</ul>
			<!-- /pr-nav-list -->

		</nav>
		<!-- /pr-nav -->

	</div>

</header>
<!-- /pr-header -->

@yield('content')

<!-- pr-btn-floating -->

<div class="pr-btn-floating">


<!-- <a class="pr-btn-floating__search"><i class="pr-fa fas fa-search"></i></a> -->
<!-- <a class="pr-btn-floating__login"><i class="pr-fa fas fa-sign-in-alt"></i></a> -->
<a class="pr-btn-floating__totop"><i class="pr-fa fas fa-angle-up"></i></a>


</div>
<!-- /pr-btn-floating -->

<footer class="pr-footer">

	<!-- pr-footer-list -->
	<div class="pr-footer-list">

		<div class="pr-wrapper pr-wrapper--large add-fix">

			<!-- pr-footer-list__item -->
			<div class="pr-footer-list__item pr-footer-list__item--contact">

				<div class="pr-footer-list__item-wrapper">

					<h4 class="pr-footer-list__item-title">Alamat</h4>

					<article class="pr-footer-list__item-desc">
						<p>{!! $office->alamat_office !!}</p>
						<p>{{ $office->telp_office }}</p>
					</article>

				</div>

			</div>
			<!-- /pr-footer-list__item -->

			<!-- pr-footer-list__item -->
			<div class="pr-footer-list__item pr-footer-list__item--menu">

				<div class="pr-footer-list__item-wrapper">

					<h4 class="pr-footer-list__item-title">Menu</h4>

					<nav class="pr-footer-list__item-nav">
						<a href="{{url('renungan')}}" class="pr-footer-list__item-nav-link">Renungan</a>
						<a href="{{ url('form/warta')}}" class="pr-footer-list__item-nav-link">Warta Paroki</a>
						<a href="{{ url('form/majalah')}}" class="pr-footer-list__item-nav-link">Majalah ERKA</a>
						<a href="{{url('info')}}" class="pr-footer-list__item-nav-link">Info Paroki</a>
						<a href="{{url('contact')}}" class="pr-footer-list__item-nav-link">Kontak</a>
					</nav>

				</div>

			</div>
			<!-- /pr-footer-list__item -->

			<!-- pr-footer-list__item -->
			<div class="pr-footer-list__item pr-footer-list__item--sosmed">

				<div class="pr-footer-list__item-wrapper">

					<h4 class="pr-footer-list__item-title">Sosial Media</h4>
					@if($office->url_fb != null)
					<a href="http://{{$office->url_fb}}" target="_blank" class="pr-footer-list__item--sosmed-link"><i class="pr-fa fab fa-facebook-square"></i></a>
					@endif
					@if($office->url_twitter != null)
					<a href="http://{{$office->url_twitter}}" target="_blank" class="pr-footer-list__item--sosmed-link"><i class="pr-fa fab fa-twitter-square"></i></a>
					@endif
					@if($office->url_instagram != null)
					<a href="http://{{$office->url_instagram}}" target="_blank" class="pr-footer-list__item--sosmed-link"><i class="pr-fa fab fa-instagram"></i></a>
					@endif
					@if($office->url_youtube != null)
					<a href="http://{{$office->url_youtube}}" target="_blank" class="pr-footer-list__item--sosmed-link"><i class="pr-fa fab fa-youtube-square"></i></a>
					@endif
					<a href='https://wa.me/62318792426?text=Selamat%20pagi%2Fsiang%2Fsore%2Fmalam%20Pak.%20Saya%20mau%20tanya...'; target="_blank" class="pr-footer-list__item--sosmed-link"><i class="pr-fa fab fa-whatsapp"></i></a>
				</div>
			</div>
			<!-- /pr-footer-list__item -->

		</div>


	</div>
	<!-- /pr-footer-list -->

	<div class="pr-footer-copyright">

		<div class="pr-wrapper pr-wrapper--large">

			<p>Copyright &copy; 2019 Paroki Roh Kudus Surabaya. All rights reserved.</p>

		</div>

	</div>

</footer>
<!-- /pr-footer -->
<!-- Scripts -->
<script type="text/javascript" src="{{ URL::asset('js/front/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/front/plugin.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/front/slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/front/script.js') }}"></script>
@yield('script')
<script type="text/javascript">
	$('.telp').keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 32 || e.which > 57 || (32 < e.which && e.which < 40) ||  e.which == 42 || ( 43 < e.which && e.which < 48 ) )) {
          return false;
        }
      });
</script>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c5e5ce97cf662208c94c1f6/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
</body>
</html>
