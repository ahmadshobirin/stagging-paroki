<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

	<!-- Url Bar Background Mobile -->
	<meta name="theme-color" content="#192532">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"  />
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
	<meta name="description" content="">
	<meta name="csrf-token" content="<?php echo csrf_token(); ?>"/>

	<link rel="manifest" href="site.webmanifest">

	<link rel="apple-touch-icon" href="{{ URL::asset('images/favicon.ico')}}">
	<!-- Place favicon.ico in the root directory -->

	<title>biztek</title>

	<!-- Slick -->
	<link href="{{ URL::asset('js/front/slick/slick.css') }}" rel="stylesheet" />

	<!-- Style -->
	<link href="{{ URL::asset('css/style.css')}}" rel="stylesheet">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet"> 

	<!-- Font Awesome -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

</head>

<body id="body">
<!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- bz-header -->
<header class="bz-header @yield('dashboard')">

	<div class="bz-wrapper bz-wrapper--large add-fix">
		
		<!-- bz-header__logo -->
		<h1 class="bz-header__logo">
			
			<!-- bz-header__logo-link -->
			<a class="bz-header__logo-link" href="{!! url('/') !!}">
				<img class="bz-header__logo-img bz-header__logo-img--white" src="{{ URL::asset('images/logo-white.png')}}" alt="biztek" title="biztek">
				<img class="bz-header__logo-img bz-header__logo-img--blue" src="{{ URL::asset('images/logo.png')}}" alt="biztek" title="biztek">
			</a>
			<!-- /bz-header__logo-link -->

		</h1>
		<!-- /bz-header__logo -->

		<!-- bz-header__nav -->
    <form id="logout-form" action="{{ url('/logout') }}" method="POST">
      {{ csrf_field() }}
      <nav class="bz-header__nav">
        <a href="{!! url('/') !!}" class="bz-header__nav-link 
            <?php if( Request::url() == $_SERVER['SERVER_NAME'] || Request::url() == 'http://localhost:8000' || Request::url() == 'http://192.168.0.169/biztekweb/public'){ echo "active";} ?>">Home</a>
        <div class="bz-header__nav-link <?php if( Request::url() == $_SERVER['SERVER_NAME']."/produk" || Request::url() == 'http://localhost:8000/produk' || Request::url() == 'http://192.168.0.169/biztekweb/public/produk'){ echo "active";} ?>bz-header__nav-link-dropdown">Produk
          <span class="bz-header__nav-link-dropdown--child">
            <?php 
              $check_kat = \DB::table('m_kategori_produk')->orderBy('id','desc')->get(); 
              
            ?>
            @foreach($check_kat as $kat)
      			<a href="{!! url('/produk-kat/'.$kat->id) !!}">{{$kat->nama}}</a>
            @endforeach
          </span>
        </div>
        <a href="{!! url('/price') !!}" class="bz-header__nav-link <?php if( Request::url() == $_SERVER['SERVER_NAME']."/price" || Request::url() == 'http://localhost:8000/price' || Request::url() == 'http://192.168.0.169/biztekweb/public/price'){ echo "active";} ?>">Harga</a>
        <a href="{!! url('/contact') !!}" class="bz-header__nav-link <?php if( Request::url() == $_SERVER['SERVER_NAME']."/contact" || Request::url() == 'http://localhost:8000/contact' || Request::url() == 'http://192.168.0.169/biztekweb/public/contact'){ echo "active";} ?>">Kontak Kami</a>
  			<a href="{!! url('/produk/1') !!}" class="bz-header__nav-link">Coba Gratis</a>
        @if(Auth::check())
        <div class="bz-header__nav-link bz-header__nav-link-login">Hai, {{Auth::user()->name}}!
          <span class="bz-header__nav-link-login-dropdown">
            <a href="{!! url('/profile/'.Auth::user()->id) !!}"><i class="bz-fa fas fa-user"></i>Dashboard Profile</a>
            <a id="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="bz-fa fas fa-sign-out-alt"></i> Keluar ? </a>
          </span>
        </div>
        @else
  			<a href="{!! url('/login') !!}" class="bz-header__nav-link">Masuk</a>
        @endif
  		</nav>
      <!-- bz-header__nav-bars -->
      <a href="#" title="" class="bz-header__nav-bars">
        <i class="bz-fa fas fa-bars" title="Menu" alt="Title"></i>
      </a>
      <!-- /bz-header__nav-bars -->

      <div class="bz-header__nav-overlay"></div>
    </form>
  		<!-- bz-header__nav -->

	</div>

</header>

@yield('content')

<!-- bz-totop -->
 <a href="#" class="bz-totop"><i class="bz-fa fas fa-angle-up"></i></a>
 
<!-- bz-footer -->
<footer class="bz-footer">

        <div class="bz-wrapper bz-wrapper--large">
            
            <!-- bz-footer__widget -->
            <div class="bz-footer__widget add-fix">

                <!-- bz-footer__widget-item -->
                 <div class="bz-footer__widget-item bz-footer__widget-item--product">

                   <h3 class="bz-footer__widget-item-title">Perusahaan</h3>

                   <div class="bz-footer__widget-item-wrapper">

                        <a href="{!! url('/about') !!}" class="bz-footer__widget-item-link">Tentang Kami</a>
                        <a href="{!! url('/contact') !!}" class="bz-footer__widget-item-link">Kontak Kami</a>

                   </div>

                </div>
                <!-- /bz-footer__widget-item -->

                <!-- bz-footer__widget-item -->
                <div class="bz-footer__widget-item bz-footer__widget-item--product">

                   <h3 class="bz-footer__widget-item-title">Bantuan</h3>

                   <div class="bz-footer__widget-item-wrapper">
                        @if(Auth::check())
                        <a href="{!! url('/tutorial') !!}" class="bz-footer__widget-item-link">Tutorial</a>
                        @endif
                        <a href="{!! url('/faq') !!}" class="bz-footer__widget-item-link">FAQ</a>

                   </div>

                </div>
                <!-- /bz-footer__widget-item -->

                <!-- bz-footer__widget-item -->
                <div class="bz-footer__widget-item bz-footer__widget-item--product">

                   <h3 class="bz-footer__widget-item-title">Produk</h3>

                   <div class="bz-footer__widget-item-wrapper">
                      <?php
                        $kategori_front = DB::table('m_kategori_produk')->get();

                      ?>
                      @foreach($kategori_front as $kat_front)
                        <a href="{!! url('/produk-kat/'.$kat_front->id) !!}" class="bz-footer__widget-item-link">{{$kat_front->nama}}</a>
                      @endforeach
                   </div>

                </div>
                <!-- /bz-footer__widget-item -->

                 <!-- bz-footer__widget-item -->
                <div class="bz-footer__widget-item bz-footer__widget-item--payment">

                   <h3 class="bz-footer__widget-item-title">Pembayaran</h3>

                   <div class="bz-footer__widget-item-wrapper">

                        <a href="#" class="bz-footer__widget-item-link">
                            <img src="{{ URL::asset('images/BNI.png')}}" alt="BNI" title="BNI">
                        </a>
                        <a href="#" class="bz-footer__widget-item-link">
                            <img src="{{ URL::asset('images/MANDIRI.png')}}" alt="Bank Mandiri" title="Bank Mandiri">
                        </a>
                        <a href="#" class="bz-footer__widget-item-link">
                            <img src="{{ URL::asset('images/BRI.png')}}" alt="BRI" title="BRI">
                        </a>
                        <a href="#" class="bz-footer__widget-item-link">
                            <img src="{{ URL::asset('images/PERMATA.png')}}" alt="Bank Permata" title="Bank Permata">
                        </a>
                        <a href="#" class="bz-footer__widget-item-link">
                            <img src="{{ URL::asset('images/BCA.png')}}" alt="BCA" title="BCA">
                        </a>
                        <a href="#" class="bz-footer__widget-item-link">
                            <img src="{{ URL::asset('images/BERSAMA.png')}}" alt="ATM Bersama" title="ATM Bersama">
                        </a>


                   </div>

                </div>
                <!-- /bz-footer__widget-item -->

                 <!-- bz-footer__widget-item -->
                <div class="bz-footer__widget-item bz-footer__widget-item--sosmed">

                   <h3 class="bz-footer__widget-item-title">Sosial Media</h3>

                   <div class="bz-footer__widget-item-wrapper">

                       <a href="#" class="bz-footer__widget-item-link"><i class="fab fa-facebook-f bz-fa"></i></a>
                       <a href="#" class="bz-footer__widget-item-link"><i class="fab fa-twitter bz-fa"></i></a>
                       <a href="#" class="bz-footer__widget-item-link"><i class="fab fa-google-plus bz-fa"></i></a>

                   </div>

                </div>
                <!-- /bz-footer__widget-item -->

            </div>
            <!-- /bz-footer__widget -->
        </div>
            <!-- bz-footer__copyright -->
            <div class="bz-footer__copyright">&copy; Copyright 2018 biztek. All rights reserved.</div>
            <!-- /bz-footer__copyright -->

</footer>
<!-- /bz-footer -->


<!-- Scripts -->
<script type="text/javascript" src="{{ URL::asset('js/front/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/front/plugin.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/front/slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/front/script.js') }}"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5b641319df040c3e9e0c409a/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
@yield('script')
</body>
</html>