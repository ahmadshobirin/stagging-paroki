@extends('app')

@section('treeview_post','active')

@section('title', 'Pencatatan '.$type)

@section('customcss')
<link rel="stylesheet" href="{{ URL::asset('css/datatables.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet"
    href="{{ URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
<link href="{{ asset('plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
@stop

    @section('contentheader_title', 'Pencatatan'.$type)

    @section('main-content')
    <div class="row">
        <div class="col-md-8">
            <!-- form start -->
            <form method="post" enctype="multipart/form-data" action="{{ $action }}">
                @csrf
                @isset($editmode)  @method('PUT') @else @method('POST') @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pencatatan {{ucfirst($type)}}</h3>
                    </div>

                    <div class="box-body">
                        {{-- form-sakramen-orang-sakit --}}
                        @if($type == 'sakit')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Nama Lengkap <span class="required">*</span></label>
                                    <input type="text" class="form-control input-sm" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap..." required>
                                </div>

                                <div class="form-group">
                                    <label for="">Tanggal <span class="required">*</span></label>
                                    <input type="text" name="date" class="form-control datepicker" value="" id="date" required>
                                </div>

                                <div class="form-group">
                                    <label for="">Tempat/Alamat <span class="required">*</span></label>
                                    <textarea name="tempat_alamat" id="tempat_alamat" class="form-control" required></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Usia <span class="required">*</span></label>
                                        <input type="number" class="form-control input-sm" name="usia" id="usia" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Romo <span class="required">*</span></label>
                                        <input type="text" name="romo" class="form-control input-sm" value="" id="romo" placeholder="Romo..." required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Keterangan</label>
                                        <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                                    </div>
                            </div>
                        </div>
                        @elseif($type == 'kunjungan')
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nama Kegiatan <span class="required">*</span></label>
                                        <input type="text" class="form-control input-sm" name="nama_kegiatan" id="nama_kegiatan" placeholder="Nama Kegiatan..." required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Tanggal <span class="required">*</span></label>
                                        <input type="text" name="date" class="form-control datepicker" value="" id="date" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Tempat/Alamat <span class="required">*</span></label>
                                        <textarea name="tempat_alamat" id="tempat_alamat" class="form-control" required></textarea>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Pengunjung <span class="required">*</span></label>
                                            <input type="text" name="pengunjung" class="form-control input-sm" value="" id="pengunjung" placeholder="Romo..." required>
                                        </div>

                                        <div class="form-group">
                                            <label for="">keterangan</label>
                                            <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                                        </div>
                                </div>
                            </div>
                        @endif

                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button class="btn btn-success btn-sm">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @endsection

        @section('customscript')
        <script type="text/javascript"
            src="{{ URL::asset('/plugins/select2/select2.full.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('/plugins/mask/mask.js') }}">
        </script>
        <script type="text/javascript"
            src="{{ URL::asset('/js/bootstrap-datepicker.min.js') }}">
        </script>
        <script type="text/javascript"
            src="{{ URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
        <script type="text/javascript">
            var date = new Date();
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            $(".select2").select2();
            $('.summernote').summernote({
                height: 150,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'strikethrough']],
                    ['fontsize', ['fontsize', 'height', 'hr']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                ],
            });

            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                orientation: 'right',
            });

            $(".datepicker").datepicker('setDate', today);

            @isset($editmode)
                var data = {!! $record !!};

                console.log(data);
                Object.keys(data).forEach(function(key) {
                    elem = $("#"+key);
                    if(elem.is('input')){

                        if(elem.hasClass('datepicker')){
                            parsedate = new Date(data[key]);
                            tdy = new Date(parsedate.getFullYear(), parsedate.getMonth(), parsedate.getDate());
                            $(".datepicker").datepicker('setDate', tdy);
                        }else{
                            elem.val(data[key])
                        }
                    }else if(elem.is('select')){
                        elem.val(data[key]).trigger('change');
                    }else if(elem.is('textarea')){
                        elem.text(data[key])
                    }
                })
            @endif
        </script>
        @endsection