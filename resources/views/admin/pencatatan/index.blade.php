@extends('app')

@section('treeview_post','active')
@section('treeview_catat_sakit','active')

@section('title', 'Pencatatan Sakramen Orang Sakit')
@section('contentheader_title', 'Pencatatan Sakramen Orang Sakit')

@section('customcss')
<link rel="stylesheet" href="{{ URL::asset('css/datatables.min.css') }}">
<style>
    table.dataTable td {
    word-break: break-word;
    }
</style>
@stop

    @section('main-content')
    <div class="box box-primary">
        <div class="box-body">
            <div class="">
                <a href="{{ route('pencatatan.create',$type) }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
            </div>
            <br>
            <table class="table table-striped table-hover table-responsive" id="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Lengkap</th>
                        <th>Tanggal</th>
                        <th>Tempat/Alamat</th>
                        <th>Usia</th>
                        <th>Romo</th>
                        <th>Keterangan</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    @endsection

    @section('customscript')
    <script type="text/javascript"
        src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'responsive': true,
                "language": {
                    "emptyTable": "Data Kosong",
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "search": "Cari:",
                    "lengthMenu": "Tampilkan _MENU_ Data",
                    "zeroRecords": "Tidak Ada Data yang Ditampilkan",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sLast": "Akhir",
                        "sNext": "Selanjutnya",
                        "sPrevious": "Sebelumnya"
                    },
                },

                processing: true,
                serverSide: true,
                ajax: '<?= url("/api/pencatatan/sakit") ?>/'+'{{ Auth::user()->id }}',
                columns: [
                    { data: 'DT_Row_Index', name: 'DT_Row_Index',searchable: false, orderable: false },
                    { data: 'nama_lengkap', name: 'nama_lengkap' },
                    { data: 'date', name: 'date' },
                    { data: 'tempat_alamat', name: 'tempat_alamat' },
                    { data: 'usia', name: 'usia' },
                    { data: 'romo', name: 'romo' },
                    { data: 'keterangan', name: 'keterangan' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });

            $('body').on('click', '.btn-danger', function(e){
                e.preventDefault();
                var url = $(this).attr('url');
                // console.log(url);

                $.ajax({
                    'url' : url,
                    'type' : 'POST',
                    'data' : {
                        '_token' : "{{ csrf_token() }}",
                        '_method' : 'DELETE'
                    },
                    success : function(results){
                        // console.log(results);
                        location.reload();
                    },
                    error : function(xhr){
                        alert(xhr.responseJSON.data.description);
                    }
                });
            });
        });
    </script>
    @stop