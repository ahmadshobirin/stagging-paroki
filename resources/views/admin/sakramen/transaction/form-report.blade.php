@extends('app')

@section('treeview_posting','active')
@section('treeview_sakramen_transaction','active')

@section('title', 'Transaction')
@section('contentheader_title', 'Transaction')

@section('customscript')
<script src="{{ URL::asset('plugins/moment/min/moment.min.js') }}">
</script>
<script src="{{ URL::asset('plugins/daterangepicker/daterangepicker.js') }}">
</script>
<script type="text/javascript">
    $('#date-range').daterangepicker({
        locale: {
            format: 'DD-MM-YYYY'
        },
    });
</script>
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-1">
        <form method="post" action="{{ url('sakramen-report') }}">
            <div class="box box-info">
                <div class="box-header with-text">
                    <h3 class="box-title">Report</h3>
                </div>
                @csrf
                <div class="box-body" style="margin-left: 30px;">
                    <div class="form-group">
                        <label for="">Periode</label>
                        <input type="text" name="periode" id="date-range" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <label for="">Tipe</label>
                        <select name="type" id="type" class="form-control input-sm" style="width:100%">
                            {{-- <option value="null" selected>Pilih Tipe</option> --}}
                            <option value="baptis" selected>Baptis</option>
                            <option value="pernikahan">Nikah</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" id="status" class="form-control input-sm" style="width:100%">
                            <option value="null" selected>Pilih Status</option>
                            <option value="paid">Lunas</option>
                            <option value="debt">Hutang</option>
                        </select>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <input type="submit" value="Export to Excel" class="btn btn-success btn-sm">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
@endsection