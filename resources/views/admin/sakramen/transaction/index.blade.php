@extends('app')

@section('treeview_posting','active')
@section('treeview_sakramen_transaction','active')

@section('title', 'Transaction')
@section('contentheader_title', 'Transaction')

@section('customcss')
<link rel="stylesheet" href="{{ URL::asset('css/datatables.min.css') }}">
<style>
    table.dataTable td {
        word-break: break-word;
    }
</style>
@stop

    @section('main-content')
    <div class="box box-primary">
        <div class="box-body">
            <div class="box-header">
                <h3 class="box-title">Transaksi</h3>
                <div class="box-tools pull-right">
                    <a href="{{ url('sakramen-report') }}" class="btn btn-sm btn-success"> <span class="fa fa-file-excel-o"></span> Report</a>
                </div>
            </div>
            <table class="table table-striped table-hover table-responsive" id="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Tanggal</th>
                        <th>Sakramen Name</th>
                        <th>Nama Pendaftar</th>
                        <th>Sakramen Tipe</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    @endsection

    @section('customscript')
    <script type="text/javascript"
        src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                'responsive': true,
                "language": {
                    "emptyTable": "Data Kosong",
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "search": "Cari:",
                    "lengthMenu": "Tampilkan _MENU_ Data",
                    "zeroRecords": "Tidak Ada Data yang Ditampilkan",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sLast": "Akhir",
                        "sNext": "Selanjutnya",
                        "sPrevious": "Sebelumnya"
                    },
                },

                processing: true,
                serverSide: true,
                ajax: '<?= url("/api/sakramen/transaction") ?>',
                columns: [{
                        data: 'DT_Row_Index',
                        name: 'DT_Row_Index',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'date',
                        name: 'date'
                    },
                    {
                        data: 'sakramen_name',
                        name: 'sakramen_name'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'sakramen_type',
                        name: 'sakramen_type'
                    },
                    {
                        data: 'price',
                        name: 'price',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }
                ]
            });

            // $('body').on('click', '.btn-danger', function(e){
            //     e.preventDefault();
            //     var url = $(this).attr('url');
            //     // console.log(url);

            //     $.ajax({
            //         'url' : url,
            //         'type' : 'POST',
            //         'data' : {
            //             '_token' : "{{ csrf_token() }}",
            //             '_method' : 'DELETE'
            //         },
            //         success : function(results){
            //             // console.log(results);
            //             location.reload();
            //         },
            //         error : function(xhr){
            //             alert(xhr.responseJSON.data.description);
            //         }
            //     });
            // });
        });
    </script>
    @stop