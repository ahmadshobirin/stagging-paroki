<p>
    Biaya Admnistrasi Sekretariat Gereja
</p>
<p> {{$options['start']}} -  {{$options['end']}}</p>
<table>
    <thead>
        <tr>
            <th style="text-align:center">#</th>
            <th style="text-align:center">Keterangan</th>
            <th colspan="2" style="text-align:center">Tempat dan Tanggal</th>
            <th style="text-align:center">Jumlah</th>
        </tr>
    </thead>
    <tbody>
        @php
            $grandTotal = 0;
        @endphp
        @foreach ($collection as $item)
            <tr>
                <td> {{ $loop->index + 1 }} </td>
                <td> {{ ucwords($item->sakramen_name) }}</td>
                <td> {{ $item->place }}</td>
                <td> {{ \Carbon\Carbon::parse($item->date_implementation)->format('d-m-Y') }}</td>
                <td style="text-align:right">{{ "Rp. ".number_format($item->allprice,0,'.','.') }}</td>
            </tr>
            @php  $grandTotal += $item->allprice;  @endphp
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4" style="text-align:right">Total</td>
            <td style="text-align:right">{{ "Rp. ".number_format($grandTotal,0,'.','.') }}</td>
        </tr>
    </tfoot>
</table>