@extends('app')

@if(Auth::user()->role == 1 || Auth::user()->role == 2)
@section('treeview_posting','active')
@else
@section('treeview_post','active')
@endif
@section('treeview_ttd_sakramen','active')
@section('treeview_ttd_sakramen_pernikahan','active')

@section('title', 'Update Status Sakramen Nikah')

@section('contentheader_title', 'Update Status Sakramen Nikah')

@section('main-content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
			{{-- 	@if($options['permission'] == true) --}}
					<div class="form-group">
						<label for="">User</label>
						<input type="text" class="form-control input-sm form-control" readonly value="{{ Auth::user()->name }}">
					</div>
					<div class="form-group">
						<label for="">Keterangan</label>
						<textarea name="note" class="form-control input-sm"></textarea>
					</div>
					<div class="form-group">
						<label for="">Tanda Tangan</label>
						<div id="container" style="border:1px #d2d6de solid"></div>
					</div>
				{{-- @else
					<h3>{{ $options['message'] }}</h3>
				@endif --}}
			</div>
			<div class="box-body">
				<div class="form-group">
						<label for="">Tanda Tangan</label>
						<div id="container2" style="border:1px #d2d6de solid"></div>
					</div>
			</div>

			{{-- @if($options['permission'] == true) --}}
			<div class="box-footer">
				<div class="pull-right">
					<button class="btn btn-success btn-sm" id="save"><span class="fa fa-save"></span> Simpan</button>
				</div>
			</div>
			{{-- @endif --}}
		</div>
	</div>
</div>
@endsection

@section('customscript')
<script src="https://unpkg.com/konva@3.4.1/konva.min.js"></script>
<script>
	var width = window.innerWidth;
	var height = window.innerHeight - 150;

	// first we need Konva core things: stage and layer
	var stage = new Konva.Stage({
		container: 'container',
		width: width,
		height: height
	});

	var layer = new Konva.Layer();
	stage.add(layer);

	// then we are going to draw into special canvas element
	var canvas = document.createElement('canvas');
	canvas.width = stage.width();
	canvas.height = stage.height();

	// created canvas we can add to layer as "Konva.Image" element
	var image = new Konva.Image({
		image: canvas,
		x: 0,
		y: 0
	});
	layer.add(image);
	stage.draw();

	// Good. Now we need to get access to context element
	var context = canvas.getContext('2d');
	context.strokeStyle = '#000000';
	context.lineJoin = 'round';
	context.lineWidth = 5;

	var isPaint = false;
	var lastPointerPosition;
	var mode = 'brush';

	// now we need to bind some events
	// we need to start drawing on mousedown
	// and stop drawing on mouseup
	image.on('mousedown touchstart', function () {
		isPaint = true;
		lastPointerPosition = stage.getPointerPosition();
	});

	// will it be better to listen move/end events on the window?

	stage.on('mouseup touchend', function () {
		isPaint = false;
	});

	// and core function - drawing
	stage.on('mousemove touchmove', function () {
		if (!isPaint) {
			return;
		}

		if (mode === 'brush') {
			context.globalCompositeOperation = 'source-over';
		}
		if (mode === 'eraser') {
			context.globalCompositeOperation = 'destination-out';
		}
		context.beginPath();

		var localPos = {
			x: lastPointerPosition.x - image.x(),
			y: lastPointerPosition.y - image.y()
		};
		context.moveTo(localPos.x, localPos.y);
		var pos = stage.getPointerPosition();
		localPos = {
			x: pos.x - image.x(),
			y: pos.y - image.y()
		};
		context.lineTo(localPos.x, localPos.y);
		context.closePath();
		context.stroke();

		lastPointerPosition = pos;
		layer.batchDraw();
	});



	var width2 = window.innerWidth;
	var height2 = window.innerHeight - 150;

	// first we need Konva core things: stage and layer
	var stage2 = new Konva.Stage({
		container: 'container2',
		width: width2,
		height: height2
	});

	var layer2 = new Konva.Layer();
	stage2.add(layer2);

	// then we are going to draw into special canvas element
	var canvas2 = document.createElement('canvas');
	canvas2.width = stage2.width();
	canvas2.height = stage2.height();

	// created canvas we can add to layer as "Konva.Image" element
	var image2 = new Konva.Image({
		image: canvas2,
		x: 0,
		y: 0
	});
	layer2.add(image2);
	stage2.draw();

	// Good. Now we need to get access to context element
	var context2 = canvas2.getContext('2d');
	context2.strokeStyle = '#000000';
	context2.lineJoin = 'round';
	context2.lineWidth = 5;

	var isPaint2 = false;
	var lastPointerPosition2;
	var mode2 = 'brush';

	// now we need to bind some events
	// we need to start drawing on mousedown
	// and stop drawing on mouseup
	image2.on('mousedown touchstart', function () {
		isPaint2 = true;
		lastPointerPosition2 = stage2.getPointerPosition();
	});

	// will it be better to listen move/end events on the window?

	stage2.on('mouseup touchend', function () {
		isPaint2 = false;
	});

	// and core function - drawing
	stage2.on('mousemove touchmove', function () {
		if (!isPaint2) {
			return;
		}

		if (mode2 === 'brush') {
			context.globalCompositeOperation = 'source-over';
		}
		if (mode2 === 'eraser') {
			context.globalCompositeOperation = 'destination-out';
		}
		context2.beginPath();

		var localPos2 = {
			x: lastPointerPosition2.x - image2.x(),
			y: lastPointerPosition2.y - image2.y()
		};
		context2.moveTo(localPos2.x, localPos2.y);
		var pos2 = stage2.getPointerPosition();
		localPos2 = {
			x: pos2.x - image2.x(),
			y: pos2.y - image2.y()
		};
		context2.lineTo(localPos2.x, localPos2.y);
		context2.closePath();
		context2.stroke();

		lastPointerPosition2 = pos2;
		layer2.batchDraw();
	});

	$('#save').click(function () {
		$.ajax({
			url: "{{ url('admin/sakramen-signature') }}",
			type: "POST",
			data: {
				note: $('#note').val(),
				signature: stage.toDataURL(),
				_token: "{{ csrf_token() }}"
			},
			success: function (results) {
				if (results.success == true) {
					window.location = results.url
				}
			}
		})
	});
</script>
@endsection