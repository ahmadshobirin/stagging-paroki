@extends('app')

@if(Auth::user()->role != 4)
    @section('treeview_posting','active')
@else
    @section('treeview_post','active')
@endif

@section('treeview_sakramen','active')
@section('treeview_sakramen_pernikahan','active')

@section('title', 'Tambah Sakramen Pernikahan')
@section('contentheader_title', 'Tambah Sakramen Pernikahan')

@section('customcss')
<link rel="stylesheet" href="{{ URL::asset('css/datatables.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet"
    href="{{ URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('plugins/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('plugins/steps/jquery.steps.css') }}">
<link href="{{ asset('plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet" />
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <form id="form" action="{{ url('sakramen-nikah/wizard/'.$step) }}" enctype="multipart/form-data" method="POST">
            <div class="box box-primary">
                <div class="box-body">
                    @csrf

                    @if(Session::has('message'))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-warning alert-dismissible" style="padding:8px; margin:10px">
                                    <h5><i class="icon fa fa-warning"></i>Tanggal Agenda sudah dipakai</h5>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($step == 1)
                        <div class="box-header with-border" style="margin-bottom:4px;">
                            <h3 class="box-title">Data Agenda</h3>
                        </div>

                        <fieldset>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="" class="control-label">Nama Agenda <span
                                            class="required">*</span></label>
                                    <input type="text" class="form-control" name="nama_agenda" id="nama_agenda"
                                        placeholder="Judul" value="{{{ $pernikahan->nama_agenda or 'Pernikahan' }}}" required>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Menikah di Luar Paroki?</label>
                                    <input type="checkbox" name="tempat_menikah" class="ck" id="tempat_menikah"
                                    {{{ (isset($pernikahan->tempat_menikah) && $pernikahan->tempat_menikah == 2) ? "checked" : "" }}}> Ya
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Gereja Tempat Menikah <span
                                            class="required">*</span></label>
                                    <input type="text" class="form-control" required readonly
                                        name="alternative_tempat_menikah" id="alternative_tempat_menikah"
                                        placeholder="Geraja Tempat Menikah"
                                        value="{{{ ( isset($pernikahan->alternative_tempat_menikah) && $pernikahan->alternative_tempat_menikah !=  "") ? $pernikahan->alternative_tempat_menikah : 'Gereja Paroki'}}}">
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tanggal Agenda <span class="required">*</span></label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="date_agenda" class="form-control input-sm agenda"
                                                name="date_agenda" value=""
                                            required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Tanggal Merah? <span class="required">*</span></label>
                                    <input type="checkbox" name="hari_libur" class="ck" id="hari_libur" value="1" {{{ ( isset($pernikahan->hari_libur) && $pernikahan->hari_libur ==  1) ? $pernikahan->hari_libur : 'Gereja Paroki'}}}> Ya
                                </div>

                                {{-- <div class="col-md-6"> --}}
                                    <div class="form-group">
                                        <label class="control-label">Waktu Mulai<span class="required">*</span></label>
                                        <select name="waktu_agenda" class="form-control input-sm">
                                            <option {{{ (isset($pernikahan->waktu_agenda) && date('H:i',strtotime($pernikahan->waktu_agenda)) == '10:00') ? "selected=\"selected\"" : "" }}} value="10:00">10:00 - 11:30</option>
                                            <option {{{ (isset($pernikahan->waktu_agenda) && date('H:i',strtotime($pernikahan->waktu_agenda)) == '12:00') ? "selected=\"selected\"" : "" }}} value="12:00">12:00 - 13:30 </option>
                                            <option {{{ (isset($pernikahan->waktu_agenda) && date('H:i',strtotime($pernikahan->waktu_agenda)) == '14:00') ? "selected=\"selected\"" : "" }}} value="14:00">14:00 - 15:30</option>
                                        </select>
                                    </div>
                                {{-- </div> --}}

                                {{-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Waktu Selesai <span
                                                class="required">*</span></label>
                                        <select name="end_agenda" id="" class="form-control input-sm">
                                            <option {{{ (isset($pernikahan->end_agenda) && date('H:i',strtotime($pernikahan->end_agenda)) == '10:00') ? "selected=\"selected\"" : "" }}} value="11:30" selected>11:30</option>
                                            <option {{{ (isset($pernikahan->end_agenda) && date('H:i',strtotime($pernikahan->end_agenda)) == '10:00') ? "selected=\"selected\"" : "" }}} value="13:30">13:30</option>
                                            <option {{{ (isset($pernikahan->end_agenda) && date('H:i',strtotime($pernikahan->end_agenda)) == '10:00') ? "selected=\"selected\"" : "" }}} value="15:30">15:30</option>
                                        </select>
                                    </div>
                                </div> --}}
                            </div>
                        </fieldset>
                    @elseif($step == 2)
                        <div class="box-header with-border" style="margin-bottom:4px;">
                            <h3 class="box-title">Data Utama</h3>
                        </div>

                        <fieldset>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Nama Pengantin Pria <span
                                            class="required">*</span></label>
                                    <input type="text" class="form-control" name="nama_pengantin_pria"
                                        id="nama_pengantin_pria" placeholder="Nama Pengantin Pria"
                                        value="" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Tempat Lahir Pengantin Pria <span
                                            class="required">*</span></label>
                                    <input type="text" class="form-control" name="tempat_lahir_pengantin_pria"
                                        id="tempat_lahir_pengantin_pria" placeholder="Tempat Lahir Pengantin Pria"
                                        value="" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Tanggal Lahir Pengantin Pria <span
                                            class="required">* </span></label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control input-sm datepicker"
                                            name="tanggal_lahir_pengantin_pria" id="tanggal_lahir_pengantin_pria"
                                            value=""
                                            required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Pekerjaan Pengantin Pria <span
                                            class="required">*</span></label>
                                    <input type="text" class="form-control" name="pekerjaan_pengantin_pria"
                                        id="pekerjaan_pengantin_pria" placeholder="Pekerjaan Pengantin Pria"
                                        value="{{ old('pekerjaan_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Lingkungan Pengantin Pria</label>
                                    <select class="form-control select2" name="id_kategori_user_pengantin_pria"
                                        id="id_kategori_user_pengantin_pria" style="width:100%">
                                        <option value="0">Pilih Salah Satu...</option>
                                        @foreach($lingkungan as $ling)
                                            <option value="{{ $ling->id_kategori_user }}">
                                                {{ $ling->nama_kategori_user }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Nama Pengantin Wanita <span
                                            class="required">*</span></label>
                                    <input type="text" class="form-control" name="nama_pengantin_wanita"
                                        id="nama_pengantin_wanita" placeholder="Nama Pengantin Wanita"
                                        value="{{ old('nama_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Tempat Lahir Pengantin Wanita <span
                                            class="required"> *</span></label>
                                    <input type="text" class="form-control" name="tempat_lahir_pengantin_wanita"
                                        id="tempat_lahir_pengantin_wanita" placeholder="Tempat Lahir Pengantin Wanita"
                                        value="{{ old('tempat_lahir_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Tanggal Lahir Pengantin Wanita <span
                                            class="required">*</span></label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control input-sm datepicker"
                                            name="tanggal_lahir_pengantin_wanita" id="tanggal_lahir_pengantin_wanita"
                                            value="{{ old('tanggal_lahir_pengantin_wanita') }}"
                                            required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Pekerjaan Pengantin Wanita <span
                                            class="required">*</span></label>
                                    <input type="text" class="form-control" name="pekerjaan_pengantin_wanita"
                                        id="pekerjaan_pengantin_wanita" placeholder="Pekerjaan Pengantin Wanita"
                                        value="{{ old('pekerjaan_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Lingkungan Pengantin Wanita</label>
                                    <select class="form-control select2" name="id_kategori_user_pengantin_wanita"
                                        id="id_kategori_user_pengantin_wanita" style="width:100%">
                                        <option value="0">Pilih Salah Satu...</option>
                                        @foreach($lingkungan as $ling)
                                            <option value="{{ $ling->id_kategori_user }}">
                                                {{ $ling->nama_kategori_user }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    @elseif($step == 3)
                        <div class="box-header with-border" style="margin-bottom:4px;">
                            <h3 class="box-title">Data Alamat</h3>
                        </div>

                        <fieldset>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Pengantin Pria  <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="alamat_pengantin_pria" id="alamat_pengantin_pria" placeholder="Alamat Pengantin Pria" value="{{ old('alamat_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Telp. Pengantin Pria  <span class="required">*</span></label>
                                    <input type="text" class="form-control telp" name="telp_pengantin_pria" id="telp_pengantin_pria" placeholder="Telp. Pengantin Pria" value="{{ old('telp_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Sebelumnya Pengantin Pria</label>
                                    <input type="text" class="form-control" name="alamat_sebelumnya_pengantin_pria" id="alamat_sebelumnya_pengantin_pria" placeholder="Alamat Sebelumnya Pengantin Pria" value="{{ old('alamat_sebelumnya_pengantin_pria') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Gereja Pengantin Pria</label>
                                    <input type="text" class="form-control" name="gereja_pengantin_pria" id="gereja_pengantin_pria" placeholder="Gereja Pengantin Pria" value="{{ old('gereja_pengantin_pria') }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Pengantin Wanita  <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="alamat_pengantin_wanita" id="alamat_pengantin_wanita" placeholder="Alamat Pengantin Wanita" value="{{ old('alamat_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Telp. Pengantin Wanita  <span class="required">*</span></label>
                                    <input type="text" class="form-control telp" name="telp_pengantin_wanita" id="telp_pengantin_wanita" placeholder="Telp. Pengantin Wanita" value="{{ old('telp_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Sebelumnya Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="alamat_sebelumnya_pengantin_wanita" id="alamat_sebelumnya_pengantin_wanita" placeholder="Alamat Sebelumnya Pengantin Wanita" value="{{ old('alamat_sebelumnya_pengantin_wanita') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Gereja Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="gereja_pengantin_wanita" id="gereja_pengantin_wanita" placeholder="Gereja Pengantin Wanita" value="{{ old('gereja_pengantin_wanita') }}">
                                </div>
                            </div>
                        </fieldset>
                    @elseif($step == 4)
                        <div class="box-header with-border" style="margin-bottom:4px;">
                            <h3 class="box-title">Data Sakramen Mempelai</h3>
                        </div>

                        <fieldset>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Agama Pengantin Pria <span class="required">*</span> </label>
                                    <select class="form-control isselect" name="agama_pengantin_pria" id="agama_pengantin_pria" required>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>

                                <div class="form-group" id="div_image_2">
                                    <label class="control-label">Surat Baptis Pengantin Pria</label>
                                    <input class="form-control" type="file" name="image2" id="image2" onchange="return ValidateFileUpload('image2','label_image2')">
                                </div>
                                <label id="label_image2">Bukan file image</label>

                                <div class="form-group">
                                    <label for="" class="control-label">Gereja Pengantin Pria</label>
                                    <input type="text" class="form-control" name="gereja_pengantin_pria" id="gereja_pengantin_pria" placeholder="Gereja Pengantin Pria" value="{{ old('gereja_pengantin_pria') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Tempat dan Tanggal Baptis Pengantin Pria</label>
                                    <input type="text" class="form-control" name="tempat_tanggal_baptis_pengantin_pria" id="tempat_tanggal_baptis_pengantin_pria" placeholder="Tempat dan Tanggal Baptis Pengantin Pria" value="{{ old('tempat_tanggal_baptis_pengantin_pria') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Paroki dan No. Buku Baptis Pengantin Pria</label>
                                    <input type="text" class="form-control" name="paroki_no_buku_pengantin_pria" id="paroki_no_buku_pengantin_pria" placeholder="Paroki dan No. Buku Baptis Pengantin Pria" value="{{ old('paroki_no_buku_pengantin_pria') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Tanggal Surat Baptis Terakhir Pengantin Pria</label>
                                    <input type="text" class="form-control" name="paroki_tanggal_surat_baptis_terakhir_pengantin_pria" id="paroki_tanggal_surat_baptis_terakhir_pengantin_pria" placeholder="Tanggal Surat Baptis Terakhir Pengantin Pria" value="{{ old('paroki_tanggal_surat_baptis_pengantin_pria') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Paroki dan Tanggal Penguatan Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="paroki_tanggal_penguatan_pengantin_wanita" id="paroki_tanggal_penguatan_pengantin_wanita" placeholder="Paroki dan Tanggal Penguatan Pengantin Wanita" value="{{ old('paroki_tanggal_penguatan_pengantin_wanita') }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Agama Pengantin Wanita <span class="required">*</span></label>
                                    <select class="form-control isselect" name="agama_pengantin_wanita" id="agama_pengantin_wanita" required>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>

                                <div class="form-group" id="div_image_3">
                                    <label class="control-label">Surat Baptis Pengantin Wanita</label>
                                    <input class="form-control" type="file" name="image3" id="image3" onchange="return ValidateFileUpload('image3','label_image3')">
                                </div>
                                <label id="label_image3">Bukan file image</label>

                                <div class="form-group">
                                    <label for="" class="control-label">Gereja Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="gereja_pengantin_wanita" id="gereja_pengantin_wanita" placeholder="Gereja Pengantin Wanita" value="{{ old('gereja_pengantin_wanita') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Tempat dan Tanggal Baptis Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="tempat_tanggal_baptis_pengantin_wanita" id="tempat_tanggal_baptis_pengantin_wanita" placeholder="Tempat dan Tanggal Baptis Pengantin Wanita" value="{{ old('tempat_tanggal_baptis_pengantin_wanita') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Paroki dan No. Buku Baptis Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="paroki_no_buku_pengantin_wanita" id="paroki_no_buku_pengantin_wanita" placeholder="Paroki dan No. Buku Baptis Pengantin Wanita" value="{{ old('paroki_no_buku_pengantin_wanita') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Tanggal Surat Baptis Terakhir Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="paroki_tanggal_surat_baptis_terakhir_pengantin_wanita" id="paroki_tanggal_surat_baptis_terakhir_pengantin_wanita" placeholder="Tanggal Surat Baptis Terakhir Pengantin Wanita" value="{{ old('paroki_tanggal_surat_baptis_pengantin_wanita') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Paroki dan Tanggal Penguatan Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="paroki_tanggal_penguatan_pengantin_wanita" id="paroki_tanggal_penguatan_pengantin_wanita" placeholder="Paroki dan Tanggal Penguatan Pengantin Wanita" value="{{ old('paroki_tanggal_penguatan_pengantin_wanita') }}">
                                </div>

                            </div>
                        </fieldset>
                    @elseif($step == 5)
                        <div class="box-header with-border" style="margin-bottom:4px;">
                            <h3 class="box-title">Data Orangtua Pengantin Pria</h3>
                        </div>
                        <fieldset>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Nama Ayah Pengantin Pria  <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="nama_ayah_pengantin_pria" id="nama_ayah_pengantin_pria" placeholder="Nama Ayah Pengantin Pria" value="{{ old('nama_ayah_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Agama Ayah Pengantin Pria  <span class="required">*</span></label>
                                    <select class="form-control isselect" name="agama_ayah_pengantin_pria" id="agama_ayah_pengantin_pria">
                                        <option value="Kristen">Kristen</option required>
                                        <option value="Islam">Islam</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Usia Ayah Pengantin Pria  <span class="required">*</span></label>
                                    <input type="number" class="form-control" name="usia_ayah_pengantin_pria" id="usia_ayah_pengantin_pria" placeholder="Usia Ayah Pengantin Pria" value="{{ old('usia_ayah_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Pekerjaan Ayah Pengantin Pria  <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="pekerjaan_ayah_pengantin_pria" id="pekerjaan_ayah_pengantin_pria" placeholder="Pekerjaan Ayah Pengantin Pria" value="{{ old('pekerjaan_ayah_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Ayah Pengantin Pria  <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="alamat_ayah_pengantin_pria" id="alamat_ayah_pengantin_pria" placeholder="Alamat Ayah Pengantin Pria" value="{{ old('alamat_ayah_pengantin_pria') }}" required>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Nama Ibu Pengantin Pria <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="nama_ibu_pengantin_pria" id="nama_ibu_pengantin_pria" placeholder="Nama Ibu Pengantin Pria" value="{{ old('nama_ibu_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Agama Ibu Pengantin Pria <span class="required">*</span></label>
                                    <select class="form-control isselect" name="agama_ibu_pengantin_pria" id="agama_ibu_pengantin_pria">
                                        <option value="Kristen">Kristen</option required>
                                        <option value="Islam">Islam</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Usia Ibu Pengantin Pria <span class="required">*</span></label>
                                    <input type="number" class="form-control" name="usia_ibu_pengantin_pria" id="usia_ibu_pengantin_pria" placeholder="Usia Ibu Pengantin Pria" value="{{ old('usia_ibu_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Pekerjaan Ibu Pengantin Pria <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="pekerjaan_ibu_pengantin_pria" id="pekerjaan_ibu_pengantin_pria" placeholder="Pekerjaan Ibu Pengantin Pria" value="{{ old('pekerjaan_ibu_pengantin_pria') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Ibu Pengantin Pria <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="alamat_ibu_pengantin_pria" id="alamat_ibu_pengantin_pria" placeholder="Alamat Ibu Pengantin Pria" value="{{ old('alamat_ibu_pengantin_pria') }}" required>
                                </div>
                            </div>
                        </fieldset>
                    @elseif($step == 6)
                        <div class="box-header with-border" style="margin-bottom:4px;">
                            <h3 class="box-title">Data Orangtua Pengantin Wanita</h3>
                        </div>
                        <fieldset>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Nama Ayah Pengantin Wanita <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="nama_ayah_pengantin_wanita" id="nama_ayah_pengantin_wanita" placeholder="Nama Ayah Pengantin Wanita" value="{{ old('nama_ayah_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Agama Ayah Pengantin Wanita <span class="required">*</span></label>
                                    <select class="form-control isselect" name="agama_ayah_pengantin_wanita" id="agama_ayah_pengantin_wanita" required>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Usia Ayah Pengantin Wanita  <span class="required">*</span></label>
                                    <input type="number" class="form-control" name="usia_ayah_pengantin_wanita" id="usia_ayah_pengantin_wanita" placeholder="Usia Ayah Pengantin Wanita" value="{{ old('usia_ayah_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Pekerjaan Ayah Pengantin Wanita <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="pekerjaan_ayah_pengantin_wanita" id="pekerjaan_ayah_pengantin_wanita" placeholder="Pekerjaan Ayah Pengantin Wanita" value="{{ old('pekerjaan_ayah_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Ayah Pengantin Wanita <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="alamat_ayah_pengantin_wanita" id="alamat_ayah_pengantin_wanita" placeholder="Alamat Ayah Pengantin Wanita" value="{{ old('alamat_ayah_pengantin_wanita') }}" required>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Nama Ibu Pengantin Wanita <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="nama_ibu_pengantin_wanita" id="nama_ibu_pengantin_wanita" placeholder="Nama Ibu Pengantin Wanita" value="{{ old('nama_ibu_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Agama Ibu Pengantin Wanita <span class="required">*</span></label>
                                    <select class="form-control isselect" name="agama_ibu_pengantin_wanita" id="agama_ibu_pengantin_wanita" required>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Usia Ibu Pengantin Wanita  <span class="required">*</span></label>
                                    <input type="number" class="form-control" name="usia_ibu_pengantin_wanita" id="usia_ibu_pengantin_wanita" placeholder="Usia Ibu Pengantin Wanita" value="{{ old('usia_ibu_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Pekerjaan Ibu Pengantin Wanita  <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="pekerjaan_ibu_pengantin_wanita" id="pekerjaan_ibu_pengantin_wanita" placeholder="Pekerjaan Ibu Pengantin Wanita" value="{{ old('pekerjaan_ibu_pengantin_wanita') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Ibu Pengantin Wanita  <span class="required">*</span></label>
                                    <input type="text" class="form-control" name="alamat_ibu_pengantin_wanita" id="alamat_ibu_pengantin_wanita" placeholder="Alamat Ibu Pengantin Wanita" value="{{ old('alamat_ibu_pengantin_wanita') }}" required>
                                </div>
                            </div>
                        </fieldset>
                    @elseif($step == 7)
                        <div class="box-header with-border" style="margin-bottom:4px;">
                            <h3 class="box-title">Data Lain-lain</h3>
                        </div>
                        <fieldset>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Foto Pengantin Pria <span class="required">*</span></label>
                                    <input class="form-control" type="file" name="image_pengantin_pria" id="image" onchange="return ValidateFileUpload('image','label_image')" @if($pernikahan->image_nikah_pengantin_pria != null || $pernikahan->image_nikah_pengantin_pria != '') @else required @endif>
                                </div>
                                <label id="label_image">Bukan file image</label>

                                <div class="form-group">
                                    <label for="" class="control-label">Surat Ijin Komandan bagi TNI & POLRI Pengantin Pria</label>
                                    <input type="text" class="form-control" name="surat_ijin_komandan_pengantin_pria" id="surat_ijin_komandan_pengantin_pria" placeholder="Surat Ijin Komandan bagi TNI & POLRI Pengantin Pria" value="{{ old('surat_ijin_komandan_pengantin_pria') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Kursus Persiapan Pernikahan Pengantin Pria</label>
                                    <input type="text" class="form-control" name="kursus_persiapan_pernikahan_pengantin_pria" id="kursus_persiapan_pernikahan_pengantin_pria" placeholder="Kursus Persiapan Pernikahan Pengantin Pria" value="{{ old('kursus_persiapan_pernikahan_pengantin_pria') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Alamat Baru Setelah Pernikahan</label>
                                    <input type="text" class="form-control" name="alamat_baru" id="alamat_baru" placeholder="Alamat Baru Setelah Pernikahan" value="{{ old('alamat_baru') }}">
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Foto Pengantin Wanita <span class="required">*</span></label>
                                    <input class="form-control" type="file" name="image_pengantin_wanita" id="image1" onchange="return ValidateFileUpload('image1','label_image1')" @if($pernikahan->image_nikah_pengantin_wanita != null || $pernikahan->image_nikah_pengantin_wanita != '' ) @else required @endif>
                                </div>
                                <label id="label_image1">Bukan file image</label>

                                <div class="form-group">
                                    <label for="" class="control-label">Surat Ijin Komandan bagi TNI & POLRI Pengantin Wanita  </label>
                                    <input type="text" class="form-control" name="surat_ijin_komandan_pengantin_wanita" id="surat_ijin_komandan_pengantin_wanita" placeholder="Surat Ijin Komandan bagi TNI & POLRI Pengantin Wanita" value="{{ old('surat_ijin_komandan_pengantin_wanita') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Kursus Persiapan Pernikahan Pengantin Wanita</label>
                                    <input type="text" class="form-control" name="kursus_persiapan_pernikahan_pengantin_wanita" id="kursus_persiapan_pernikahan_pengantin_wanita" placeholder="Kursus Persiapan Pernikahan Pengantin Wanita" value="{{ old('kursus_persiapan_pernikahan_pengantin_wanita') }}">
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label">Email <span class="required">*</span></label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required>
                                </div>
                            </div>
                        </fieldset>
                    @endif
                </div>
                <div class="box-footer">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <a href="{{ url('admin/sakramen/nikah') }}" class="btn btn-default btn-md">Semua data</a>
                            <a href="{{ url('sakramen-nikah/wizard/'.($step-1)) }}" class="btn btn-default btn-md">Kembali</a>
                            <input type="submit" value="Simpan dan Lanjut" class="btn btn-md btn-primary">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('customscript')
<script type="text/javascript" src="{{ URL::asset('/js/bootstrap-datepicker.min.js') }}">
</script>
<script type="text/javascript"
    src="{{ URL::asset('/plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript"
    src="{{ URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}">
</script>
<script type="text/javascript" src="{{ URL::asset('/plugins/steps/jquery.steps.js') }}">
</script>
<script type="text/javascript"
    src="{{ URL::asset('/plugins/validate/jquery.validate.js') }}"></script>
<script>
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $("#label_image").hide();
    $("#label_image1").hide();
    $("#label_image2").hide();
    $("#div_image_2").hide();
    $("#label_image3").hide();
    $("#div_image_3").hide();

    @if($editmode == true)
        var data = {!! $pernikahan !!};

        $('.ck').each(function (index, value){
            var a = $(this).attr('id');
            for (var key in data) {
                if(a == key){
                console.log(key+" "+data[key]);
                elem = $("#"+a);
                    if(elem.is('input')){
                        if(elem.attr("id") == "hari_libur"){
                            if(data[key] == 1){
                                elem.attr("checked", "checked");
                            }
                        }else if(elem.attr("id") == "tempat_menikah"){
                            if(data[key] == 2){
                                elem.attr("checked", "checked");
                                $("#alernative_tempat_menikah").prop('readonly', false);
                            }else{
                                $("#alernative_tempat_menikah").prop('readonly', true);
                            }
                        }
                    }
                }
            }
        });


        $('.form-control').each(function (index, value){
            var a = $(this).attr('id');
            // console.log(a);
            for (var key in data) {
                if(a == key){
                    // console.log(key+" "+data[key]);
                    // $('#'+a).val(data[key]);
                    elem = $("#"+a);
                    if(elem.is('input')){
                        if(elem.hasClass('datepicker')){
                                if(data[key] == '' || data[key] == null){
                                    parsedate = new Date();
                                }else{
                                    parsedate = new Date(data[key]);
                                }

                                tdy = new Date(parsedate.getFullYear(), parsedate.getMonth(), parsedate.getDate());
                                elem.datepicker({
                                    autoclose: true,
                                    format: 'dd-mm-yyyy',
                                    todayHighlight: true,
                                    endDate : today,
                                    weekStart: 1,
                                });

                                elem.datepicker('setDate', tdy);
                            }else if(elem.hasClass('agenda')){
                                parsedate1 = new Date(data[key]);
                                tdy1 = new Date(parsedate1.getFullYear(), parsedate1.getMonth(), parsedate1.getDate());
                                elem.datepicker({
                                    autoclose: true,
                                    format: 'dd-mm-yyyy',
                                    todayHighlight: true,
                                    startDate : today,
                                    endDate : '+18m',
                                    weekStart: 1,
                                });
                                elem.datepicker('setDate', tdy1);
                            }else{
                                elem.val(data[key]);
                            }
                        }else if(elem.is('select')){
                            elem.val(data[key]).trigger('change');
                        }else if(elem.is('textarea')){
                            elem.text(data[key])
                        }
                }
            }
        });
    @endif

    $('.select2').select2();

    $('#date_agenda').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        startDate: today,
        endDate: '+18m',
        weekStart: 1,
    });

    $('#date_agenda').datepicker('setDate', today);

    $('#tanggal_lahir_pengantin_pria').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        endDate: today,
        weekStart: 1,
    });

    $('#tanggal_lahir_pengantin_wanita').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        endDate: today,
        weekStart: 1,
    });

    $("#tempat_menikah").on('click', function () {
        var check = $("#tempat_menikah:checked").val();
        if (check == "on") {
            $("#alternative_tempat_menikah").prop('readonly', false);
            $("#alternative_tempat_menikah").val(" ");
        } else {
            $("#alternative_tempat_menikah").prop('readonly', true);
            $("#alternative_tempat_menikah").val("Gereja Paroki");
        }

    });

    $("#tempat_menikah").on('click', function () {
        var check = $("#tempat_menikah:checked").val();
        if (check == "on") {
            $("#alternative_tempat_menikah").prop('readonly', false);
            $("#alternative_tempat_menikah").val(" ");
        } else {
            $("#alternative_tempat_menikah").prop('readonly', true);
            $("#alternative_tempat_menikah").val("Gereja Paroki");
        }
    });

    $("#agama_pengantin_pria").on("change", function(){
        var cek = $("#agama_pengantin_pria option:selected").val();
        if(cek != "Katolik"){
            $("#div_image_2").hide();
            $("#image2").prop('disabled', true);
            $("#image2").prop('required', false);
        }else{
            $("#div_image_2").show();
            $("#image2").prop('disabled', false);
            $("#image2").prop('required', true);
        }
    });

    $("#agama_pengantin_wanita").on("change", function(){
        var cek = $("#agama_pengantin_wanita option:selected").val();
        if(cek != "Katolik"){
            $("#div_image_3").hide();
            $("#image3").prop('disabled', true);
            $("#image3").prop('required', false);
        }else{
            $("#div_image_3").show();
            $("#image3").prop('disabled', false);
            $("#image3").prop('required', true);
        }
    });

    function ValidateFileUpload(id,label) {
        // var fuData = $('#'+id);
        var fuData = document.getElementById(id);
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+id).attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                // var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                // var filename = fuData.substring(startIndex);
                // if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                //     filename = filename.substring(1);
                //     $('#'+id).val(filename);
                // }
            }
            $("#"+label).hide();
        } else {
            $('#'+id).val('');
            $("#"+label).show();
            return false;
        }
    }
</script>
@endsection