@extends('app')

@if(Auth::user()->role != 4)
@section('treeview_posting','active')
@else
@section('treeview_post','active')
@endif
@section('treeview_sakramen','active')
@section('treeview_sakramen_pernikahan','active')

@section('title', 'Sakramen Nikah')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop
@section('contentheader_title', 'Sakramen Nikah')

@section('main-content')
  <div class="box box-primary">
        <div class="box-body">
            {{-- <div class="">
                <a href="{{url('admin/sakramen-create/nikah')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
            </div> --}}
            <div class="">
                <a href="{{url('sakramen-nikah/wizard/1')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
            </div>
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Pasangan</th>
                        <th>Tanggal/Tempat</th>
                        <th>Waktu</th>
                        <th style="visibility: none"></th>
                        <th style="visibility: none"></th>
                        <th style="visibility: none"></th>
                        <th style="visibility: none"></th>
                        <th>status</th>
                        <th>status TTD</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

  <script type="text/javascript">
   $(document).ready(function(){
      $('#table').DataTable({
         'paging': true,
         'lengthChange': true,
         'searching': true,
         'ordering': true,
         'info': true,
         'autoWidth': false,
         "language": {
             "emptyTable": "Data Kosong",
             "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
             "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
             "infoFiltered": "(disaring dari _MAX_ total data)",
             "search": "Cari:",
             "lengthMenu": "Tampilkan _MENU_ Data",
             "zeroRecords": "Tidak Ada Data yang Ditampilkan",
             "oPaginate": {
                 "sFirst": "Awal",
                 "sLast": "Akhir",
                 "sNext": "Selanjutnya",
                 "sPrevious": "Sebelumnya"
             },
         },

        processing: true,
        serverSide: true,
        ajax: "<?= url("/api/sakramen/nikah") ?>/"+"{{ Auth::user()->id }}",
        columns: [
            {data: 'DT_Row_Index', name: 'DT_Row_Index', searchable: false, orderable: false},
            {data: 'nama_agenda', name: 'nama_agenda'},
            {data: 'pasangan', name: 'pasangan', searchable: false, visible: true},
            {data: 'date_agenda', name: 'date_agenda', orderable: false},
            {data: 'waktu', name: 'waktu', searchable: false, orderable: false},
            {data: 'nama_pengantin_pria', name: 'nama_pengantin_pria', visible: false, searchable: true, orderable: false,},
            {data: 'nama_pengantin_wanita', name: 'nama_pengantin_wanita', visible: false, searchable: true, orderable: false,},
            {data: 'tempat_menikah', name: 'tempat_menikah', visible: false, searchable: true, orderable: false,},
            {data: 'alternative_tempat_menikah', name: 'alternative_tempat_menikah', visible: false, searchable: true, orderable: false,},
            {data: 'status', name: 'status', orderable: false},
            {data: 'status_ttd', name: 'status_ttd', orderable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false, visible: true}
        ]
      });
    });
  </script>
@stop