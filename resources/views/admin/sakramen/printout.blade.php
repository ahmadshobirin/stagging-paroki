<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sakramen {{ $type }}</title>
    <style>
        body {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            padding: 6px;
        }

        table {
            /*margin-top: 8px;*/
            border-collapse: collapse;
            width: 100%;
        }

        table td{

        }
        table th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            color: black;
            /* background-color: #4CAF50; */
        }
    </style>
</head>

<body>
    {{-- {{ public_path('/bootstrap/css/bootstrap.min.css') }}<br>
    {{ asset('/bootstrap/css/bootstrap.min.css') }}<br>
    {{ URL::asset('/bootstrap/css/bootstrap.min.css') }}<br> --}}
    @if ( $type == "Pernikahan" )
    <img src="{{ public_path('dist/img/kop-surat.png') }}" style="margin-left: 150px;" min-width="100%" height="120px">
    @else
    <img src="{{ public_path('dist/img/kop-surat.png') }}" alt="" min-width="100%" height="120px">
    @endif
    <hr>
    @if ( $type == "Pernikahan" )

    <h2 align="center">FORMULIR PERMOHONAN PEMBERKATAN PERNIKAHAN GEREJA</h2>
    <div class="box-body">

        <table style="margin-top : 15px;">
            <tr>
                <td style="width:33%;" >
                    
                </td>
                <td style="width:33%;">
                    <h3 align="center">CALON SUAMI</h3>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                   <h3 align="center">CALON ISTRI</h3>
                </td>
            </tr>
            <tr>
                 <td style="width:33%;" >
                    Foto :
                </td>
                <td style="width:33%;">
                    @if($sakramen->image_nikah_pengantin_pria != null)
                    <img style="width:120px; height:120px; margin-left: 75px;" src="{{ public_path($sakramen->image_nikah_pengantin_pria) }}" alt="">
                    @endif
                </td>
                <td style="width:33%;word-wrap: break-word;">
                   @if($sakramen->image_nikah_pengantin_wanita != null)
                   <img style="width:120px; height:120px; margin-left: 170px;" src="{{ public_path($sakramen->image_nikah_pengantin_wanita) }}" alt="">
                   @endif
                </td>
            </tr>
             <tr>
                <td style="width:33%;" >
                    01. Nama Lengkap (Tanpa Singkatan)
                </td>
                <td style="width:33%;">
                    <p align="center">{{$sakramen->nama_pengantin_pria}}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                   <p align="center">{{$sakramen->nama_pengantin_wanita}}</p>
                </td>
            </tr> 
            <tr>
                <td style="width:33%;" >
                    02. Tempat dan Tanggal Lahir
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->tempat_lahir_pengantin_pria }}, {{date('d-m-Y', strtotime($sakramen->tanggal_lahir_pengantin_pria))}}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->tempat_lahir_pengantin_wanita }}, {{date('d-m-Y', strtotime($sakramen->tanggal_lahir_pengantin_wanita))}}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    03. a.Pekerjaan
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->pekerjaan_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->pekerjaan_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Surat Ijin Komandan bagi anggota TNI & Polri
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->surat_ijin_komandan_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->surat_ijin_komandan_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    04. a.Alamat sekarang dan No. Telp
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->alamat_pengantin_pria }},Telp : {{ $sakramen->telp_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->alamat_pengantin_wanita }},Telp : {{ $sakramen->telp_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Alamat sebelumnya
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->alamat_sebelumnya_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->alamat_sebelumnya_pengantin_wanita }}</p>
                </td>
            </tr>

            <tr>
                <td style="width:33%;" >
                    05. Agama/Gereja
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->agama_pengantin_pria }}/{{ $sakramen->gereja_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->agama_pengantin_wanita }}/{{ $sakramen->gereja_pengantin_wanita }}</p>
                </td>
            </tr>
             <tr>
                <td style="width:33%;" >
                    06. a.Tempat dan Tanggal Baptis
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->tempat_tanggal_baptis_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->tempat_tanggal_baptis_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Paroki dan No. Buku Baptis
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->paroki_no_buku_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->paroki_no_buku_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c. Tanggal Surat Baptis Terakhr/Terbaru
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->paroki_tanggal_surat_baptis_terakhir_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->paroki_tanggal_surat_baptis_terakhir_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    07. Paroki dan Tanggal Penguatan
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->paroki_tanggal_penguatan_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->paroki_tanggal_penguatan_pengantin_wanita }}</p>
                </td>
            </tr>

            <tr>
                <td style="width:33%;" >
                    08. AYAH KANDUNG
                </td>
                <td style="width:33%;">
                    
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    
                </td>
            </tr>

            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Nama Lengkap (tanpa singkatan)
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->nama_ayah_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->nama_ayah_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Agama
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->agama_ayah_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->agama_ayah_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c. Usia
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->usia_ayah_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->usia_ayah_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d. Pekerjaan
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->pekerjaan_ayah_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->pekerjaan_ayah_pengantin_wanita }}</p>
                </td>
            </tr><tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e. Alamat
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->alamat_ayah_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->alamat_ayah_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    09. IBU KANDUNG
                </td>
                <td style="width:33%;">
                    
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    
                </td>
            </tr>

            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Nama Lengkap (tanpa singkatan)
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->nama_ibu_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->nama_ibu_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Agama
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->agama_ibu_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->agama_ibu_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c. Usia
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->usia_ibu_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->usia_ibu_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d. Pekerjaan
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->pekerjaan_ibu_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->pekerjaan_ibu_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e. Alamat
                </td>
                <td style="width:33%;">
                    <p align="center">{{ $sakramen->alamat_ibu_pengantin_pria }}</p>
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    <p align="center">{{ $sakramen->alamat_ayah_pengantin_wanita }}</p>
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    10. Pernikahan akan berlangsung di Gereja
                </td>
                <td style="width:10%;">
                    @if($sakramen->tempat_menikah == 1)
                    <p align="center">Gereja Paroki</p>
                    @else
                    <p align="center">{{$sakramen->alternative_tempat_menikah}}</p>
                    @endif
                </td>
                <td style="width:54%;word-wrap: break-word;">
                    <?php 
                        $hari = date('l', strtotime($sakramen->date_agenda));
                        if($hari == "Monday"){
                            $hari = "Senin";
                        }elseif($hari == "Tuesday"){
                            $hari = "Selasa";
                        }if($hari == "Wednesday"){
                            $hari = "Rabu";
                        }if($hari == "Thursday"){
                            $hari = "Kamis";
                        }if($hari == "Friday"){
                            $hari = "Jumat";
                        }if($hari == "Saturday"){
                            $hari = "Sabtu";
                        }if($hari == "Sunday"){
                            $hari = "Minggu";
                        }
                    ?>
                    Hari : {{$hari}}&nbsp;&nbsp;&nbsp;&nbsp;
                    Tanggal : {{ date('d-m-Y', strtotime($sakramen->date_agenda)) }}&nbsp;&nbsp;&nbsp;&nbsp;
                    Pukul : {{ date('H:i', strtotime($sakramen->waktu_agenda)) }} - {{ date('H:i', strtotime($sakramen->end_agenda)) }}
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    11. Yang Mempersiapkan Perkawinan
                </td>
                <td colspan="2"  style="width:67%;">
                    Pastor : {{$sakramen->pastor_mempersiapkan_pernikahan}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    di Gereja : {{$sakramen->gereja_mempersiapkan_pernikahan}}
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    12. Yang Meneguhkan Perkawinan
                </td>
                <td colspan="2"  style="width:67%;">
                    Pastor : {{$sakramen->pastor_meneguhkan_pernikahan}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    di Gereja : {{$sakramen->gereja_meneguhkan_pernikahan}}
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    13. Alamat setelah pernikahan
                </td>
                <td colspan="2"  style="width:67%;">
                    {{$sakramen->alamat_baru}}
                </td>
            </tr>
            <tr>
                <td style="width:33%;" >
                    14. Kursus Persiapan Pernikahan dilangsungkan
                </td>
                <td style="width:33%;">
                    {{ $sakramen->kursus_persiapan_pernikahan_pengantin_pria }}
                </td>
                <td style="width:33%;word-wrap: break-word;">
                    {{ $sakramen->kursus_persiapan_pernikahan_pengantin_wanita }}
                </td>
            </tr>

            <tr>
                <td colspan="2" style="width:50%;">
                    <h3>Tanda Tangan Calon Suami</h3>
                </td>
                <td style="width:50%;word-wrap: break-word;">
                   <h3>Tanda Tangan Calon Istri</h3>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width:50%;">
                    <div style="">
                        <label for="">Tanggal : {{date('d-m-Y', strtotime($sakramen->updated_at))}}</label>
                    </div>
                    <br>
                    <div>
                        @if($sakramen->signature_pengantin_pria != null)
                            <img style="width:120px; height:120px" src="{{ public_path($sakramen->signature_pengantin_pria) }}" alt="">
                        @endif
                        <br>
                            <label for="">{{ $sakramen->nama_pengantin_pria }}</label>
                    </div>
                </td>
                <td style="width:50%;word-wrap: break-word;">
                    <div style="">
                        <label for="">Tanggal : {{date('d-m-Y', strtotime($sakramen->updated_at))}}</label>
                    </div>
                    <br>
                    <div>
                        @if($sakramen->signature_pengantin_wanita != null)
                            <img style="width:120px; height:120px" src="{{ public_path($sakramen->signature_pengantin_wanita) }}" alt="">
                        @endif
                        <br>
                        <label for="">{{ $sakramen->nama_pengantin_wanita }}</label>
                    </div>
                </td>
            </tr>
            <?php $cek_seq = 0; ?>
            @foreach(@$sakramen->signature as $sig)
            @if($cek_seq != $sig->sequence )
            <tr>
            @endif
            <td @if($cek_seq != $sig->sequence ) colspan="2" @endif style="width:50%;">
                <div style="">
                    @if($sig->sequence == 1)
                    Ketua Lingkungan :
                    @elseif($sig->sequence == 2)
                    Ketua Wilayah : 
                    @elseif($sig->sequence == 3)
                    Pastor : 
                    @endif
                </div>
                <div>
                    @if($sig->signature_path != null)
                        <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                    @endif
                    <br>
                        <label for="">{{ $sig->username }}</label>
                </div>
            </td>
            @if($cek_seq == $sig->sequence)
            </tr>   
            @endif 
            <?php $cek_seq = $sig->sequence ?>
            @endforeach

        </table>
        
        <div>
            <p align="center"></p>
            <div>
               @if($sakramen->agama_pengantin_pria == "Katolik")
                    @if($sakramen->surat_baptis_pengantin_pria != null)
                        <img style="max-width:100%" src="{{ public_path($sakramen->surat_baptis_pengantin_pria) }}" alt="">
                    @endif
                @endif
                
                @if($sakramen->agama_pengantin_wanita == "Katolik")
                    @if($sakramen->surat_baptis_pengantin_wanita != null)
                         <img style="max-width:100%" src="{{ public_path($sakramen->surat_baptis_pengantin_wanita) }}" alt="">
                    @endif
                @endif
            </div>
        </div>
        
    </div>

    @else

        @if($type == "Baptis Dewasa")
        <h2 align="center">PENDAFTARAN CALON BAPTIS DEWASA</h2>
        <div class="box-body">

            <table style="margin-top : 15px;">
                <tr>
                    <td  style="width:50%;" >
                        Foto :
                    </td>
                    <td colspan="2"  style="width:50%;" >
                        @if($sakramen->image_baptis != null)
                         <img style="width:120px; height:120px;" src="{{ public_path($sakramen->image_baptis) }}" alt="">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;">
                        01. Nama Diri 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_diri}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        02. Tempat & Tanggal Lahir 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->tempat_lahir}}, {{date('d-m-Y', strtotime($sakramen->tanggal_lahir))}}
                    </td>
                </tr>
                <tr>
                    <td style="width:33%;" >
                        03. Nama Ayah (tanpa singkatan) 
                    </td>
                    <td  style="width:33%;">:
                        {{$sakramen->nama_ayah}}
                    </td>
                    <td  style="width:33%;">
                        Agama : {{$sakramen->agama_ayah}}
                    </td>
                </tr>
                <tr>
                    <td style="width:33%;" >
                        04. Nama Ibu (tanpa singkatan) 
                    </td>
                    <td style="width:33%;">:
                        {{$sakramen->nama_ibu}}
                    </td>
                    <td  style="width:33%;">
                        Agama : {{$sakramen->agama_ibu}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        05. Apakah Anda ada ijin dari orangtua? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ ucfirst($sakramen->ada_ijin_ortu) }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        06. Apakah Anda sudah/belum menikah? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ ucfirst($sakramen->sudah_menikah)}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        07. Apakah ada ijin dari Suami/Istri? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->ada_ijin_pasangan != null)
                        {{ ucfirst($sakramen->ada_ijin_pasangan) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:33%;" >
                        08. Nama Lengkap Suami/Istri? 
                    </td>
                    <td style="width:33%;">:
                        @if($sakramen->nama_pasangan != null)
                        {{ ucfirst($sakramen->nama_pasangan) }}
                        @else
                        -
                        @endif
                    </td>
                    <td  style="width:33%;">
                        Agama :  @if($sakramen->agama_pasangan != null)
                        {{ ucfirst($sakramen->agama_pasangan) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:33%;" >
                        09. Pernikahan secara agama apa? 
                    </td>
                    <td style="width:33%;">:
                        @if($sakramen->pernikahan_agama != null)
                        {{ ucfirst($sakramen->nama_pasangan) }}
                        @else
                        -
                        @endif
                    </td>
                    <td  style="width:33%;">
                        Tanggal :  @if($sakramen->pernikahan_agama != null)
                        {{ date('d-m-Y',strtotime($sakramen->agama_pasangan)) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        10. Apakah alasan Anda menjadi Katolik? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->alasan_menjadi_katolik }}
                    </td>
                </tr>
                <tr>
                    <td style="width:33%;" >
                        11. Alamat Anda sendiri 
                    </td>
                    <td style="width:33%;">:
                        {{ $sakramen->alamat }}
                    </td>
                    <td  style="width:33%;">
                        Telp :  
                        {{ $sakramen->no_telp }}
                    </td>
                </tr>
                <tr>
                    <td style="width:33%;" >
                        12. Alamat Orangtua 
                    </td>
                    <td style="width:33%;">:
                        {{ $sakramen->alamat_ortu }}
                    </td>
                    <td  style="width:33%;">
                        Telp : 
                        {{ $sakramen->no_telp_ortu }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        13. Apakah di lingkungan Anda ada keluarga Katolik yang lain? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->lingkungan_katolik != null)
                        {{ ucfirst($sakramen->lingkungan_katolik) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        14. Apakah dalam keluarga Anda ada yang beragama Katolik? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->keluarga_katolik != null)
                        {{ ucfirst($sakramen->keluarga_katolik) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        15. Apakah berasal dari Paroki mana? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->asal_paroki }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        16. Apakah masuk Lingkungan mana? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->nama_kategori_user }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        17. Apakah Anda merasa mantap untuk dipermandikan? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->merasa_mantap != null)
                        {{ ucfirst($sakramen->merasa_mantap) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        18. Sebelumnya Agama apa yang Anda anut? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->agama_sebelumnya != null)
                        {{ ucfirst($sakramen->agama_sebelumnya) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        19. Nama Permandian yang Anda Pilih? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->nama_pemandian != null)
                        {{ ucfirst($sakramen->nama_pemandian) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        20. Nama lengkap Wali Permandian? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->nama_wali_pemandian != null)
                        {{ ucfirst($sakramen->nama_wali_pemandian) }}
                        @else
                        -
                        @endif
                    </td>
                </tr> 
                <tr>
                    <td style="width:50%;" >
                        21. Tempat & Tanggal Permandian? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        22. Nomor ID K3? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->no_id_k3 != null)
                        {{ $sakramen->no_id_k3 }}
                        @else
                        -
                        @endif
                    </td>
                </tr>                
            </table>
            <br>
            <br>
            <br>
            <p align="right"></p>
            <table>
                <tr>
                    <td colspan="2" style="width:50%;" >
                        Mengetahui,<br>
                        Ketua Lingkungan<br>
                        @foreach(@$sakramen->signature as $sig)
                            @if($sig->signature_path != null)
                                <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                            @else
                            <br>
                            <br>
                            <br>
                            @endif
                        <br>
                        <label for="">{{ $sig->username }}</label>
                        @endforeach
                    </td>
                    <td style="width:50%;">
                        Surabaya, {{ date('d-m-Y', strtotime($sakramen->created_at)) }}<br>
                        Yang bersangkutan<br>
                        
                        @if($sakramen->signature_dewasa != null)
                            <br>
                            <img style="width:120px; height:120px" src="{{ public_path($sakramen->signature_dewasa) }}" alt="">
                        @else
                        <br>
                        <br>
                        <br>
                        @endif
                        <br>
                        <label for="">{{ $sakramen->nama_diri }}</label>
                    </td>
                </tr>
            </table>    
            <table>
                <tr>
                    <td style="width:33%;">
                        
                    </td>
                    <td style="width:67%;">
                    </td>
                </tr>
            </table>
            <br>
            <div>
                <p align="center"></p>
                <div style="">
                    <p></p>
                    @if($sakramen->sudah_menikah == "sudah")
                        <p></p>
                        @if($sakramen->surat_nikah != null)
                        <img src="{{ public_path($sakramen->surat_nikah) }}" style="max-height: 100%; max-width: 100%" alt="">
                        @endif
                    @endif
                </div>
            </div>  
        </div>

        @elseif($type == "Baptis Anak")
        <h2 align="center">PENDAFTARAN CALON BAPTIS ANAK DAN REMAJA</h2>
        <div class="box-body">
            <table style="margin-top : 15px;">
                <tr>
                    <td  style="width:50%;" >
                        Foto :
                    </td>
                    <td colspan="2"  style="width:50%;" >
                        @if($sakramen->image_baptis != null)
                         <img style="width:120px; height:120px;" src="{{ public_path($sakramen->image_baptis) }}" alt="">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        01. Nama Diri 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_diri}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        02. Tempat & Tanggal Lahir 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->tempat_lahir}}, {{date('d-m-Y', strtotime($sakramen->tanggal_lahir))}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        03. Nama Lengkap Ayah 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_ayah}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        04. Nama Lengkap Ibu 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_ibu}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        05. Apakah Anda ada ijin dari orangtua? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ ucfirst($sakramen->ada_ijin_ortu) }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        06. Pernikahan orangtua secara agama apa? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ ucfirst($sakramen->pernikahan_agama_ortu) }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        07. Alamat Anda sendiri 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->alamat }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        08. Telp 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->no_telp }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        09. Apakah berasal dari Paroki mana? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->asal_paroki }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        10. Apakah masuk Lingkungan mana? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->nama_kategori_user }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        11. Nama Permandian yang Anda Pilih? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->nama_pemandian != null)
                        {{ ucfirst($sakramen->nama_pemandian) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        12. Nama lengkap Wali Permandian? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->nama_wali_pemandian != null)
                        {{ ucfirst($sakramen->nama_wali_pemandian) }}
                        @else
                        -
                        @endif
                    </td>
                </tr> 
                <tr>
                    <td style="width:50%;" >
                        13. Apakah Anda merasa mantap untuk dipermandikan? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        @if($sakramen->merasa_mantap != null)
                        {{ ucfirst($sakramen->merasa_mantap) }}
                        @else
                        -
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        14. No ID K3 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->no_id_k3}}
                    </td>
                </tr>
            </table>
            <p align="right"></p>
            <table style="margin-top : 15px;">
                <tr>
                    <td style="width:33%;" >
                        Mengetahui,<br>
                        Ketua Lingkungan<br>
                        @foreach(@$sakramen->signature as $sig)
                            @if($sig->signature_path != null)
                                <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                            @else
                            <br>
                            <br>
                            <br>
                            @endif
                        <br>
                        <label for="">{{ $sig->username }}</label>
                        @endforeach
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        <br>
                        Orang Tua<br>
                        
                        @if($sakramen->signature_orang_tua != null)
                            <br>
                            <img style="width:120px; height:120px" src="{{ public_path($sakramen->signature_orang_tua) }}" alt="">
                        @endif
                        <br>
                        {{-- <label for="">{{ $sakramen->nama_diri }}</label> --}}
                    </td>
                    <td>
                        Surabaya, {{ date('d-m-Y', strtotime($sakramen->created_at)) }}<br>
                        Yang bersangkutan<br>
                        
                        @if($sakramen->signature_anak != null)
                            <br>
                            <img style="width:120px; height:120px" src="{{ public_path($sakramen->signature_anak) }}" alt="">
                        @else
                        <br>
                        <br>
                        <br>
                        @endif
                        <br>
                        <label for="">{{ $sakramen->nama_diri }}</label>
                    </td>
                </tr>
            </table>
            <div>
                <p align="center"></p>
                <div style="">
                    <p></p>
                        @if($sakramen->surat_nikah_ortu != null)
                           <img src="{{ public_path($sakramen->surat_nikah_ortu) }}" style="max-width:100%; max-height:100%">
                       @endif
                </div>
            </div>

        </div>


        @elseif($type == "Baptis Balita")
        <p align="right">Surabaya, {{ date('d-m-Y', strtotime($sakramen->created_at)) }} </p>
        <h2 align="center">PENDAFTARAN CALON BAPTIS BALITA</h2>
        <div class="box-body">
            Bersama ini kami hadapkan kepada Pastor, Warga Lingkungan kami  :
            <table style="margin-top : 15px;">
                <tr>
                    <td style="width:50%;" >
                        01. Nama Lengkap Ayah 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_ayah}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        02. Nama Lengkap Ibu 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_ibu}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        03. No ID K3 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->no_id_k3}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        04. Alamat 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->alamat}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        05. Telp 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->no_telp}}
                    </td>
                </tr>
            </table>
            Yang berniat memohon <b>Sakramen Permandian</b> bagi putera/puteri meraka  :
            <table style="margin-top : 15px;">
                <tr>
                    <td  style="width:50%;" >
                        Foto :
                    </td>
                    <td colspan="2"  style="width:67%;" >
                        @if($sakramen->image_baptis != null)
                         <img style="width:120px; height:120px;" src="{{ public_path($sakramen->image_baptis) }}" alt="">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        01. Nama (diri) Anak
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_diri}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        02. Nama Permandian yang dipilih
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_pemandian}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        03. Tempat & Tanggal Lahir
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->tempat_lahir}}, {{date('d-m-Y', strtotime($sakramen->tanggal_lahir))}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        04. Nama Bapak/Ibu Permandian
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_wali_pemandian}}
                    </td>
                </tr>
            </table>
            <br>
            Mengetahui,<br>
            Ketua Lingkungan<br>
            @foreach(@$sakramen->signature as $sig)
                @if(@$sig->signature_path != null)
                    <br>
                    <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                @else
                <br>
                <br>
                <br>
                @endif
            <br>
            <label for="">{{ $sig->username }}</label>
            @endforeach
            <div>
                <p align="center"></p>
                <div>
                    @if($sakramen->surat_nikah_ortu != null)
                   <img src="{{ public_path($sakramen->surat_nikah_ortu) }}" style="max-height: 100%; max-width: 100%" alt="">
                   @endif
                </div>

                <div style="">
                    @if($sakramen->surat_baptis_wali != null)
                    <img src="{{ public_path($sakramen->surat_baptis_wali) }}" style="max-height: 100%; max-width: 100%" alt="">
                    @endif
                </div>
            </div>
        </div>

        @elseif($type == "Komuni")
        <h2 align="center">DATA CALON PENERIMA KOMUNI PERTAMA</h2>
        <div class="box-body">
            <table>
                <tr>
                    <td  style="width:50%;" >
                        Foto :
                    </td>
                    <td style="width:50%;" >
                        @if($sakramen->image_komuni != null)
                         <img style="width:120px; height:120px;" src="{{ public_path($sakramen->image_komuni) }}" alt="">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        01. Nama Lengkap 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_diri}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        02. Nama Panggilan 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_panggilan}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        03. Alamat Calon 
                    </td>
                    <td colspan="2" style="width:50%;">:
                        {{ $sakramen->alamat }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        04. Telp/HP 
                    </td>
                    <td colspan="2" style="width:50%;">:
                        {{ $sakramen->no_telp }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        05. Tempat & Tanggal Lahir 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->tempat_lahir}}, {{date('d-m-Y', strtotime($sakramen->tanggal_lahir))}}
                    </td>
                </tr>
                <tr>
                    <td style="width:33%;" >
                        06. Nama Sekolah 
                    </td>
                    <td style="width:33%;">:
                        {{$sakramen->nama_sekolah}}
                    </td>
                    <td style="width:33%;">
                        Kelas : {{$sakramen->kelas}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        07. Lingkungan 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_kategori_user}}
                    </td>
                </tr>
                <tr>
                    <td style="width:33%;" >
                        08. Tinggi Badan 
                    </td>
                    <td style="width:33%;">:
                        {{$sakramen->tinggi_badan}} cm
                    </td>
                    <td style="width:33%;">
                        No ID K3 : {{$sakramen->no_id_k3}}
                    </td>
                </tr>
            </table>
            <h4>DATA ORANG TUA</h4>
            <table>
                <tr>
                    <td style="width:50%;" >
                        1. Nama Lengkap Ayah 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_ayah}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        &nbsp;&nbsp;1.1. Agama 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->agama_ayah}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        &nbsp;&nbsp;1.2. Pekerjaan 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->pekerjaan_ayah}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        &nbsp;&nbsp;1.3. No HP 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->no_telp_ayah}}
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="5">
                        
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        2. Nama Lengkap Ibu 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_ibu}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        &nbsp;&nbsp;2.1. Agama 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->agama_ibu}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        &nbsp;&nbsp;2.2. Pekerjaan 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->pekerjaan_ibu}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        &nbsp;&nbsp;2.3. No HP 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->no_telp_ibu}}
                    </td>
                </tr>
            </table>
            <p align="right">Surabaya, {{ date('d-m-Y', strtotime($sakramen->created_at)) }}</p>
            <table>
                <tr>
                    <td colspan="2" style="width: 50%">
                        Tanda Tangan Ayah
                        <br>
                        @if($sakramen->signature_ayah != null)
                            <img style="width:120px; height:120px" src="{{ public_path($sakramen->signature_ayah) }}" alt="">
                        @else
                        <br>
                        <br>
                        <br>
                        @endif
                        <br>
                        {{$sakramen->nama_ayah}}
                    </td>
                    <td  style="width:33%;">
                        Tanda Tangan Ibu
                        <br>
                        @if($sakramen->signature_ibu != null)
                            <img style="width:120px; height:120px" src="{{ public_path($sakramen->signature_ibu) }}" alt="">
                        @else
                        <br>
                        <br>
                        <br>
                        @endif
                        <br>
                        {{$sakramen->nama_ibu}}
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width:50%;" >
                        Mengetahui,<br>
                        Ketua Lingkungan<br>
                        @foreach(@$sakramen->signature as $sig)
                            @if($sig->sequence == 1)
                                @if($sig->signature_path != null)
                                    <br>
                                    <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                                @else
                                <br>
                                <br>
                                <br>
                                @endif
                            <br>
                            <label for="">{{ $sig->username }}</label>
                            @endif
                        @endforeach
                    </td>
                    <td  style="width:50%;" >
                        <br>
                        Ketua BIAK<br>
                        @foreach(@$sakramen->signature as $sig)
                            @if($sig->sequence == 2)
                                @if($sig->signature_path != null)
                                    <br>
                                    <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                                @else
                                <br>
                                <br>
                                <br>
                                @endif
                            <br>
                            <label for="">{{ $sig->username }}</label>
                            @endif
                        @endforeach
                    </td>
                </tr>
            </table>
            <div>
                <p align="center"></p>
                <div style="">
                    @if($sakramen->image_surat_nikah_ortu != null)
                   <img src="{{ public_path($sakramen->image_surat_nikah_ortu) }}" style="max-height: 100%; max-width: 100%" alt="">
                   @endif
                </div>

                <div style="">
                    @if($sakramen->image_surat_baptis != null)
                    <img src="{{ public_path($sakramen->image_surat_baptis) }}" style="max-height: 100%; max-width: 100%" alt="">
                    @endif
                </div>
            </div>
        </div>

        @elseif($type == "Krisma")
        <h2 align="center">PENDAFTARAN CALON PENERIMA SAKRAMEN PENGUATAN SUCI (KRISMA)</h2>
        <div class="box-body">
            <table style="margin-top : 15px;">
                <tr>
                    <td  style="width:50%;" >
                        Foto :
                    </td>
                    <td style="width:50%;" >
                        @if($sakramen->image_krisma != null)
                         <img style="width:120px; height:120px;" src="{{ public_path($sakramen->image_krisma) }}" alt="">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        01. Nama Diri 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_diri}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        02. Nama Baptis 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_baptis}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        03. Tempat & Tanggal Lahir 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->tempat_lahir}}, {{date('d-m-Y', strtotime($sakramen->tanggal_lahir))}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        04. Alamat Calon 
                    </td>
                    <td colspan="2" style="width:50%;">:
                        {{ $sakramen->alamat }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        05. Telp 
                    </td>
                    <td colspan="2" style="width:50%;">:
                        {{ $sakramen->no_telp }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        06. Nama Lengkap Ayah 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_ayah}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        07. Nama Lengkap Ibu 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->nama_ibu}}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        08. Apakah berasal dari Paroki mana? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->asal_paroki }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        09. Apakah masuk Lingkungan mana? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ $sakramen->nama_kategori_user }}
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;" >
                        10. Apakah Anda sudah/belum menikah? 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{ ucfirst($sakramen->sudah_menikah)}}
                        
                    </td>
                </tr> 
                <tr>
                    <td style="width:50%;" >
                        11. No ID K3 
                    </td>
                    <td colspan="2"  style="width:50%;">:
                        {{$sakramen->no_id_k3}}
                    </td>
                </tr>
            </table>
            <p align="right"></p>
            <table style="margin-top : 15px;">
                <tr>
                    <td style="width:33%;" >
                        Mengetahui,<br>
                        Ketua Wilayah<br>
                        @foreach(@$sakramen->signature as $sig)
                            @if($sig->sequence == 2)
                                @if($sig->signature_path != null)
                                    <br>
                                    <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                                @endif
                                <br>
                                <label for="">{{ $sig->username }}</label>
                            @endif
                        @endforeach
                    </td>
                    <td style="width:33%;" >
                        <br>
                        Ketua Lingkungan<br>
                        @foreach(@$sakramen->signature as $sig)
                            @if($sig->sequence == 1)
                                @if($sig->signature_path != null)
                                    <br>
                                    <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                                @endif
                                <br>
                                <label for="">{{ $sig->username }}</label>
                            @endif
                        @endforeach
                    </td>
                    <td style="width: 33%">
                        <p align="center">Surabaya, {{ date('d-m-Y', strtotime($sakramen->created_at)) }}</p>
                        <p align="center">Calon Penerima Krisma
                        <br>
                        @if($sakramen->signature_umat != null)
                            <img style="width:120px; height:120px" src="{{ public_path($sakramen->signature_umat) }}" alt="">
                        @endif
                        <p align="center">{{$sakramen->nama_diri}}</p>
                    </td>
                </tr>
            </table>
            <div>
                <p align="center"></p>
                <div style="">
                    @if($sakramen->sudah_menikah == "sudah")
                        @if($sakramen->image_surat_nikah != null)
                        <img src="{{ public_path($sakramen->image_surat_nikah) }}" style="max-height: 100%; max-width: 100%" alt="">
                        @endif
                    @endif
                </div>

                <div style="">
                    @if($sakramen->image_surat_baptis != null)
                    <img src="{{ public_path($sakramen->image_surat_baptis) }}" style="max-height: 100%; max-width: 100%" alt="">
                    @endif
                </div>
            </div>
        </div>
        @endif
    @endif
</body>

</html>