@extends('app')

@section('treeview_post','active')
@section('treeview_transaksi_sakramen','active')

@section('title', 'List Transaksi Sakramen')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@section('contentheader_title', 'List Transaksi Sakramen')

@section('main-content')
  <div class="box box-primary">
        <div class="box-body">
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th style="width: 10%">No.</th>
                        <th>Nama Sakramen</th>
                        <th>Nominal</th>
                        <th>Status</th>
                        {{-- <th class="nosort" style="width: 20%">Aksi</th> --}}
                    </tr>
                </thead>
                <tbody>
                  <?php $i=1 ?>
                  @foreach($cek_baptis as $baptis)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>Sakramen {{ ucfirst($baptis->type) }} {{ $baptis->nama_diri }}</td>
                        <td>Rp. {{ number_format($baptis->price,0,'.','.') }}</td>
                        <td>
                          @if($baptis->status == "paid")
                          Lunas
                          @else
                          Belum Lunas
                          @endif
                        </td>
                    </tr>
                  @endforeach
                  @foreach($cek_nikah as $nikah)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{ $nikah->nama_agenda }}</td>
                        <td>Rp. {{ number_format($nikah->price,0,'.','.') }}</td>
                        <td>
                          @if($nikah->status == "paid")
                          Lunas
                          @else
                          Belum Lunas
                          @endif
                        </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

  <script type="text/javascript">
    $('#table').DataTable({
       'paging': true,
       'lengthChange': true,
       'searching': true,
       'ordering': true,
       'info': true,
       'autoWidth': false,
       "language": {
           "emptyTable": "Data Kosong",
           "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
           "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
           "infoFiltered": "(disaring dari _MAX_ total data)",
           "search": "Cari:",
           "lengthMenu": "Tampilkan _MENU_ Data",
           "zeroRecords": "Tidak Ada Data yang Ditampilkan",
           "oPaginate": {
               "sFirst": "Awal",
               "sLast": "Akhir",
               "sNext": "Selanjutnya",
               "sPrevious": "Sebelumnya"
           },
       },
        'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': ['nosort']
      }],
    });
  </script>
@stop