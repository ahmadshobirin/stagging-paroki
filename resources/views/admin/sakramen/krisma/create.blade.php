@extends('app')

@if(Auth::user()->role != 4)
@section('treeview_posting','active')
@else
@section('treeview_post','active')
@endif
@section('treeview_sakramen','active')
@section('treeview_sakramen_krisma','active')

@section('title', 'Tambah Sakramen Krisma')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@section('contentheader_title', 'Tambah Sakramen Krisma')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ $url }}" enctype="multipart/form-data">
                @include('admin.displayerror')
                {{ csrf_field() }}
                @isset($editmode) 
                @method('PUT')
                @else
                @method('POST')
                @endif

                <input type="hidden" name="type" value="krisma">
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Lingkungan  <span class="required">*</span></label>
                                <select class="form-control select2" id="id_kategori_user" name="id_kategori_user">
                                    @foreach($lingkungan as $ling)
                                    <option value="{{ $ling->id_kategori_user }}">{{ $ling->nama_kategori_user }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Periode <span class="required">*
                                </span></label>
                                <select class="form-control select2" required id="id_periode" name="id_periode">
                                    @foreach($periode as $prd)
                                    <option value="{{ $prd->id_periode }}">{{ $prd->nama_periode }} {{ date('d-m-Y', strtotime($prd->start_date)) }} s/d {{ date('d-m-Y', strtotime($prd->end_date)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_diri" id="nama_diri" placeholder="Nama Lengkap" value="{{ old('nama_diri') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Nama Baptis</label>
                                <input type="text" class="form-control" name="nama_baptis" id="nama_baptis" placeholder="Nama Baptis" value="{{ old('nama_baptis') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat</label>
                                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="{{ old('alamat') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No. Telp</label>
                                <input type="text" class="form-control telp" name="no_telp" placeholder="No. Telp" id="no_telp" value="{{ old('no_telp') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tempat Lahir <span class="required">*</span></label>
                                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="{{ old('tempat_lahir') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tanggal Lahir <span class="required">*
                                </span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="tanggal_lahir" class="form-control input-sm datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Paroki</label>
                                <input type="text" class="form-control" name="asal_paroki" id="asal_paroki" placeholder="Asal Paroki" value="{{ old('asal_paroki') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tanggal Baptis</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="tanggal_baptis" class="form-control input-sm datepicker" name="tanggal_baptis" value="{{ old('tanggal_baptis') }}" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap Ayah <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_ayah" id="nama_ayah" placeholder="Nama Lengkap Ayah" value="{{ old('nama_ayah') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap Ibu <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Lengkap Ibu" id="nama_ibu" value="{{ old('nama_ibu') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Santo Pelindung Penguatan</label>
                                <input type="text" class="form-control" name="nama_santo_pelindung_penguatan" id="nama_santo_pelindung_penguatan" placeholder="Nama Santo Pelindung Penguatan" value="{{ old('nama_santo_pelindung_penguatan') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Wali Penguatan</label>
                                <input type="text" class="form-control" name="nama_wali_penguatan" placeholder="Nama Wali Penguatan" id="nama_wali_penguatan" value="{{ old('nama_wali_penguatan') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Email <span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" required id="email" placeholder="Email" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No ID K3</label>
                                <input type="text" class="form-control" name="no_id_k3" placeholder="No. ID K3" id="no_id_k3" value="{{ old('no_id_k3') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Apakah sudah menikah?</label>
                                 <select class="form-control" name="sudah_menikah" id="sudah_menikah">
                                    <option selected value="belum">Belum</option>
                                    <option value="sudah">Sudah</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6" id="pasangan">
                             <div class="form-group">
                                <label class="control-label">Surat Nikah </label>
                                <input class="form-control" type="file" name="image1" id="image1" onchange="return ValidateFileUpload1()">
                            </div>
                            <label id="label_image1">Bukan file image</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Surat Baptis </label>
                                <input class="form-control" type="file" name="image2" id="image2" onchange="return ValidateFileUpload2()">
                            </div>
                            <label id="label_image2">Bukan file image</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Foto yang bersangkutan </label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()" required>
                            </div>
                            <label id="label_image">Bukan file image</label>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/sakramen/krisma') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $(document).ready(function () {

        $("#pasangan").hide();
        $("#image1").prop('disabled', true);

        @isset($editmode)
        var data = {!! $krisma !!};
        $('.form-control').each(function (index, value){
            var a = $(this).attr('id');
            for (var key in data) {
                if(a == key){
                //console.log(key+" "+data[key]);
                    // $('#'+a).val(data[key]);
                elem = $("#"+a);
                    if(elem.is('input')){
                        if(elem.hasClass('datepicker')){
                            parsedate = new Date(data[key]);
                            tdy = new Date(parsedate.getFullYear(), parsedate.getMonth(), parsedate.getDate());
                            elem.datepicker({
                                autoclose: true,
                                format: 'dd-mm-yyyy',
                                todayHighlight: true,
                                endDate : today,
                                weekStart: 1,
                            });
                            elem.datepicker('setDate', tdy);
                        }else{
                            elem.val(data[key]);
                        }
                    }else if(elem.is('select')){
                        elem.val(data[key]).trigger('change');
                    }else if(elem.is('textarea')){
                        elem.text(data[key])
                    }
                }   
            }
        });
        @endif

        // $("#pasangan").hide();
        // $("#ada_ijin_pasangan").prop('disabled', true);
        // $("#nama_pasangan").prop('disabled', true);
        // $("#agama_pasangan").prop('disabled', true);
        // $("#pernikahan_agama").prop('disabled', true);
        // $("#tempat_menikah").prop('disabled', true);
        // $("#datepicker4").prop('disabled', true);

        $('.select2').select2();

        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            endDate : today,
            weekStart: 1,
        });

        $('#tanggal_menikah').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            endDate: today,
            weekStart: 1,
        });

         $('#tanggal_baptis').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            endDate: today,
            weekStart: 1,
        });

        $('#datepicker1').datepicker('setDate', today);
        $('#datepicker2').datepicker('setDate', today);
        $('#datepicker5').datepicker('setDate', today);
        $('.timepicker').timepicker({
            //timePickerIncrement: 10,
            showMeridian: false,
            minuteStep :5,
        });

        $("#sudah_menikah").on("change", function(){
            var cek = $("#sudah_menikah option:selected").val();
            if(cek == "belum"){
                $("#pasangan").hide();
                $("#image1").prop('disabled', true);
            }else{
                $("#pasangan").show();
                $("#image1").prop('disabled', false);
            }
        });

        $("#label_image").hide();
        $("#label_image1").hide();
        $("#label_image2").hide();
    });

</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }

    function ValidateFileUpload1() {
        var fuData = document.getElementById('image1');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image1').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image1").val(filename);
                }
            }
            $("#label_image1").hide();
        } else {
            $('#image1').val('');
            $("#label_image1").show();
            return false;
        }
    }

    function ValidateFileUpload1() {
        var fuData = document.getElementById('image2');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image2').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image2").val(filename);
                }
            }
            $("#label_image2").hide();
        } else {
            $('#image2').val('');
            $("#label_image2").show();
            return false;
        }
    }
</script>
@endsection