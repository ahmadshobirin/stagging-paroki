@extends('app')

@section('title', 'Tahapan '.$header) @section('contentheader_title', 'Tahapan '.$header)

@section('treeview_post','active')
@section('treeview_tahapan_sakramen','active')
@if($type == "baptis_balita" || $type == "baptis_anak" || $type == "baptis_dewasa")
@section('treeview_tahapan_sakramen_baptis','active')
@endif
@section('treeview_tahapan_sakramen_'.$side,'active')

@section('customcss')
	<link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
    <div class="box box-primary">
        <div class="box-body">
            <div class="">
            </div>
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Tahapan</th>
                        <th>Nama Diri</th>
                        <th>Periode</th>
                        <th>Tanggal</th>
                        <th>Gambar</th>
                        <th>Upload</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1 ?>
                    @foreach($tahapan as $thp)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{ $thp->nama_tahapan }}</td>
                            @if($type != "nikah")
                            <td>{{ $thp->nama_diri }}</td>
                            <td>{{ date('d-m-Y', strtotime($thp->start_date))}} s/d {{ date('d-m-Y', strtotime($thp->end_date))}}</td>
                            @else
                            <td>{{ $thp->nama_diri }}</td>
                            <td>{{ date('d-m-Y', strtotime($thp->date_agenda))}}</td>
                            @endif
                            <td>{{ date('d-m-Y', strtotime($thp->tanggal_tahapan))}}</td>
                            <td>
                                @foreach($thp->image as $image)
                                <a href="{{ url('view-image/'.$image->image_tahapan) }}" target="_blank">{{ $image->image_tahapan }}</a>
                                <a href="{{url('/admin/tahapan-image-delete/'.$image->id)}}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah yakin menghapus data ini?')"><i class="fa fa-trash" ></i></a>
                                <br>
                                @endforeach
                            </td>
                            <td>
                                @if($thp->upload_file == "Ya")
                                  <form method="POST" action="{{ url('/admin/tahapan-image/'.$thp->id_tahapan_detail.'/'.$type) }}" enctype="multipart/form-data" onsubmit="return ValidateFileUpload(this)">
                                    {{csrf_field()}}
                                      <input type="file" name="image">
                                      <span>
                                      <input type="submit" class="btn btn-success btn-sm" value="Upload">
                                      </span>
                                  </form>
                                @else
                                  -
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('customscript')
	<script type="text/javascript" src="{{URL::asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

   <script type="text/javascript">
   $(document).ready(function(){
      $('#table').DataTable({
         'paging': true,
         'lengthChange': true,
         'searching': true,
         'ordering': true,
         'info': true,
         'autoWidth': false,
         "language": {
             "emptyTable": "Data Kosong",
             "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
             "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
             "infoFiltered": "(disaring dari _MAX_ total data)",
             "search": "Cari:",
             "lengthMenu": "Tampilkan _MENU_ Data",
             "zeroRecords": "Tidak Ada Data yang Ditampilkan",
             "oPaginate": {
                 "sFirst": "Awal",
                 "sLast": "Akhir",
                 "sNext": "Selanjutnya",
                 "sPrevious": "Sebelumnya"
             },
         },
      });
    });
  </script>
  <script type="text/javascript">
      function ValidateFileUpload(elem) {
        // console.log($(elem).children('input[name="image"]').val());
        var fuData = $(elem).children('input[name="image"]');
        var FileUploadPath = fuData.val();
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        // The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    fuData.attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    fuData.val(filename);
                }
            }
        } else {
            $(elem).children('input[name="image"]').val('');
            alert("Masukkan file yang akan di Upload")
            return false;
        }
    }
  </script>
@stop
