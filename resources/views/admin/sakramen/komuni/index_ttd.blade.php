@extends('app') 

@section('title', 'Sakramen Komuni') @section('contentheader_title', 'Sakramen Komuni')

@if(Auth::user()->role == 1 || Auth::user()->role == 2)
@section('treeview_posting','active')
@else
@section('treeview_post','active')
@endif
@section('treeview_ttd_sakramen','active')
@section('treeview_ttd_sakramen_komuni','active')

@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
    <div class="box box-primary">
        <div class="box-body">
            {{-- <div class="">
              <a href="{{url('admin/sakramen-create/komuni')}}" class="btn btn-success btn-md">
                  <i class="fa fa-plus"></i> Tambah Data
              </a>
            </div> --}}
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Status</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@stop

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

   <script type="text/javascript">
   $(document).ready(function(){    
      $('#table').DataTable({
         'paging': true,
         'lengthChange': true,
         'searching': true,
         'ordering': true,
         'info': true,
         'autoWidth': false,
         "language": {
             "emptyTable": "Data Kosong",
             "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
             "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
             "infoFiltered": "(disaring dari _MAX_ total data)",
             "search": "Cari:",
             "lengthMenu": "Tampilkan _MENU_ Data",
             "zeroRecords": "Tidak Ada Data yang Ditampilkan",
             "oPaginate": {
                 "sFirst": "Awal",
                 "sLast": "Akhir",
                 "sNext": "Selanjutnya",
                 "sPrevious": "Sebelumnya"
             },
         },

        processing: true,
        serverSide: true,
        ajax: "<?= url("/api/sakramen_ttd/komuni") ?>/"+"{{ Auth::user()->id }}",
        columns: [
            {data: 'DT_Row_Index', name: 'DT_Row_Index', searchable: false, orderable: false},
            {data: 'nama_diri', name: 'nama_diri'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false, visible: true}
        ]
      });
    });
  </script>
@stop
