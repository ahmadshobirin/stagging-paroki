@extends('app')

@section('treeview_post','active')

@section('treeview_ttd_sakramen','active')
@section('treeview_ttd_sakramen_baptis','active')
@section('treeview_ttd_sakramen_baptis_dewasa','active')

@section('title', 'Update Status Sakramen Baptis Dewasa')

@section('contentheader_title', 'Update Status Sakramen Baptis Dewasa')

@section('main-content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-body">
                @if($options['permission'] == true)
					<div class="form-group">
						<label for="">Umat</label>
						<div id="container" style="border:1px #d2d6de solid"></div>
					</div>
				@else
					<h3>Tidak punya akses</h3>
				@endif
			</div>

			@if($options['permission'] == true)
			<div class="box-footer">
				<div class="pull-right">
					<button class="btn btn-success btn-sm" id="save"><span class="fa fa-save"></span> Simpan</button>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
@endsection

@section('customscript')
<script src="https://unpkg.com/konva@3.4.1/konva.min.js"></script>
<script>
	var width = window.innerWidth;
	var height = window.innerHeight - 150;

	// first we need Konva core things: stage and layer
	var stage = new Konva.Stage({
		container: 'container',
		width: width,
		height: height
	});

	var layer = new Konva.Layer();
	stage.add(layer);

	// then we are going to draw into special canvas element
	var canvas = document.createElement('canvas');
	canvas.width = stage.width();
	canvas.height = stage.height();

	// created canvas we can add to layer as "Konva.Image" element
	var image = new Konva.Image({
		image: canvas,
		x: 0,
		y: 0
	});

	layer.add(image);
	stage.draw();

	// Good. Now we need to get access to context element
	var context = canvas.getContext('2d');
	context.strokeStyle = '#000000';
	context.lineJoin = 'round';
	context.lineWidth = 5;

	var isPaint = false;
	var lastPointerPosition;
	var mode = 'brush';

	image.on('mousedown touchstart', function () {
		isPaint = true;
		lastPointerPosition = stage.getPointerPosition();
	});

	// will it be better to listen move/end events on the window?

	stage.on('mouseup touchend', function () {
		isPaint = false;
	});

	// and core function - drawing
	stage.on('mousemove touchmove', function () {
		if (!isPaint) {
			return;
		}

		if (mode === 'brush') {
			context.globalCompositeOperation = 'source-over';
		}
		if (mode === 'eraser') {
			context.globalCompositeOperation = 'destination-out';
		}
		context.beginPath();

		var localPos = {
			x: lastPointerPosition.x - image.x(),
			y: lastPointerPosition.y - image.y()
		};
		context.moveTo(localPos.x, localPos.y);
		var pos = stage.getPointerPosition();
		localPos = {
			x: pos.x - image.x(),
			y: pos.y - image.y()
		};
		context.lineTo(localPos.x, localPos.y);
		context.closePath();
		context.stroke();

		lastPointerPosition = pos;
		layer.batchDraw();
	});

	$('#save').click(function () {
		$.ajax({
			url: "{{ url('sakramen-signature-ummat') }}",
			type: "POST",
			data: {
				id: "{{ $id }}",
				type: 'dewasa',
				umat: stage.toDataURL(),
				_token: "{{ csrf_token() }}"
			},
			success: function (results) {
				// console.log(results);
				if (results.success == true) {
					window.location = results.url
				}
			}
		})
	});
</script>
@endsection