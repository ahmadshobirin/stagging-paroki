@extends('app')

@if(Auth::user()->role != 4)
@section('treeview_posting','active')
@else
@section('treeview_post','active')
@endif
@section('treeview_sakramen','active')
@section('treeview_sakramen_baptis','active')

@section('title', 'Tambah Sakramen Baptis '.$type)

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@section('contentheader_title', 'Tambah Sakramen Baptis '.$type)

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ $url }}" enctype="multipart/form-data">
                @include('admin.displayerror')
                {{ csrf_field() }}
                @isset($editmode)
                @method('PUT')
                @else
                @method('POST')
                @endif

                <input type="hidden" name="type" value="{{$type}}">
                <div class="box-body" style="margin-left: 20px;">
                @if($type == "balita")
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Lingkungan  <span class="required">*</span></label>
                                <select class="form-control select2" id="id_kategori_user" name="id_kategori_user">
                                    @foreach($lingkungan as $ling)
                                    <option value="{{ $ling->id_kategori_user }}">{{ $ling->nama_kategori_user }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Periode <span class="required">*
                                </span></label>
                                <select class="form-control select2" required id="id_periode" name="id_periode">
                                    @foreach($periode as $prd)
                                    <option value="{{ $prd->id_periode }}">{{ $prd->nama_periode }} {{ date('d-m-Y', strtotime($prd->start_date)) }} s/d {{ date('d-m-Y', strtotime($prd->end_date)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap Ayah <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_ayah" id="nama_ayah" placeholder="Nama Lengkap Ayah" value="{{ old('nama_ayah') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap Ibu <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Lengkap Ibu" id="nama_ibu" value="{{ old('nama_ibu') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat <span class="required">*</span></label>
                                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="{{ old('alamat') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No. Telp <span class="required">*</span></label>
                                <input type="text" class="form-control telp" name="no_telp" placeholder="No. Telp" id="no_telp" value="{{ old('no_telp') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Email <span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" required id="email" placeholder="Email" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No ID K3</label>
                                <input type="text" class="form-control" name="no_id_k3" placeholder="No. ID K3" id="no_id_k3" value="{{ old('no_id_k3') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Anak <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_diri" id="nama_diri" placeholder="Nama Anak" value="{{ old('nama_diri') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Pemandian yang Dipilih <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_pemandian" placeholder="Nama Pemandian yang Dipilih" id="nama_pemandian" value="{{ old('nama_pemandian') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tempat Lahir Anak <span class="required">*</span></label>
                                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir Anak" value="{{ old('tempat_lahir') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tanggal Lahir Anak <span class="required">*
                                </span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="tanggal_lahir" class="form-control input-sm datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Bapak/Ibu Permandian <span class="required">* </span></label>
                                <input type="text" class="form-control" name="nama_wali_pemandian" id="nama_wali_pemandian" placeholder="Nama Bapak/Ibu Permandian" value="{{ old('nama_wali_pemandian') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Foto yang bersangkutan <span class="required">*</span></label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()" required>
                            </div>
                            <label id="label_image">Bukan file image</label>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Surat Baptis Wali <span class="required">*</span></label>
                                <input class="form-control" type="file" name="image1" id="image1" onchange="return ValidateFileUpload1()" required>
                            </div>
                            <label id="label_image1">Bukan file image</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Surat Nikah Orang Tua <span class="required">*</span></label>
                                <input class="form-control" type="file" name="image2" id="image2" onchange="return ValidateFileUpload2()" required>
                            </div>
                            <label id="label_image2">Bukan file image</label>
                        </div>
                    </div>


                @elseif($type == "anak")

                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Lingkungan  <span class="required">*</span></label>
                                <select class="form-control select2" id="id_kategori_user" name="id_kategori_user">
                                    @foreach($lingkungan as $ling)
                                    <option value="{{ $ling->id_kategori_user }}">{{ $ling->nama_kategori_user }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_diri" id="nama_diri" placeholder="Nama" value="{{ old('nama_diri') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tempat Lahir <span class="required">*</span></label>
                                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="{{ old('tempat_lahir') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tanggal Lahir <span class="required">*</span>
                                </label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="tanggal_lahir" class="form-control input-sm datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap Ayah <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_ayah" id="nama_ayah" placeholder="Nama Lengkap Ayah" value="{{ old('nama_ayah') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap Ibu <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Lengkap Ibu" id="nama_ibu" value="{{ old('nama_ibu') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Anda ada Ijin dari Orang Tua?</label>
                                <select class="form-control" name="ada_ijin_ortu" id="ada_ijin_ortu">
                                    <option value="ya">Ya</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Pernikahan Orang Tua secara Agama apa?</label>
                                <select class="form-control" name="pernikahan_agama_ortu" id="pernikahan_agama_ortu">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Konghucu">Konghucu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat <span class="required">*</span></label>
                                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="{{ old('alamat') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No. Telp <span class="required">*</span></label>
                                <input type="text" class="form-control telp" name="no_telp" placeholder="No. Telp" id="no_telp" value="{{ old('no_telp') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Asal Paroki <span class="required">*</span></label>
                                <input type="text" class="form-control" name="asal_paroki" id="asal_paroki" placeholder="Asal Paroki" value="{{ old('asal_paroki') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Pemandian yang Dipilih</label>
                                <input type="text" class="form-control" name="nama_pemandian" placeholder="Nama Pemandian yang Dipilih" id="nama_pemandian" value="{{ old('nama_pemandian') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Email <span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" required id="email" placeholder="Email" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No ID K3</label>
                                <input type="text" class="form-control" name="no_id_k3" placeholder="No. ID K3" id="no_id_k3" value="{{ old('no_id_k3') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Bapak/Ibu Permandian</label>
                                <input type="text" class="form-control" name="nama_wali_pemandian" id="nama_wali_pemandian" placeholder="Nama Bapak/Ibu Permandian" value="{{ old('nama_wali_pemandian') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                              <div class="form-group">
                                <label for="" class="control-label">Periode <span class="required">*
                                </span></label>
                                <select class="form-control select2" required id="id_periode" name="id_periode">
                                    @foreach($periode as $prd)
                                    <option value="{{ $prd->id_periode }}">{{ $prd->nama_periode }} {{ date('d-m-Y', strtotime($prd->start_date)) }} s/d {{ date('d-m-Y', strtotime($prd->end_date)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Merasa Mantap untuk dipermandikan?</label>
                                <select class="form-control" name="merasa_mantap" id="merasa_mantap">
                                    <option value="ya">Ya</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Surat Nikah Orang Tua <span class="required">*</span></label>
                                <input class="form-control" type="file" name="image2" id="image2" onchange="return ValidateFileUpload2()" required>
                            </div>
                            <label id="label_image2">Bukan file image</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Foto yang bersangkutan <span class="required">*</span></label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()" required>
                            </div>
                            <label id="label_image">Bukan file image</label>
                        </div>
                    </div>

                @elseif($type == "dewasa")

                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Lingkungan  <span class="required">*</span></label>
                                <select class="form-control select2" id="id_kategori_user" name="id_kategori_user">
                                    @foreach($lingkungan as $ling)
                                    <option value="{{ $ling->id_kategori_user }}">{{ $ling->nama_kategori_user }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_diri" id="nama_diri" placeholder="Nama" value="{{ old('nama_diri') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tempat Lahir <span class="required">*</span></label>
                                <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="{{ old('tempat_lahir') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tanggal Lahir <span class="required">*
                                </span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="tanggal_lahir" class="form-control input-sm datepicker" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap Ayah <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_ayah" id="nama_ayah" placeholder="Nama Lengkap Ayah" value="{{ old('nama_ayah') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Lengkap Ibu <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_ibu" placeholder="Nama Lengkap Ibu" id="nama_ibu" value="{{ old('nama_ibu') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Agama Ayah <span class="required">*</span></label>
                                <select class="form-control" name="agama_ayah" id="agama_ayah">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Konghucu">Konghucu</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Agama Ibu <span class="required">*</span></label>
                                <select class="form-control" name="agama_ibu" id="agama_ibu">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Konghucu">Konghucu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Anda ada Ijin dari Orang Tua?</label>
                                 <select class="form-control" name="ada_ijin_ortu" id="ada_ijin_ortu">
                                    <option value="ya">Ya</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Apakah sudah menikah?</label>
                                 <select class="form-control" name="sudah_menikah" id="sudah_menikah">
                                    <option selected value="belum">Belum</option>
                                    <option value="sudah">Sudah</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="pasangan">
                        <div class="row" >
                            <div class="col-md-4">
                                 <div class="form-group">
                                    <label for="" class="control-label">Anda ada Ijin dari Suami/Istri?</label>
                                     <select class="form-control" name="ada_ijin_pasangan" id="ada_ijin_pasangan">
                                        <option value="ya">Ya</option>
                                        <option value="tidak">Tidak</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="control-label">Nama Lengkap Suami/Istri</label>
                                    <input type="text" class="form-control" name="nama_pasangan" placeholder="Nama Lengkap Suami/Istri" id="nama_pasangan" value="{{ old('nama_pasangan') }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                 <div class="form-group">
                                    <label for="" class="control-label">Agama Suami/Istri</label>
                                    <select class="form-control" name="agama_pasangan" id="agama_pasangan">
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-4">
                                 <div class="form-group">
                                    <label for="" class="control-label">Pernikahan secara Agama apa?</label>
                                    <select class="form-control" name="pernikahan_agama" id="pernikahan_agama">
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Katolik">Katolik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="control-label">Tempat Menikah</label>
                                    <input type="text" class="form-control" name="tempat_pernikahan" placeholder="Tempat Menikah" id="tempat_pernikahan" value="{{ old('tempat_pernikahan') }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="control-label">Tanggal Menikah <span class="required">*
                                    </span></label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="tanggal_pernikahan" class="form-control input-sm datepicker" name="tanggal_pernikahan" value="{{ old('tanggal_pernikahan') }}" required readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Surat Nikah <span class="required">*
                                    </span></label>
                                    <input class="form-control" type="file" name="image3" id="image3" onchange="return ValidateFileUpload3()">
                                </div>
                                <label id="label_image3">Bukan file image</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Alasan Menjadi Katolik<span class="required">*
                                </span></label>
                                <textarea class="form-control" name="alasan_menjadi_katolik" id="alasan_menjadi_katolik" placeholder="Alasan Menjadi Katolik" required>{{ old('alasan_menjadi_katolik') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat<span class="required">*
                                </span></label>
                                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="{{ old('alamat') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No. Telp<span class="required">*
                                </span></label>
                                <input type="text" class="form-control telp" name="no_telp" placeholder="No. Telp" id="no_telp" value="{{ old('no_telp') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat Orang Tua <span class="required">*
                                </span></label>
                                <input type="text" class="form-control" name="alamat_ortu" id="alamat_ortu" placeholder="Alamat Orang Tua" value="{{ old('alamat_ortu') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No. Telp Orang Tua <span class="required">*
                                </span></label>
                                <input type="text" class="form-control telp" name="no_telp_ortu" placeholder="No. Telp Orang Tua" id="no_telp_ortu" value="{{ old('no_telp_ortu') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Apakah di lingkungan Anda ada keluarga Katolik yang lain?</label>
                                 <select class="form-control" name="lingkungan_katolik" id="lingkungan_katolik">
                                    <option value="ya">Ya</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Apakah dalam keluarga Anda ada yang beragama Katolik?</label>
                                 <select class="form-control" name="keluarga_katolik" id="keluarga_katolik">
                                    <option value="ada">Ada</option>
                                    <option value="tidak ada">Tidak Ada</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Asal Paroki<span class="required">*
                                </span></label>
                                <input type="text" class="form-control" name="asal_paroki" id="asal_paroki" placeholder="Asal Paroki" value="{{ old('asal_paroki') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Pemandian yang Dipilih <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_pemandian" placeholder="Nama Pemandian yang Dipilih" id="nama_pemandian" value="{{ old('nama_pemandian') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Email <span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" required id="email" placeholder="Email" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">No ID K3</label>
                                <input type="text" class="form-control" name="no_id_k3" placeholder="No. ID K3" id="no_id_k3" value="{{ old('no_id_k3') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Bapak/Ibu Permandian</label>
                                <input type="text" class="form-control" name="nama_wali_pemandian" id="nama_wali_pemandian" placeholder="Nama Bapak/Ibu Permandian" value="{{ old('nama_wali_pemandian') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                              <div class="form-group">
                                <label for="" class="control-label">Periode <span class="required">*
                                </span></label>
                                <select class="form-control select2" required id="id_periode" name="id_periode">
                                    @foreach($periode as $prd)
                                    <option value="{{ $prd->id_periode }}">{{ $prd->nama_periode }} {{ date('d-m-Y', strtotime($prd->start_date)) }} s/d {{ date('d-m-Y', strtotime($prd->end_date)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Merasa Mantap untuk dipermandikan?</label>
                                <select class="form-control" name="merasa_mantap" id="merasa_mantap">
                                    <option value="ya">Ya</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Sebelumnya Agama apa yang Anda anut?</label>
                                <select class="form-control" name="agama_sebelumnya" id="agama_sebelumnya">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Simpatisan Katolik">Simpatisan Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    <option value="Konghucu">Konghucu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Foto yang bersangkutan <span class="required">*
                                </span></label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()" required>
                            </div>
                            <label id="label_image">Bukan file image</label>
                        </div>
                    </div>
                @endif
                </div>

                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/sakramen/baptis/'.$type) }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $(document).ready(function () {

        $("#pasangan").hide();
        $("#ada_ijin_pasangan").prop('disabled', true);
        $("#nama_pasangan").prop('disabled', true);
        $("#agama_pasangan").prop('disabled', true);
        $("#pernikahan_agama").prop('disabled', true);
        $("#tempat_pernikahan").prop('disabled', true);
        $("#tanggal_pernikahan").prop('disabled', true);
        $("#image3").prop('disabled', true);

        @isset($editmode)
        var data = {!! $baptis !!};
        $('.form-control').each(function (index, value){
            var a = $(this).attr('id');
            for (var key in data) {
                if(a == key){
                //console.log(key+" "+data[key]);
                    // $('#'+a).val(data[key]);
                elem = $("#"+a);
                    if(elem.is('input')){
                        if(elem.hasClass('datepicker')){
                            parsedate = new Date(data[key]);
                            tdy = new Date(parsedate.getFullYear(), parsedate.getMonth(), parsedate.getDate());
                            elem.datepicker({
                                autoclose: true,
                                format: 'dd-mm-yyyy',
                                todayHighlight: true,
                                endDate : today,
                                weekStart: 1,
                            });
                            elem.datepicker('setDate', tdy);
                        }else{
                            elem.val(data[key]);
                        }
                    }else if(elem.is('select')){
                        elem.val(data[key]).trigger('change');
                    }else if(elem.is('textarea')){
                        elem.text(data[key])
                    }
                }
            }
        });
        @endif
        // $("#pasangan").hide();
        // $("#ada_ijin_pasangan").prop('disabled', true);
        // $("#nama_pasangan").prop('disabled', true);
        // $("#agama_pasangan").prop('disabled', true);
        // $("#pernikahan_agama").prop('disabled', true);
        // $("#tempat_menikah").prop('disabled', true);
        // $("#datepicker4").prop('disabled', true);

        $('.select2').select2();

        $('#datepicker1').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });

        $('#datepicker2').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });

        $('#datepicker3').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });

        $('#tanggal_lahir').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            endDate : today,
            weekStart: 1,
        });

        $('#tanggal_pernikahan').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            endDate: today,
            weekStart: 1,
        });

        $('#datepicker5').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            weekStart: 1,
        });

        $('#datepicker1').datepicker('setDate', today);
        $('#datepicker2').datepicker('setDate', today);
        $('#datepicker5').datepicker('setDate', today);
        $('.timepicker').timepicker({
            //timePickerIncrement: 10,
            showMeridian: false,
            minuteStep :5,
        });

        $("#label_image").hide();
        $("#label_image1").hide();
        $("#label_image2").hide();
        $("#label_image3").hide();

    });

    $("#sudah_menikah").on("change", function(){
        var cek = $("#sudah_menikah option:selected").val();
        if(cek == "belum"){
            $("#pasangan").hide();
            $("#ada_ijin_pasangan").prop('disabled', true);
            $("#nama_pasangan").prop('disabled', true);
            $("#agama_pasangan").prop('disabled', true);
            $("#pernikahan_agama").prop('disabled', true);
            $("#tempat_pernikahan").prop('disabled', true);
            $("#tanggal_pernikahan").prop('disabled', true);
            $("#image3").prop('disabled', true);
            $("#image3").prop('required', false);
        }else{
            $("#pasangan").show();
            $("#ada_ijin_pasangan").prop('disabled', false);
            $("#nama_pasangan").prop('disabled', false);
            $("#agama_pasangan").prop('disabled', false);
            $("#pernikahan_agama").prop('disabled', false);
            $("#tempat_pernikahan").prop('disabled', false);
            $("#tanggal_pernikahan").prop('disabled', false);
            $("#image3").prop('disabled', false);
            $("#image3").prop('required', true);
        }
    });
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }

    function ValidateFileUpload1() {
        var fuData = document.getElementById('image1');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image1').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image1").val(filename);
                }
            }
            $("#label_image1").hide();
        } else {
            $('#image1').val('');
            $("#label_image1").show();
            return false;
        }
    }

    function ValidateFileUpload2() {
        var fuData = document.getElementById('image2');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image2').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image2").val(filename);
                }
            }
            $("#label_image2").hide();
        } else {
            $('#image2').val('');
            $("#label_image2").show();
            return false;
        }
    }


    function ValidateFileUpload3() {
        var fuData = document.getElementById('image3');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image2').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image3").val(filename);
                }
            }
            $("#label_image3").hide();
        } else {
            $('#image3').val('');
            $("#label_image3").show();
            return false;
        }
    }
</script>
@endsection