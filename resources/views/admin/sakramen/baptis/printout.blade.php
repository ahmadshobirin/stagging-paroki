<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sakramen Baptis - {{ $sakramen->nama_diri }}</title>
    <style>
        body {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            padding: 6px;
        }

        table {
            margin-top: 8px;
            border-collapse: collapse;
            width: 100%;
        }

        table td,
        table th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        table tr:hover {
            background-color: #ddd;
        }

        table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            color: black;
            /* background-color: #4CAF50; */
        }
    </style>
</head>

<body>
    {{-- {{ public_path('/bootstrap/css/bootstrap.min.css') }}<br>
    {{ asset('/bootstrap/css/bootstrap.min.css') }}<br>
    {{ URL::asset('/bootstrap/css/bootstrap.min.css') }}<br> --}}
    @if ( $sakramen->type == "balita" )

    @elseif($sakramen->type == "anak")

    @elseif($sakramen->type == "dewasa")

    @endif

    <h3 align="center">{{ ucwords($sakramen->judul) }}</h3>
    <br>
    <b>1. Latar Belakang Masalah</b>
    <p>{!! $sakramen->latar_belakang !!}</p>
    <br>
    <b>2. Nama Kegiatan</b>
    <p>{!! $sakramen->latar_belakang !!}</p>
    <br>
    <b>3. Subjek Sasaran</b>
    <p>{!! $sakramen->subjek_sasaran !!}</p>
    <br>
    <b>4. Tujuan Kegiatan</b>
    <p>{!! $sakramen->tujuan_kegiatan !!}</p>
    <br>
    <b>5. Bentuk Kegiatan</b>
    <p>{!! $sakramen->bentuk_kegiatan !!}</p>
    <br>
    <b>6. Pelaksanaan</b>
    <p>
        {{ $sakramen->temp_agenda[0]->name }}
        @if($sakramen->temp_agenda[0]->place_id != null)
            {{$sakramen->temp_agenda[0]->place->nama_tempat}}
        @else
            {{$proposal->temp_agenda->other_place}}
        @endif
    </p>
    <table>
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Hari Libur</th>
            </tr>
        </thead>
        <tbody>
            @foreach($proposal->temp_agenda as $ag)
                <tr>
                    <td>{{ \Carbon\Carbon::parse($ag->date)->format('d-m-Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($ag->time_start)->format('H:i') }}</td>
                    <td>{{ \Carbon\Carbon::parse($ag->time_end)->format('H:i') }}</td>
                    <td>{!! ($ag->holiday == true) ? "<span class=\"fa fa-check\"></span>" : '-' !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <b>7. Susunan Acara</b>
    <p>{!! $proposal->susunan_acara !!}</p>
    <br>
    <b>8. Susunan Panitia</b>
    <p>{!! $proposal->susunan_panitia !!}</p>
    <b>9. Pemasukan Anggaran</b>
    <table>
        <thead>
            <tr>
                <th>Pemasukan</th>
                <th>Anggaran</th>
            </tr>
        </thead>
        <tbody>
            @foreach($proposal->budget_in as $in)
                <tr>
                    <td>{{ ucfirst($in->pemasukan) }}</td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($in->pemasukan_total,0,'.','.') }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <b>10. Pengeluaran Anggaran</b>
    <table>
        <thead>
            <tr>
                <th>Pengeluaran</th>
                <th>Anggaran</th>
                <th>Rincian Anggaran</th>
                <th>Total Anggaran</th>
            </tr>
        </thead>
        <tbody>
            @foreach($proposal->budget_out as $out)
                <tr>
                    <td>{{ ucfirst($out->pengeluaran) }}</td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($out->pengeluaran_price,0,'.','.') }}
                    </td>
                    <td style="text-align:right">
                        {{ $out->pengeluaran_qty }}
                    </td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($out->pengeluaran_total,0,'.','.') }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <b>11. Penutup</b>
    <p>{!! $proposal->penutup !!}</p>
    @endif

    @if($proposal->signature()->exists())
        @foreach($proposal->signature->chunk(2) as $signature)
            @foreach($signature as $sig)
                @if($sig->signature_path != null)
                    <div style="">
                            <img style="width:120px; height:120px" src="{{ public_path($sig->signature_path) }}" alt="">
                            <br>
                            <label for="">{{ $sig->username }}</label>
                    </div>
                @endif
            @endforeach
        @endforeach
    @endif
</body>

</html>