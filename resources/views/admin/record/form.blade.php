@extends('app')

@section('treeview_post','active')
@section('treeview_agenda','active')
@section('treeview_agenda_pernikahan','active')

@section('title', 'Tambah Agenda Pernikahan Paroki')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@section('contentheader_title', 'Tambah Agenda Pernikahan Paroki')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/kegiatan-nikah/') }}" enctype="multipart/form-data" onsubmit="return CekWaktu();">
                @include('admin.displayerror')
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Agenda  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Judul" value="Pernikahan">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tanggal Kanonik</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" readonly class="form-control" name="tgl_kanonik" id="datepicker2" value="{{ old('tgl_kanonik') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Romo Kanonik</label>
                                <input type="text" class="form-control" name="romo_kanonik" placeholder="Romo Kanonik" id="romo_kanonik" value="{{ old('romo_kanonik') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Memberkati Perkawinan</label>
                                <input type="text" class="form-control" name="berkat_kawin" placeholder="Yang Memberkati Perkawinan" value="{{ old('berkat_kawin') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Keterangan</label>
                                <textarea class="form-control" name="keterangan">{{ old('keterangan') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/kegiatan-nikah') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $(document).ready(function () {
        $('.select2').select2();
        $('.datepicker2').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });
</script>
@endsection