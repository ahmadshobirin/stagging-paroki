@extends('app')

@section('htmlheader_title')
{{ trans('adminlte_lang::message.home') }}
@endsection
@section('contentheader_title', 'Beranda')
<?php $no=1; $roleUser = \DB::table('m_role')->where('id',Auth::user()->role)->first();?>
@section('main-content')
<div class="container-fluid spark-screen">
	<div class="row">
		<section class="content">
			<div class="row">
				@php $arrX = array("bg-aqua", "bg-green", "bg-yellow","bg-red"); @endphp
				@foreach ($datas as $key => $val)
					<div class="col-lg-3 col-xs-6">
						<div class="small-box {{ $arrX[array_rand($arrX)] }}">
							<div class="inner">
								<h3 id="countInfo">{{ $val }}</h3>
							{{-- <p>{{ucfirst($key)}} Waiting List <br>(Butuh Persetujuan)</p> --}}
							<p>{{ucfirst($key)}} Waiting List</p>
							</div>
							<div class="icon">
								<i class="fa fa-paper-o"></i>
							</div>
							{{-- @if($roleUser->id < 3 )
								<a href="{{ url('admin/approval/info') }}"
									class="small-box-footer">Lihat Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
							@endif --}}
						</div>
					</div>
				@endforeach
			</div>
		</section>
	</div>
</div>
@endsection

@section('customscript')
<script>
	function doSomething() {
		var userId = "{{Auth::user()->id}}"
		$.ajax({
			url: "{{ url('admin/get-new-count') }}/"+userId,
			method: 'get',
			success: function (result) {
				$('#countInfo').text(result.waitinglistInfo);
				$('#countArtikel').text(result.waitinglistArtikel);
			}
		});
	}

	setInterval(doSomething, 3000); // Time in milliseconds
</script>
@endsection