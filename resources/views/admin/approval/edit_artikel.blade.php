@extends('app')

@section('treeview_posting','active')
@section('treeview_artikel','active')

@if($type == "show")
@section('title', 'Lihat Artikel Paroki Bidang')
@else
@section('title', 'Ubah Artikel Paroki Bidang')
@endif

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@if($type == "show")
@section('contentheader_title', 'Lihat Artikel Paroki Bidang')
@else
@section('contentheader_title', 'Ubah Artikel Paroki Bidang')
@endif

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/approval/artikel/'.$cek_post->id_posting) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Artikel <span class="required">*</span></label>
                                @if($type == "show")
                                <textarea disabled name="artikel" id="artikel" class="form-control" placeholder="artikel">{!!  strip_tags($cek_post->isi_post) !!}</textarea>
                                @else
                                <textarea name="artikel" id="artikel" class="form-control tinymce" placeholder="artikel">{{ $cek_post->isi_post }}</textarea>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if($cek_post->published == 1)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Info Published <span class="required">*</span></label>
                                @if($type == "show")
                                <textarea disabled maxlength="200" name="info_publish" id="info_publish" class="form-control" placeholder="Info(max 200 characters)">{!! strip_tags($cek_post->isi_publish_post) !!}</textarea> 
                                @else
                                <textarea  maxlength="200" name="info_publish" id="info_publish" class="form-control tinymce" placeholder="Info(max 200 characters)">{{ $cek_post->isi_publish_post }}</textarea>
                                @endif 
                                
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/approval/artikel') }}" class="btn btn-info">Kembali</a>
                        @if($type == "edit")
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection