@extends('app')

@section('treeview_post','active')
@section('treeview_proposal','active')

@section('title', 'Proposal')
@section('contentheader_title', 'Proposal')

@section('customcss')
<link rel="stylesheet" href="{{ URL::asset('css/datatables.min.css') }}">
@stop

    @section('main-content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Proposal {{ ($type == 'submission') ? 'Pengajuan' : '' }}</h3>
            <div class="box-tools pull-right">
                @if(Auth::user()->role == 3)
                    @php $katUser = \DB::table('m_kategori_user')->where('id_kategori_user',Auth::user()->id_header_user)->first(); @endphp
                        @if(!in_array($katUser->type,['romo','wilayah','bidang']))
                            <a href="{{ route('proposal.create') }}" class="btn btn-success btn-sm">
                                <i class="fa fa-plus"></i> Tambah Data
                            </a>

                            <a href="{{ route('proposal.upload') }}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-up"></i> Upload
                            </a>
                        @endif
                @else
                    <a href="{{ route('proposal.create') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i> Tambah Data
                    </a>

                    <a href="{{ route('proposal.upload') }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-arrow-up"></i> Upload
                    </a>
                @endif

                <a href="{{ route('proposal.index') }}" class="btn btn-default btn-sm">
                    <i class="fa fa-list"></i> Proposal
                </a>

                <a href="{{ route('proposal.submission','submission') }}" class="btn btn-warning btn-sm">
                    <i class="fa fa-signature"></i> Pengajuan
                </a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-striped table-hover table-responsive" id="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Judul</th>
                        <th>Dibuat</th>
                        <th>Tanggal</th>
                        <th>Tipe</th>
                        <th>Status</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    @endsection

    @section('customscript')
    <script type="text/javascript"
        src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        var user = "{{ Auth::user()->id }}";

        $(document).ready(function () {
            $('#table').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                "language": {
                    "emptyTable": "Data Kosong",
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "search": "Cari:",
                    "lengthMenu": "Tampilkan _MENU_ Data",
                    "zeroRecords": "Tidak Ada Data yang Ditampilkan",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sLast": "Akhir",
                        "sNext": "Selanjutnya",
                        "sPrevious": "Sebelumnya"
                    },
                },

                processing: true,
                serverSide: true,
                ajax: '<?= url("/api/proposal") ?>/'+user+"/{{$type}}",
                columns: [
                    { data: 'DT_Row_Index', name: 'DT_Row_Index',searchable: false, orderable: false },
                    { data: 'judul', name: 'proposal.judul' },
                    { data: 'createuser', name: 'proposal.createuser' },
                    { data: 'created_at', name: 'proposal.created_at' },
                    { data: 'type', name: 'proposal.type' },
                    { data: 'status', name: 'proposal.status' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });

            $('body').on('click', '.upstatus', function(e){
                e.preventDefault();
                var url = $(this).attr('href');
                $.ajax({
                    'url' : url,
                    'type' : 'get',
                    success : function(results){
                        // console.log(results);
                        location.reload();
                    },
                    error : function(xhr){
                        alert(xhr.responseJSON.data.description);
                    }
                });
            });
        });
    </script>
    @stop