@extends('app')

@section('treeview_post','active')
@section('treeview_proposal','active')

@section('title', 'Note Proposal')

@section('contentheader_title', 'Note Proposal')

@section('main-content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <form action="{{ route('proposal.note',$id) }}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Keterangan</label>
                            <textarea name="note" class="form-control input-sm">{{$pro->note}}</textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <button class="btn btn-success btn-sm" id="save"><span class="fa fa-save"></span> Simpan</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@endsection