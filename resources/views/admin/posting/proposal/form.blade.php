@extends('app')

@section('treeview_post','active')
@section('treeview_proposal','active')

@section('title', 'Tambah Proposal')

@section('customcss')
<link rel="stylesheet" href="{{ URL::asset('css/datatables.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet"
    href="{{ URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
<link href="{{ asset('plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ URL::asset('/plugins/select2/select2.min.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
@stop

    @section('contentheader_title', 'Tambah Proposal')

    @section('main-content')
    <div class="row">
        <div class="col-md-12">
            <!-- form start -->
            <form method="post" enctype="multipart/form-data"
                action="@if(!isset($editmode)){{ route('proposal.store') }}@else{{ route('proposal.update',$id) }}@endif">
                <!-- /.box-header -->
                <div class="box box-primary">
                    {{ csrf_field() }} @if(!isset($editmode)) @method('POST') @else @method('PUT') @endif
                    <div class="box-body" style="margin-left: 20px;">

                        @include('admin.displayerror')

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Nomor Proposal <span class="required">*</span></label>
                                <input type="text"
                                    class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }} input-sm"
                                    name="no_proposal" id="no_proposal" value="{{ old('judul') }}"
                                    placeholder="No Proposal..." required>
                                @if($errors->has('no_proposal'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('no_proposal') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Nomor Rekening</label>
                                <input type="text"
                                    class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }} input-sm"
                                    name="no_rekening" id="no_rekening" value="{{ old('judul') }}"
                                    placeholder="Nomor Rekening...">
                                @if($errors->has('no_rekening'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('no_rekening') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Judul Proposal <span class="required">*</span></label>
                                <input type="text"
                                    class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }} input-sm"
                                    name="judul" id="judul" value="{{ old('judul') }}"
                                    placeholder="judul..." required>
                                @if($errors->has('judul'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('judul') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Nama Kegiatan <span class="required">*</span></label>
                                <input type="text"
                                    class="form-control{{ $errors->has('nama_kegiatan') ? ' is-invalid' : '' }} input-sm"
                                    name="nama_kegiatan" id="nama_kegiatan"
                                    value="{{ old('nama_kegiatan') }}" placeholder="kegiatan...">
                                @if($errors->has('nama_kegiatan'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nama_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Latar Belakang <span class="required">*</span></label>
                                <textarea name="latar_belakang" id="latar_belakang"
                                    class="form-control{{ $errors->has('latar_belakang') ? ' is-invalid' : '' }} summernote">{{ old('latar_belakang') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('latar_belakang') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Tujuan Kegiatan <span class="required">*</span></label>
                                <textarea name="tujuan_kegiatan" id="tujuan_kegiatan"
                                    class="form-control{{ $errors->has('tujuan_kegiatan') ? ' is-invalid' : '' }} summernote">{{ old('tujuan_kegiatan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tujuan_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Bentuk Kegiatan <span class="required">*</span></label>
                                <textarea name="bentuk_kegiatan" id="bentuk_kegiatan"
                                    class="form-control{{ $errors->has('bentuk_kegiatan') ? ' is-invalid' : '' }} summernote">{{ old('bentuk_kegiatan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bentuk_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Subjek Sasaran <span class="required">*</span></label>
                                <textarea name="subjek_sasaran" id="subjek_sasaran"
                                    class="form-control{{ $errors->has('subjek_sasaran') ? ' is-invalid' : '' }} summernote">{{ old('subjek_sasaran') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('subjek_sasaran') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Indikator Keberhasilan <span class="required">*</span></label>
                                <textarea name="indikator_keberhasilan" id="indikator_keberhasilan"
                                    class="form-control{{ $errors->has('indikator_keberhasilan') ? ' is-invalid' : '' }} summernote">{{ old('indikator_keberhasilan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('indikator_keberhasilan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            {{-- <div class="form-group col-md-6">
                                <label for="">Pelaksanaan <span class="required">*</span></label>
                                <textarea name="pelaksanaan" id="pelaksanaan"
                                    class="form-control{{ $errors->has('pelaksanaan') ? ' is-invalid' : '' }} summernote">{{ old('pelaksanaan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pelaksanaan') }}</strong>
                                    </span>
                                @endif
                            </div> --}}

                            <div class="form-group col-md-6">
                                <label for="">Susunan Acara <span class="required">*</span></label>
                                <textarea name="susunan_acara" id="susunan_acara"
                                    class="form-control{{ $errors->has('susunan_acara') ? ' is-invalid' : '' }} summernote">{{ old('susunan_acara') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('susunan_acara') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Susunan Kepanitiaan <span class="required">*</span></label>
                                <textarea name="susunan_panitia" id="susunan_panitia"
                                    class="form-control{{ $errors->has('susunan_panitia') ? ' is-invalid' : '' }} summernote">{{ old('susunan_panitia') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('susunan_panitia') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Penutup <span class="required">*</span></label>
                                <textarea name="penutup" id="penutup"
                                    class="form-control{{ $errors->has('penutup') ? ' is-invalid' : '' }} summernote">{{ old('penutup') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('penutup') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Rencana Agenda</label>
                                <hr/>
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="control-label">Nama Agenda  <span class="required">*</span></label>
                                            <input type="text" name="nameagenda" id="nameagenda" class="form-control input-sm" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="control-label">Tempat Agenda  <span class="required">*</span></label>
                                            <select  class="form-control select2" id="tempat" style="width: 100%;" name="place_id" required>
                                                <option selected="" value="" disabled="">Pilih Tempat...</option>
                                                @foreach($tempat as $tmpt)
                                                    <option value="{{ $tmpt->id_tempat }}">{{ $tmpt->nama_tempat }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">di Luar Paroki?
                                                <input type="checkbox" id="check_other_place"> Ya
                                            </label>
                                            <input type="text" name="other_place" id="other_place" class="form-control input-sm" readonly>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <table class="table table-bordered" id="table-rencana">
                                    <thead>
                                        <tr>
                                            <th>
                                                <button type="button" class="btn btn-xs btn-success"
                                                    title="Tambah Data Pengeluaran" onclick="addRow('table-rencana')">
                                                    <span class="fa fa-plus"></span>
                                                </button>
                                            </th>
                                            <th>Tanggal</th>
                                            <th>Waktu Mulai</th>
                                            <th>Waktu Selesai</th>
                                            <th>Hari Libur</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <hr/>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Rencana Anggaran</label>
                                <table class="table table-bordered" id="table-pemasukan">
                                    <thead>
                                        <th>
                                            <button type="button" class="btn btn-xs btn-success"
                                                title="Tambah Data Pengeluaran" onclick="addRow('table-pemasukan')">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </th>
                                        <th>Pemasukan</th>
                                        <th>Total</th>
                                    </thead>
                                    <tbody>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tbody>
                                    <tfoot>
                                        {{-- <td></td> --}}
                                        <td colspan="2" style="text-align:right">Jumlah : </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="jmlpem" readonly
                                                value="0" style="text-align:right">
                                        </td>
                                    </tfoot>
                                </table>

                                <table class="table table-bordered" id="table-pengeluaran">
                                    <thead>
                                        <th>
                                            <button type="button" class="btn btn-xs btn-success"
                                                title="Tambah Data Pengeluaran" onclick="addRow('table-pengeluaran')">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </th>
                                        <th>Pengeluaran</th>
                                        <th>Nominal</th>
                                        <th width="15%">Rincian</th>
                                        <th>Total</th>
                                    </thead>
                                    <tbody>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tbody>
                                    <tfoot>
                                        <td style="text-align:right" colspan="4">Jumlah : </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="jmlpeng" readonly
                                                value="0" style="text-align:right">
                                        </td>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Laporan Keuangan <span class=""></span></label>
                                <input type="file" name="lapkeu" id="" class="form-control input-sm">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Program Kerja <span class="required">*</span></label>
                                <input type="file" name="proker" id="" class="form-control input-sm" required>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <a href="{{ url('/admin/proposal') }}" class="btn btn-info">Kembali</a>
                            <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endsection
    @section('customscript')
    <script type="text/javascript"
        src="{{ URL::asset('/plugins/select2/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/plugins/mask/mask.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/bootstrap-datepicker.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script type="text/javascript">
        $(".select2").select2();
        $('.price').mask('000.000.000', {
            reverse: true
        });

        $('.summernote').summernote({
            height: 150,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'strikethrough']],
                ['fontsize', ['fontsize', 'height', 'hr']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
            ],
        });

        $('#check_other_place').change(function(){
            if ($(this).is(':checked')) {
                $('#other_place').attr('readonly',false);
                $('#other_place').attr('required',true);
                $('#tempat').attr('disabled',true);
                $('#tempat').attr('required',false);
            }else{
                $('#tempat').attr('disabled',false);
                $('#other_place').attr('required',false);
                $('#other_place').attr('readonly',true);
                $('#other_place').attr('required',false);
            }
        })

        @isset($editmode)
            var data = {!! $data !!};

            $('#no_proposal').val(data.no_proposal);
            $('#no_rekening').val(data.no_rekening);
            $('#judul').val(data.judul);
            $('#nama_kegiatan').val(data.nama_kegiatan);
            $('#latar_belakang').summernote('code', data.latar_belakang);
            $('#tujuan_kegiatan').summernote('code', data.tujuan_kegiatan);
            $('#bentuk_kegiatan').summernote('code', data.bentuk_kegiatan);
            $('#subjek_sasaran').summernote('code', data.subjek_sasaran);
            $('#indikator_keberhasilan').summernote('code', data.indikator_keberhasilan);
            $('#pelaksanaan').summernote('code', data.pelaksanaan);
            $('#susunan_acara').summernote('code', data.susunan_acara);
            $('#susunan_panitia').summernote('code', data.susunan_acara);
            $('#penutup').summernote('code', data.penutup);

            $('#nameagenda').val(data.temp_agenda[0].name);
            $("#tempat option[value="+data.temp_agenda[0].place_id+"]").attr('selected', 'selected');
            $('#tempat').select2().trigger('change');

            $('.summernote').summernote({
                height: 150,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'strikethrough']],
                    ['fontsize', ['fontsize', 'height', 'hr']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                ],
            });

            $.each(data.budget_in, function (k, v) {
                $('#table-pemasukan tbody').append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger in' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pemasukan[]' value='" + v
                    .pemasukan + "'></td>" +
                    "<td><input type='text' class='form-control input-sm price' name='pemasukan_total[]' value='" +
                    fixDec(v.pemasukan_total) + "' onkeyup='jml()' style='text-align:right'></td>" +
                    "</tr>"
                );

            });

            $.each(data.budget_out, function (k, v) {
                $('#table-pengeluaran tbody').append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger out' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pengeluaran[]' value='" + v
                    .pengeluaran + "'> </td>" +
                    "<td><input type='text' class='form-control input-sm price nominal' name='pengeluaran_price[]' value='" +
                    fixDec(v.pengeluaran_price) + "' onkeyup='sub(this)' style='text-align:right'> </td> " +
                    "<td><input type='text' class='form-control input-sm price qty' name='pengeluaran_qty[]' value='" +
                    v.pengeluaran_qty + "' onkeyup='sub(this)' style='text-align:right'> </td> " +
                    "<td><input type='text' class='form-control input-sm total' name='pengeluaran_total[]' value='" +
                    fixDec(v.pengeluaran_total) + "'readonly style='text-align:right'> </td>" +
                    "</tr>"
                );
            });

            $.each(data.temp_agenda,function(k,v){

                selected = (v.holiday == true) ? "" : "selected";

                $("#table-rencana tbody").append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger renc' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm gen-date' name='date[]' value='"+setDate(v.date)+"'> </td>" +
                    "<td><input type='text' class='form-control input-sm gen-time' name='time_start[]' value='"+v.time_start+"'> </td> " +
                    "<td><input type='text' class='form-control input-sm gen-time' name='time_end[]' value='"+v.time_start+"'> </td> " +
                    "<td><select class='form-control input-sm' name='holiday[]'>"+
                    "<option value='1' "+selected+">Ya</option><option value='0' "+selected+">Tidak</option></select></td>" +
                    "</tr>"
                );
            });

            jml();
            jmlpeng();
            initFieldClass();
        @endif

        function addRow(table, data = null) {
            if (table == 'table-pemasukan') {
                return $("#" + table + " tbody").append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger in' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pemasukan[]' value=''></td>" +
                    "<td><input type='text' class='form-control input-sm price' name='pemasukan_total[]' value='' onkeyup='jml()' style='text-align:right'></td>" +
                    "</tr>"
                );
            } else if (table == 'table-pengeluaran') {
                return $("#" + table + " tbody").append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger out' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pengeluaran[]' value=''> </td>" +
                    "<td><input type='text' class='form-control input-sm price nominal' name='pengeluaran_price[]' value='' onkeyup='sub(this)' style='text-align:right'> </td> " +
                    "<td><input type='text' class='form-control input-sm price qty' name='pengeluaran_qty[]' value='1' onkeyup='sub(this)' style='text-align:right'> </td> " +
                    "<td><input type='text' class='form-control input-sm total' name='pengeluaran_total[]' value=''readonly style='text-align:right'> </td>" +
                    "</tr>"
                );
            } else if (table == 'table-rencana') {
                $("#" + table + " tbody ").append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger renc' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm gen-date' name='date[]' value=''> </td>" +
                    "<td><input type='text' class='form-control input-sm gen-time' name='time_start[]' value=''> </td> " +
                    "<td><input type='text' class='form-control input-sm gen-time' name='time_end[]' value=''> </td> " +
                    "<td><select class='form-control input-sm' name='holiday[]'>"+
                    "<option value='1' selected>Ya</option><option value='0'>Tidak</option></select></td>" +
                    "</tr>"
                );

                initFieldClass();
            }
        }

        function jml(price = 0) {
            $('#table-pemasukan tr input.price').each(function () {
                price += toNumber($(this).val());
            });
            $('#jmlpem').val(toCurrency(price))
        }

        function jmlpeng(price = 0) {
            $('#table-pengeluaran tr input.total').each(function () {
                price += toNumber($(this).val());
            });

            $('#jmlpeng').val(toCurrency(price));
        }

        function sub(elem) {
            nominal = toNumber($(elem).closest('tr').find('td').find('.nominal').val());
            qty = toNumber($(elem).closest('tr').find('td').find('.qty').val());
            total = nominal * qty;
            $(elem).closest('tr').find('td').find('.total').val(toCurrency(total));

            jmlpeng();
        }

        function del(elem, type = null) {
            if (confirm('Hapus?')) {
                elem.closest('tr').remove();

                if ($(elem).hasClass('in')) {
                    jml()
                }else if($(elem).hasClass('out')) {
                    total()
                } else {
                    return true;
                }
            }
        }

        function initFieldClass() {
            var date = new Date();
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            $('.gen-date').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                startDate: new Date()
            });

            $(".gen-date").last().datepicker('setDate', today);


            $('.gen-time').timepicker({
                //timePickerIncrement: 10,
                showMeridian: false,
                minuteStep: 5,
            })
        }

        function CekWaktu() {
            var awal = $("#timepicker1").val();
            var akhir = $("#timepicker2").val();
            var time1 = ((Number(akhir.split(':')[0]) * 60 * 60) + Number(akhir.split(':')[1]) * 60) * 1000;
            var time2 = ((Number(awal.split(':')[0]) * 60 * 60) + Number(awal.split(':')[1]) * 60) * 1000;
            if (parseInt(time2) > parseInt(time1)) {
                alert("cek kembali waktu agenda anda");
                return false;
            }

            return true;

        }

        function setDate(dateObject){
            var d = new Date(dateObject);
            var day = d.getDate();
            var month = d.getMonth() + 1;
            var year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            var date = day + "-" + month + "-" + year;

            return date;
        }
    </script>
    @endsection