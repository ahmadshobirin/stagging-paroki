<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proposal - {{ $proposal->judul }}</title>
    <style>
        body {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            padding: 6px;
        }

        .only table {
            margin-top: 8px;
            border-collapse: collapse;
            width: 100%;
        }

        .only table td,
        .only table th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .only table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .only table tr:hover {
            background-color: #ddd;
        }

        .only table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            color: black;
            /* background-color: #4CAF50; */
        }
    </style>
</head>

<body>
    {{-- {{ public_path('/bootstrap/css/bootstrap.min.css') }}<br>
    {{ asset('/bootstrap/css/bootstrap.min.css') }}<br>
    {{ URL::asset('/bootstrap/css/bootstrap.min.css') }}<br> --}}
    <img src="{{ public_path('dist/img/kop-surat.png') }}" alt="" min-width="100%" height="120px">
    <hr>
    @if ( $proposal->type == "form" )

    <label>Nomor Proposal : {{ $proposal->no_proposal }}</label><br>
    <label>Nomor Rekening : {{ $proposal->no_rekening }}</label>
    <h3 align="center">{{ ucwords($proposal->judul) }}</h3>
    <br>
    <b>1. Latar Belakang Masalah</b>
    <p>{!! $proposal->latar_belakang !!}</p>
    <br>
    <b>2. Nama Kegiatan</b>
    <p>{!! $proposal->latar_belakang !!}</p>
    <br>
    <b>3. Subjek Sasaran</b>
    <p>{!! $proposal->subjek_sasaran !!}</p>
    <br>
    <b>4. Tujuan Kegiatan</b>
    <p>{!! $proposal->tujuan_kegiatan !!}</p>
    <br>
    <b>5. Bentuk Kegiatan</b>
    <p>{!! $proposal->bentuk_kegiatan !!}</p>
    <br>
    <b>6. Pelaksanaan</b>
    <p>
        @if($proposal->temp_agenda()->exists())
            {{ $proposal->temp_agenda[0]->name }}
            @if($proposal->temp_agenda[0]->place_id != null)
                {{$proposal->temp_agenda[0]->place->nama_tempat}}
            @else
                {{$proposal->temp_agenda[0]->other_place}}
            @endif
        @else
            -
        @endif
    </p>
    <table>
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Mulai</th>
                <th>Selesai</th>
                <th>Hari Libur</th>
            </tr>
        </thead>
        <tbody>
            @foreach($proposal->temp_agenda as $ag)
                <tr>
                    <td>{{ \Carbon\Carbon::parse($ag->date)->format('d-m-Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($ag->time_start)->format('H:i') }}</td>
                    <td>{{ \Carbon\Carbon::parse($ag->time_end)->format('H:i') }}</td>
                    <td>{!! ($ag->holiday == true) ? "<span class=\"fa fa-check\"></span>" : '-' !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <b>7. Susunan Acara</b>
    <p>{!! $proposal->susunan_acara !!}</p>
    <br>
    <b>8. Susunan Panitia</b>
    <p>{!! $proposal->susunan_panitia !!}</p>
    <b>9. Pemasukan Anggaran</b>
    <table class="only">
        <thead>
            <tr>
                <th>Pemasukan</th>
                <th>Anggaran</th>
            </tr>
        </thead>
        <tbody>
            @foreach($proposal->budget_in as $in)
                <tr>
                    <td>{{ ucfirst($in->pemasukan) }}</td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($in->pemasukan_total,0,'.','.') }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <b>10. Pengeluaran Anggaran</b>
    <table class="only">
        <thead>
            <tr>
                <th>Pengeluaran</th>
                <th>Anggaran</th>
                <th>Rincian Anggaran</th>
                <th>Total Anggaran</th>
            </tr>
        </thead>
        <tbody>
            @foreach($proposal->budget_out as $out)
                <tr>
                    <td>{{ ucfirst($out->pengeluaran) }}</td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($out->pengeluaran_price,0,'.','.') }}
                    </td>
                    <td style="text-align:right">
                        {{ $out->pengeluaran_qty }}
                    </td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($out->pengeluaran_total,0,'.','.') }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <b>11. Penutup</b>
    <p>{!! $proposal->penutup !!}</p>
    <br>
    @if($proposal->note != null)
        <b>Catatan</b>
        <p>{!! $proposal->note !!}</p>
        <br>
    @endif
    <br>
@else
    <table style="width:100%; border-spacing: 0 1em;">
        <tr style="margin:30px;">
            <td style="width:33%; text-align:center">&nbsp;</td>
            <td style="width:33%; text-align:center">&nbsp;</td>
            <td style="width:33%; text-align:center">Surabaya, {{$proposal->created_at->format('d F Y')}}</td>
        </tr>
    </table>
    @if($proposal->signature()->exists())
        <table style="width:100%; border-spacing: 0 1em; direction: rtl;" dir="rtl">
            @foreach($proposal->signature->chunk(2) as $signature)
            <tr>
                @foreach($signature->reverse() as $key => $sig)
                    @if($sig->status == 'complete')
                        @if(count($signature) == 1)
                            <td style="width:33%; text-align:center">&nbsp;</td>
                            <td style="width:33%; text-align:center">
                                <img src="{{ public_path($sig->signature_path) }}" alt="">
                                <br>
                                <label for="">{{ $sig->username }}</label>
                            </td>
                            <td style="width:33%; text-align:center">&nbsp;</td>
                        @elseif(count($signature) == 2)
                            <td style="width:33%; text-align:center">
                                <img src="{{ public_path($sig->signature_path) }}" alt="">
                                <br>
                                <label for="" style="">{{ $sig->username }}</label>
                            </td>
                            @if( ($key+1) % 2 == 0 )
                                <td style="width:33%;">&nbsp;</td>
                            @endif
                        @endif
                    @endif
                @endforeach
            </tr>
            @endforeach
        <table>
        <b></b>
        @if($proposal->program_kerja != null)
            <img src="{{ asset($proposal->program_kerja) }}" alt="" style="margin-top:10px;margin-bottom:10px; max-width:100%; max-height:100%;">
        @endif
        <br>
        <b></b>
            @if($proposal->laporan_keuangan != null)
                <img src="{{ asset($proposal->laporan_keuangan) }}" alt="" style="margin-top:10px;margin-bottom:10px; max-width:100%; max-height:100%;">
            @endif
        @endif
        <br>
    @endif
</body>

</html>