@extends('app')

@section('treeview_post','active')
@section('treeview_artikel','active')

@section('title', 'Tambah Artikel Paroki')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@section('contentheader_title', 'Tambah Artikel Paroki')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            @include('admin.displayerror')
            <form class="" method="post" action="{{ url('admin/artikel/') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Judul Artikel  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Judul" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Artikel <span class="required">*</span></label>
                                <textarea name="artikel" id="artikel" class="form-control tinymce"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Publish? <span class="required">*</span></label>
                                <input type="checkbox" name="publish" id="publish"> Ya
                            </div>
                        </div>
                    </div>
                    <div class="row" id="div_info_publish">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Info Publish <span class="required">*</span></label>
                                <textarea name="info_publish" id="info_publish" maxlength="200" class="form-control tinymce">Info Publish max 200 characters</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Image Artikel</label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                            </div>
                            <label id="label_image">Bukan file image</label>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/artikel') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('customscript')
<script type="text/javascript">
    $(document).ready(function () {
        $("#div_info_publish").hide();
        $("#label_image").hide();

    });
    $("#publish").on('click', function(){
        var checkedValue = $('#publish:checked').val();
        if(checkedValue == "on"){
            $("#div_info_publish").show();
        }else{
            $("#div_info_publish").hide();
        }
    })
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }
</script>
@endsection