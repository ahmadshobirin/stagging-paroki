@extends('app')

@section('treeview_post','active')
@section('treeview_artikel','active')

@if($type == "show")
@section('title', 'Lihat Artikel Paroki')
@else
@section('title', 'Ubah Artikel Paroki')
@endif

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@if($type == "show")
@section('contentheader_title', 'Lihat Info Paroki')
@else
@section('contentheader_title', 'Ubah Info Paroki')
@endif

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/artikel/'.$cek_post->id_posting) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Judul Artikel  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="title" placeholder="Judul" value="{{$cek_post->title_post}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Artikel <span class="required">*</span></label>
                                <textarea @if($type == "show") disabled @endif name="artikel" id="artikel" class="form-control @if($type == "edit") tinymce @endif">{{$cek_post->isi_post}}</textarea>
                            </div>
                        </div>
                    </div>
                    @if($type == "edit")
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Publish? <span class="required">*</span></label>
                                    <input type="checkbox" name="publish" @if($cek_post->published == 1) checked @endif id="publish"> Ya
                                </div>
                            </div>
                        </div>
                        <div class="row" id="div_info_publish">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Info Publish <span class="required">*</span></label>
                                    <textarea name="info_publish" id="info_publish" maxlength="200" class="form-control tinymce">@if($cek_post->published == 1) {{$cek_post->isi_publish_post}} @else Info Publish max 200 characters @endif </textarea>
                                </div>
                            </div>
                        </div>
                    @else
                         @if($cek_post->published == 1)
                         <div class="row" id="div_info_publish">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Info Publish <span class="required">*</span></label>
                                    <textarea  @if($type == "show") disabled @endif name="info_publish" id="info_publish" maxlength="200" class="form-control @if($type == "edit") tinymce @endif">@if($cek_post->published == 1) {{$cek_post->isi_publish_post}} @else Info Publish max 200 characters @endif </textarea>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif
                    @if($type == "edit")
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Image Artikel</label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                            </div>
                            <label id="label_image">Bukan file image</label>
                            @if($cek_post->image_post != 'no_image.png')
                            <div class="col-md-5">
                                <img src="{{ asset('upload/artikel/'.$cek_post->image_post) }}" style="width: 200px; height: 200px" name="artikel_image" id="artikel_image">
                                <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="true">
                                <input type="hidden" name="image_hidden" id="image_hidden" value="{{$cek_post->image_post}}">
                            </div>
                            <input type="button" class="btn btn-danger" id="reset_image" value="Delete">
                            @else
                            <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="false">
                            <input type="hidden" name="image_hidden" id="image_hidden" value="@if($cek_post->image_post != "no_image.png") {{$cek_post->image_post}} @endif">
                            @endif
                        </div>
                    </div>
                    @else
                    <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Image Artikel</label>
                                
                            </div>
                            <div class="col-md-5">
                                <img src="{{ asset('upload/artikel/'.$cek_post->image_post) }}" style="width: 200px; height: 200px" name="artikel_image" id="artikel_image">
                            </div>
                        </div>
                    @endif
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/artikel') }}" class="btn btn-info">Kembali</a>
                        @if($type == "edit")
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('customscript')
<script type="text/javascript">
    $(document).ready(function () {
        var checked = $('#publish:checked').val();
        if(checked == "on") {
            $("#div_info_publish").show();
        }else{
            $("#div_info_publish").hide();
        }
        $("#label_image").hide();

    });
        $("#publish").on('click', function(){
            var checkedValue = $('#publish:checked').val();
            if(checkedValue == "on"){
                $("#div_info_publish").show();
            }else{
                $("#div_info_publish").hide();
            }
        });

        $("#reset_image").click(function(){
            var val = "false";
            $("#value_image_hidden").val(val);
            $("#artikel_image").hide();
            $("#reset_image").hide();
        });
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }
</script>
@endsection