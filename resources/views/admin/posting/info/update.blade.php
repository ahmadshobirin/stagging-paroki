@extends('app')

@section('treeview_post','active')
@section('treeview_info','active')

@if($type == "show")
@section('title', 'Lihat Info Paroki')
@else
@section('title', 'Ubah Info Paroki')
@endif

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@if($type == "show")
@section('contentheader_title', 'Lihat Info Paroki')
@else
@section('contentheader_title', 'Ubah Info Paroki')
@endif

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/info/'.$cek_post->id_posting) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    @include('admin.displayerror')
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Judul Info  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="title" placeholder="Judul" value="{{ $cek_post->title_post }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Info <span class="required">*</span></label>
                                <textarea @if($type == "show") disabled @endif maxlength="200" name="info_publish" id="info_publish" class="form-control @if($type == "edit")tinymce @endif" placeholder="Info(max 200 characters)">{{ $cek_post->isi_post }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/info') }}" class="btn btn-info">Kembali</a>
                        @if($type == "edit")
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection