@extends('app')

@section('treeview_post','active')
@section('treeview_info','active')

@section('title', 'Info Paroki')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop
@if(Auth::user()->role < 3)
@section('contentheader_title', 'Info Paroki')
@else
<?php $cek_kat_user = \DB::table('m_user')
                        ->join('m_kategori_user','m_kategori_user.id_kategori_user','m_user.id_header_user')
                        ->join('m_bidang','m_bidang.id_bidang','m_kategori_user.id_bidang')
                        ->where('id',Auth::user()->id)
                        ->first();
$bidang = $cek_kat_user->nama_bidang; ?>
@section('contentheader_title', 'Info Paroki '.$bidang)
@endif

@section('main-content')
  <div class="box box-primary">
        <div class="box-body">
            <div class="">
                <a href="{{url('admin/info/create')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
                {{--<a href="{{url('admin/report-master-userrole/view')}}" class="btn btn-default btn-md">
                    <i class="fa fa-file"></i> View Report
                </a>
                <a href="{{url('admin/report-excel-master-userrole')}}" class="btn bg-green color-palette">
                    <i class="fa fa-file-excel-o"></i> Export To Excel
                </a>
                <a href="{{url('admin/report-master-userrole/download')}}" class="btn btn-warning btn-md">
                    <i class="fa fa-file-pdf-o"></i> Export to PDF
                </a>--}}
            </div>
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        {{-- <th>Nama</th> --}}
                        <th style="width: 40%;">Info</th>
                        <th>Status</th>
                        <th>Tanggal</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

  <script type="text/javascript">
   $(document).ready(function(){    
      $('#table').DataTable({
         'paging': true,
         'lengthChange': true,
         'searching': true,
         'ordering': true,
         'info': true,
         'autoWidth': false,
         "language": {
             "emptyTable": "Data Kosong",
             "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
             "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
             "infoFiltered": "(disaring dari _MAX_ total data)",
             "search": "Cari:",
             "lengthMenu": "Tampilkan _MENU_ Data",
             "zeroRecords": "Tidak Ada Data yang Ditampilkan",
             "oPaginate": {
                 "sFirst": "Awal",
                 "sLast": "Akhir",
                 "sNext": "Selanjutnya",
                 "sPrevious": "Sebelumnya"
             },
         },

        processing: true,
        serverSide: true,
        ajax: '<?= url("/admin/api/info") ?>',
        columns: [
            {data: 'DT_Row_Index', name: 'DT_Row_Index', searchable: false, orderable: false},
            // {data: 'name', name: 'name'},
            {data: 'title_post', name: 'title_post'},
            {data: 'status_post', name: 'status_post'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
      });
    });
  </script>
@stop