<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LPJ - {{ $lpj->judul }}</title>
    <style>
        body {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            padding: 6px;
        }

        .only table {
            margin-top: 8px;
            border-collapse: collapse;
            width: 100%;
        }

        .only table td,
        .only table th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .only table tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .only table tr:hover {
            background-color: #ddd;
        }

        .only table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: center;
            color: black;
            /* background-color: #4CAF50; */
        }
    </style>
</head>

<body>
    {{-- {{ public_path('/bootstrap/css/bootstrap.min.css') }}<br>
    {{ asset('/bootstrap/css/bootstrap.min.css') }}<br>
    {{ URL::asset('/bootstrap/css/bootstrap.min.css') }}<br> --}}
    <img src="{{ public_path('dist/img/kop-surat.png') }}" alt="" min-width="100%" height="120px">
    <hr>
    @if ( $lpj->type == "form" )
    <label>Nomor LPJ : {{ $lpj->no_lpj }}</label>
    <h3 align="center">{{ ucwords($lpj->judul) }}</h3>
    <br>
    <b>1. Latar Belakang Masalah</b>
    <p>{!! $lpj->latar_belakang !!}</p>
    <br>
    <b>2. Nama Kegiatan</b>
    <p>{!! $lpj->latar_belakang !!}</p>
    <br>
    <b>3. Tujuan Kegiatan</b>
    <p>{!! $lpj->tujuan_kegiatan !!}</p>
    <br>
    <b>4. Jenis Kegiatan</b>
    <p>{!! $lpj->jenis_kegiatan !!}</p>
    <br>
    <b>5. Temmpat dan Kegiatan</b>
    <p>{!! $lpj->tempat_waktu_kegiatan !!}</p>
    <br>
    <b>6. Peserta Kegiatan</b>
    <p>{!! $lpj->peserta_kegiatan !!}</p>
    <br>
    <b>7. Evaluasi Kegiatan</b>
    <p>{!! $lpj->evaluasi_kegiatan !!}</p>
    <br>
    <b>8. Hasil Kegiatan</b>
    <p>{!! $lpj->hasil_kegiatan !!}</p>
    <br>
    <b>9. Refleksi</b>
    <p>{!! $lpj->refleksi !!}</p>
    <br>
    <b>10. Pemasukan Anggaran</b>
    <table class="only">
        <thead>
            <tr>
                <th>Pemasukan</th>
                <th>Anggaran</th>
            </tr>
        </thead>
        <tbody>
            @foreach($lpj->budget_in as $in)
                <tr>
                    <td>{{ ucfirst($in->pemasukan) }}</td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($in->pemasukan_total,0,'.','.') }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <b>11. Pengeluaran Anggaran</b>
    <table class="only">
        <thead>
            <tr>
                <th>Pengeluaran</th>
                <th>Anggaran</th>
                <th>Rincian Anggaran</th>
                <th>Total Anggaran</th>
            </tr>
        </thead>
        <tbody>
            @foreach($lpj->budget_out as $out)
                <tr>
                    <td>{{ ucfirst($out->pengeluaran) }}</td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($in->pengeluaran_price,0,'.','.') }}
                    </td>
                    <td style="text-align:right">
                        {{ $in->pengeluaran_qty }}
                    </td>
                    <td style="text-align:right">
                        {{ "Rp ".number_format($in->pengeluaran_total,0,'.','.') }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <b>12. Penutup</b>
    <p>{!! $lpj->penutup !!}</p>
    <br>
    @if($lpj->note != null)
        <b>Catatan</b>
        <p>{!! $lpj->note !!}</p>
        <br>
    @endif

    <br>
    <table style="width:100%; border-spacing: 0 1em;">
        <tr style="margin:30px;">
            <td style="width:33%; text-align:center">&nbsp;</td>
            <td style="width:33%; text-align:center">&nbsp;</td>
            <td style="width:33%; text-align:center">Surabaya, {{$lpj->created_at->format('d F Y')}}</td>
        </tr>
    </table>
@else
    @if($lpj->signature()->exists())
    <table style="width:100%; border-spacing: 0 1em;">
        @foreach($lpj->signature->chunk(2) as $signature)
        <tr>
            @foreach($signature->reverse() as $key => $sig)
                @if($sig->status == 'complete')
                    @if(count($signature) == 1)
                        <td style="width:33%; text-align:center">&nbsp;</td>
                        <td style="width:33%; text-align:center">
                            <img src="{{ public_path($sig->signature_path) }}" alt="">
                            <br>
                            <label for="">{{ $sig->username }}</label>
                        </td>
                        <td style="width:33%; text-align:center">&nbsp;</td>
                    @elseif(count($signature) == 2)
                        <td style="width:33%; text-align:center">
                            <img src="{{ public_path($sig->signature_path) }}" alt="">
                            <br>
                            <label for="" style="">{{ $sig->username }}</label>
                        </td>
                        @if( ($key+1) % 2 == 0 )
                            <td style="width:33%;">&nbsp;</td>
                        @endif
                    @endif
                @endif
            @endforeach
        </tr>
        @endforeach
    </table>
    @endif

    <b></b>
        @if($lpj->foto_nota != null)
            <img src="{{ public_path($lpj->foto_nota) }}" alt="" style="margin-top:10px;margin-bottom:10px; max-width:100%; max-height:100%;">
        @endif
    <br>
    <b></b>
        @if($lpj->foto_kegiatan != null)
            <img src="{{ public_path($lpj->foto_kegiatan) }}" alt="" style="margin-top:10px;margin-bottom:10px; max-width:100%; max-height:100%;">
        @endif
    @endif
</body>

</html>