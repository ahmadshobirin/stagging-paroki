{{-- <span href="{{ $links['show'] }}" class="btn btn-default btn-xs" title="{{ucwords($data->judul)}}"><span class="fa fa-eye"></span></span> --}}
@if($data->status == 'pending')
    @if($user->id == $data->user_id)
        <a href="{{ $links['edit'] }}" class="btn btn-default btn-xs" title="{{ucwords($data->judul)}}"><span class="fa fa-edit"></span></a>

        <a href="{{ route('lpj.upstatus',['oldstatus' => $data->status,'id' => $data->id, 'change' => 'in-approval']) }}"
            class="btn btn-primary btn-xs" title="send to in-approval {{ucwords($data->judul)}}?">
            <span class="fa fa-send"></span>
        </a>
        <a href="{{ $links['destroy'] }}" class="btn btn-danger btn-xs postback" title="Hapuss {{ucwords($data->judul)}}?"><span class="fa fa-trash"></span></a>
    @else
        -
    @endif
@elseif($data->status == 'in-approval')
    @if(in_array($user->role,[1,2]))
        <a href="{{ route('lpj.upstatus',['oldstatus' => $data->status,'id' => $data->id, 'change' => 'approved']) }}"
            class="btn btn-success btn-xs btn-approved" title="approve {{ucwords($data->judul)}}?">
            <span class="fa fa-check"></span>
        </a>

        <a href="{{ route('lpj.upstatus',['oldstatus' => $data->status,'id' => $data->id, 'change' => 'cancel']) }}"
            class="btn btn-danger btn-xs" title="cancel {{ucwords($data->judul)}}?">
            <span class="fa fa-close"></span>
        </a>
    @else
    -
    @endif
@elseif($data->status == 'approved')
    <a href="{{ route('lpj.note',['id' => $data->id]) }}"
        class="btn btn-primary btn-xs" title="Catatan {{ucwords($data->judul)}}?">
        <span class="fa fa-book"></span>
    </a>

    <a href="{{ route('lpj.signature',['id' => $data->id]) }}"
        class="btn btn-default btn-xs" title="Tanda Tangan {{ucwords($data->judul)}}?">
        <span class="fa fa-signature"></span>
    </a>

    <a href="{{ route('lpj.signature.reject',['id' => $data->id]) }}"
        class="btn btn-danger btn-xs" title="Reject {{ucwords($data->judul)}}?">
        <span class="fa fa-close"></span>
    </a>
@else
-
@endif

@if($data->type == 'upload' )
    <a href="{{ route('lpj.download',$data->id) }}" download=""
        class="btn btn-default btn-xs" title="download {{ucwords($data->judul)}}?">
        <span class="fa fa-arrow-down"></span>
    </a>
@endif