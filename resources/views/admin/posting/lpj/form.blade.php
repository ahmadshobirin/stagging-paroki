@extends('app')

@section('treeview_post','active')
@section('treeview_lpj','active')

@section('title', 'Tambah LPJ')

@section('customcss')
<link rel="stylesheet" href="{{ URL::asset('css/datatables.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/plugins/select2/select2.min.css') }}">
<link href="{{ asset('plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
@stop

    @section('contentheader_title', 'Tambah LPJ')

    @section('main-content')

    <div class="row">
        <div class="col-md-12">
            <!-- form start -->
            <form method="post" onsubmit="return cekBalance();" enctype="multipart/form-data"
            action="@if(!isset($editmode)){{route('lpj.store')}}@else{{route('lpj.update',$id)}}@endif">
                <!-- /.box-header -->
                <div class="box box-primary">
                    {{ csrf_field() }} @if(!isset($editmode)) @method('POST') @else  @method('PUT') @endif
                    <div class="box-body" style="margin-left: 20px;">

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Proposal <span class="required">*</span></label>
                                <select class="form-control select2" required name="proposal_id" id="proposal_id" style="width:100%">
                                    <option value="">Pilih Salah Satu</option>
                                    @foreach(@$proposal as $prop)
                                    <option data-text="{{$prop->judul}}" value="{{$prop->id}}">{{ $prop->judul }}</option>
                                    @endforeach
                                </select>
                            </div>

                             <div class="form-group col-md-6">
                                <label for="">No LPJ <span class="required">*</span></label>
                                <input type="text"
                                    class="form-control{{ $errors->has('no_lpj') ? ' is-invalid' : '' }}"
                                    name="no_lpj" id="no_lpj" value="{{ old('no_lpj') }}"
                                    placeholder="no_lpj...">
                                @if($errors->has('no_lpj'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('no_lpj') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Judul LPJ <span class="required">*</span></label>
                                <input type="text"
                                    class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }}"
                                    name="judul" id="judul" value="{{ old('judul') }}"
                                    placeholder="judul...">
                                @if($errors->has('judul'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('judul') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Nama Kegiatan <span class="required">*</span></label>
                                <input type="text"
                                    class="form-control{{ $errors->has('nama_kegiatan') ? ' is-invalid' : '' }}"
                                    name="nama_kegiatan" id="nama_kegiatan" value="{{ old('nama_kegiatan') }}"
                                    placeholder="kegiatan...">
                                @if($errors->has('nama_kegiatan'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nama_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Latar Belakang <span class="required">*</span></label>
                                <textarea name="latar_belakang" id="latar_belakang"
                                    class="form-control{{ $errors->has('latar_belakang') ? ' is-invalid' : '' }} summernote">{{ old('latar_belakang') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('latar_belakang') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Tujuan Kegiatan <span class="required">*</span></label>
                                <textarea name="tujuan_kegiatan" id="tujuan_kegiatan"
                                    class="form-control{{ $errors->has('tujuan_kegiatan') ? ' is-invalid' : '' }} summernote">{{ old('tujuan_kegiatan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tujuan_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Jenis Kegiatan <span class="required">*</span></label>
                                <textarea name="jenis_kegiatan" id="jenis_kegiatan"
                                    class="form-control{{ $errors->has('jenis_kegiatan') ? ' is-invalid' : '' }} summernote">{{ old('jenis_kegiatan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('jenis_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Tempat dan Waktu Kegiatan <span class="required">*</span></label>
                                <textarea name="tempat_waktu_kegiatan" id="tempat_waktu_kegiatan"
                                    class="form-control{{ $errors->has('tempat_waktu_kegiatan') ? ' is-invalid' : '' }} summernote">{{ old('tempat_waktu_kegiatan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tempat_waktu_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Peserta Kegiatan <span class="required">*</span></label>
                                <textarea name="peserta_kegiatan" id="peserta_kegiatan"
                                    class="form-control{{ $errors->has('peserta_kegiatan') ? ' is-invalid' : '' }} summernote">{{ old('peserta_kegiatan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('peserta_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Evaluasi Kegiatan <span class="required">*</span></label>
                                <textarea name="evaluasi_kegiatan" id="evaluasi_kegiatan"
                                    class="form-control{{ $errors->has('evaluasi_kegiatan') ? ' is-invalid' : '' }} summernote">{{ old('evaluasi_kegiatan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('evaluasi_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Hasil Kegiatan <span class="required">*</span></label>
                                <textarea name="hasil_kegiatan" id="hasil_kegiatan"
                                    class="form-control{{ $errors->has('hasil_kegiatan') ? ' is-invalid' : '' }} summernote">{{ old('hasil_kegiatan') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('hasil_kegiatan') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Refleksi <span class="required">*</span></label>
                                <textarea name="refleksi" id="refleksi"
                                    class="form-control{{ $errors->has('refleksi') ? ' is-invalid' : '' }} summernote">{{ old('refleksi') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('refleksi') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Rencana Anggaran</label>
                                <table class="table table-bordered" id="table-pemasukan">
                                    <thead>
                                        <th>
                                            <button type="button" class="btn btn-xs btn-success"
                                                title="Tambah Data Pengeluaran" onclick="addRow('table-pemasukan')">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </th>
                                        <th>Pemasukan</th>
                                        <th>Total</th>
                                    </thead>
                                    <tbody>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tbody>
                                    <tfoot>
                                        {{-- <td></td> --}}
                                        <td colspan="2" style="text-align:right">Jumlah : </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="jmlpem" readonly
                                                value="0" style="text-align:right">
                                        </td>
                                    </tfoot>
                                </table>

                                <table class="table table-bordered" id="table-pengeluaran">
                                    <thead>
                                        <th>
                                            <button type="button" class="btn btn-xs btn-success"
                                                title="Tambah Data Pengeluaran" onclick="addRow('table-pengeluaran')">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </th>
                                        <th>Pengeluaran</th>
                                        <th>Nominal</th>
                                        <th width="15%">Rincian</th>
                                        <th>Total</th>
                                    </thead>
                                    <tbody>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tbody>
                                    <tfoot>
                                        <td style="text-align:right" colspan="4">Jumlah : </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="jmlpeng" readonly
                                                value="0" style="text-align:right">
                                        </td>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Nota<span class="required">*</span></label>
                                    <input type="file" name="nota" id="nota" class="form-control input-sm" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Foto Kegiatan<span class="required">*</span></label>
                                    <input type="file" name="kegiatan" id="kegiatan" class="form-control input-sm" required>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="">Penutup <span class="required">*</span></label>
                                <textarea name="penutup" id="penutup"
                                    class="form-control{{ $errors->has('penutup') ? ' is-invalid' : '' }} summernote">{{ old('penutup') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('penutup') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <a href="{{ url('/admin/kronik') }}" class="btn btn-info">Kembali</a>
                            <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endsection
    @section('customscript')
    <script type="text/javascript"
        src="{{ URL::asset('/plugins/select2/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/plugins/mask/mask.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script type="text/javascript">

        $(".select2").select2();
        $('.price').mask('000.000.000', {
            reverse: true
        });

        $("#proposal_id").on("change", function(){
            var judul = $("#proposal_id option:selected").attr('data-text');
            var id = $("#proposal_id option:selected").val();
            if(id != ""){
                $.ajax({
                    url : "<?= url('/admin/get-agenda'); ?>/" + id,
                    method : "get",
                    success : function(text){
                        console.log(text);
                        $('#tempat_waktu_kegiatan').summernote('code',text);
                    },
                });
            }else{

            }
            $("#judul").val(judul);
        });

        $('.summernote').summernote({
            height: 150,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'strikethrough']],
                ['fontsize', ['fontsize', 'height', 'hr']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
            ],
        });

        @isset($editmode)
            var data = {!! $data !!};


            $('#judul').val(data.judul);
            $('#no_lpj').val(data.no_lpj);
            $('#proposal_id option[value="'+data.proposal_id+'"]').attr('selected', 'selected');
            $('#nama_kegiatan').val(data.nama_kegiatan);
            $('#latar_belakang').summernote('code',data.latar_belakang);
            $('#tujuan_kegiatan').summernote('code',data.tujuan_kegiatan);
            $('#jenis_kegiatan').summernote('code',data.jenis_kegiatan);
            $('#tempat_waktu_kegiatan').summernote('code',data.tempat_waktu_kegiatan);
            $('#peserta_kegiatan').summernote('code',data.peserta_kegiatan);
            $('#refleksi').summernote('code',data.refleksi);
            $('#evaluasi_kegiatan').summernote('code',data.evaluasi_kegiatan);
            $('#hasil_kegiatan').summernote('code',data.hasil_kegiatan);
            $('#penutup').summernote('code',data.penutup);

            $('.summernote').summernote({
                height: 150,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'strikethrough']],
                    ['fontsize', ['fontsize', 'height', 'hr']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                ],
            });

            $.each(data.budget_in,function(k,v){
                $('#table-pemasukan tbody').append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger in' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pemasukan[]' value='"+v.pemasukan+"'></td>" +
                    "<td><input type='text' class='form-control input-sm price' name='pemasukan_total[]' value='"+fixDec(v.pemasukan_total)+"' onkeyup='jml()' style='text-align:right'></td>" +
                    "</tr>"
                );

            });

            $.each(data.budget_out,function(k,v){
                $('#table-pengeluaran tbody').append(
                    "<tr>" +
                        "<td> <button type='button' class='btn btn-xs btn-danger out' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                        "<td><input type='text' class='form-control input-sm' name='pengeluaran[]' value='"+v.pengeluaran+"'> </td>" +
                        "<td><input type='text' class='form-control input-sm price nominal' name='pengeluaran_price[]' value='"+fixDec(v.pengeluaran_price)+"' onkeyup='sub(this)' style='text-align:right'> </td> " +
                        "<td><input type='text' class='form-control input-sm price qty' name='pengeluaran_qty[]' value='"+v.pengeluaran_qty+"' onkeyup='sub(this)' style='text-align:right'> </td> " +
                        "<td><input type='text' class='form-control input-sm total' name='pengeluaran_total[]' value='"+fixDec(v.pengeluaran_total)+"'readonly style='text-align:right'> </td>" +
                    "</tr>"
                );
            });

            jml();
            jmlpeng();
        @endif

        function addRow(table,data = null) {
            if (table == 'table-pemasukan') {
                return $("#" + table + " tbody").append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger in' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pemasukan[]' value=''></td>" +
                    "<td><input type='text' class='form-control input-sm price' name='pemasukan_total[]' value='' onkeyup='jml()' style='text-align:right'></td>" +
                    "</tr>"
                );
            } else if (table == 'table-pengeluaran') {
                return $("#" + table + " tbody").append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger out' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pengeluaran[]' value=''> </td>" +
                    "<td><input type='text' class='form-control input-sm price nominal' name='pengeluaran_price[]' value='' onkeyup='sub(this)' style='text-align:right'> </td> " +
                    "<td><input type='text' class='form-control input-sm price qty' name='pengeluaran_qty[]' value='1' onkeyup='sub(this)' style='text-align:right'> </td> " +
                    "<td><input type='text' class='form-control input-sm total' name='pengeluaran_total[]' value=''readonly style='text-align:right'> </td>" +
                    "</tr>"
                );
            }
        }

        function jml(price = 0) {
            $('#table-pemasukan tr input.price').each(function () {
                price += toNumber($(this).val());
            });
            $('#jmlpem').val(toCurrency(price))
        }

        function jmlpeng(price = 0) {
            $('#table-pengeluaran tr input.total').each(function () {
                price += toNumber($(this).val());
            });

            $('#jmlpeng').val(toCurrency(price));
        }

        function sub(elem) {
            nominal = toNumber($(elem).closest('tr').find('td').find('.nominal').val());
            qty = toNumber($(elem).closest('tr').find('td').find('.qty').val());
            total = nominal * qty;
            $(elem).closest('tr').find('td').find('.total').val(toCurrency(total));

            jmlpeng();
        }

        function del(elem, type = null) {
            if (confirm('Hapus?')) {

                ($(elem).hasClass('in')) ? type = 'in': type = 'out';

                elem.closest('tr').remove();

                (type === 'out') ? total(): jml();
            }
        }

        function cekBalance() {
            var pem = toNumber($('#jmlpem').val());
            var peng = toNumber($('#jmlpeng').val());

            if(pem != peng){
                alert("Belum Balance");
                return false;
            }

            return true;
        }
    </script>
    @endsection