@extends('app')

@section('treeview_post','active')
@section('treeview_lpj','active')

@section('title', 'Upload LPS')

@section('customcss')
<link rel="stylesheet" href="{{ URL::asset('css/datatables.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/plugins/select2/select2.min.css') }}">
<link href="{{ asset('plugins/select2/select2-bootstrap.min.css') }}" rel="stylesheet" />
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
@stop

    @section('contentheader_title', 'Upload LPJ')

    @section('main-content')

    <div class="row">
        <div class="col-md-12">
            <!-- form start -->
            <form method="post" enctype="multipart/form-data" onsubmit="return cekBalance();"
            action="@if(!isset($editmode)){{route('lpj.store')}}@else{{route('lpj.update',$id)}}@endif">
                <!-- /.box-header -->
                <div class="box box-primary">
                    {{ csrf_field() }} @if(!isset($editmode)) @method('POST') @else  @method('PUT') @endif
                    <div class="box-body" style="margin-left: 20px;">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Judul LPJ <span class="required">*</span></label>
                                <input type="text"
                                    class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }}"
                                    name="judul" id="judul" value="{{ old('judul') }}"
                                    placeholder="judul...">
                                @if($errors->has('judul'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('judul') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group col-md-6 @if($editmode) has-warning @endif"">
                                <label for="">file <span class="required">*</span></label>
                                <input type="file"
                                class="form-control{{ $errors->has('file_upload') ? ' is-invalid' : '' }}" name="file_upload" id="file_upload"
                                    value="{{ old('file_upload') }}" placeholder="kegiatan...">
                                @if($errors->has('file_upload'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('file_upload') }}</strong>
                                    </span>
                                @endif
                                @if ($editmode)
                                    <span class="help-block">Timpa File yang sudah terupload?</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <label for="">Rencana Anggaran</label>
                            <table class="table table-bordered" id="table-pemasukan">
                                <thead>
                                    <th>
                                        <button type="button" class="btn btn-xs btn-success"
                                            title="Tambah Data Pengeluaran" onclick="addRow('table-pemasukan')">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </th>
                                    <th>Pemasukan</th>
                                    <th>Total</th>
                                </thead>
                                <tbody>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tbody>
                                <tfoot>
                                    {{-- <td></td> --}}
                                    <td colspan="2" style="text-align:right">Jumlah : </td>
                                    <td>
                                        <input type="text" class="form-control input-sm" id="jmlpem" readonly value="0"
                                            style="text-align:right">
                                    </td>
                                </tfoot>
                            </table>

                            <table class="table table-bordered" id="table-pengeluaran">
                                <thead>
                                    <th>
                                        <button type="button" class="btn btn-xs btn-success"
                                            title="Tambah Data Pengeluaran" onclick="addRow('table-pengeluaran')">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </th>
                                    <th>Pengeluaran</th>
                                    <th>Nominal</th>
                                    <th width="15%">Rincian</th>
                                    <th>Total</th>
                                </thead>
                                <tbody>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tbody>
                                <tfoot>
                                    <td style="text-align:right" colspan="4">Jumlah : </td>
                                    <td>
                                        <input type="text" class="form-control input-sm" id="jmlpeng" readonly value="0"
                                            style="text-align:right">
                                    </td>
                                </tfoot>
                            </table>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="form-group col-md-10 col-md-offset-1">
                                <label for="">Catatan <span class="required">*</span></label>
                                <textarea name="note" id="note"
                                    class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }} summernote">{{ old('note') }}</textarea>
                                @if($errors->has('form'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <a href="{{ url('/admin/kronik') }}" class="btn btn-info">Kembali</a>
                            <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endsection
    @section('customscript')
    <script type="text/javascript"
        src="{{ URL::asset('/plugins/select2/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/plugins/mask/mask.js') }}"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script type="text/javascript">

        @isset($editmode)
            var data = {!! $data !!};
            callBackData(data);
        @endif

        $(".select2").select2();
        $('.price').mask('000.000.000', {
            reverse: true
        });

        $('.summernote').summernote({
            height: 150,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'strikethrough']],
                ['fontsize', ['fontsize', 'height', 'hr']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
            ],
        });

        function addRow(table, data = null) {
            if (table == 'table-pemasukan') {
                return $("#" + table + " tbody").append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger in' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pemasukan[]' value=''></td>" +
                    "<td><input type='text' class='form-control input-sm price' name='pemasukan_total[]' value='' onkeyup='jml()' style='text-align:right'></td>" +
                    "</tr>"
                );
            } else if (table == 'table-pengeluaran') {
                return $("#" + table + " tbody").append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger out' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pengeluaran[]' value=''> </td>" +
                    "<td><input type='text' class='form-control input-sm price nominal' name='pengeluaran_price[]' value='' onkeyup='sub(this)' style='text-align:right'> </td> " +
                    "<td><input type='text' class='form-control input-sm price qty' name='pengeluaran_qty[]' value='1' onkeyup='sub(this)' style='text-align:right'> </td> " +
                    "<td><input type='text' class='form-control input-sm total' name='pengeluaran_total[]' value=''readonly style='text-align:right'> </td>" +
                    "</tr>"
                );
            }
        }

        function jml(price = 0) {
            $('#table-pemasukan tr input.price').each(function () {
                price += toNumber($(this).val());
            });
            $('#jmlpem').val(toCurrency(price))
        }

        function jmlpeng(price = 0) {
            $('#table-pengeluaran tr input.total').each(function () {
                price += toNumber($(this).val());
            });

            $('#jmlpeng').val(toCurrency(price));
        }

        function sub(elem) {
            nominal = toNumber($(elem).closest('tr').find('td').find('.nominal').val());
            qty = toNumber($(elem).closest('tr').find('td').find('.qty').val());
            total = nominal * qty;
            $(elem).closest('tr').find('td').find('.total').val(toCurrency(total));

            jmlpeng();
        }

        function del(elem, type = null) {
            if (confirm('Hapus?')) {

                ($(elem).hasClass('in')) ? type = 'in': type = 'out';

                elem.closest('tr').remove();

                (type === 'out') ? total(): jml();
            }
        }

        function callBackData(data){
            $('#judul').val(data.judul);
            $('#note').val(data.note);
            $.each(data.budget_in,function(k,v){
                $('#table-pemasukan tbody').append(
                    "<tr>" +
                    "<td> <button type='button' class='btn btn-xs btn-danger in' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                    "<td><input type='text' class='form-control input-sm' name='pemasukan[]' value='"+v.pemasukan+"'></td>" +
                    "<td><input type='text' class='form-control input-sm price' name='pemasukan_total[]' value='"+fixDec(v.pemasukan_total)+"' onkeyup='jml()' style='text-align:right'></td>" +
                    "</tr>"
                );

            });

            $.each(data.budget_out,function(k,v){
                $('#table-pengeluaran tbody').append(
                    "<tr>" +
                        "<td> <button type='button' class='btn btn-xs btn-danger out' title='Hapus?' onclick='del(this)'> <span class='fa fa-trash'></span> </button> </td>" +
                        "<td><input type='text' class='form-control input-sm' name='pengeluaran[]' value='"+v.pengeluaran+"'> </td>" +
                        "<td><input type='text' class='form-control input-sm price nominal' name='pengeluaran_price[]' value='"+fixDec(v.pengeluaran_price)+"' onkeyup='sub(this)' style='text-align:right'> </td> " +
                        "<td><input type='text' class='form-control input-sm price qty' name='pengeluaran_qty[]' value='"+v.pengeluaran_qty+"' onkeyup='sub(this)' style='text-align:right'> </td> " +
                        "<td><input type='text' class='form-control input-sm total' name='pengeluaran_total[]' value='"+fixDec(v.pengeluaran_total)+"'readonly style='text-align:right'> </td>" +
                    "</tr>"
                );
            });

            jml();
            jmlpeng();
        }

        function cekBalance() {
            var pem = toNumber($('#jmlpem').val());
            var peng = toNumber($('#jmlpeng').val());

            if(pem != peng){
                alert("Belum Balance");
                return false;
            }

            return true;
        }
    </script>
    @endsection