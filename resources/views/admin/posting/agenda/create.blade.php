@extends('app')

@section('treeview_post','active')
@section('treeview_agenda','active')
@section('treeview_agenda_umum','active')

@section('title', 'Tambah Agenda Umum Paroki')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@section('contentheader_title', 'Tambah Agenda Umum Paroki')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/kegiatan-umum/') }}" enctype="multipart/form-data" onsubmit="return CekWaktu();">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                @include('admin.displayerror')
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Agenda  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Judul" value="{{ old('title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tanggal Agenda <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="datepicker1" class="form-control input-sm" name="date_agenda" value="{{ old('date_agenda') }}" required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tanggal Merah? <span class="required">*</span></label>
                                <input type="checkbox" name="libur" id="libur"> Ya
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Waktu Mulai <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" id="timepicker1" class="form-control input-sm timepicker" name="waktu_agenda" value="" required readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Waktu Selesai <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" id="timepicker2" class="form-control input-sm timepicker" name="end_agenda" value="" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tempat Agenda  <span class="required">*</span></label>
                                <select  class="form-control select2" id="tempat" style="width: 100%;" name="tempat_agenda">
                                    <option selected="" value="" disabled="">Pilih Tempat...</option>
                                    @foreach($tempat as $tmpt)
                                        <option value="{{ $tmpt->id_tempat }}">{{ $tmpt->nama_tempat }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Bidang  <span class="required">*</span></label>
                                <select  class="form-control select2" id="bidang" style="width: 100%;" name="bidang">
                                    <option selected="" value="" disabled="">Pilih Bidang...</option>
                                    <option value="0">Agenda Umum</option>
                                    @foreach($bidang as $bdg)
                                        <option value="{{ $bdg->id_bidang }}">{{ $bdg->nama_bidang }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/kegiatan-umum') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $(document).ready(function () {
        $('.select2').select2();
        $('#datepicker1').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });
        $('#datepicker1').datepicker('setDate', today);
        $('.timepicker').timepicker({
            //timePickerIncrement: 10,
            showMeridian: false,
            minuteStep :5,
        })
    });

    function CekWaktu(){
        var awal = $("#timepicker1").val();
        var akhir = $("#timepicker2").val();
        var time1 = ((Number(akhir.split(':')[0]) * 60 * 60 )+ Number(akhir.split(':')[1]) * 60) * 1000;
        var time2 = ((Number(awal.split(':')[0]) * 60 * 60) + Number(awal.split(':')[1]) * 60) * 1000;
        if(parseInt(time2) > parseInt(time1)){
            alert("cek kembali waktu agenda anda");
            return false;
        }

        return true;

    }
</script>
@endsection