@extends('app')

@section('treeview_post','active')
@section('treeview_kronik','active')

@if($type == "show")
@section('title', 'Lihat Kronik Paroki')
@else
@section('title', 'Ubah Kronik Paroki')
@endif

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@if($type == "show")
@section('contentheader_title', 'Lihat Artikel Paroki')
@else
@section('contentheader_title', 'Ubah Artikel Paroki')
@endif

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/kronik/'.$cek_post->id_posting) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    @include('admin.displayerror')
                     <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Agenda <span class="required">*</span></label>
                                <select @if($type == "show") disabled @endif class="select2 form-control" name="agenda" id="agenda" style="width: 100%;" required>
                                    <option value="">Pilih Agenda...</option>
                                    @foreach($agenda as $agn)
                                        <option @if($cek_post->id_agenda == $agn->id_agenda) selected @endif  value="{{ $agn->id_agenda }}">{{ $agn->nama_agenda }} - {{ date('d M Y', strtotime($agn->date_agenda)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Judul Kronik  <span class="required">*</span></label>
                                <input  @if($type == "show") disabled @endif type="text" class="form-control" name="title" id="title" placeholder="Judul" value="{{$cek_post->title_post}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Kronik <span class="required">*</span></label>
                                <textarea  @if($type == "show") disabled @endif name="kronik" id="kronik" class="form-control  @if($type == "edit") tinymce @endif ">{{$cek_post->isi_post}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            @if($type == "edit") 
                                <div class="form-group">
                                    <label class="control-label">Image Kronik 1</label>
                                    <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                                </div>
                                <label id="label_image">Bukan file image</label>
                                @if($cek_post->image_post != 'no_image.png')
                                <div class="col-md-5">
                                    <img src="{{ asset('upload/kronik/'.$cek_post->image_post) }}" style="width: 200px; height: 200px" name="kronik_image" id="kronik_image">
                                    <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="true">
                                    <input type="hidden" name="image_hidden" id="image_hidden" value="{{$cek_post->image_post}}">
                                </div>
                                <input type="button" class="btn btn-danger" id="reset_image" value="Delete">
                                @else
                                <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="false">
                                <input type="hidden" name="image_hidden" id="image_hidden" value="@if($cek_post->image_post != "no_image.png") {{$cek_post->image_post}} @endif">
                                @endif
                            @else
                                <div class="form-group">
                                    <label class="control-label">Image Kronik 1</label>
                                </div>
                                <div class="col-md-5">
                                    <img src="{{ asset('upload/kronik/'.$cek_post->image_post) }}" style="width: 200px; height: 200px" name="kronik_image" id="kronik_image">
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if($type == "edit") 
                                <div class="form-group">
                                    <label class="control-label">Image Kronik 2</label>
                                    <input class="form-control" type="file" name="image2" id="image2" onchange="return ValidateFileUpload2()">
                                </div>
                                <label id="label_image2">Bukan file image</label>
                                @if($cek_post->image2_post != 'no_image.png')
                                <div class="col-md-5">
                                    <img src="{{ asset('upload/kronik/'.$cek_post->image2_post) }}" style="width: 200px; height: 200px" name="kronik_image2" id="kronik_image2">
                                    <input type="hidden" name="value_image_hidden2" id="value_image_hidden2" value="true">
                                    <input type="hidden" name="image_hidden2" id="image_hidden2" value="{{$cek_post->image2_post}}">
                                </div>
                                <input type="button" class="btn btn-danger" id="reset_image2" value="Delete">
                                @else
                                <input type="hidden" name="value_image_hidden2" id="value_image_hidden2" value="false">
                                <input type="hidden" name="image_hidden2" id="image_hidden2" value="@if($cek_post->image2_post != "no_image.png") {{$cek_post->image2_post}} @endif">
                                @endif
                             @else
                                <div class="form-group">
                                    <label class="control-label">Image Kronik 2</label>
                                </div>
                                <div class="col-md-5">
                                    <img src="{{ asset('upload/kronik/'.$cek_post->image2_post) }}" style="width: 200px; height: 200px" name="kronik_image2" id="kronik_image2">
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             @if($type == "edit") 
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Image Kronik 3</label>
                                    <input class="form-control" type="file" name="image3" id="image3" onchange="return ValidateFileUpload3()">
                                </div>
                                <label id="label_image3">Bukan file image</label>
                                @if($cek_post->image3_post != 'no_image.png')
                                <div class="col-md-5">
                                    <img src="{{ asset('upload/kronik/'.$cek_post->image3_post) }}" style="width: 200px; height: 200px" name="kronik_image3" id="kronik_image3">
                                    <input type="hidden" name="value_image_hidden3" id="value_image_hidden3" value="true">
                                    <input type="hidden" name="image_hidden3" id="image_hidden3" value="{{$cek_post->image3_post}}">
                                </div>
                                <input type="button" class="btn btn-danger" id="reset_image3" value="Delete">
                                @else
                                <input type="hidden" name="value_image_hidden3" id="value_image_hidden3" value="false">
                                <input type="hidden" name="image_hidden3" id="image_hidden3" value="@if($cek_post->image3_post != "no_image.png") {{$cek_post->image3_post}} @endif">
                                @endif
                            </div>
                         @else
                            <div class="form-group">
                                <label class="control-label">Image Kronik 3</label>
                            </div>
                            <div class="col-md-5">
                                <img src="{{ asset('upload/kronik/'.$cek_post->image3_post) }}" style="width: 200px; height: 200px" name="kronik_image3" id="kronik_image3">
                            </div>
                        @endif
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/kronik') }}" class="btn btn-info">Kembali</a>
                        @if($type == "edit")
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#label_image").hide();
        $("#label_image2").hide();
        $("#label_image3").hide();
        $(".select2").select2();


       $("#reset_image").click(function(){
            var val = "false";
            $("#value_image_hidden").val(val);
            $("#kronik_image").hide();
            $("#reset_image").hide();
        });

       $("#reset_image2").click(function(){
            var val = "false";
            $("#value_image_hidden2").val(val);
            $("#kronik_image2").hide();
            $("#reset_image2").hide();
        });

        $("#reset_image3").click(function(){
            var val = "false";
            $("#value_image_hidden3").val(val);
            $("#kronik_image3").hide();
            $("#reset_image3").hide();
        });

    });
     $("#agenda").on('change', function(){
            var title = $("#agenda option:selected").text();
            $("#title").val(title);
        });
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }

     function ValidateFileUpload2() {
        var fuData = document.getElementById('image2');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image2').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image2").val(filename);
                }
            }
            $("#label_image2").hide();
        } else {
            $('#image2').val('');
            $("#label_image2").show();
            return false;
        }
    }

     function ValidateFileUpload3() {
        var fuData = document.getElementById('image3');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image3').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image3").val(filename);
                }
            }
            $("#label_image3").hide();
        } else {
            $('#image3').val('');
            $("#label_image3").show();
            return false;
        }
    }
</script>
@endsection