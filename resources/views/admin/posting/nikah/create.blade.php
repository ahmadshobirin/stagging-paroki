@extends('app')

@section('treeview_post','active')
@section('treeview_agenda','active')
@section('treeview_agenda_pernikahan','active')

@section('title', 'Tambah Agenda Pernikahan Paroki')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@section('contentheader_title', 'Tambah Agenda Pernikahan Paroki')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/kegiatan-nikah/') }}" enctype="multipart/form-data" onsubmit="return CekWaktu();">
                @include('admin.displayerror')
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Agenda  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Judul" value="Pernikahan">
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Mempelai Pria  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="pria" placeholder="Nama Pengantin Pria" value="{{ old('pria') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Mempelai Wanita  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="wanita" placeholder="Nama Pengantin Wanita" value="{{ old('wanita') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat Mempelai Pria  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="address_pria" placeholder="Alamat Pengantin Pria" value="{{ old('address_pria') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat Mempelai Wanita  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="address_wanita" placeholder="Alamat Pengantin Wanita" value="{{ old('address_wanita') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Telp. Mempelai Pria  <span class="required">*</span></label>
                                <input type="text" class="form-control telp" name="phone_pria" placeholder="Telp. Pengantin Pria" value="{{ old('phone_pria') }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Telp. Mempelai Wanita  <span class="required">*</span></label>
                                <input type="text" class="form-control telp" name="phone_wanita" placeholder="Telp. Pengantin Wanita" value="{{ old('phone_wanita') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label class="control-label">Menikah di Luar Paroki? </label>
                                <input type="checkbox" name="id_tempat_menikah" id="id_tempat_menikah"> Ya
                            </div>
                             <div class="form-group">
                                <label for="" class="control-label">Gereja Tempat Menikah  <span class="required">*</span></label>
                                <input type="text" class="form-control" required readonly name="tempat_menikah" id="tempat_menikah" placeholder="Geraja Tempat Menikah" value="Gereja Paroki">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Romo  <span class="required">*</span></label>
                                <input type="text" name="romo" id="romo" class="form-control" value="{{ old('romo') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tanggal Agenda <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="datepicker1" class="form-control input-sm" name="date_agenda" value="{{ old('date_agenda') }}" required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tanggal Merah? <span class="required">*</span></label>
                                <input type="checkbox" name="libur" id="libur"> Ya
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Waktu Mulai <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" id="timepicker1" class="form-control input-sm timepicker" name="waktu_agenda" value="{{ old('waktu_agenda') }}" required readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Waktu Selesai <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" id="timepicker2" class="form-control input-sm timepicker" name="end_agenda" value="{{ old('end_agenda') }}" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Image Pasangan</label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                            </div>
                            <label id="label_image">Bukan file image</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tanggal Kanonik</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" readonly class="form-control" name="tgl_kanonik" id="datepicker2" value="{{ old('tgl_kanonik') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Romo Kanonik</label>
                                <input type="text" class="form-control" name="romo_kanonik" placeholder="Romo Kanonik" id="romo_kanonik" value="{{ old('romo_kanonik') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Memberkati Perkawinan</label>
                                <input type="text" class="form-control" name="berkat_kawin" placeholder="Yang Memberkati Perkawinan" value="{{ old('berkat_kawin') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Keterangan</label>
                                <textarea class="form-control" name="keterangan">{{ old('keterangan') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/kegiatan-nikah') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $(document).ready(function () {
        $('.select2').select2();
        $('#datepicker1').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });

        $('#datepicker2').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });

        $('#datepicker1').datepicker('setDate', today);
        $('#datepicker2').datepicker('setDate', today);
        $('.timepicker').timepicker({
            //timePickerIncrement: 10,
            showMeridian: false,
            minuteStep :5,
        });

        $("#label_image").hide();
    });

    $("#id_tempat_menikah").on('click', function(){
        var check = $("#id_tempat_menikah:checked").val();
        if(check == "on"){
            $("#tempat_menikah").prop('readonly', false);
            $("#tempat_menikah").val(" ");
        }else{
            $("#tempat_menikah").prop('readonly', true);
            $("#tempat_menikah").val("Gereja Paroki");
        }

    });

    function CekWaktu(){
        var awal = $("#timepicker1").val();
        var akhir = $("#timepicker2").val();
        var time1 = ((Number(akhir.split(':')[0]) * 60 * 60 )+ Number(akhir.split(':')[1]) * 60) * 1000;
        var time2 = ((Number(awal.split(':')[0]) * 60 * 60) + Number(awal.split(':')[1]) * 60) * 1000;
        if(parseInt(time2) > parseInt(time1)){
            alert("cek kembali waktu agenda anda");
            return false;
        }

        return true;

    }
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }
</script>
@endsection