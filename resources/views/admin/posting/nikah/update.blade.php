@extends('app')

@section('treeview_post','active')
@section('treeview_agenda','active')
@section('treeview_agenda_pernikahan','active')

@if($type == "show")
@section('title', 'Lihat Agenda Pernikahan Paroki')
@else
@section('title', 'Ubah Agenda Pernikahan Paroki')
@endif

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@if($type == "show")
@section('contentheader_title', 'Lihat Agenda Pernikahan Paroki')
@else
@section('contentheader_title', 'Ubah Agenda Pernikahan Paroki')
@endif

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/kegiatan-nikah/'.$agenda->id_agenda) }}" enctype="multipart/form-data" onsubmit="return CekWaktu();">
                @include('admin.displayerror')
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Agenda  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="title" placeholder="Judul" value="{{ $agenda->nama_agenda }}">
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Mempelai Pria  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="pria" placeholder="Nama Pengantin Pria" value="{{ $agenda->nama_pengantin_pria }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Mempelai Wanita  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="wanita" placeholder="Nama Pengantin Wanita" value="{{ $agenda->nama_pengantin_wanita }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat Mempelai Pria  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="address_pria" placeholder="Alamat Pengantin Pria" value="{{ $agenda->alamat_pengantin_pria }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat Mempelai Wanita  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="address_wanita" placeholder="Alamat Pengantin Wanita" value="{{ $agenda->alamat_pengantin_wanita }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Telp. Mempelai Pria  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control telp" name="phone_pria" placeholder="Telp. Pengantin Pria" value="{{ $agenda->telp_pengantin_pria }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Telp. Mempelai Wanita  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control telp" name="phone_wanita" placeholder="Telp. Pengantin Wanita" value="{{ $agenda->telp_pengantin_wanita }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label class="control-label">Menikah di Luar Paroki? </label>
                                <input type="checkbox" @if($agenda->tempat_menikah == 2) checked @endif name="id_tempat_menikah" id="id_tempat_menikah" @if($type == "show") disabled @endif> Ya
                            </div>
                             <div class="form-group">
                                <label for="" class="control-label">Gereja Tempat Menikah  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" required name="tempat_menikah" id="tempat_menikah" placeholder="Geraja Tempat Menikah" @if($agenda->tempat_menikah == 2) value="{{$agenda->alternative_tempat_menikahh }}" @else readonly value="Gereja Paroki" @endif>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Romo  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" name="romo" id="romo" class="form-control" value="{{$agenda->romo_penerima_pendaftaran }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Tanggal Agenda <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input @if($type == "show") disabled @endif type="text" id="datepicker1" class="form-control input-sm" name="date_agenda" value="{{date('d-m-Y', strtotime($agenda->date_agenda))}}" required readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tanggal Merah? <span class="required">*</span></label>
                                <input type="checkbox" @if($agenda->hari_libur == 1) checked @endif name="libur" id="libur"> Ya
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Waktu Mulai <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input @if($type == "show") disabled @endif type="text" id="timepicker1" class="form-control input-sm timepicker" name="waktu_agenda" value="{{date('H:i', strtotime($agenda->waktu_agenda))}}" required readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Waktu Selesai <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input @if($type == "show") disabled @endif type="text" id="timepicker2" class="form-control input-sm timepicker" name="end_agenda" value="{{date('H:i', strtotime($agenda->end_agenda))}}" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if($type == "edit")
                                <div class="form-group">
                                    <label class="control-label">Image Pasangan</label>
                                    <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                                </div>
                                <label id="label_image">Bukan file image</label>
                                @if($agenda->image_mempelai != 'no_image.png')
                                    <div class="col-md-5">
                                        <img src="{{ asset('upload/wedding/'.$agenda->image_mempelai) }}" style="width: 200px; height: 200px" name="nikah_image" id="nikah_image">
                                        <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="true">
                                        <input type="hidden" name="image_hidden" id="image_hidden" value="{{$agenda->image_mempelai}}">
                                    </div>
                                    <input type="button" class="btn btn-danger" id="reset_image" value="Delete">
                                    @else
                                    <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="false">
                                    <input type="hidden" name="image_hidden" id="image_hidden" value="@if($agenda->image_mempelai != "no_image.png") {{$agenda->image_mempelai}} @endif">
                                @endif
                            @else
                            <div class="form-group">
                                <label class="control-label">Image Pasangan</label>
                            </div>
                            <div class="col-md-5">
                                <img src="{{ asset('upload/wedding/'.$agenda->image_mempelai) }}" style="width: 200px; height: 200px" name="nikah_image" id="nikah_image">
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if($type == "edit")
                                <div class="form-group">
                                    <label class="control-label">Image Pengantin Pria</label>
                                    <input class="form-control" type="file" name="image1" id="image1" onchange="return ValidateFileUpload1()">
                                </div>
                                <label id="label_image1">Bukan file image</label>
                                @if($agenda->image_nikah_pengantin_pria != null)
                                    <div class="col-md-5">
                                        <img src="{{ asset($agenda->image_nikah_pengantin_pria) }}" style="width: 200px; height: 200px" name="nikah_image_pria" id="nikah_image_pria">
                                        <input type="hidden" name="value_image_hidden_pria" id="value_image_hidden_pria" value="true">
                                        <input type="hidden" name="image_hidden_pria" id="image_hidden_pria" value="{{$agenda->image_nikah_pengantin_pria}}">
                                    </div>
                                    <input type="button" class="btn btn-danger" id="reset_image1" value="Delete">
                                    @else
                                    <input type="hidden" name="value_image_hidden_pria" id="value_image_hidden_pria" value="false">
                                    <input type="hidden" name="image_hidden_pria" id="image_hidden_pria" value="@if($agenda->image_nikah_pengantin_pria != null) {{$agenda->image_nikah_pengantin_pria}} @endif">
                                @endif
                            @else
                            <div class="form-group">
                                <label class="control-label">Image Pengantin Pria</label>
                            </div>
                            <div class="col-md-5">
                                <img src="{{ asset($agenda->image_nikah_pengantin_pria) }}" style="width: 200px; height: 200px" name="nikah_image_pria" id="nikah_image_pria">
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                           @if($type == "edit")
                                <div class="form-group">
                                    <label class="control-label">Image Pengantin Wanita</label>
                                    <input class="form-control" type="file" name="image2" id="image2" onchange="return ValidateFileUpload2()">
                                </div>
                                <label id="label_image2">Bukan file image</label>
                                @if($agenda->image_nikah_pengantin_wanita != null)
                                    <div class="col-md-5">
                                        <img src="{{ asset($agenda->image_nikah_pengantin_wanita) }}" style="width: 200px; height: 200px" name="nikah_image_wanita" id="nikah_image_wanita">
                                        <input type="hidden" name="value_image_hidden_wanita" id="value_image_hidden_wanita" value="true">
                                        <input type="hidden" name="image_hidden_wanita" id="image_hidden_wanita" value="{{$agenda->image_nikah_pengantin_wanita}}">
                                    </div>
                                    <input type="button" class="btn btn-danger" id="reset_image2" value="Delete">
                                    @else
                                    <input type="hidden" name="value_image_hidden_wanita" id="value_image_hidden_wanita" value="false">
                                    <input type="hidden" name="image_hidden_wanita" id="image_hidden_wanita" value="@if($agenda->image_nikah_pengantin_wanita != null) {{$agenda->image_nikah_pengantin_wanita}} @endif">
                                @endif
                            @else
                            <div class="form-group">
                                <label class="control-label">Image Pengantin Wanita</label>
                            </div>
                            <div class="col-md-5">
                                <img src="{{ asset($agenda->image_nikah_pengantin_wanita) }}" style="width: 200px; height: 200px" name="nikah_image_wanita" id="nikah_image_wanita">
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Tanggal Kanonik</label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="tgl_kanonik" id="datepicker2" placeholder="Tanggal Kanonik" @if($agenda->tanggal_kanonik == null) value="{{date('d-m-y')}}" @else value="{{ date('d-m-Y', strtotime($agenda->tanggal_kanonik)) }} @endif">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Romo  <span class="required">*</span></label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="romo_kanonik" placeholder="Romo Kanonik" id="romo_kanonik" value="{{$agenda->romo_kanonik}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Memberkati Perkawinan</label>
                                <input @if($type == "show") disabled @endif type="text" class="form-control" name="berkat_kawin" placeholder="Yang Memberkati Perkawinan" value="{{$agenda->memberkati_perkawinan}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Keterangan</label>
                                <textarea @if($type == "show") disabled @endif class="form-control" name="keterangan">{{$agenda->keterangan}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/kegiatan-nikah') }}" class="btn btn-info">Kembali</a>
                        @if($type == "edit")
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $(document).ready(function () {
        $('.select2').select2();
        $('#datepicker1').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });

        $('#datepicker2').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            endDate : '+18m',
            weekStart: 1,
        });
        // $('#datepicker1').datepicker('setDate', today);
        // $('#datepicker2').datepicker('setDate', today);
        $('.timepicker').timepicker({
            //timePickerIncrement: 10,
            showMeridian: false,
            minuteStep :5,
        });

        $("#label_image").hide();
        $("#label_image1").hide();
        $("#label_image2").hide();

        $("#reset_image").click(function(){
            var val = "false";
            $("#value_image_hidden").val(val);
            $("#nikah_image").hide();
            $("#reset_image").hide();
        });

        $("#reset_image1").click(function(){
            var val1 = "false";
            $("#value_image_hidden_pria").val(val1);
            $("#nikah_image_pria").hide();
            $("#reset_image1").hide();
        });

        $("#reset_image2").click(function(){
            var val2 = "false";
            $("#value_image_hidden_wanita").val(val);
            $("#nikah_image_wanita").hide();
            $("#reset_image2").hide();
        });
    });

    $("#id_tempat_menikah").on('click', function(){
        var check = $("#id_tempat_menikah:checked").val();
        if(check == "on"){
            $("#tempat_menikah").prop('readonly', false);
            $("#tempat_menikah").val(" ");
        }else{
            $("#tempat_menikah").prop('readonly', true);
            $("#tempat_menikah").val("Gereja Paroki");
        }

    });

    function CekWaktu(){
        var awal = $("#timepicker1").val();
        var akhir = $("#timepicker2").val();
        var time1 = ((Number(akhir.split(':')[0]) * 60 * 60 )+ Number(akhir.split(':')[1]) * 60) * 1000;
        var time2 = ((Number(awal.split(':')[0]) * 60 * 60) + Number(awal.split(':')[1]) * 60) * 1000;
        if(parseInt(time2) > parseInt(time1)){
            alert("cek kembali waktu agenda anda");
            return false;
        }

        return true;

    }
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }

    function ValidateFileUpload1() {
        var fuData = document.getElementById('image1');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image1').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image1").val(filename);
                }
            }
            $("#label_image1").hide();
        } else {
            $('#image1').val('');
            $("#label_image1").show();
            return false;
        }
    }

    function ValidateFileUpload2() {
        var fuData = document.getElementById('image2');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image2').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image2").val(filename);
                }
            }
            $("#label_image2").hide();
        } else {
            $('#image2').val('');
            $("#label_image2").show();
            return false;
        }
    }
</script>
@endsection