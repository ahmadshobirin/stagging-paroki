@extends('app')

@section('treeview_post','active')
@section('treeview_agenda','active')
@section('treeview_agenda_pernikahan','active')

@section('title', 'Agenda Nikah Paroki')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop
@section('contentheader_title', 'Agenda Nikah Paroki')

@section('main-content')
  <div class="box box-primary">
        <div class="box-body">
            <div class="">
                <a href="{{url('admin/kegiatan-nikah/create')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
                {{--<a href="{{url('admin/report-master-userrole/view')}}" class="btn btn-default btn-md">
                    <i class="fa fa-file"></i> View Report
                </a>
                <a href="{{url('admin/report-excel-master-userrole')}}" class="btn bg-green color-palette">
                    <i class="fa fa-file-excel-o"></i> Export To Excel
                </a>
                <a href="{{url('admin/report-master-userrole/download')}}" class="btn btn-warning btn-md">
                    <i class="fa fa-file-pdf-o"></i> Export to PDF
                </a>--}}
            </div>
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Pasangan</th>
                        <th>Romo</th>
                        <th>Tanggal/Tempat</th>
                        <th>Waktu</th>
                        <th style="visibility: none"></th>
                        <th style="visibility: none"></th>
                        <th style="visibility: none"></th>
                        <th style="visibility: none"></th>
                        <th>Status</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

  <script type="text/javascript">
   $(document).ready(function(){
      $('#table').DataTable({
         'paging': true,
         'lengthChange': true,
         'searching': true,
         'ordering': true,
         'info': true,
         'autoWidth': false,
         "language": {
             "emptyTable": "Data Kosong",
             "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
             "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
             "infoFiltered": "(disaring dari _MAX_ total data)",
             "search": "Cari:",
             "lengthMenu": "Tampilkan _MENU_ Data",
             "zeroRecords": "Tidak Ada Data yang Ditampilkan",
             "oPaginate": {
                 "sFirst": "Awal",
                 "sLast": "Akhir",
                 "sNext": "Selanjutnya",
                 "sPrevious": "Sebelumnya"
             },
         },

        processing: true,
        serverSide: true,
        ajax: '<?= url("/admin/api/kegiatan-nikah") ?>',
        columns: [
            {data: 'DT_Row_Index', name: 'DT_Row_Index', searchable: false, orderable: false},
            {data: 'nama_agenda', name: 'nama_agenda'},
            {data: 'pasangan', name: 'pasangan', searchable: false, visible: true},
            {data: 'romo_penerima_pendaftaran', name: 'romo_penerima_pendaftaran'},
            {data: 'date_agenda', name: 'date_agenda', orderable: false},
            {data: 'waktu', name: 'waktu', searchable: false, orderable: false},
            {data: 'nama_pengantin_pria', name: 'nama_pengantin_pria', visible: false, searchable: true, orderable: false,},
            {data: 'nama_pengantin_wanita', name: 'nama_pengantin_wanita', visible: false, searchable: true, orderable: false,},
            {data: 'tempat_menikah', name: 'tempat_menikah', visible: false, searchable: true, orderable: false,},
            {data: 'alternative_tempat_menikah', name: 'alternative_tempat_menikah', visible: false, searchable: true, orderable: false,},
            {data: 'status', name: 'status', orderable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false, visible: true}
        ]
      });
    });
  </script>
@stop