@extends('app') 

@section('title', 'Kelurahan') @section('contentheader_title', 'Ubah Kelurahan')

@section('treeview_master','active')
@section('treeview_lokasi','active')
@section('treeview_kelurahan','active')

@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
    <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
        @include('admin.displayerror')
            <form class="form-horizontal" action="{{ route('kelurahan.store') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="kecamatan" id="val-kecamatan" value="">
                {{ method_field('post') }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-md-offset-1 control-label">Kode</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Kode" disabled value="{{$setCodeKelurahan}}">
                            <input type="hidden" name="code" value="{{$setCodeKelurahan}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-md-offset-1 control-label">Kota Kab</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="kota" style="width: 100%;" id="kota">
                                <option value="" selected="">Pilih Kota / Kab...</option>
                                @foreach($getKota as $kota)
                                    <option value="{{ $kota->id }}">{{ strtoupper($kota->type) }} {{ strtoupper($kota->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 col-md-offset-1 control-label">Kecamatan</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" style="width: 100%;" id="kecamatan">
                                <option value="" selected="">Pilih kecamatan...</option>
                            </select>
                            {{-- <span><small>Kecamatan {{ strtoupper($getKecamatan->name) }}</small></span> --}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-2 col-md-offset-1 control-label">Kode Pos</label>
                        <div class="col-sm-8">
                            <input type="text" name="zipcode" class="form-control" placeholder="Kode Pos..." value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-2 col-md-offset-1 control-label">Nama Kelurahan / Desa</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" placeholder="Kelurahan..." value="">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                     <div class="pull-right">
                         <a href="{{ url('/admin/kelurahan') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger" id="reset">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@stop

@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
    var langId = "{{asset('vendor/select2/js/i18n/id.js')}}";
    $(document).ready(function () {
        $(".select2").select2();

        $('#kota').change(function (e) {
            $.ajax({
                url: "<?= url('/admin/get-kelurahan') ?>/" + $(this).val(),
                method: 'GET',
                success: function (data) {
                    //console.log(data);

                    $('#kecamatan').children('option:not(:first)').remove().end();

                    $.each(data,function(index,kecamObj){
                        $('#kecamatan').append('<option value="'+kecamObj.id+'"> '+kecamObj.name+' </option>')
                    });
                }
            });
        });

        $('#kecamatan').change(function (e) {
            var kecamatanVal = $(this).val();
            $('#val-kecamatan').val(kecamatanVal);
        });

        $("#reset").click(function (e) {
            e.preventDefault();
            $('#kota').val('').trigger("change");
            $('#kecamatan').val('').trigger("change");
        });

    });
</script>
@stop