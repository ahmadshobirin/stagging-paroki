@extends('app') 

@section('title', 'Sejarah') @section('contentheader_title', 'Tambah Sejarah')

@section('treeview_master','active')
@section('treeview_gereja','active')
@section('treeview_gereja_sejarah','active')


@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            @include('admin.displayerror')
            <form action="{{ url('admin/gereja/sejarah/'.$dataSejarah->id_sejarah) }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Judul Sejarah <span class="required">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Judul Sejarah" value="{{ $dataSejarah->title_sejarah }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Tag Sejarah <span class="required">*</span></label>
                                <input type="text" class="form-control" name="tag" placeholder="Tag Judul Sejarah" value="{{ $dataSejarah->tag_sejarah }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Isi Sejarah <span class="required">*</span></label>
                                <textarea class="form-control tinymce" name="isi" >{{ $dataSejarah->isi_sejarah }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Image Sejarah</label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                            </div>
                            <label id="label_image">Bukan file image</label>
                             @if($dataSejarah->image_sejarah != 'no_image.png')
                            <div class="col-md-5">
                                <img src="{{ asset('upload/sejarah/'.$dataSejarah->image_sejarah) }}" style="width: 200px; height: 200px" name="sejarah_image" id="sejarah_image">
                                <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="true">
                                <input type="hidden" name="image_hidden" id="image_hidden" value="{{$dataSejarah->image_sejarah}}">
                            </div>
                            <input type="button" class="btn btn-danger" id="reset_image" value="Delete">
                            @else
                            <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="false">
                            <input type="hidden" name="image_hidden" id="image_hidden" value="@if($dataSejarah->image_sejarah != "no_image.png") {{$dataSejarah->image_sejarah}} @endif">
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/gereja/sejarah') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@stop

@section('customscript')
<script type="text/javascript">
    $(document).ready(function () {
        $("#label_image").hide();
    });
    $("#reset_image").click(function(){
            var val = "false";
            $("#value_image_hidden").val(val);
            $("#sejarah_image").hide();
            $("#reset_image").hide();
        });
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }
</script>
@stop