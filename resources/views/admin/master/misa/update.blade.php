@extends('app')

@section('treeview_master','active')
@section('treeview_gereja','active')
@section('treeview_gereja_misa','active')

@section('title', 'Tambah Jadwal Misa')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
@stop

@section('contentheader_title', 'Tambah Jadwal Misa')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/gereja/misa/'.$dataMisa->id_detail_jadwal_misa) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Jadwal Misa <span class="required">*</span></label>
                                <select class="form-control select2" id="jadwal_misa" name="jadwal_misa">
                                    <option disabled selected value="">Pilih Satu...</option>
                                    @foreach($header_misa as $misa)
                                    <option @if($dataMisa->id_jadwal_misa == $misa->id_jadwal_misa) selected @endif value="{{$misa->id_jadwal_misa}}">{{$misa->nama_jadwal_misa}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Hari Misa <span class="required">*</span></label>
                                <select class="form-control select2" id="hari_misa" name="hari_misa">
                                    <option disabled selected value="">Pilih Satu...</option>
                                    <option @if($dataMisa->hari == "Senin") selected @endif value="Senin">Senin</option>
                                    <option @if($dataMisa->hari == "Selasa") selected @endif value="Selasa">Selasa</option>
                                    <option @if($dataMisa->hari == "Rabu") selected @endif value="Rabu">Rabu</option>
                                    <option @if($dataMisa->hari == "Kamis") selected @endif value="Kamis">Kamis</option>
                                    <option @if($dataMisa->hari == "Jumat Pertama") selected @endif value="Jumat Pertama">Jumat Pertama</option>
                                    <option @if($dataMisa->hari == "Sabtu") selected @endif value="Sabtu">Sabtu</option>
                                    <option @if($dataMisa->hari == "Minggu") selected @endif value="Minggu">Minggu</option>
                                    <option @if($dataMisa->hari == "Harian") selected @endif value="Harian">Harian</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Waktu Misa <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" id="timepicker1" class="form-control input-sm timepicker" name="waktu_misa" value="{{ date('H.i', strtotime($dataMisa->waktu)) }}" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('admin/gereja/misa') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".select2").select2();

    });
    $('.timepicker').timepicker({
            //timePickerIncrement: 10,
            showMeridian: false,
            minuteStep :5,
        })
</script>
@endsection