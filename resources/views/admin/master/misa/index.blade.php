@extends('app')

@section('treeview_master','active')
@section('treeview_gereja','active')
@section('treeview_gereja_misa','active')

@section('title', 'Master Jadwal Misa')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@section('contentheader_title', 'Master Jadwal Misa')

@section('main-content')
  <div class="box box-primary">
        <div class="box-body">
            <div class="">
                <a href="{{url('admin/gereja/misa/create')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
                {{--<a href="{{url('admin/report-master-userrole/view')}}" class="btn btn-default btn-md">
                    <i class="fa fa-file"></i> View Report
                </a>
                <a href="{{url('admin/report-excel-master-userrole')}}" class="btn bg-green color-palette">
                    <i class="fa fa-file-excel-o"></i> Export To Excel
                </a>
                <a href="{{url('admin/report-master-userrole/download')}}" class="btn btn-warning btn-md">
                    <i class="fa fa-file-pdf-o"></i> Export to PDF
                </a>--}}
            </div>
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th style="width: 5%">No.</th>
                        <th>Nama Misa</th>
                        <th>Hari Misa</th>
                        <th>Tanggal Misa</th>
                        <th class="nosort" style="width: 10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $i=1 ?>
                  @foreach($misa as $list_misa)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$list_misa->nama_jadwal_misa}}</td>
                            <td>{{$list_misa->hari}}</td>
                            <td>{{ date('H.i', strtotime($list_misa->waktu))}} WIB</td>
                            <td>
                              <a href="{{url('/admin/gereja/misa/'.$list_misa->id_detail_jadwal_misa.'/edit')}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                              <a href="{{url('/admin/gereja/misa-delete/'.$list_misa->id_detail_jadwal_misa)}}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin menghapus Misa ini?')"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

  <script type="text/javascript">    
    $('#table').DataTable({
       'paging': true,
       'lengthChange': true,
       'searching': true,
       'ordering': true,
       'info': true,
       'autoWidth': false,
       "language": {
           "emptyTable": "Data Kosong",
           "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
           "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
           "infoFiltered": "(disaring dari _MAX_ total data)",
           "search": "Cari:",
           "lengthMenu": "Tampilkan _MENU_ Data",
           "zeroRecords": "Tidak Ada Data yang Ditampilkan",
           "oPaginate": {
               "sFirst": "Awal",
               "sLast": "Akhir",
               "sNext": "Selanjutnya",
               "sPrevious": "Sebelumnya"
           },
       },
        'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': ['nosort']
      }],
    });
  </script>
@stop