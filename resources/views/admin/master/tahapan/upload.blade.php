@extends('app')

@section('title', 'Tahapan') @section('contentheader_title', 'Download File Tahapan - '.$judul->nama_tahapan)

@section('treeview_master','active')
@section('treeview_tahapan','active')


@section('customcss')
    <link href="{{ URL::asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ URL::asset('/plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            <div class="box-body">
            @foreach(@$cek_upload as $upload)
                <div class="form-group">
                    Nama : {{$upload->nama_diri}}
                    <br>
                    @foreach(@$upload->image as $image)
                        <a href="{{ url('/download-tahapan/'.$image->id) }}">{{$image->image_tahapan}}</a>
                        <br>
                    @endforeach
                </div>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
</script>
@stop