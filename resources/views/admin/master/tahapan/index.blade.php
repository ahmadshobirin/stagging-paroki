@extends('app')

@section('treeview_master','active')
@section('treeview_tahapan','active')

@section('title', 'Master Tahapan')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@section('contentheader_title', 'Master Tahapan')

@section('main-content')
  <div class="box box-primary">
        <div class="box-body">
            <div class="">
                <a href="{{url('admin/tahapan/create')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
            </div>
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th style="width: 10%">No.</th>
                        <th>Nama Tahapan</th>
                        <th>Periode</th>
                        <th>Type Sakramen</th>
                        <th class="nosort" style="width: 20%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $i=1 ?>
                  @foreach($tahapan as $thp)
                        <tr>
                            <td>{{$i++}}</td>
                            @if($thp->upload_file == "Ya")
                            <td><a href="{{ url('/admin/tahapan-detail/'.$thp->type.'/'.$thp->id_tahapan) }}">{{ $thp->nama_tahapan }}</a></td>
                            @else
                            <td>{{ $thp->nama_tahapan }}</td>
                            @endif
                            <td>
                              @if($thp->id_periode != 0)
                              {{ date('d-m-Y', strtotime($thp->start_date))}} s/d {{ date('d-m-Y', strtotime($thp->end_date))}}
                              @else
                              {{ date('d-m-Y', strtotime($thp->date_agenda))}}
                              @endif
                            </td>
                            <td>{{ ucfirst(str_replace("_", " ", $thp->type)) }}</td>
                            <td>
                              @if($thp->status == "in process")
                              <a href="{{url('/admin/tahapan/'.$thp->id_tahapan.'/edit')}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                              <a href="{{url('/admin/tahapan-approve/'.$thp->id_tahapan)}}" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>
                              @endif
                              <a href="{{url('/admin/tahapan-delete/'.$thp->id_tahapan)}}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah yakin menghapus data ini?')"><i class="fa fa-trash" ></i></a>
                            </td>
                        </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

  <script type="text/javascript">
    $('#table').DataTable({
       'paging': true,
       'lengthChange': true,
       'searching': true,
       'ordering': true,
       'info': true,
       'autoWidth': false,
       "language": {
           "emptyTable": "Data Kosong",
           "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
           "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
           "infoFiltered": "(disaring dari _MAX_ total data)",
           "search": "Cari:",
           "lengthMenu": "Tampilkan _MENU_ Data",
           "zeroRecords": "Tidak Ada Data yang Ditampilkan",
           "oPaginate": {
               "sFirst": "Awal",
               "sLast": "Akhir",
               "sNext": "Selanjutnya",
               "sPrevious": "Sebelumnya"
           },
       },
        'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': ['nosort']
      }],
    });
  </script>
@stop