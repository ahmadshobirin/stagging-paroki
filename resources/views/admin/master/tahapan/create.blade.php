@extends('app')

@section('title', 'Tahapan') @section('contentheader_title', 'Tambah Tahapan')

@section('treeview_master','active')
@section('treeview_tahapan','active')


@section('customcss')
    <link href="{{ URL::asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ URL::asset('/plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="@isset($editmode) {{ url('admin/tahapan/'.$id) }}@else{{ url('admin/tahapan') }}@endif" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @isset($editmode) {{ method_field('put') }} @else {{ method_field('post') }} @endif
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Nama Tahapan</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" name="nama_tahapan" id="nama_tahapan" placeholder="Nama Tahapan" value="{{ @$tahapan->nama_tahapan }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Type Tahapan</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="type" name="type">
                                <option value="">Pilih Salah Satu</option>
                                <option @if(@$tahapan->type == 'baptis_balita') selected @endif value="baptis_balita">Baptis Balita</option>
                                <option @if(@$tahapan->type == 'baptis_anak') selected @endif value="baptis_anak">Baptis Anak</option>
                                <option @if(@$tahapan->type == 'baptis_dewasa') selected @endif value="baptis_dewasa">Baptis Dewasa</option>
                                <option @if(@$tahapan->type == 'komuni') selected @endif value="komuni">Komuni</option>
                                <option @if(@$tahapan->type == 'krisma') selected @endif value="krisma">Krisma</option>
                                <option @if(@$tahapan->type == 'nikah') selected @endif value="nikah">Pernikahan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Periode</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="id_periode" id="id_periode" required>
                                <option selected="" disabled>Pilih Periode...</option>
                                @if(isset($editmode))
                                    @foreach ($periode as $p)
                                        <option
                                            @if($tahapan->type == 'nikah' && $p->id == $tahapan->id_agenda)
                                            selected
                                            @elseif($tahapan->type != 'nikah' && $p->id == $tahapan->id_periode)
                                            selected
                                            @endif value="{{ $p->id }}">{{ $p->periode_tampil }}</option>
                                    @endforeach
                                @endif
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Agenda <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="tanggal_tahapan" class="form-control input-sm" name="tanggal_tahapan" value="{{ old('tanggal_tahapan') }}" value="{{ @$tahapan->tanggal_agenda }}" required readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Upload File</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="upload_file" name="upload_file">
                                <option @if(@$tahapan->upload_file == 'Ya') selected @endif value="Ya">Ya</option>
                                <option @if(@$tahapan->upload_file == 'Tidak') selected @endif value="Tidak">Tidak</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/periode') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@endsection

@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $(document).ready(function () {
        var oldId = null;
        $(".select2").select2();

        $('#tanggal_tahapan').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            startDate: today,
            weekStart: 1,
        });

        $("#type").on("change", function(){
            var cek = $("#type options:selected").val();
            $('#id_periode').children('option:not(:first)').remove().end();
            $.ajax({
                url : "<?= url('api/tahapan/get-periode/'); ?>/" + $(this).val(),
                method : 'GET',
                success : function(data) {
                    var opt = '';
                    $.each(data,function(index,periodeObj){
                        // console.log(periodeObj.id);
                        opt += '<option value='+periodeObj.id+'> '+periodeObj.periode_tampil+'</option>';
                    });
                    $('#id_periode').append(opt);
                }
            });
        });
    });
</script>
@stop