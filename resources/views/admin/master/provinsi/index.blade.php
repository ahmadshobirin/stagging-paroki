@extends('app') 

@section('title', 'Provinsi') @section('contentheader_title', 'Provinsi')

@section('treeview_master','active')
@section('treeview_lokasi','active')
@section('treeview_provinsi','active')

@section('customcss')
	<link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
    <div class="box box-primary">
        <div class="box-body">

            <div class="">
                <a href="{{url('admin/provinsi/create')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
                <a href="{{url('admin/report-master-provinsi/view')}}" class="btn btn-default btn-md">
                    <i class="fa fa-file"></i> View Report
                </a>
                <a href="{{url('admin/report-excel-master-provinsi')}}" class="btn bg-green color-palette">
                    <i class="fa fa-file-excel-o"></i> Export To Excel
                </a>
                <a href="{{url('admin/report-master-provinsi/download')}}" class="btn btn-warning btn-md">
                    <i class="fa fa-file-pdf-o"></i> Export to PDF
                </a>
            </div>
            <br>
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Kode</th>
                        <th>Provinsi</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@stop

@section('customscript')
	<script type="text/javascript" src="{{URL::asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ url('api/provinsi')  }}",
                    "language": {
                           "emptyTable": "Data Kosong",
                           "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                           "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                           "infoFiltered": "(disaring dari _MAX_ total data)",
                           "search": "Cari:",
                           "lengthMenu": "Tampilkan _MENU_ Data",
                           "zeroRecords": "Tidak Ada Data yang Ditampilkan",
                           "oPaginate": {
                               "sFirst": "Awal",
                               "sLast": "Akhir",
                               "sNext": "Selanjutnya",
                               "sPrevious": "Sebelumnya"
                           },
                       },
                    columns: [
                        {data: 'DT_Row_Index',searchable: false},
                        {data: 'code', name: 'code'},
                        {data: 'name', name: 'name'},
                        {data: 'action', name: 'action'},
                    ]
            });
        });
	</script>
@stop
