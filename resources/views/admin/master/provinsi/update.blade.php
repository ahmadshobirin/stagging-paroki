@extends('app') 

@section('title', 'Provinsi') @section('contentheader_title', 'Ubah Provinsi')

@section('treeview_master','active')
@section('treeview_lokasi','active')
@section('treeview_provinsi','active')

@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{url('admin/provinsi/'.$getData->id)}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('put') }}
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Kode</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Kode" disabled value="{{$getData->code}}">
                            <input type="hidden" name="code" value="{{$getData->code}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Provinsi</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="name" placeholder="Provinsi" value="{{$getData->name}}">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/provinsi') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@stop

@section('customscript')
@stop