@extends('app')

@section('treeview_master','active')
@section('treeview_gereja','active')
@section('treeview_gereja_renungan','active')

@section('title', 'Ubah Renungan Paroki')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@section('contentheader_title', 'Ubah Renungan Paroki')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/gereja/renungan/'.$dataRenungan->id_renungan) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Judul Renungan  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="title" placeholder="Judul" value="{{ $dataRenungan->title_renungan }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Renungan <span class="required">*</span></label>
                                <textarea name="renungan" id="renungan" class="form-control tinymce">
                                    {{ $dataRenungan->isi_renungan }}
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Author Renungan <span class="required">*</span></label>
                                <select class="form-control select2" name="romo">
                                    <option disabled value="">Pilih Satu...</option>
                                    @foreach($romo as $rom)
                                    <option @if($dataRenungan->author_renungan == $rom->id_romo) selected @endif value="{{$rom->id_romo}}">{{$rom->nama_romo}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Image Renungan</label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                            </div>
                            <label id="label_image">Bukan file image</label>
                            @if($dataRenungan->image_renungan != 'no_image.png')
                            <div class="col-md-5">
                                <img src="{{ asset('upload/renungan/'.$dataRenungan->image_renungan) }}" style="width: 200px; height: 200px" name="renungan_image" id="renungan_image">
                                <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="true">
                                <input type="hidden" name="image_hidden" id="image_hidden" value="{{$dataRenungan->image_renungan}}">
                            </div>
                            <input type="button" class="btn btn-danger" id="reset_image" value="Delete">
                            @else
                            <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="false">
                            <input type="hidden" name="image_hidden" id="image_hidden" value="@if($dataRenungan->image_renungan != "no_image.png") {{$dataRenungan->image_renungan}} @endif">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/gereja/renungan') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#div_info_publish").hide();
        $("#label_image").hide();
        $(".select2").select2();

        $("#reset_image").click(function(){
            var val = "false";
            $("#value_image_hidden").val(val);
            $("#renungan_image").hide();
            $("#reset_image").hide();
        });

    });
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }
</script>
@endsection