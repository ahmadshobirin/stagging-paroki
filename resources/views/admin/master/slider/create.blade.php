@extends('app') 

@section('title', 'Ubah Slider') @section('contentheader_title', 'Ubah Slider')

@section('treeview_master','active')
@section('treeview_frontend','active')
@section('treeview_slider','active')

@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{url('admin/slider/'.$dataSlider->id)}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('put') }}
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">Judul Slider <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" name="title_slider" required class="form-control" placeholder="Judul Slider.." value="{{$dataSlider->title_slider}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">Desc Slider <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <textarea class="form-control">{{$dataSlider->desc_slider}}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">Produk Terkait <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-control">
                                <option value="0" @if($dataSlider->produk_related == 0) selected @endif>Tidak Terkait Produk</option>
                                @foreach($dataProduk as $produk)
                                 <option value="0" @if($dataSlider->produk_related == $produk->id) selected @endif>{{$produk->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-offset-1 control-label">Image Slider <span class="required">*</span></label>
                        <div class="col-sm-8">
                            @if($dataSlider->image_slider != 'no_image.png')
                                <img src="{{ asset('upload/slider/'.$dataSlider->image_slider) }}" style="width: 400px; height: 400px" name="image_slider" id="image_slider">
                                <input type="hidden" name="slider_image_hidden" id="slider_image_hidden" value="true">
                                <input type="hidden" name="slider_hidden" id="slider_hidden" value="{{$dataSlider->image_slider}}">
                                <input type="button" class="btn btn-danger" id="reset_image" value="x">
                            @else
                            <input type="hidden" name="slider_image_hidden" id="slider_image_hidden" value="false">
                            <input type="hidden" name="slider_hidden" id="slider_hidden" value="@if($dataSlider->image_slider != "no_image.png") {{$dataSlider->image_slider}} @endif">
                            @endif
                            <input type="file" name="image" class="form-control" accept="images/*" id="image" onchange="return ValidateFileUpload()">
                            <label id="label_image">Bukan file image</label>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('admin/slider/') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('customscript')
<script type="text/javascript">
    $(document).ready(function () {
        $("#label_image").hide();
        
        $("#reset_image").click(function(){
            var val = "false";
            $("#slider_image_hidden").val(val);
            $("#image_slider").hide();
            $("#reset_image").hide();
        });
    });
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }
</script>
@stop