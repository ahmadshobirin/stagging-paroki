@extends('app') 

@section('title', 'Slider') @section('contentheader_title', 'Slider')

@section('treeview_master','active')
@section('treeview_frontend','active')
@section('treeview_slider','active')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
    <div class="box box-primary">
        <div class="box-body">
            <div class="">
              <a href="{{url('admin/slider/create')}}" class="btn btn-success btn-md">
                  <i class="fa fa-plus"></i> Tambah Data
              </a>
              {{-- <a href="javascript:;" class="btn btn-default btn-md" data-toggle="modal" data-target="#print" id="btnprintout">
                  <i class="fa fa-file"></i> View Report
              </a>
              <a href="javascript:;" data-toggle="modal" data-target="#excel" class="btn bg-green color-palette">
                  <i class="fa fa-file-excel-o"></i> Export To Excel
              </a>
              <a href="javascript:;" class="btn btn-warning btn-md" data-toggle="modal" data-target="#print" id="btnprint" class="btn btn-warning btn-md">
                  <i class="fa fa-file-pdf-o"></i> Export to PDF
              </a> --}}
            </div>
            <br>
            @include('admin.displayerror')
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Judul Slider</th>
                        <th>Desc</th>
                        <th>Produk Related</th>
                        <th class="nosort">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@stop

@section('customscript')
  <script type="text/javascript" src="{{URL::asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ url('api/slider')  }}",
                    "language": {
                           "emptyTable": "Data Kosong",
                           "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                           "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                           "infoFiltered": "(disaring dari _MAX_ total data)",
                           "search": "Cari:",
                           "lengthMenu": "Tampilkan _MENU_ Data",
                           "zeroRecords": "Tidak Ada Data yang Ditampilkan",
                           "oPaginate": {
                               "sFirst": "Awal",
                               "sLast": "Akhir",
                               "sNext": "Selanjutnya",
                               "sPrevious": "Sebelumnya"
                           },
                       },
                    columns: [
                        {data: 'DT_Row_Index',searchable: false},
                        {data: 'title_slider', name: 'title_slider'},
                        {data: 'desc_slider', name: 'desc_slider'},
                        {data: 'name_produk', name: 'name_produk'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
            });
        });
  </script>
@stop
