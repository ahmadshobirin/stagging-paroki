@extends('app')

@section('title', 'DPP') @section('contentheader_title', 'Ubah DPP')

@section('treeview_master','active')
@section('treeview_user','active')
@section('treeview_user_bidang','active')

@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ route('bidang.update',$dataBidang->id_bidang) }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('put') }}
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Nama DPP</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="nama_bidang" placeholder="DPP" value="{{$dataBidang->nama_bidang}}">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/bidang') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@stop

@section('customscript')
@stop