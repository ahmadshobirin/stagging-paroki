@extends('app')

@section('treeview_master','active')
@section('treeview_download','active')
@section('treeview_download_'.$nama,'active')

@section('title', 'Master Form '.ucfirst($nama))

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@section('contentheader_title', 'Master Form '.ucfirst($nama))

@section('main-content')
  <div class="box box-primary">
        <div class="box-body">
            <div class="">
                <a href="{{url('admin/download/'.$nama.'/create')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
                {{--<a href="{{url('admin/report-master-userrole/view')}}" class="btn btn-default btn-md">
                    <i class="fa fa-file"></i> View Report
                </a>
                <a href="{{url('admin/report-excel-master-userrole')}}" class="btn bg-green color-palette">
                    <i class="fa fa-file-excel-o"></i> Export To Excel
                </a>
                <a href="{{url('admin/report-master-userrole/download')}}" class="btn btn-warning btn-md">
                    <i class="fa fa-file-pdf-o"></i> Export to PDF
                </a>--}}
            </div>
            <br>
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th >No.</th>
                        <th>Nama Form</th>
                        <th>Periode Form</th>
                        <th class="nosort" style="width: 10%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $i=1 ?>
                  @foreach($form as $list_form)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$list_form->nama_form}}</td>
                            <td>{{date('F Y', strtotime($list_form->periode_form)) }}</td>
                            <td>
                             {{--  <a href="{{url('/admin/download/'.$list_form->id_form.'/edit')}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a> --}}
                               <a href="{{url('/admin/download/'.$list_form->id_form.'/delete')}}" data-toggle="tooltip" onclick="return confirm('Apakah Yakin Menghapus data ini?')" data-placement="top"  class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                               <a href="{{url('/download/'.$list_form->id_form)}}" class="btn btn-primary btn-sm"><i class="fa fa-download"></i></a>
                            </td>
                        </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

  <script type="text/javascript">    
    $('#table').DataTable({
       'paging': true,
       'lengthChange': true,
       'searching': true,
       'ordering': true,
       'info': true,
       'autoWidth': false,
       "language": {
           "emptyTable": "Data Kosong",
           "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
           "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
           "infoFiltered": "(disaring dari _MAX_ total data)",
           "search": "Cari:",
           "lengthMenu": "Tampilkan _MENU_ Data",
           "zeroRecords": "Tidak Ada Data yang Ditampilkan",
           "oPaginate": {
               "sFirst": "Awal",
               "sLast": "Akhir",
               "sNext": "Selanjutnya",
               "sPrevious": "Sebelumnya"
           },
       },
        'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': ['nosort']
      }],
    });
  </script>
@stop