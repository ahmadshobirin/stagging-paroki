@extends('app') 

@section('title', 'Form') @section('contentheader_title', 'Tambah Form '.ucfirst($nama))

@section('treeview_master','active')
@section('treeview_download','active')
@section('treeview_download_'.$nama,'active')

@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('plugins/datepicker/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('plugins/datepicker/datepicker3.css')}}">
    <link href="{{asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('admin/download/'.$nama) }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{-- {{ method_field('post') }} --}}
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Nama {{ ucfirst($nama) }} <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="nama" placeholder="Nama Form" required value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Periode {{ ucfirst($nama) }} <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" readonly id="datepicker1" class="form-control" required name="periode" value="{{ date('M Y') }}" required data-date-end-month="18m">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Image {{ ucfirst($nama) }}</label>
                        <div class="col-sm-8">
                            <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                        </div>
                    </div>
                    <label id="label_image">Bukan file image</label>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">File {{ ucfirst($nama) }} <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input class="form-control" required type="file" name="pdf" id="pdf" onchange="return ValidatePDFUpload()">
                        </div>
                    </div>
                    <label id="label_pdf">Bukan file pdf</label>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/download/'.$nama) }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@stop

@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#label_image").hide();
        $("#label_pdf").hide();

        $('#datepicker1').datepicker({
            autoclose : true,
            format: 'M yyyy',
            viewMode : 'months',
            minViewMode : 'months',
            
        });
    });
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }
</script>
<script>
    function ValidatePDFUpload() {
        var fuData = document.getElementById('pdf');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "pdf") {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#pdf').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#pdf").val(filename);
                }
            }
            $("#label_pdf").hide();
        } else {
            $('#pdf').val('');
            $("#label_pdf").show();
            return false;
        }
    }
</script>
@stop