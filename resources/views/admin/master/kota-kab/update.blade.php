@extends('app') 

@section('title', 'Kota Kab') @section('contentheader_title', 'Ubah Kota Kab')

@section('treeview_master','active')
@section('treeview_lokasi','active')
@section('treeview_kota_kab','active')

@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
    <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{url('admin/kota-kab/'.$getCity->id)}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('put') }}
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-md-offset-1 control-label">Kode</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Kode" disabled value="{{$getCity->code}}">
                            <input type="hidden" name="code" value="{{$getCity->code}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-md-offset-1 control-label">Provinsi</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="provinsi" id="provinsi" style="width: 100%;" required>
                                <option value="" selected="">Pilih Provinsi...</option>
                                @foreach($getAllProvinsi as $provinsi)
                                    <option @if($provinsi->id == $getCity->id_provinsi) selected @endif value="{{ $provinsi->id }}">{{ $provinsi->name }}</option>
                                @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 col-md-offset-1 control-label">Kota/Kabupaten</label>
                        <div class="col-sm-8">
                            <select class="form-control select2" name="tipe" style="width: 100%;" required id="kota">
                                <option value="">Pilih Kota/Kabupaten...</option>
                                <option value="KABUPATEN" @if( $getCity->type == 'KABUPATEN') selected="" @endif >KABUPATEN</option>
                                <option value="KOTA" @if( $getCity->type == 'KOTA' ) selected="" @endif >KOTA</option>
                        </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 col-md-offset-1 control-label">Nama Kota/Kabupaten</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control" placeholder="Nama Kota/Kabupaten..." value="{{$getCity->name}}">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/kota-kab') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@stop

@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
    var langId = "{{asset('vendor/select2/js/i18n/id.js')}}";
    $(document).ready(function () {
        $(".select2").select2();

    });
</script>
@stop