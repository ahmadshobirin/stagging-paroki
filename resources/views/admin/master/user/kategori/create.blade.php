
@section('contentheader_title', 'Kategori User')
@extends('app')

@section('treeview_master','active')
@section('treeview_user','active')
@section('treeview_kat_user','active')

@section('title', 'Kategori User')

@section('contentheader_title', 'Tambah Kategori User')
@section('customcss')
<link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
<link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('/plugins/datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('/bootstrap/css/bootstrap.css')}}" rel="stylesheet" />
<style>
    .required {
        font-size: 12px;
        color: red;
    }
</style>
 @stop @section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">User Update Form</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->

            <form class="form-horizontal" action="{{ route('katuser.store')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('post') }}
                <div class="box-body">
                @include('admin.displayerror')
                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1  control-label">Nama <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="nama_kategori_user" class="form-control" placeholder="Nama" value="{{ old('nama_kategori_user') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">DPP <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="select2 form-control" name="id_bidang" id="id_bidang" style="width: 100%;" required>
                                <option selected="" disabled="" >Pilih DPP</option>
								<option value="0" >Admin</option>
								@foreach($bidang as $bid)
									<option value="{{$bid->id_bidang}}">{{$bid->nama_bidang}}</option>
								@endforeach
							</select>
                        </div>
                    </div>

                    <div class="form-group" id="type">
                        <label class="col-md-2 col-md-offset-1 control-label">Tipe</label>
                        <div class="col-md-8">
                            <select class="form-control" name="type" id="type">
                                <option value="">Pilih Salah Satu</option>
                                <option value="bidang">Bidang</option>
                                <option value="sekretaris">Sekretaris</option>
                                <option value="biak">Biak</option>
                                <option value="lingkungan">Lingkungan</option>
                                <option value="wilayah">Wlayah</option>
                                <option value="romo">Romo</option>
                                <option value="seksi">Seksi</option>
                                <option value="subseksi">Sub Seksi</option>
                                <option value="koordinator">Koordinator</option>
                                <option value="subkoordinator">Sub Koordinator</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">Upline User <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="select2 form-control" name="id_parent" id="id_parent" style="width: 100%;" required>
								<option value="0" selected>Tidak Ada</option>
							</select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/katuser/') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>



@stop @section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
    var langId = "{{asset('vendor/select2/js/i18n/id.js')}}";
    $(document).ready(function () {
        $(".select2").select2();

        $('#datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            //startDate: new Date(),
            endDate: '+0d',
            weekStart: 1,
        });

        $('#id_bidang').change(function(){
            // if($(this).val() == 10){
            //     $("#type").show();
            // }else{
            //     $("#type").hide();
            // }
            $.getJSON("{{ url('api/upline-user/') }}/"+$(this).val(),function(results){
                $('#id_parent').find('option').not(':first').remove();
                $.each(results,function(k,o){
                    // console.log(o.id_kategori_user);
                    $('#id_parent').append('<option value='+o.id_kategori_user+'>'+o.nama_kategori_user+'</option>');
                });
            });
        });
    });
</script>
@stop
