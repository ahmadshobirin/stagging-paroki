@extends('app')

@section('title', 'Kategori User') @section('contentheader_title', 'Kategori User')

@section('treeview_master','active')
@section('treeview_user','active')
@section('treeview_kat_user','active')

@section('customcss')
	<link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
  <div class="box box-primary">
      <div class="box-body">

          <div class="">
              <a href="{{route('katuser.create')}}" class="btn btn-success btn-md">
                  <i class="fa fa-plus"></i> Tambah Data
              </a>
          </div>
          <br>
          @include('admin.displayerror')
          <table class="table table-striped table-hover table-responsive" id="table">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Kategori User</th>
                      <th>DPP</th>
                      <th>Upline User</th>
                      <th class="nosort" width="10%">Aksi</th>
                  </tr>
              </thead>
              <tbody>

              </tbody>
          </table>
      </div>
  </div>


@stop

@section('customscript')
	<script type="text/javascript" src="{{URL::asset('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "<?= url("api/katuser") ?>",
                    "language": {
                           "emptyTable": "Data Kosong",
                           "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                           "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                           "infoFiltered": "(disaring dari _MAX_ total data)",
                           "search": "Cari:",
                           "lengthMenu": "Tampilkan _MENU_ Data",
                           "zeroRecords": "Tidak Ada Data yang Ditampilkan",
                           "oPaginate": {
                               "sFirst": "Awal",
                               "sLast": "Akhir",
                               "sNext": "Selanjutnya",
                               "sPrevious": "Sebelumnya"
                           },
                       },
                       columns: [
                            {data: 'DT_Row_Index',searchable: false},
                            {data: 'nama_kategori_user', name: 'nama_kategori_user'},
                            {data: 'nama_bidang', name: 'nama_bidang'},
                            {data: 'name_parent', name: 'name_parent'},
                            {data: 'action', name: 'action', orderable: false, searchable: false}
                        ]
            });
        });
	</script>
  <script type="text/javascript">

  </script>
@stop
