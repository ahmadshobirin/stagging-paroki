
@section('contentheader_title', 'Kategori User')
@extends('app')

@section('treeview_master','active')
@section('treeview_user','active')
@section('treeview_kat_user','active')

@section('title', 'Kategori User')

@section('contentheader_title', 'Ubah Kategori User')
@section('customcss')
<link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
<link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('/plugins/datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('/bootstrap/css/bootstrap.css')}}" rel="stylesheet" />
<style>
    .required {
        font-size: 12px;
        color: red;
    }
</style>
 @stop @section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">User Update Form</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->

            <form class="form-horizontal" action="{{ route('katuser.update',$dataKategori->id_kategori_user)}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('put') }}
                <div class="box-body">
                @include('admin.displayerror')
                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1  control-label">Nama <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="nama_kategori_user" class="form-control" placeholder="Kategori User" value="{{ $dataKategori->nama_kategori_user}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">DPP <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="select2 form-control" name="id_bidang" id="id_bidang" style="width: 100%;" required>
                                <option disabled="" >Pilih DPP</option>
								<option @if($dataKategori->id_bidang == 0) selected @endif value="0" >Admin</option>
								@foreach($bidang as $bid)
									<option @if($dataKategori->id_bidang == $bid->id_bidang) selected @endif value="{{$bid->id_bidang}}" >{{$bid->nama_bidang}}</option>
								@endforeach
							</select>
                        </div>
                    </div>

                     <div class="form-group" id="type">
                        <label class="col-md-2 col-md-offset-1 control-label">Tipe</label>
                        <div class="col-md-8">
                            <select class="form-control" name="type" id="type">
                                <option @if($dataKategori->type == "") selected @endif value="">Pilih Salah Satu</option>
                                <option @if($dataKategori->type == "bidang") selected @endif value="bidang">Bidang</option>
                                <option @if($dataKategori->type == "sekretaris") selected @endif value="sekretaris">Sekretaris</option>
                                <option @if($dataKategori->type == "biak") selected @endif value="biak">Biak</option>
                                <option @if($dataKategori->type == "lingkungan") selected @endif value="lingkungan">Lingkungan</option>
                                <option @if($dataKategori->type == "wilayah") selected @endif value="wilayah">Wlayah</option>
                                <option @if($dataKategori->type == "romo") selected @endif value="romo">Romo</option>
                                <option @if($dataKategori->type == "seksi") selected @endif value="seksi">Seksi</option>
                                <option @if($dataKategori->type == "subseksi") selected @endif value="subseksi">Sub Seksi</option>
                                <option @if($dataKategori->type == "koordinator") selected @endif value="koordinator">Koordinator</option>
                                <option @if($dataKategori->type == "subkoordinator") selected @endif value="subkoordinator">Sub Koordinator</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">Upline User <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="select2 form-control" name="id_parent" id="id_parent" style="width: 100%;" required>
                                <option value="0" selected>Tidak Ada</option>
                                @foreach ($upline as $up)
                                <option @if($dataKategori->id_parent == $up->id_kategori_user) selected @endif value="{{ $up->id_kategori_user }}">{{ $up->nama_kategori_user }}</option>
                                @endforeach
							</select>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/katuser/') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>



@stop @section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
    var langId = "{{asset('vendor/select2/js/i18n/id.js')}}";
    $(document).ready(function () {
        $(".select2").select2();

        $('#datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            //startDate: new Date(),
            endDate: '+0d',
            weekStart: 1,
        });

        $('#id_bidang').change(function(){
            $.getJSON("{{ url('api/upline-user/') }}/"+$(this).val(),function(results){
                $('#id_parent').find('option').not(':first').remove();
                $.each(results,function(k,o){
                    console.log(o.id_kategori_user);
                    $('#id_parent').append('<option value='+o.id_kategori_user+'>'+o.nama_kategori_user+'</option>');
                });
            });
        });
    });
</script>
@stop
