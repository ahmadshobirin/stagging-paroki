@extends('app')

@section('treeview_master','active')
@section('treeview_user','active')
@section('treeview_user_list','active')

@section('title', 'Reset Password User')

@section('contentheader_title', 'Reset Password User')
@section('customcss')
<link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
<link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('/plugins/datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('/bootstrap/css/bootstrap.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
<style>
    .required {
        font-size: 12px;
        color: red;
    }
</style>
 @stop @section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">User Update Form</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('admin/user/'.$id.'/reset-password') }}" method="post" onsubmit="return CekPassword();">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                @include('admin.displayerror')
                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1  control-label">New Password <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="password" name="password" id="password" class="form-control" placeholder="New Password" value="{{ old('password') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1  control-label">Confirm Password <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password" value="{{ old('password') }}">
                        </div>
                    </div>
                    
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/user/') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>



@stop @section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript">
    var langId = "{{asset('vendor/select2/js/i18n/id.js')}}";
    $(document).ready(function () {
        $(".select2").select2();

        $('#datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            //startDate: new Date(),
            weekStart: 1,
        });

        $('#periode').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
            },
        });
    });

    function CekPassword(){
        var pass = $("#password").val();
        var cek_pass = $("#confirm_password").val();

        if(pass != cek_pass){
            alert("Password dan Confirm Password yang Anda masukkan tidak sama")
            return false;
        }

        return true;
    }
</script>
@stop
