@section('contentheader_title', 'User')
@extends('app')

@section('treeview_master','active')
@section('treeview_user','active')
@section('treeview_user_list','active')

@section('title', 'User')

@section('contentheader_title', 'User Add')
@section('customcss')
<link rel="stylesheet" href="{{URL::asset('/plugins/select2/select2.min.css')}}">
<link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('/plugins/datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" />
<link href="{{asset('/bootstrap/css/bootstrap.css')}}" rel="stylesheet" />
<link href="{{asset('plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
<style>
    .required {
        font-size: 12px;
        color: red;
    }
</style>
 @stop @section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">User Update Form</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <?php $roleUser = \DB::table('m_role')->where('id',Auth::user()->role)->first(); ?>
            <form class="form-horizontal" action="{{ route('user.store')}}" method="post" onsubmit="return CekPassword();">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('post') }}
                <div class="box-body">
                @include('admin.displayerror')
                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1  control-label">Nama <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="nama" class="form-control" placeholder="Nama" value="">
                        </div>
                    </div>

                    {{-- <div class="form-group">
                        <label for="" class="col-md-2 col-md-offset-1 control-label">Username @if( $roleUser->status_role == 1)<span class="required">*</span> @endif</label>
                        <div class="col-md-8">
                            <input type="text"  name="username" @if( $roleUser->status_role != 1) readonly @endif  class="form-control" placeholder="username" value="">
                        </div>
                    </div> --}}

                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">E-mail
                            @if( Auth::user()->role == 1)<span class="required">*</span> @endif</label>
                        <div class="col-md-8">
                            <input type="email" name="email" @if( Auth::user()->role != 1) readonly @endif class="form-control" placeholder="example@gmail.com" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">Alamat</label>
                        <div class="col-md-8">
                            <input type="text" name="alamat" class="form-control" placeholder="Alamat" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 col-md-offset-1 control-label">Tanggal Lahir</label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" name="birthdate" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-2 col-md-offset-1 control-label">Password <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="password" name="password" id="password" class="password form-control" value="" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1  control-label">Confirm Password <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Nama" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">Role <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="select2 form-control" name="role" id="role" style="width: 100%;" required>
								<option selected="" disabled="" >Pilih Role</option>
								@foreach($getMRole as $role)
									<option value="{{$role->id}}" >{{$role->name}}</option>
								@endforeach
							</select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">Kategori User <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="select2 form-control" name="katuser" id="katuser" style="width: 100%;" required>
                                <option selected="" disabled="" >Pilih Kategori User</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="div_periode">
                        <label for="inputPassword3" class="col-md-2 col-md-offset-1 control-label">Periode User <span class="required">*</span></label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" readonly class="form-control pull-right" id="periode" name="periode" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/user/') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>



@stop @section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript">
    var langId = "{{asset('vendor/select2/js/i18n/id.js')}}";
    $(document).ready(function () {
        $(".select2").select2();

        $('#datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            //startDate: new Date(),
            endDate: '+0d',
            weekStart: 1,
        });

        $('#periode').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
            },
        });

        $('#role').on('change',function(){
            var id = $(this).val();
            if(id < 3){
                $("#div_periode").hide();
                $.ajax({
                    url : "{{ url('get-KategoriAdmin') }}",
                    method : "GET",
                    success : function(data){
                        console.log(data);

                        $('#katuser').children('option:not(:first)').remove().end();

                        $.each(data,function(index,obj){
                            $('#katuser').append('<option value="'+obj.id_kategori_user+'"> '+obj.nama_kategori_user+' - Admin</option>')
                        });
                    }
                })

            }else{
                $("#div_periode").show();
                $.ajax({
                    url : "{{ url('get-KategoriUser') }}",
                    method : "GET",
                    success : function(data){
                        console.log(data);

                        $('#katuser').children('option:not(:first)').remove().end();

                        $.each(data,function(index,obj){
                            $('#katuser').append('<option value="'+obj.id_kategori_user+'"> '+obj.nama_kategori_user+' - '+ obj.nama_bidang+'</option>')
                        });
                    }
                })

            }
        });
    });
    function CekPassword(){
        var pass = $("#password").val();
        var cek_pass = $("#confirm_password").val();

        if(pass != cek_pass){
            alert("Password dan Confirm Password yang Anda masukkan tidak sama")
            return false;
        }

        return true;
    }
</script>
@stop
