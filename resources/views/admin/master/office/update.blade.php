@extends('app')

@section('treeview_master','active')
@section('treeview_frontend','active')
@section('treeview_office','active')

@section('title', 'Master Gereja')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@section('contentheader_title', 'Master Gereja')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/office/') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                    @include('admin.displayerror')
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Email Sekretariat  <span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" placeholder="Email Sekretariat" value="{{$office->email_office }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Alamat Gereja </label>
                                <textarea name="alamat" required id="alamat" class="form-control tinymce">{{$office->alamat_office}}</textarea>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Provinsi <span class="required">*</span></label>
                                <select class="form-control select2" id="provinsi" name="provinsi">
                                    <option disabled selected value="">Pilih Satu...</option>
                                    @foreach($prov as $provinsi)
                                    <option @if($office->id_provinsi == $provinsi->id) selected @endif value="{{$provinsi->id}}">{{$provinsi->code}} - {{$provinsi->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Kota / Kab <span class="required">*</span></label>
                                <select class="form-control select2" id="kota_kab" name="kota_kab">
                                    <option disabled selected value="">Pilih Satu...</option>
                                    @foreach($kota as $kota_kab)
                                    <option @if($office->id_kota_kab == $kota_kab->id) selected @endif value="{{$kota_kab->id}}">{{$kota_kab->code}} - {{$kota_kab->type}} {{$kota_kab->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Kecamatan <span class="required">*</span></label>
                                <select class="form-control select2" id="kec" name="kec">
                                    <option disabled selected value="">Pilih Satu...</option>
                                    @foreach($kec as $kecamatan)
                                    <option @if($office->id_kecamatan == $kecamatan->id) selected @endif value="{{$kecamatan->id}}">{{$kecamatan->code}} - {{$kecamatan->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Kelurahan <span class="required">*</span></label>
                                <select class="form-control select2" id="kel" name="kel">
                                    <option disabled selected value="">Pilih Satu...</option>
                                    @foreach($kel as $kelurahan)
                                    <option @if($office->id_kelurahan == $kelurahan->id) selected @endif value="{{$kelurahan->id}}">{{$kelurahan->code}} - {{$kelurahan->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Telp Sekretariat  <span class="required">*</span></label>
                                <input type="text" class="form-control telp" name="telp" placeholder="Telp Sekretariat" value="{{ $office->telp_office }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <label for="" class="control-label">Iframe Google Maps  <span class="required">*</span></label>
                                <textarea class="form-control" name="map">{{$office->iframe_office }}</textarea>
                                {{-- <input type="text" placeholder="Iframe Google Maps" value="{{$office->iframe_office }}"> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Facebook</label>
                                <input type="text" class="form-control" name="fb" id="fb" placeholder="Facebook Sekretariat" value="{{ $office->url_fb }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Twitter</label>
                                <input type="text" class="form-control" name="twitter" id="twitter" placeholder="Twitter Sekretariat" value="{{ $office->url_twitter }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Instagram</label>
                                <input type="text" class="form-control" name="ig" id="ig" placeholder="Instagram Sekretariat" value="{{ $office->url_instagram }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="control-label">Youtube</label>
                                <input type="text" class="form-control" name="youtube" id="youtube" placeholder="Youtube Sekretariat" value="{{ $office->url_youtube }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        {{-- <a href="{{ url('/admin/artikel') }}" class="btn btn-info">Kembali</a> --}}
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('customscript')
@endsection