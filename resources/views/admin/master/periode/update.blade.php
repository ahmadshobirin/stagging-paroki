@extends('app')

@section('title', 'Periode') @section('contentheader_title', 'Ubah Periode')

@section('treeview_master','active')
@section('treeview_periode','active')


@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
    <link href="{{ URL::asset('/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('admin/periode/'.$dataPeriode->id_periode) }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('put') }}
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Nama Periode</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="nama_periode" placeholder="Nama Periode" value="{{ $dataPeriode->nama_periode }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Type Periode</label>
                        <div class="col-sm-8">
                            <select class="form-control" disabled name="type_periode">
                                <option @if($dataPeriode->type_periode == "baptis_balita") selected @endif value="baptis_balita">Baptis Balita</option>
                                <option @if($dataPeriode->type_periode == "baptis_anak") selected @endif value="baptis_anak">Baptis Anak</option>
                                <option @if($dataPeriode->type_periode == "baptis_dewasa") selected @endif value="baptis_dewasa">Baptis Dewasa</option>
                                <option @if($dataPeriode->type_periode == "komuni") selected @endif value="komuni">Komuni</option>
                                <option @if($dataPeriode->type_periode == "krisma") selected @endif value="krisma">Krisma</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Periode</label>
                        <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" readonly class="form-control pull-right" id="periode" name="periode" value="{{ date('d-m-Y', strtotime($dataPeriode->start_date)) }} - {{ date('d-m-Y', strtotime($dataPeriode->end_date)) }}">
                                </div>
                                <!-- /.input group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Tanggal Pelaksanaan </label>
                        <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right datepicker" name="tgl_pelaksanaan" id="tgl_pelaksanaan" value="" value="">
                                </div>
                                <!-- /.input group -->
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/periode') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@endsection

@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/moment.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#periode').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
            },
        });

        $('#tgl_pelaksanaan').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            orientation : 'bottom',
            weekStart: 1,
        });

        $('#periode').on('change.datepicker', function(ev){
            $('#tgl_pelaksanaan').attr('disabled',false);
            var picker = $(ev.target).data('daterangepicker');
            start = new Date(picker.startDate);
            $('#tgl_pelaksanaan').val('');
            $('#tgl_pelaksanaan').datepicker('setStartDate', start);
        });

        var date = new Date("{!!$dataPeriode->tgl_pelaksanaan !!}")
        $('#tgl_pelaksanaan').datepicker('setDate', date);
    });
</script>
@stop