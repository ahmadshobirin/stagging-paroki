@extends('app')

@section('title', 'Periode') @section('contentheader_title', 'Tambah Periode')

@section('treeview_master','active')
@section('treeview_periode','active')


@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
    <link href="{{ URL::asset('/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('admin/periode') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('post') }}
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Nama Periode</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="nama_periode" placeholder="Nama Periode" value="{{ old('nama_periode') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Type Periode</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="type_periode">
                                <option value="baptis_balita">Baptis Balita</option>
                                <option value="baptis_anak">Baptis Anak</option>
                                <option value="baptis_dewasa">Baptis Dewasa</option>
                                <option value="komuni">Komuni</option>
                                <option value="krisma">Krisma</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Periode</label>
                        <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" readonly class="form-control pull-right" id="periode" name="periode" value="">
                                    <span id="periodespan"></span>
                                </div>
                                <!-- /.input group -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Tanggal Pelaksanaan</label>
                        <div class="col-sm-8">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right datepicker" name="tgl_pelaksanaan" id="tgl_pelaksanaan" value="" disabled>
                                </div>
                                <!-- /.input group -->
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/periode') }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@endsection

@section('customscript')
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/moment.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#periode').daterangepicker({
            startDate : moment(),
            locale: {
              format: 'DD-MM-YYYY'
            },
        });

        $('#tgl_pelaksanaan').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            orientation : 'bottom',
            weekStart: 1,
        });

        $('#periode').on('change.datepicker', function(ev){
            var picker = $(ev.target).data('daterangepicker');
            start = new Date(picker.startDate)
            $('#tgl_pelaksanaan').attr('disabled',false);
            $('#tgl_pelaksanaan').datepicker('setStartDate', start);
        });
    });
</script>
@stop