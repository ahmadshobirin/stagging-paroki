@extends('app')

@section('treeview_master','active')
@section('treeview_user','active')
@section('treeview_role','active')

@section('title', 'User Role')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
@stop

@section('contentheader_title', 'User Role')

@section('main-content')
  <div class="box box-primary">
        <div class="box-body">
            <div class="">
                <a href="{{url('admin/userrole/create')}}" class="btn btn-success btn-md">
                    <i class="fa fa-plus"></i> Tambah Data
                </a>
                {{--<a href="{{url('admin/report-master-userrole/view')}}" class="btn btn-default btn-md">
                    <i class="fa fa-file"></i> View Report
                </a>
                <a href="{{url('admin/report-excel-master-userrole')}}" class="btn bg-green color-palette">
                    <i class="fa fa-file-excel-o"></i> Export To Excel
                </a>
                <a href="{{url('admin/report-master-userrole/download')}}" class="btn btn-warning btn-md">
                    <i class="fa fa-file-pdf-o"></i> Export to PDF
                </a>--}}
            </div>
            <br>
            <table class="table table-striped table-hover table-responsive" id="table">
                <thead>
                    <tr>
                        <th style="width: 10%">No.</th>
                        <th>Name</th>
                        <th class="nosort" style="width: 20%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                  <?php $i=1 ?>
                  @foreach($dataRole as $role)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$role->name}}</td>
                            <td><a href="{{url('/admin/userrole/'.$role->id.'/edit')}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a></td>
                        </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('customscript')
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

  <script type="text/javascript">    
    $('#table').DataTable({
       'paging': true,
       'lengthChange': true,
       'searching': true,
       'ordering': true,
       'info': true,
       'autoWidth': false,
       "language": {
           "emptyTable": "Data Kosong",
           "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
           "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
           "infoFiltered": "(disaring dari _MAX_ total data)",
           "search": "Cari:",
           "lengthMenu": "Tampilkan _MENU_ Data",
           "zeroRecords": "Tidak Ada Data yang Ditampilkan",
           "oPaginate": {
               "sFirst": "Awal",
               "sLast": "Akhir",
               "sNext": "Selanjutnya",
               "sPrevious": "Sebelumnya"
           },
       },
        'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': ['nosort']
      }],
    });
  </script>
@stop