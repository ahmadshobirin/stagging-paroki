@extends('app')

@section('treeview_master','active')
@section('treeview_user','active')
@section('treeview_role','active')

@section('title', 'User Role')

@section('contentheader_title', 'User Role Update')

@section('main-content')
<div class="row">
    <div class="col-md-10 col-md-offset-1" style="max-width:500px">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Role Update Form</h3>
            </div> --}}
            <form class="form-horizontal" action="{{ route('userrole.update',$dataRole->id) }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{ method_field('put') }}
                <div class="box-body">
                @include('admin.displayerror')
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" value="{{$dataRole->name}}" placeholder="role user...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Permission</label>
                        <div style="margin-top: 8px">
                            <div class="col-sm-4">
                                <input type="checkbox" @if($dataRole->status_role == 1) checked @endif name="role" class="minimal">
                                Role User
                            </div>
                            <div class="col-sm-5">
                                <input type="checkbox" @if($dataRole->status_approval == 1) checked @endif name="approval" class="minimal">
                                Approval
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="margin-top: -10px">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div style="margin-top: 8px">
                            <div class="col-sm-4">
                                <input type="checkbox" @if($dataRole->status_trx == 1) checked @endif name="trx" class="minimal">
                                Transaksi
                            </div>
                             <div class="col-sm-5">
                                <input type="checkbox" @if($dataRole->status_master_produk == 1) checked @endif name="produk" class="minimal">
                                Produk
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/userrole') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" value="Batal">
                        <input type="submit"  class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
