@extends('app') 

@section('title', 'Link') @section('contentheader_title', 'Tambah Link '.ucfirst($nama))

@section('treeview_master','active')
@section('treeview_link','active')
@section('treeview_link_'.$nama,'active')

@section('customcss')
    <link rel="stylesheet" href="{{URL::asset('/css/datatables.min.css')}}">
@stop

@section('main-content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-info">
            {{-- <div class="box-header with-border">
                <h3 class="box-title">Provinsi Form Update</h3>
            </div> --}}
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('admin/link/'.$nama) }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{-- {{ method_field('post') }} --}}
                <div class="box-body">
                    @include('admin.displayerror')
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Judul Link</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="judul" placeholder="Judul Link" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">URL Link</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="url" placeholder="URL Link" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Desc Link</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="desc"></textarea>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/link/'.$nama) }}" class="btn btn-info">Kembali</a>
                        <button type="reset" class="btn btn-danger">Batal</button>
                        <button  type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
</div>
@stop

@section('customscript')
@stop