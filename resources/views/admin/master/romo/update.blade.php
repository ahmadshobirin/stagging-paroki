@extends('app')

@section('treeview_post','active')
@section('treeview_gereja','active')
@section('treeview_gereja_tempat','active')

@section('title', 'Ubah Romo Paroki')

@section('customcss')
  <link rel="stylesheet" href="{{URL::asset('css/datatables.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('plugins/select2/select2.min.css')}}">
  <link href="{{asset('plugins/select2/select2-bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />
@stop

@section('contentheader_title', 'Ubah Romo Paroki')

@section('main-content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form class="" method="post" action="{{ url('admin/gereja/romo/'.$dataRomo->id_romo) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body" style="margin-left: 20px;">
                @include('admin.displayerror')
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Nama Romo  <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama_romo" placeholder="Nama Romo" value="{{ $dataRomo->nama_romo }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Tempat Lahir Romo <span class="required">*</span></label>
                                <input type="text" class="form-control" name="tempat_romo" placeholder="Tempat Lahir Romo" value="{{ $dataRomo->tempat_lahir }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Tanggal Tahbisan Romo <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="datepicker1" class="form-control input-sm" name="tanggal_lahir" value="{{ date('d-m-Y', strtotime($dataRomo->tanggal_lahir)) }}" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Tempat Tahbisan Imam <span class="required">*</span></label>
                                <input type="text" class="form-control" name="tempat_tahbisan_romo" placeholder="Tempat Tahbisan Imam" value="{{ $dataRomo->tempat_tahbisan_imam }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Tanggal Tahbisan Imam <span class="required">*</span></label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="datepicker2" class="form-control input-sm" name="tanggal_tahbisan" value="{{ date('d-m-Y', strtotime($dataRomo->tanggal_tahbisan_imam)) }}" required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="control-label">Periode Masa Tugas <span class="required">*</span></label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" readonly class="form-control pull-right" id="periode" name="periode" value="{{date('d-m-Y', strtotime($dataRomo->awal_masa_tugas))}} - {{date('d-m-Y', strtotime($dataRomo->akhir_masa_tugas))}}">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Misa Perdana </label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" id="datepicker3" class="form-control input-sm" name="misa_perdana" @if($dataRomo->tanggal_lahir == null) value="{{ old('tanggal_tahbisan') }}" @else {{ date('d-m-Y', strtotime($dataRomo->misa_perdana)) }} @endif required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                             <div class="form-group">
                                <label for="" class="control-label">Status</label>
                                <input type="text" class="form-control" name="status_romo" placeholder="Status Romo" value="{{ $dataRomo->status }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Image Romo</label>
                                <input class="form-control" type="file" name="image" id="image" onchange="return ValidateFileUpload()">
                            </div>
                            <label id="label_image">Bukan file image</label>
                            @if($dataRomo->image_romo != 'no_image.png')
                            <div class="col-md-6">
                                <img src="{{ asset('upload/romo/'.$dataRomo->image_romo) }}" style="width: 200px; height: 200px" name="romo_image" id="romo_image">
                                <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="true">
                                <input type="hidden" name="image_hidden" id="image_hidden" value="{{$dataRomo->image_romo}}">
                            <input type="button" class="btn btn-danger" id="reset_image" value="Delete">
                            </div>
                            @else
                            <input type="hidden" name="value_image_hidden" id="value_image_hidden" value="false">
                            <input type="hidden" name="image_hidden" id="image_hidden" value="@if($dataRomo->image_romo != "no_image.png") {{$dataRomo->image_romo}} @endif">
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Tipe <span class="required">*</span></label>
                                <select class="form-control" name="tipe_romo">
                                    <option value="">Pilih Satu...</option>
                                    <option @if($dataRomo->tipe == "romo") selected @endif value="romo">Romo</option>
                                    <option @if($dataRomo->tipe == "diakon") selected @endif value="diakon">Diakon</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Desc Romo</label>
                                <textarea class="form-control" name="desc_romo">{{$dataRomo->desc_romo }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <a href="{{ url('/admin/gereja/romo') }}" class="btn btn-info">Kembali</a>
                        <input type="reset" class="btn btn-danger" id="reset" value="Batal">
                        <input  type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('customscript')
<script type="text/javascript" src="{{URL::asset('/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/select2/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $(document).ready(function () {
        $('.select2').select2();
        $("#label_image").hide();
        $('#datepicker1').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            endDate : today,
            weekStart: 1,
        });
        $('#datepicker2').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            endDate: today,
            weekStart: 1,
        });

        $('#datepicker3').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            endDate: today,
            weekStart: 1,
        });

        $("#reset_image").click(function(){
            var val = "false";
            $("#value_image_hidden").val(val);
            $("#romo_image").hide();
            $("#reset_image").hide();
        });

        $('.timepicker').timepicker({
            //timePickerIncrement: 10,
            showMeridian: false,
            minuteStep :5,
        });

        $('#periode').daterangepicker({
            locale: {
              format: 'DD-MM-YYYY'
            },
        });
    });
</script>
<script>
    function ValidateFileUpload() {
        var fuData = document.getElementById('image');
        var FileUploadPath = fuData.value;
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

        //The file uploaded is an image
        if (Extension == "jpeg" || Extension == "jpg" || Extension == 'gif' || Extension == 'png') {
        // To Display
            if (fuData.files && fuData.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#image').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                var startIndex = (fuData.indexOf('\\') >= 0 ? fuData.lastIndexOf('\\') : fuData.lastIndexOf('/'));
                var filename = fuData.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                    $("#image").val(filename);
                }
            }
            $("#label_image").hide();
        } else {
            $('#image').val('');
            $("#label_image").show();
            return false;
        }
    }
</script>
@endsection