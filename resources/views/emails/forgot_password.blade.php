<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f3f3f3" style="table-layout:fixed;">
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
<tr>
	<td>
		<table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
			<tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
			<tr>
				<td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="http://www.suppliermep.com/"><img width="125" height="50" border="0" alt="Supplier MEP" src="{{ URL::asset('images/logo.png') }}"></a>
				</td>
			</tr>
			<tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#ffffff">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
							<td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
									<tr>
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											<strong>{{$username}}</strong> kami menerima permintaan untuk mengubah password Anda. Silahkan klik link berikut untuk menyetel ulang password anda :
										</td>
									</tr>
									<tr>
										<td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td width="100%" style="line-height:23px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
											<a href="{{url('password/reset/'.$token)}}" target="_blank" style="color:#ffffff;text-decoration:none;padding:12px 24px;background:#0c4c8a;">Setel Ulang Password</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#0c4c8a">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
							<td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
							&copy; 2018 Suppliermep.com. All rights reserved
							</td>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
</table>