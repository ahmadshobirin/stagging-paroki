<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f6f6f6" style="table-layout:fixed;">
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
<tr>
	<td>
		<table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
			<tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
			<tr>
				<td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="http://www.biztekweb.com/"><img width="70" height="75" border="0" alt="Biztek" title="Biztek" src="{{ URL::asset('images/logo.png') }}"></a>
				</td>
			</tr>
			<tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#ffffff">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
							<td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
									<tr>
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Hai, <strong>{{$order->name_user}}</strong><br>Selamat anda telah bergabung dengan <strong>Biztek</strong>, <br><strong>Paket {{$order->name_produk}}</strong> anda sudah aktif, berikut detailnya :
									</tr>
									<tr>
										<td height="24" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Subdomain
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong><a href="http://trial.biztekweb.com/" target="_blank" style="color:#20a1db;text-decoration:none;">http:/{{$order->subdomain}}.biztekweb.com/</a></strong>
													</td>
												</tr>
												@if($order->id_produk == 1)
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Company Code
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{$order->company_code}}
													</td>
												</tr>
												@endif
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Username
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														@if($order->id_produk == 1)
															{{$order->email}}
														@else
															{{$order->username}}
														@endif
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Password
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{$password}}
													</td>
												</tr>
											</table>
										</td>
									</tr>
									@if($order->id_produk == 1)
									<tr>
										<td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Paket {{$order->name_produk}} hanya dapat digunakan selama {{$order->masa_aktif}} bulan setelah pengaktifan, jika anda ingin menggunakannya kembali silahkan upgrade ke paket lainnya.<br>Lihat paket lainnya? <a href="{{url('/produk')}}" target="_blank" style="color:#20a1db;text-decoration:none;">disini</a>
										</td>
									</tr>
									@else
									<tr>
										<td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									@endif			
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#20a1db">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
							<td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
							&copy; 2018 <a href="http://www.biztekweb.com/" target="_blank" style="color:#ffffff;text-decoration:none;">biztekweb.com</a>. All rights reserved
							</td>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
</table>