<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f6f6f6" style="table-layout:fixed;">
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
<tr>
	<td>
		<table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
			<tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
			<tr>
				<td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="http://www.biztekweb.com/"><img width="70" height="75" border="0" alt="Biztek" title="Biztek" src="../images/logo.png"></a>
				</td>
			</tr>
			<tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#ffffff">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
							<td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
									<tr>
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Hai, <strong>{{$confirm->customer_name}}</strong><br>
											@if($confirm->status == "accept")
											Kami telah memverifikasi pembayaran anda, dengan data sebagai berikut : 
											@else
											Mohon Maaf kami tidak dapat memverifikasi pembayaran anda, dengan data sebagai berikut:
											@endif
										</td>
									</tr>
									<tr>
										<td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Kode
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong>{{$confirm->code_trx}}</strong>
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Email
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{$confirm->email_pengirim}}
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Bank Tujuan Transfer
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{$confirm->nama_bank_tujuan}} {{$confirm->nomer_rekening}} a/n {{$confirm->nama_rekening_tujuan}}
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Tanggal transfer
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{date('d F Y', strtotime($confirm->tanggal_transfer))}}
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Jumlah Transfer
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#20a1db;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Rp. {{ number_format($confirm->nominal_transfer, 0, '.', '.') }}
													</td>
												</tr>
												@if($confirm->status == "reject")
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Keterangan Penolakan
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{$confirm->keterangan_admin}}
													</td>
												</tr>
												@endif
											</table>
										</td>
									</tr>
									<tr>
										<td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										@if($confirm->status == "accept")
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Kami akan segera mengaktifkan subdomain pesanan anda, proses aktifasi <br> 1-2 Hari kerja.
										</td>
										@else
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Silahkan lakukan konfirmasi pembayaran ulang atau hubungi kami : <strong>{{$confirm->telp_office}}</strong>
										</td>
										@endif
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#20a1db">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
							<td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
							&copy; 2018 <a href="http://www.biztekweb.com/" target="_blank" style="color:#ffffff;text-decoration:none;">biztekweb.com</a>. All rights reserved
							</td>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
</table>