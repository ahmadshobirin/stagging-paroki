<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f6f6f6" style="table-layout:fixed;">
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
<tr>
	<td>
		<table align="center" border="0" width="600" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
			<tr><td height="24" style="line-height:0;font-size:0;" bgcolor="#ffffff">&nbsp;</td></tr>
			<tr>
				<td height="43" align="center" bgcolor="#ffffff" style="mso-line-height-alt:0;mso-margin-top-alt:1px;line-height:0;font-size:0;"><a target="_blank" href="http://www.biztekweb.com/"><img width="70" height="75" border="0" alt="Biztek" title="Biztek" src="{{ URL::asset('images/logo.png') }}"></a>
				</td>
			</tr>
			<tr><td height="24" bgcolor="#ffffff" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#ffffff">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
							<td height="60" style="line-height:0;font-size:0;">&nbsp;</td>
							<td rowspan="2" width="24" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
									<tr>
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Hai, <strong>{{$order->nama_user}}</strong><br>
											@if($order->id_produk != 1)
											Terima kasih telah menggunakan paket dari <strong>Biztek</strong>, <em>Jika Anda telah melakukan pembayaran, silakan abaikan pesan ini</em>, berikut ini detail pesanan anda :
											@else
											Paket akan diaktifkan 1-2 Hari kerja, berikut data pesanan anda :
											@endif
									</tr>
									<tr>
										<td height="24" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Kode
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong>{{$order->code}}</strong>
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Tanggal Pemesanan
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{date('d F Y', strtotime($order->created_at))}}
													</td>
												</tr>
												@if($order->id_produk != 1)
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Metode Pembayaran
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Bank Transfer
													</td>
												</tr>
												@endif
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Pilihan Paket
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Paket {{$order->nama_produk}}
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Subdomain
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														{{$order->nama_subdomain}}.biztekweb.com
													</td>
												</tr>
												<tr>
													<td width="30%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														Harga
													</td>
													<td width="5%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
														:
													</td>
													<td style="line-height:23px;font-size:15px;color:#20a1db;text-decoration:none;font-family:Arial, Helvetica, sans-serif;">
														<strong>Rp. {{ number_format($order->harga, 0, '.','.') }}</strong>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										@if($order->id_produk != 1)
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Selelah melakukan pembayaran silahkan Konfirmasi Pembayaran anda atau <br>email ke billing@biztek.co.id untuk kami verifikasi.<br>Akun biztek akan diaktifasi 1-2 Hari kerja setelah konfirmasi pembayaran diterima.
										</td>
										@else
										<td width="100%" style="line-height:23px;font-size:15px;color:#333333;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
											Paket {{$order->nama_produk}} hanya dapat digunakan selama {{$order->masa_aktif}} bulan setelah pengaktifan, jika anda ingin menggunakannya kembali silahkan upgrade ke paket lainnya.<br>Lihat paket lainnya? <a href="{{url('/produk')}}" target="_blank" style="color:#20a1db;text-decoration:none;">disini</a>
										@endif
									</tr>
									<tr>
										<td height="36" style="line-height:0;font-size:0;">&nbsp;</td>
									</tr>
									<tr>
										<td width="100%" style="line-height:23px;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:center;">
											<a href="{!! url('/detail-trx/'.$order->id) !!}" target="_blank" style="color:#ffffff;text-decoration:none;padding:12px 24px;background:#20a1db;">Konfirmasi Pembayaran</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="3" height="60" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td height="24" style="line-height:0;font-size:0;">&nbsp;</td></tr>
			<tr>
				<td width="100%" bgcolor="#20a1db">
					<table border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
						<tr>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
							<td width="750" valign="middle" height="50" style="line-height:23px;font-size:15px;color:#ffffff;text-decoration:none;font-family:Arial, Helvetica, sans-serif;text-align:justify;">
							&copy; 2018 <a href="http://www.biztekweb.com/" target="_blank" style="color:#ffffff;text-decoration:none;">biztekweb.com</a>. All rights reserved
							</td>
							<td width="25" style="line-height:0;font-size:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr><td height="60" style="line-height:0;font-size:0;">&nbsp;</td></tr>
</table>