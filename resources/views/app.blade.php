<!DOCTYPE html>
<html>
@include('template.htmlheader')

<body class="hold-transition skin-purple sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<!-- header -->
		@include('template.mainheader')
		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		@include('template.sidebar')

		<!-- =============================================== -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			@include('template.contentheader')

			<!-- Main content -->
			<section class="content">
				@yield('main-content')
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<strong>
					<strong>Copyright &copy; {{ date('Y') }} <a href="!#">Paroki Roh Kudus</a></strong>
			</strong>
		</footer>

		<!-- Control Sidebar -->
		{{-- @include('template.partial.controlsidebar') --}}
		<!-- /.control-sidebar -->

		<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div>
	<!-- ./wrapper -->
	@include('template.scripts')

</body>

</html>