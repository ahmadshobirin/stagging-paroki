<!-- jQuery 2.2.3 -->
<script src="{{ URL::asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ URL::asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ URL::asset('plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ URL::asset('dist/js/demo.js') }}"></script>

<script src="{{ URL::asset('js/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ URL::asset('js/tinymce/js/tinymce/jquery.tinymce.min.js') }}"></script>
<script>
	$('.telp').keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 32 || e.which > 57 || (32 < e.which && e.which < 40) || e
				.which == 42 || (43 < e.which && e.which < 48))) {
			return false;
		}
	});

	$('body').on('click', 'a.postback', function(e) {
		e.preventDefault(); // does not go through with the link.

		var $this = $(this);

		$.ajax({
			url: $this.attr('href'),
			type: 'POST',
			data : {
				_token : "{{ csrf_token() }}",
				_method : "DELETE",
			}
		}).done(function (results) {
			location.reload()
		});
	});
	tinymce.init({
		selector: 'textarea.tinymce',
		plugins: [
			"advlist autolink link lists charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars insertdatetime nonbreaking",
			"table contextmenu directionality emoticons paste textcolor code"
		],
		toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
		setup: function (ed) {
			ed.on('change', function (e) {
				tinyMCE.triggerSave();
			});
			ed.on('SetContent', function (e) {
				tinyMCE.triggerSave();
			});
		}
	});

	function toNumber(string) {
		if (string === "" || string === null) {
			return 0;
		}
		return parseInt(string.replace(/\./g, ""));
	}

	function toCurrency(data) {
		return data.toLocaleString('id');
	}

	function fixDec(num) {
		return num | 0;
	}
</script>

@yield('customscript')