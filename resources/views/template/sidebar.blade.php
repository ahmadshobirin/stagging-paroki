<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    @php
        $roleUser = \DB::table('m_role')->where('id',Auth::user()->role)->first();
        $katUser = \DB::table('m_kategori_user')->where('id_kategori_user',Auth::user()->id_header_user)->first();
    @endphp
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="{{ url('admin') }}">
                    <i class="fa fa-tachometer-alt"></i>
                    <span>
                        Beranda
                        {{-- {{Auth::user()->id}} --}}
                        {{-- {{$katUser->type}} --}}
                    </span>
                </a>
            </li>
            @if(in_array($roleUser->id,[1,2]))
            <li class="treeview @yield('treeview_master')">
                <a href="#">
                    <i class="fa fa-book"></i>
                        <span>Master</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li style="margin-left: 7px;" class="@yield('treeview_user')">
                        <a href="#">
                            <i class="fa fa-address-card"></i> User
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_user_bidang')">
                                <a href="{{ url('admin/bidang') }}">
                                    <i class='fa fa-user-circle'></i>
                                    <span>DPP User</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_kat_user')">
                                <a href="{{ url('admin/katuser') }}">
                                    <i class='fa fa-user-circle'></i>
                                    <span>Kategori User</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_user_list')">
                                <a href="{{ url('admin/user') }}">
                                    <i class='fa fa-user-circle'></i>
                                    <span>Daftar User</span>
                                </a>
                            </li>

                            <li style="" class="@yield('treeview_user_list_ummat')">
                                <a href="{{ url('admin/users/ummat') }}">
                                    <i class='fa fa-user-circle'></i>
                                    <span>Daftar User Umat</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                </ul>
                <ul class="treeview-menu">
                </ul>
                <ul class="treeview-menu">
                    <li style="margin-left: 7px;" class="@yield('treeview_frontend')">
                        <a href="#">
                            <i class="fa fa-globe"></i>Website
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_office')">
                                <a href="{{ url('admin/office') }}">
                                    <i class='fa fa-building'></i>
                                    <span>Office</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_visi_misi')">
                                <a href="{{ url('admin/visi-misi') }}">
                                    <i class='fa fa-building'></i>
                                    <span>Visi & Misi</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li style="margin-left: 7px;" class="@yield('treeview_link')">
                        <a href="#">
                            <i class="fa fa-link"></i> Link
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_link_lingkungan')">
                                <a href="{{ url('admin/link/lingkungan') }}">
                                    <i class='fa fa-link'></i>
                                    <span>Lingkungan</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_link_wilayah')">
                                <a href="{{ url('admin/link/wilayah') }}">
                                    <i class='fa fa-link'></i>
                                    <span>Wilayah</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_link_seksi')">
                                <a href="{{ url('admin/link/seksi') }}">
                                    <i class='fa fa-link'></i>
                                    <span>Seksi</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li style="margin-left: 7px;" class="@yield('treeview_download')">
                        <a href="#">
                            <i class="fa fa-download"></i> Download
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_download_warta')">
                                <a href="{{ url('admin/download/warta') }}">
                                    <i class='fa fa-file'></i>
                                    <span>Warta Paroki</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_download_majalah')">
                                <a href="{{ url('admin/download/majalah') }}">
                                    <i class='fa fa-file'></i>
                                    <span>Majalah ERKA</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li style="margin-left: 7px;" class="@yield('treeview_periode')">
                        <a href="{{ url('admin/periode') }}">
                            <i class="fa fa-calendar"></i>
                            <span>Periode</span>
                        </a>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li style="margin-left: 7px;" class="@yield('treeview_tahapan')">
                        <a href="{{ url('admin/tahapan') }}">
                            <i class="fa fa-calendar"></i>
                            <span>Tahapan</span>
                        </a>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li style="margin-left: 7px;" class="@yield('treeview_gereja')">
                        <a href="#">
                            <i class="fa fa-church"></i> Gereja
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_gereja_romo')">
                                <a href="{{ url('admin/gereja/romo') }}">
                                    <i class='fa fa-user'></i>
                                    <span>Romo</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_gereja_tempat')">
                                <a href="{{ url('admin/gereja/tempat') }}">
                                    <i class='fa fa-home'></i>
                                    <span>Tempat</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_gereja_renungan')">
                                <a href="{{ url('admin/gereja/renungan') }}">
                                    <i class='fa fa-pray'></i>
                                    <span>Renungan</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_gereja_misa')">
                                <a href="{{ url('admin/gereja/misa') }}">
                                    <i class='fa fa-cross'></i>
                                    <span>Misa</span>
                                </a>
                            </li>
                            <li style="" class="@yield('treeview_gereja_sejarah')">
                                <a href="{{ url('admin/gereja/sejarah') }}">
                                    <i class='fa fa-bible'></i>
                                    <span>Sejarah Gereja</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            @endif
            <li class="treeview @yield('treeview_posting')">
                @if(in_array($roleUser->id,[1,2]))
                <a href="#">
                    <i class="fa fa-check-square"></i>
                        <span>Approval</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li style="" class="@yield('treeview_info')">
                        <a href="{{ url('admin/approval/info') }}">
                            <i class='fa fa-info'></i>
                            <span>Info</span>
                        </a>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li style="" class="@yield('treeview_artikel')">
                        <a href="{{ url('admin/approval/artikel') }}">
                            <i class='fa fa-info'></i>
                            <span>Artikel</span>
                        </a>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li style="" class="@yield('treeview_kronik')">
                        <a href="{{ url('admin/approval/kronik') }}">
                            <i class='fa fa-info'></i>
                            <span>Kronik</span>
                        </a>
                    </li>
                </ul>
                <ul class="treeview-menu">
                    <li class="treeview @yield('treeview_sakramen')">
                        <a href="#">
                            <i class="fa fa-calendar"></i>
                                <span>Sakramen</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_sakramen_pernikahan')">
                                <a href="{{ url('admin/sakramen/nikah') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Pernikahan</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_sakramen_baptis')">
                                <a href="#">
                                    <i class='fa fa-calendar'></i>
                                    <span>Baptis</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_sakramen_baptis_balita')">
                                        <a href="{{ url('admin/sakramen/baptis/balita') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Balita</span>
                                        </a>
                                    </li>
                                </ul>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_sakramen_baptis_anak')">
                                        <a href="{{ url('admin/sakramen/baptis/anak') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Anak</span>
                                        </a>
                                    </li>
                                </ul>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_sakramen_baptis_dewasa')">
                                        <a href="{{ url('admin/sakramen/baptis/dewasa') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Dewasa</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_sakramen_komuni')">
                                <a href="{{ url('admin/sakramen/komuni') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Komuni</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_sakramen_krisma')">
                                <a href="{{ url('admin/sakramen/krisma') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Krisma</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview @yield('treeview_sakramen_transaction')">
                        <a href="{{ url('admin/sakramen/transaction') }}">
                            <i class="fa fa-calendar"></i>
                            <span>Transaksi Sakramen</span>
                        </a>
                    </li>
                </ul>
                @endif
            </li>

            <li class="treeview @yield('treeview_post')">
                <a href="#">
                    <i class="fa fa-edit"></i>
                        <span>Posting</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                @if($roleUser->id == 3)
                {{-- condition typeUser(m_kategori_user) role kontributor (3) --}}
                @if($katUser->type == 'lingkungan')
                    <ul class="treeview-menu">
                        <li style="" class="@yield('treeview_kronik')">
                            <a href="{{ url('admin/kronik') }}">
                                <i class='fa fa-info'></i>
                                <span>Kronik</span>
                            </a>
                        </li>

                        <li style="" class="@yield('treeview_info')">
                            <a href="{{ url('admin/info') }}">
                                <i class='fa fa-info'></i>
                                <span>Info</span>
                            </a>
                        </li>

                        <li style="" class="@yield('treeview_artikel')">
                            <a href="{{ url('admin/artikel') }}">
                                <i class='fa fa-info'></i>
                                <span>Artikel</span>
                            </a>
                        </li>

                        <li  style="" class="@yield('treeview_proposal')">
                            <a href="{{ url('/admin/proposal') }}">
                                <i class='fa fa-file'></i>
                                <span>Proposal</span>
                            </a>
                        </li>
                        <li  style="" class="@yield('treeview_lpj')">
                            <a href="{{ url('/admin/lpj') }}">
                                <i class='fa fa-file'></i>
                                <span>LPJ</span>
                            </a>
                        </li>
                        <li class="treeview @yield('treeview_ttd_sakramen')">
                            <a href="#">
                                <i class="fa fa-calendar"></i>
                                    <span>Sakramen</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                            </a>
                            <ul class="treeview-menu">
                                <li style="" class="@yield('treeview_ttd_sakramen_pernikahan')">
                                    <a href="{{ url('admin/sakramen_ttd/nikah') }}">
                                        <i class='fa fa-calendar'></i>
                                        <span>Pernikahan</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="treeview-menu">
                                <li style="" class="@yield('treeview_ttd_sakramen_baptis')">
                                    <a href="#">
                                        <i class='fa fa-calendar'></i>
                                        <span>Baptis</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                    </a>
                                    <ul class="treeview-menu">
                                        <li style="" class="@yield('treeview_ttd_sakramen_baptis_balita')">
                                            <a href="{{ url('admin/sakramen_ttd/baptis/balita') }}">
                                                <i class='fa fa-calendar'></i>
                                                <span>Balita</span>
                                            </a>
                                        </li>
                                        <li style="" class="@yield('treeview_ttd_sakramen_baptis_anak')">
                                            <a href="{{ url('admin/sakramen_ttd/baptis/anak') }}">
                                                <i class='fa fa-calendar'></i>
                                                <span>Anak</span>
                                            </a>
                                        </li>
                                        <li style="" class="@yield('treeview_ttd_sakramen_baptis_dewasa')">
                                            <a href="{{ url('admin/sakramen_ttd/baptis/dewasa') }}">
                                                <i class='fa fa-calendar'></i>
                                                <span>Dewasa</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="treeview-menu">
                                <li style="" class="@yield('treeview_ttd_sakramen_komuni')">
                                    <a href="{{ url('admin/sakramen_ttd/komuni') }}">
                                        <i class='fa fa-calendar'></i>
                                        <span>Komuni</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="treeview-menu">
                                <li style="" class="@yield('treeview_ttd_sakramen_krisma')">
                                    <a href="{{ url('admin/sakramen_ttd/krisma') }}">
                                        <i class='fa fa-calendar'></i>
                                        <span>Krisma</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li style="" class="@yield('treeview_catat_sakit')">
                            <a href="{{ url('/admin/pencatatan-sakit') }}">
                                <i class='fa fa-calendar'></i>
                                <span>Orang Sakit</span>
                            </a>
                        </li>
                        <li style="" class="@yield('treeview_catat_kunjungan')">
                            <a href="{{ url('/admin/pencatatan-kunjungan') }}">
                                <i class='fa fa-calendar'></i>
                                <span>Kunjungan</span>
                            </a>
                        </li>
                    </ul>
                @elseif($katUser->type == 'wilayah')
                    <ul class="treeview-menu">
                        <li style="" class="@yield('treeview_kronik')">
                            <a href="{{ url('admin/kronik') }}">
                                <i class='fa fa-info'></i>
                                <span>Kronik</span>
                            </a>
                        </li>

                        <li style="" class="@yield('treeview_info')">
                            <a href="{{ url('admin/info') }}">
                                <i class='fa fa-info'></i>
                                <span>Info</span>
                            </a>
                        </li>

                        <li style="" class="@yield('treeview_artikel')">
                            <a href="{{ url('admin/artikel') }}">
                                <i class='fa fa-info'></i>
                                <span>Artikel</span>
                            </a>
                        </li>

                        <li  style="" class="@yield('treeview_proposal')">
                            <a href="{{ url('/admin/proposal') }}">
                                <i class='fa fa-file'></i>
                                <span>Proposal</span>
                            </a>
                        </li>
                        <li  style="" class="@yield('treeview_lpj')">
                            <a href="{{ url('/admin/lpj') }}">
                                <i class='fa fa-file'></i>
                                <span>LPJ</span>
                            </a>
                        </li>
                        <li class="treeview @yield('treeview_ttd_sakramen')">
                            <a href="#">
                                <i class="fa fa-calendar"></i>
                                    <span>Sakramen</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                            </a>
                            <ul class="treeview-menu">
                                <li style="" class="@yield('treeview_ttd_sakramen_pernikahan')">
                                    <a href="{{ url('admin/sakramen_ttd/nikah') }}">
                                        <i class='fa fa-calendar'></i>
                                        <span>Pernikahan</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="treeview-menu">
                                <li style="" class="@yield('treeview_ttd_sakramen_krisma')">
                                    <a href="{{ url('admin/sakramen_ttd/krisma') }}">
                                        <i class='fa fa-calendar'></i>
                                        <span>Krisma</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @elseif($katUser->type == 'romo')
                    <ul class="treeview-menu">
                        <li  style="" class="@yield('treeview_proposal')">
                            <a href="{{ url('/admin/proposal') }}">
                                <i class='fa fa-file'></i>
                                <span>Proposal</span>
                            </a>
                        </li>
                        <li  style="" class="@yield('treeview_lpj')">
                            <a href="{{ url('/admin/lpj') }}">
                                <i class='fa fa-file'></i>
                                <span>LPJ</span>
                            </a>
                        </li>
                        <li class="treeview @yield('treeview_ttd_sakramen')">
                            <a href="#">
                                <i class="fa fa-calendar"></i>
                                    <span>Sakramen</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                            </a>
                            <ul class="treeview-menu">
                                <li style="" class="@yield('treeview_ttd_sakramen_pernikahan')">
                                    <a href="{{ url('admin/sakramen_ttd/nikah') }}">
                                        <i class='fa fa-calendar'></i>
                                        <span>Pernikahan</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @elseif($katUser->type == 'biak')
                    <ul class="treeview-menu">
                        <li class="treeview @yield('treeview_ttd_sakramen')">
                            <a href="#">
                                <i class="fa fa-calendar"></i>
                                    <span>Sakramen</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                            </a>
                            <ul class="treeview-menu">
                                <li style="" class="@yield('treeview_ttd_sakramen_komuni')">
                                    <a href="{{ url('admin/sakramen_ttd/komuni') }}">
                                        <i class='fa fa-calendar'></i>
                                        <span>Komuni</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @elseif($katUser->type == 'seketaris')
                    <ul class="treeview-menu">
                        <li style="" class="@yield('treeview_kronik')">
                            <a href="{{ url('admin/kronik/create') }}">
                                <i class='fa fa-info'></i>
                                <span>Kronik</span>
                            </a>
                        </li>
                        <li style="" class="@yield('treeview_info')">
                            <a href="{{ url('admin/info/create') }}">
                                <i class='fa fa-info'></i>
                                <span>Info</span>
                            </a>
                        </li>

                        <li style="" class="@yield('treeview_artikel')">
                            <a href="{{ url('admin/artikel/create') }}">
                                <i class='fa fa-info'></i>
                                <span>Artikel</span>
                            </a>
                        </li>
                    </ul>
                @elseif(in_array($katUser->type,['seksi','subseksi','koordinator','subkoordinator','bidang']))
                <ul class="treeview-menu">
                    <li style="" class="@yield('treeview_kronik')">
                        <a href="{{ url('admin/kronik/create') }}">
                            <i class='fa fa-info'></i>
                            <span>Kronik</span>
                        </a>
                    </li>
                    <li style="" class="@yield('treeview_info')">
                        <a href="{{ url('admin/info/create') }}">
                            <i class='fa fa-info'></i>
                            <span>Info</span>
                        </a>
                    </li>

                    <li style="" class="@yield('treeview_artikel')">
                        <a href="{{ url('admin/artikel/create') }}">
                            <i class='fa fa-info'></i>
                            <span>Artikel</span>
                        </a>
                    </li>
                    <li  style="" class="@yield('treeview_proposal')">
                        <a href="{{ url('/admin/proposal') }}">
                            <i class='fa fa-file'></i>
                            <span>Proposal</span>
                        </a>
                    </li>
                    <li  style="" class="@yield('treeview_lpj')">
                        <a href="{{ url('/admin/lpj') }}">
                            <i class='fa fa-file'></i>
                            <span>LPJ</span>
                        </a>
                    </li>
                </ul>
                @endif


                @elseif($roleUser->id == 4)
                <ul class="treeview-menu">
                    <li class="treeview @yield('treeview_sakramen')">
                        <a href="#">
                            <i class="fa fa-calendar"></i>
                                <span>Sakramen</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_sakramen_pernikahan')">
                                <a href="{{ url('admin/sakramen/nikah') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Pernikahan</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_sakramen_baptis')">
                                <a href="#">
                                    <i class='fa fa-calendar'></i>
                                    <span>Baptis</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_sakramen_baptis_balita')">
                                        <a href="{{ url('admin/sakramen/baptis/balita') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Balita</span>
                                        </a>
                                    </li>
                                </ul>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_sakramen_baptis_anak')">
                                        <a href="{{ url('admin/sakramen/baptis/anak') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Anak</span>
                                        </a>
                                    </li>
                                </ul>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_sakramen_baptis_dewasa')">
                                        <a href="{{ url('admin/sakramen/baptis/dewasa') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Dewasa</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_sakramen_komuni')">
                                <a href="{{ url('admin/sakramen/komuni') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Komuni</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_sakramen_krisma')">
                                <a href="{{ url('admin/sakramen/krisma') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Krisma</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="treeview-menu">
                    <li class="treeview @yield('treeview_tahapan_sakramen')">
                        <a href="#">
                            <i class="fa fa-calendar"></i>
                                <span>Tahapan Sakramen</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_tahapan_sakramen_pernikahan')">
                                <a href="{{ url('admin/tahapan-sakramen/nikah') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Pernikahan</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_tahapan_sakramen_baptis')">
                                <a href="#">
                                    <i class='fa fa-calendar'></i>
                                    <span>Baptis</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_tahapan_sakramen_baptis_balita')">
                                        <a href="{{ url('admin/tahapan-sakramen/baptis_balita') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Balita</span>
                                        </a>
                                    </li>
                                </ul>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_tahapan_sakramen_baptis_anak')">
                                        <a href="{{ url('admin/tahapan-sakramen/baptis_anak') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Anak</span>
                                        </a>
                                    </li>
                                </ul>
                                 <ul class="treeview-menu">
                                    <li style="" class="@yield('treeview_tahapan_sakramen_baptis_dewasa')">
                                        <a href="{{ url('admin/tahapan-sakramen/baptis_dewasa') }}">
                                            <i class='fa fa-calendar'></i>
                                            <span>Dewasa</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_tahapan_sakramen_komuni')">
                                <a href="{{ url('admin/tahapan-sakramen/komuni') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Komuni</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_tahapan_sakramen_krisma')">
                                <a href="{{ url('admin/tahapan-sakramen/krisma') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Krisma</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="treeview-menu">
                    <li style="" class="@yield('treeview_transaksi_sakramen')">
                        <a href="{{ url('admin/transaksi-sakramen') }}">
                            <i class='fa fa-calendar'></i>
                            <span>Transaksi Sakramen</span>
                        </a>
                    </li>
                </ul>

                @else
                <ul class="treeview-menu">
                    <li style="" class="@yield('treeview_info')">
                        <a href="{{ url('admin/info') }}">
                            <i class='fa fa-info'></i>
                            <span>Info</span>
                        </a>
                    </li>

                    <li style="" class="@yield('treeview_kronik')">
                        <a href="{{ url('admin/kronik') }}">
                            <i class='fa fa-info'></i>
                            <span>Kronik</span>
                        </a>
                    </li>
                    @if(in_array($roleUser->id,[1,2]))
                    <li class="treeview @yield('treeview_agenda')">
                        <a href="#">
                            <i class="fa fa-calendar"></i>
                                <span>Agenda</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_agenda_umum')">
                                <a href="{{ url('admin/kegiatan-umum') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Kegiatan Umum</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="treeview-menu">
                            <li style="" class="@yield('treeview_agenda_pernikahan')">
                                <a href="{{ url('admin/kegiatan-nikah') }}">
                                    <i class='fa fa-calendar'></i>
                                    <span>Pernikahan</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @else
                    <li style="" class="@yield('treeview_artikel')">
                        <a href="{{ url('admin/artikel') }}">
                            <i class='fa fa-info'></i>
                            <span>Artikel</span>
                        </a>
                    </li>
                    @endif

                    <li  style="" class="@yield('treeview_proposal')">
                        <a href="{{ url('/admin/proposal') }}">
                            <i class='fa fa-file'></i>
                            <span>Proposal</span>
                        </a>
                    </li>
                    <li  style="" class="@yield('treeview_lpj')">
                        <a href="{{ url('/admin/lpj') }}">
                            <i class='fa fa-file'></i>
                            <span>LPJ</span>
                        </a>
                    </li>
                    <li style="" class="@yield('treeview_catat_sakit')">
                        <a href="{{ url('/admin/pencatatan-sakit') }}">
                            <i class='fa fa-calendar'></i>
                            <span>Pencatatan Orang Sakit</span>
                        </a>
                    </li>
                    <li style="" class="@yield('treeview_catat_kunjungan')">
                        <a href="{{ url('/admin/pencatatan-kunjungan') }}">
                            <i class='fa fa-calendar'></i>
                            <span>Pencatatan Kunjungan</span>
                        </a>
                    </li>
                </ul>
                @endif
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>